<?php /* Smarty version 2.6.6, created on 2012-02-14 15:07:24
         compiled from C:%5CMiniClinic%5CApplication//Tpl/tpl.mo_miniclinic/tpl.paso7.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'number_format', 'C:\MiniClinic\Application//Tpl/tpl.mo_miniclinic/tpl.paso7.html', 26, false),)), $this); ?>
<?php echo $this->_tpl_vars['cabecera']; ?>

	<link href="Resources/Css/paso7.css" type="text/css" rel="stylesheet" />
	<script type="text/javascript" src="Resources/Js/validacion.paso7.js"></script>
	<div id="separador">
		<div id="confirmacion">
			<img src="Resources/Images/arrow1.png" id="arrow"/>
			<img src="Resources/Images/Paso7/presione.png" id="presione"/>
			<span id="info_motivo">
				Su motivo de consulta 
				<br />es <span class="rel_info"><?php echo $this->_tpl_vars['motivoConsulta']; ?>
</span> 
				
				<?php echo $this->_tpl_vars['mensajeDetalle']; ?>
 <span class="rel_info"> <?php if ($this->_tpl_vars['sintomasMostrar']):  echo $this->_tpl_vars['sintomasMostrar']; ?>
 <?php else: ?> <?php if ($this->_tpl_vars['sintoma']): ?> <?php echo $this->_tpl_vars['sintoma']; ?>
. <?php endif; ?> <?php endif; ?></span>
				
				<br />
				<br />Su prevision es <span class="rel_info"><?php echo $this->_tpl_vars['nombrePrevision']; ?>
</span>.
			</span>
		</div>
		
		<div id="medico">
			<img src="Resources/Images/Paso7/indicador.png" class="indicador" />
			 M&Eacute;DICO : <span style="color:cyan" id="nombreMedico"><?php echo $this->_tpl_vars['medico']; ?>
</span>
		</div>

		<div id="valor">
			<img src="Resources/Images/Paso7/indicador.png" class="indicador" />
			 VALOR : <span style="color:cyan" id="monto">$ <?php echo ((is_array($_tmp=$this->_tpl_vars['monto'])) ? $this->_run_mod_handler('number_format', true, $_tmp, 0, ".", ".") : number_format($_tmp, 0, ".", ".")); ?>
</span>
		</div>
		
		<div id="info">
			<img src="Resources/Images/Paso7/el_monot.png" id="el_monot"/>
		</div>
	</div>
<?php echo $this->_tpl_vars['pie']; ?>