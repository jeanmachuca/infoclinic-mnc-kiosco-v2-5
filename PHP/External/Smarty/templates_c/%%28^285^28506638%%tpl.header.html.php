<?php /* Smarty version 2.6.6, created on 2012-02-22 16:49:50
         compiled from C:%5CKiosco%5CMiniclinic%5CApplication//Tpl/tpl.mo_miniclinic//tpl.header.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'lower', 'C:\Kiosco\Miniclinic\Application//Tpl/tpl.mo_miniclinic//tpl.header.html', 4, false),)), $this); ?>
<html>
  <head>
	<title>Mini Clinic</title>
	<LINK REL="shortcut icon" HREF="Resources/Icons/ico.<?php echo ((is_array($_tmp=$this->_tpl_vars['CoreHandler']->oModule->moduleName)) ? $this->_run_mod_handler('lower', true, $_tmp) : smarty_modifier_lower($_tmp)); ?>
.ico" TYPE="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="Resources/Css/header.css" />
	<link rel="stylesheet" type="text/css" href="Resources/Css/footer.css" />
	<link type="text/css" href="Resources/Css/alert.css" rel="stylesheet" />
	<link type="text/css" href="Resources/Css/cargando.css" rel="stylesheet" />
 <script type="text/javascript" src="Resources/Js/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" src="Resources/Js/page.control.js"></script>
    	<!--[if IE]>
		<script type="text/javascript">
			//estos script solo se ejecutan en IE
			window.onload = function(){
				//previene que el usuario seleccione los textos
				document.onselectstart = function(){return false }
			}
    	</script>
    	<![endif]-->
	<script type="text/javascript">
	    $(document).ready( function(){ 
		    init('<?php echo $this->_tpl_vars['pasoActual']; ?>
');
		 });
	</script>
  </head>
<body>
<div id="contenedor">
   <div id="header">
   		<span id="title_page"><?php echo $this->_tpl_vars['tituloPagina']; ?>
</span>
   		<span id="subtitle_page"><?php echo $this->_tpl_vars['subTituloPagina']; ?>
</span>
		<img src="Resources/Images/barra_pasos.png" id="barra_pasos" />
		<span id="paso">PASO:
			<span id="paso1" >01</span>
			<span id="paso2" >02</span>
			<span id="paso3" >03</span>
			<span id="paso4" >04</span>
			<span id="paso5" >05</span>
			<span id="paso6" >06</span>
			<span id="paso7" >07</span>
			<span id="paso8" >08</span>
			<span id="paso9" >09</span>
			<span id="paso10" >10</span>
			<span id="paso11" >11</span>
		</span>
		<img id="line" src="Resources/Images/line.png" />
  	</div> <!-- fin header -->
<!--   	<form action="index.php" method="post" id="formulario" onsubmit="return validate()"> -->
<?php echo $this->_tpl_vars['cabecera_form']; ?>

  		<div id="body">