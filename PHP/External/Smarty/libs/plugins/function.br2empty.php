<?php

/*
 * Smarty plugin
 * -------------------------------------------------------------
 * Type:     block function
 * Name:     existfile
 * Purpose:  return if exits file
 * Params:   file: string (file)
 * -------------------------------------------------------------
 */
function smarty_function_br2empty($params, &$smarty) {
	extract($params);
	if (isset($value)) {
		if(empty($length)) {
			return br2empty($value);
		} else {
			if (strlen($value) > $length) {
				$length -= strlen($etc);
				return br2empty(substr($string, 0, $length))."...";
			} else
			return br2empty($value);
		}
	} else {
		return "";
	}


}

/* vim: set expandtab: */

?>
