<?php

/*
 * Smarty plugin
 * -------------------------------------------------------------
 * Type:     block function
 * Name:     existfile
 * Purpose:  return if exits file
 * Params:   file: string (file)
 * -------------------------------------------------------------
 */
function smarty_block_repeat_block($params, $content, &$this) {
	extract($params);
	return str_repeat($content,$mult);	
		
}

/* vim: set expandtab: */

?>
