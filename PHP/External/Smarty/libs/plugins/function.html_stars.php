<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


function smarty_function_html_stars($params, &$smarty)
{
    extract($params);
	$total = (int)abs($porcentaje/$div);
	  
    if (empty($file)) {
        $smarty->trigger_error("html_image: missing 'file' parameter", E_USER_NOTICE);
        return;
    }

    if (substr($file,0,1) == '/') {
        $_image_path = $basedir . $file;
    } else {
        $_image_path = $file;
    }

    if ($smarty->security &&
		($_params = array('resource_type' => 'file', 'resource_name' => $_image_path)) &&
        (require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.is_secure.php')) &&
        (!smarty_core_is_secure($_params, $smarty)) ) {
        $smarty->trigger_error("html_image: (secure) '$_image_path' not in secure directory", E_USER_NOTICE);

    } elseif (!$_image_data = @getimagesize($_image_path)) {
		if(!file_exists($_image_path)) {
			$smarty->trigger_error("html_image: unable to find '$_image_path'", E_USER_NOTICE);
                return;
        } else if(!is_readable($_image_path)) {
			$smarty->trigger_error("html_image: unable to read '$_image_path'", E_USER_NOTICE);
                return;
        } else {
                $smarty->trigger_error("html_image: '$_image_path' is not a valid image file", E_USER_NOTICE);
                return;
        }
   }

   for($cont;$cont<$total;$cont++){
		$stars.="<img src='$file' alt='$alt'>";
   }
   
   $maximo = 5;
   $max = $maximo - $total;
   for($max;$max<$maximo;$max++){
	$stars.="<img src='$file_black' alt='$alt'>";
   }	
   
   return $stars;
   //return $prefix . '<img src="'.$file.'" alt="'.$alt.'" border="'.$border.'" width="'.$width.'" height="'.$height.'"'.$extra.' />' . $suffix;
}

/* vim: set expandtab: */

?>
