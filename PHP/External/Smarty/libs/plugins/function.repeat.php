<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

function smarty_function_repeat($params, &$smarty)
{   
     extract($params);

     return str_repeat($input, $mult);
    
}

/* vim: set expandtab: */

?>
