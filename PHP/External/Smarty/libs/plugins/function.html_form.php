<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty {html_form} function plugin
 *
 * Type:     function<br>
 * Name:     html_form<br>
 * Date:     Feb 24, 2003<br>
 * Purpose:  evaluar funcion del boton personalizando campo hidden del formulario para ejecutar funcion en el servidor
 * Input:<br>
 *         - name = nombre del formulario
 *         - method = metodo del formulario(optional, default post)
 *         - action = action del form(optional, default php_self)
 *         - onsubmit =funcion a evaluar en el submit del form(optional, default empty)
 *         - target = target del form (optional, default self(''))
 *
 * Examples: {{html_form name="form" method="post" action="guardar.php" onsubmit="validar" target="_blank"}}
 * Output:   <form name="form" method="post" action="guardar.php" OnSubmit="return validar(this)" target="_blank">
 * @author   Patricio Aguero <paguero@inter-media.cl>
 * @version  1.0
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_html_form($params, &$smarty) {
	extract($params);
	$submitFunction = ($onsubmit!=''?'return '.$onsubmit.'(this);':'');
	$methodForm = (strtolower($method)=='get'?'get':'post');
	$actionForm = ($action==''?$_SERVER["php_self"]:$action);
	return '<form onkeypress="if(window.event.keyCode==13){onKey13PressForm(this);};" name="'.$name.'" method="'.$methodForm.'" action="'.$actionForm.'" OnSubmit="'.$submitFunction.'" target=""><input type="hidden" name="UIFunctionInput" value="">';
}

/* vim: set expandtab: */

?>
