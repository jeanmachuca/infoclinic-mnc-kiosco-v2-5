<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty {html_textbox} function plugin
 *
 * Type:     function<br>
 * Name:     html_textbox<br>
 * Date:     Feb 24, 2003<br>
 * Purpose:  Generar un input (text, hidden) el cual tendr� la opci�n de manejar persistencia de los datos
 * Input:<br>
 *         - name = nombre del input
 *         - visible = definir� el tipo de campo.(opcional, default 'text')
 *         - class = estilo asociado (opcional, default '')
 *         - value = Valor Inicial del campo(optional, default '')
 *         - persistente = (B) Valor del campo luego del postback(optional, default '')
 *         - maxlength = M�ximo de caracteres a ingresar(optional, default '')
 *	       - extras = parametros adicionales
 *
 * Examples: {{html_textbox name="fecha" visible="true" class="campo" persistente="true" value="12/12/2004" maxlength="10"}}
 * Output:   <input type="text" name="fecha" class="campo" value="12/12/2004" maxlength="10">
 * @author   Patricio Aguero <paguero@inter-media.cl>
 * @version  1.0
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_html_textbox($params, &$smarty) {
	//extract($params);
	require_once $smarty->_get_plugin_filepath('shared','escape_special_chars');
	foreach($params as $_key => $_val) {	
		switch($_key) {
			case 'name':
				$name = $_val;
				break;
			case 'visible':
				$type = (strtolower($_val)=='true'?'text':'hidden');
				break;
			case 'class':
				$class = $_val;
				break;
			case 'value':
				$value = $_val;
				break;
			case 'persistente':
				$persistente = $_val;
				break;
			case 'maxlength':
				$maxlength = ($_val==''?'':'maxlength="'.$_val.'"');
				break;
			default:
				$extra .= ' '.$_key.'="'.smarty_function_escape_special_chars($_val).'"';
				break;					
		}
	}
	$value = ((strtolower($persistente)=='true' && $_REQUEST[$name]!='')?$_REQUEST[$name]:$value);
	return '<input name="'.$name.'" type="'.$type.'" value="'.$value.'" '.$maxlength.' class="'.$class.'" '.$extra.'>';
}

/* vim: set expandtab: */

?>
