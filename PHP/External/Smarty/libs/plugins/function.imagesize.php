<?php

/*
 * Smarty plugin
 * -------------------------------------------------------------
 * Type:     function
 * Name:     imagesize
 * Purpose:  return tama�o de imagenes
 * Params:   file: string (file)
 * -------------------------------------------------------------
 */
function smarty_function_imagesize($file, &$smarty) {
	if (isset($file) && !empty($file) && file_exists($file)) {
		list($widthImage, $heightImage, $type, $attr) = getimagesize($file); 
		return $widthImage;
	} else {
		//return 0;
	}


}

/* vim: set expandtab: */

?>
