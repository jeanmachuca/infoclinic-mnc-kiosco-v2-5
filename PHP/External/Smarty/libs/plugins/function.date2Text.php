<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Smarty {eval} function plugin
 *
 * Type:     function<br>
 * Name:     date2Text<br>
 * Purpose:  evaluate a template variable as a template<br>
 * @param date yyyy-mm-dd
 * @param Smarty
 */
function smarty_function_date2Text($params, &$smarty)
{	
	extract($params);
    if (!isset($date)) {
        $smarty->trigger_error("eval: missing 'var' Date");
        return;
    }

    if($date == '') {
        return;
    }
	list( $year, $month, $day) = split( '[/.-]', $date );
	if(!$dayDisplay) {
		return getMonth($month)." de ".$year; 
	}
	return getLongDate($day, $month, $year);
	
    
}

/* vim: set expandtab: */

?>
