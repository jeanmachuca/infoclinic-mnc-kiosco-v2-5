<?php

/*
 * Smarty plugin
 * -------------------------------------------------------------
 * Type:     block function
 * Name:     existfile
 * Purpose:  return if exits file
 * Params:   file: string (file)
 * -------------------------------------------------------------
 */
function smarty_block_existsfile($params, $content, &$this) {
	extract($params);
	if(!ereg(".", $file)) {
		return false;
	}
	$dir=($dir?$dir:'');
	$file = $dir.$file;
	if (isset($file) && !empty($file) && file_exists($file)) {
		return $content;
	}


}

/* vim: set expandtab: */

?>
