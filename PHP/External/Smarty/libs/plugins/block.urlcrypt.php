<?php

/*
 * Smarty plugin
 * -------------------------------------------------------------
 * Type:     block function
 * Name:     textformat
 * Purpose:  format text a certain way with preset styles
 *           or custom wrap/indent settings
 * Params:   style: string (email)
 *           indent: integer (0)
 *           wrap: integer (80)
 *           wrap_char string ("\n")
 *           indent_char: string (" ")
 *           wrap_boundary: boolean (true)
 * -------------------------------------------------------------
 */
function smarty_block_urlcrypt($params, $content, &$this)
{
	if (isset($content)) {
		return (URL::addCode($content));
    }
}

/* vim: set expandtab: */

?>
