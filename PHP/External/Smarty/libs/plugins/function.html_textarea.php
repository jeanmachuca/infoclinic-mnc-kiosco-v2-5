<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty {html_textarea} function plugin
 *
 * Type:     function<br>
 * Name:     html_textarea<br>
 * Date:     Feb 24, 2003<br>
 * Purpose:  Generar un input (text, hidden) el cual tendr� la opci�n de manejar persistencia de los datos
 * Input:<br>
 *         - name = nombre del input
 *         - visible = definir� el tipo de campo.(opcional, default 'text')
 *         - class = estilo asociado (opcional, default '')
 *         - value = Valor Inicial del campo(optional, default '')
 *         - persistente = (B) Valor del campo luego del postback(optional, default '')
 *         - maxlength = M�ximo de caracteres a ingresar(optional, default '')
 *	       - extras = parametros adicionales
 *
 * Examples: {{html_textarea name="comentario" cols="40" rows="3" class="campo" persistente="true" value="ingrese comentario"}}
 * Output:   <textarea name="comentario" cols="40" rows="3" class="campo">ingrese comentario</textarea>
 * @author   Patricio Aguero <paguero@inter-media.cl>
 * @version  1.0
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_html_textarea($params, &$smarty) {
	//extract($params);
	require_once $smarty->_get_plugin_filepath('shared','escape_special_chars');
	foreach($params as $_key => $_val) {	
		switch($_key) {
			case 'name':
				$name = $_val;
				break;
			case 'class':
				$class = $_val;
				break;
			case 'value':
				$value = $_val;
				break;
			case 'persistente':
				$persistente = $_val;
				break;
			case 'rows':
				$rows = $_val;
				break;
			case 'cols':
				$cols = $_val;
				break;
			default:
				$extra .= ' '.$_key.'="'.smarty_function_escape_special_chars($_val).'"';
				break;					
		}
	}
	$value = ((strtolower($persistente)=='true' && $_REQUEST[$name]!='')?$_REQUEST[$name]:$value);
	return '<textarea name="'.$name.'" rows="'.$rows.'" cols="'.$cols.'" class="'.$class.'" '.$extra.'>'.$value.'</textarea>';
}

/* vim: set expandtab: */

?>
