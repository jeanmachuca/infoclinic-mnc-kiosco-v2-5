<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty {html_select} function plugin
 *
 * Type:     function<br>
 * Name:     html_select<br>
 * Date:     Feb 24, 2003<br>
 * Purpose:  Generar un input (select) el cual tendr� la opci�n de manejar persistencia de los datos, cargandose desde un 			 array
 * Input:<br>
 *         - name = nombre del input
 *         - selected = campo valor a dejar selected
 *         - text = campo text
 *         - source = Array con datos
 *         - class = estilo asociado (opcional, default '')
 *         - value = Valor Inicial del campo(optional, default '')
 *         - null = Valor del indice 0 (de la forma value:text)
 *         - persist = (Bool) Valor del campo luego del postback(optional, default '')
 *	       - extras = parametros adicionales
 *
 * Examples: {{html_select name="usuarios" null="0:Seleccione" visible="true" class="campo" persist="true" selected="2" value="especie_id" text="espe_nombre" source="especies"}}
 * Output:   <select name="usuarios" class="campo"><option value="0">Seleccione</option></select>
 * @author   Patricio Aguero <paguero@inter-media.cl>
 * @version  1.0
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_html_select($params, &$smarty) {
	//extract($params);
	require_once $smarty->_get_plugin_filepath('shared','escape_special_chars');
	foreach($params as $_key => $_val) {	
		switch($_key) {
			case 'name':
				$name = $_val;
				break;
			case 'source':
				$source = $_val;
				break;
			case 'selected':
				$selected = $_val;
				break;
			case 'text':
				$text = $_val;
				break;
			case 'class':
				$class = $_val;
				break;
			case 'value':
				$value = $_val;
				break;
			case 'persist':
				$persist = $_val;
				break;
			case 'null':
				$null = $_val;
				break;
			default:
				$extra .= ' '.$_key.'="'.smarty_function_escape_special_chars($_val).'"';
				break;					
		}
	}
	$null = split(":", $null);
	if(is_array($null)) $null = '<option value="'.$null[0].'">'.$null[1].'</option>';
	//'<option>:"'.smarty_function_escape_special_chars($null).'"</option>';
	$selected = ((strtolower($persist)=='true' && $_REQUEST[$name]!='')?$_REQUEST[$name]:$selected);
	if(is_array($source)) {
		foreach($source As $key=>$valor) {
			$valores.= '<option value="'.$valor->$value.'"'.($valor->$value==$selected?' selected':'').'>'.$valor->$text.'</option>';
		} 
	}
	//value="'.$value.'" '.$maxlength.' 
	return '<select name="'.$name.'" class="'.$class.'" '.$extra.'>'.$null.''.$valores.'
			</select>';
}

/* vim: set expandtab: */

?>
