<?php

/*
 * Smarty plugin
 * -------------------------------------------------------------
 * Type:     function
 * Name:     existfile
 * Purpose:  return if exits file
 * Params:   file: string (file)
 * -------------------------------------------------------------
 */
function smarty_function_existsfile($params, &$smarty) {
	extract($params);
	if(!ereg(".", $file)) {
		return false;
	}
	$dir=($dir?$dir:'');
	$file = $dir.$file;
	if (isset($file) && !empty($file) && file_exists($file)) {
		return true;
	}
}

/* vim: set expandtab: */

?>
