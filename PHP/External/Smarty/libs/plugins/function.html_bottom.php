<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty {html_form} function plugin
 *
 * Type:     function<br>
 * Name:     html_bottom<br>
 * Date:     Feb 24, 2003<br>
 * Purpose:  Generar un boton el cual ejecutar� una funcion javascript que cargar� un valor a un hidden del form para ejecutar riutina en servidor
 * Input:<br>
 *         - name = nombre del bot�n
 *         - type = tipo de bot�n.(image, submit, button, reset)
 *         - class = estilo asociado (opcional, default '')
 *         - value = Value del bot�n(optional, default 'Enviar')
 *         - src = Para casos de input image la url de la imagen(optional, default '')
 *	       - extras = parametros adicionales
 *
 * Examples: {{html_bottom name="Submit" type="submit" class="boton" value="Enviar"}}
 * Output:   <input type="submit" name="Submit" class="boton" value="Enviar">
 * @author   Patricio Aguero <paguero@inter-media.cl>
 * @version  1.0
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_html_bottom($params, &$smarty) {
	//extract($params);
	require_once $smarty->_get_plugin_filepath('shared','escape_special_chars');
	foreach($params as $_key => $_val) {	
		switch($_key) {
			case 'name':
				$name = $_val;
				break;
			case 'type':
				$type = $_val;
				break;
			case 'class':
				$class = $_val;
				break;
			case 'value':
				$value = $_val;
				break;
			case 'src':
				$src = $_val;
				break;
			case 'function':
				$function = $_val;
				break;
			default:
				$extra .= ' '.$_key.'="'.smarty_function_escape_special_chars($_val).'"';
				break;					
		}
	}
	$srcBottom = ($src!=''?'src="'.$src.'"':'');
	$UIFunction = ($function!=''?'OnClick="onEvaluateServerFunction(this.form, \''.$function.'\');"':'');
	if($type!='image') $src = "";
	return '<input name="'.$name.'" type="'.$type.'" '.$UIFunction.' value="'.$value.'" class="'.$class.'" '.$srcBottom.' '.$extra.'>';
}

/* vim: set expandtab: */

?>
