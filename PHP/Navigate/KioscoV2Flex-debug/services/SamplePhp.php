<?php

/*
    README for sample service

    This generated sample service contains functions that illustrate typical service operations.
    Use these functions as a starting point for creating your own service implementation. Modify the 
    function signatures, references to the database, and implementation according to your needs. 
    Delete the functions that you do not use.

    Save your changes and return to Flash Builder. In Flash Builder Data/Services View, refresh 
    the service. Then drag service operations onto user interface components in Design View. For 
    example, drag the getAllItems() operation onto a DataGrid.
    
    This code is for prototyping only.
    
    Authenticate the user prior to allowing them to call these methods. You can find more 
    information at http://www.adobe.com/go/flex_security

*/

class SamplePhp {

	/*
	 * STEP 1 of 2: Enter Details below
	 */
	var $username = "<ENTER USER NAME HERE>";
	var $password = "<ENTER PASSWORD HERE>";
	var $server = "<ENTER SERVER URL HERE e.g. localhost>";
	var $port = "<ENTER DB PORT HERE e.g. 3306>";
	var $tablename = "<ENTER TABLE NAME HERE>";
	var $databasename = "<ENTER DATABASE NAME HERE>";
	
	var $connection;
	/*
	 * STEP 2 of 2: Update the columns names in the SQL queries in the following methods: 
	 *     a. createItem($item)
	 *     b. getItem($itemID),
	 *     c. deleteItem($itemID) 
	 *     d. updateItem($item)
	 */ 

	public function __construct() {
		// TODO Establish the database connection
		
		// Sample code
		/*
	  	$this->connection = mysqli_connect(
	  							$this->server,  
	  							$this->username,  
	  							$this->password, 
	  							$this->databasename,
	  							$this->port
	  						);

		$this->throwExceptionOnError($this->connection);
		*/
	}

	public function getAllItems() {
		// TODO Auto-generated method stub
		// Retrieve a array of records from the database and return that

		// Sample code
		/*
		$stmt = mysqli_prepare($this->connection, "SELECT * FROM $this->tablename");		
		$this->throwExceptionOnError();
		
		mysqli_stmt_execute($stmt);
		$this->throwExceptionOnError();
		
		$rows = array();
		
		//
		// e.g. if the table has columns ID, NAME then mysqli_stmt_bind_result
		// would look like this => mysqli_stmt_bind_result($stmt, $row->ID, $row->NAME);
		//
		mysqli_stmt_bind_result($stmt, <ENTER VALUES BASED ON ABOVE COMMENT>);
		
	    while (mysqli_stmt_fetch($stmt)) {
	      $rows[] = $row;
	      $row = new stdClass();
	      mysqli_stmt_bind_result($stmt, <ENTER VALUES AS ON LINE 79>);
	    }
		
		mysqli_stmt_free_result($stmt);
	    mysqli_close($this->connection);
	
	    return $rows;
	    */ 	
	}

	public function getItem($itemID) {
		// TODO Auto-generated method stub
		// Return a single record from the database and return the item
		
		// Sample code
		/*		
		$stmt = mysqli_prepare($this->connection, "SELECT * FROM $this->tablename where <ENTER PRIMARY KEY COLUMN NAME>=?");
		$this->throwExceptionOnError();
		
		//
		// Refer documentation for 'types' parameter at http://www.php.net/manual/en/mysqli-stmt.bind-param.php
		//
		mysqli_stmt_bind_param($stmt, '<REFER ABOVE COMMENT>', $itemID);		
		$this->throwExceptionOnError();
		
		mysqli_stmt_execute($stmt);
		$this->throwExceptionOnError();
		
		//
		// e.g. if the table has columns ID, NAME then mysqli_stmt_bind_result
		// would look like this => mysqli_stmt_bind_result($stmt, $row->ID, $row->NAME);
		//
		mysqli_stmt_bind_result($stmt, <ENTER VALUES BASED ON ABOVE COMMENT>);
		
		if(mysqli_stmt_fetch($stmt)) {
			return $row;
		} else {
			return null;
		}
		*/ 	
	}

	public function createItem($item) {
		// TODO Auto-generated method stub
		// Insert a new record in the database using the parameter and return the item
		
		// Sample code
		/*	
		$stmt = mysqli_prepare($this->connection, "INSERT INTO $this->tablename (FIELD1, FIELD2, FIELD3) VALUES (?, ?, ?)");		
		$this->throwExceptionOnError();
		
		//
		// Refer documentation for 'types' parameter at http://www.php.net/manual/en/mysqli-stmt.bind-param.php
		//
		mysqli_stmt_bind_param($stmt, '<REFER ABOVE COMMENT>', '$item->FIELD1','$item->FIELD2','$item->FIELD3');		
		$this->throwExceptionOnError();

		mysqli_stmt_execute($stmt);		
		$this->throwExceptionOnError();
		
		$autoid = mysqli_stmt_insert_id($stmt);
		
		mysqli_stmt_free_result($stmt);		
		mysqli_close($this->connection);
		
		return $autoid;
	    */ 	
	}

	public function updateItem($item) {
		// TODO Auto-generated method stub
		// Update an existing record in the database and return the item
		
		// Sample code
		/*
		$stmt = mysqli_prepare($this->connection, "UPDATE $this->tablename SET FIELD1=?, FIELD2=?, FIELD3=?	WHERE itemID=?");		
		$this->throwExceptionOnError();		
		
		//
		// Refer documentation for 'types' parameter at http://www.php.net/manual/en/mysqli-stmt.bind-param.php
		//
		mysqli_stmt_bind_param($stmt, '<REFER ABOVE COMMENT>', '$item->FIELD1','$item->FIELD2','$item->FIELD3','$item->itemID');			
		$this->throwExceptionOnError();

		mysqli_stmt_execute($stmt);		
		$this->throwExceptionOnError();
		
		mysqli_stmt_free_result($stmt);		
		mysqli_close($this->connection);
		*/
	}

	public function deleteItem($itemID) {
		// TODO Auto-generated method stub
		// Delete a record in the database
		
		// Sample code
		/*			
		$stmt = mysqli_prepare($this->connection, "DELETE FROM $this->tablename WHERE itemID = ?");
		$this->throwExceptionOnError();
		
		//
		// Refer documentation for 'types' parameter at http://www.php.net/manual/en/mysqli-stmt.bind-param.php
		//
		mysqli_stmt_bind_param($stmt, '<REFER ABOVE COMMENT>', $itemID);	
		mysqli_stmt_execute($stmt);
		$this->throwExceptionOnError();
		
		mysqli_stmt_free_result($stmt);		
		mysqli_close($this->connection);
		*/
	}

	public function count() {
		// TODO Auto-generated method stub
		// Return the number of items in your array of records
		
		// Sample code
		/*
		$stmt = mysqli_prepare($this->connection, "SELECT COUNT(*) AS COUNT FROM $this->tablename");
		$this->throwExceptionOnError();

		mysqli_stmt_execute($stmt);
		$this->throwExceptionOnError();
		
		mysqli_stmt_bind_result($stmt, $rec_count);
		$this->throwExceptionOnError();
		
		mysqli_stmt_fetch($stmt);
		$this->throwExceptionOnError();
		
		mysqli_stmt_free_result($stmt);
		mysqli_close($this->connection);
		
		return $rec_count;
	    */ 	
	}
	
	public function getItems_paged($startIndex, $numItems) {
		// TODO Auto-generated method stub
		// Return a page of records as an array from the database for this startIndex
		
		// Sample code
		/*
		
		$stmt = mysqli_prepare($this->connection, "SELECT * FROM $this->tablename LIMIT ?, ?");
		$this->throwExceptionOnError();
		
		mysqli_stmt_bind_param($stmt, 'ii', $startIndex, $numItems);
		mysqli_stmt_execute($stmt);
		$this->throwExceptionOnError();
		
		$rows = array();
		
		//
		// e.g. if the table has columns ID, NAME then mysqli_stmt_bind_result
		// would look like this => mysqli_stmt_bind_result($stmt, $row->ID, $row->NAME);
		//
		mysqli_stmt_bind_result($stmt, <ENTER VALUES BASED ON ABOVE COMMENT>);
		
	    while (mysqli_stmt_fetch($stmt)) {
	      $rows[] = $row;
	      $row = new stdClass();
	      mysqli_stmt_bind_result($stmt, <ENTER VALUES AS ON LINE 237>);
	    }
		
		mysqli_stmt_free_result($stmt);		
		mysqli_close($this->connection);
		
		return $rows;
	    */ 	
	}	
	
	/**
	 * Utility function to throw an exception if an error occurs 
	 * while running a mysql command.
	 */
	private function throwExceptionOnError($link = null) {
		if($link == null) {
			$link = $this->connection;
		}
		if(mysqli_error($link)) {
			$msg = mysqli_errno($link) . ": " . mysqli_error($link);
			throw new Exception('MySQL Error - '. $msg);
		}		
	}

}

?>