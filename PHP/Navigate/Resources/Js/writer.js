targets = ['day','month','year'];
currentTarget = 0;

function _write(value)
{
	var txt = document.getElementById(targets[currentTarget]);
	var text = txt.value;
	
	if(value == 'K'){
	   return;
	}
	
	if(value == 'Delete')
	{
	   if(text.length == 0)
	   {
		   if(currentTarget == 0)
		   {
			   return;
		   }
		   else{
			   txt = document.getElementById(targets[--currentTarget]);
			   txt.value = txt.value.substring(0,txt.value.length-1);
		   }
	   }
	   else
	   {
		   txt.value = text.substring(0,text.length-1);
	   }
	}
	else
	{
	    if(text.length < 2 && currentTarget != 2){
           txt.value = text+value;
           if(txt.value.length == 2){
        	   changeTarget(++currentTarget);
           }
	    }
		else if(text.length < 4 && currentTarget === 2){
		    txt.value = text+value;
		}
	}
	
	return false;
}

function changeTarget(newTarget){
   currentTarget = newTarget;
}

$(document).ready(function(){
	
   $('#day').focus(function(){
       changeTarget(0);
   });
   
   $('#month').focus(function(){
       changeTarget(1);
   });
   
   $('#year').focus(function(){
       changeTarget(2);
   });
   
});