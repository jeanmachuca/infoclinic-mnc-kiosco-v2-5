/**
 * Funcion que se ejecuta al comienzo del paso5 crea un input text invisible el
 * cual se agrega al formulario y oculta el boton siguiente.
 */
 var cargando; 
 
$(document).ready(function()
{
	var hiddens = ['idTipoConsulta','nombreTipoConsulta','monto'];
	var frm = document.getElementById('formulario');
	cargando = document.getElementById('contenedor_cargando');
	for(var i in hiddens)
	{
		var input = document.createElement('input');
		input.setAttribute('name',hiddens[i]);
		input.setAttribute('id','_'+hiddens[i]);
		input.style.display = 'none';
		frm.appendChild(input);
	}
	
	$('#btnContinuar').hide();
});

/**
 * Asigna a los inputs creado al comienzo con id "_idTipoConsulta" y "_precio"
 * los valores por parametro y desabilita la navegacion
 * 
 * @param string idTipoConsulta
 * @param string nombre
 * @param string monto
 */
function next(idTipoConsulta,nombreTipoConsulta,monto, arrSintomas) 
{	

	if(arrSintomas.length == 0 && nombreTipoConsulta != 'Vacunas')
	{
		op = document.getElementById('op');
		op.value = 'paso7'
	}

	$('#_idTipoConsulta').val(idTipoConsulta);
	$('#_nombreTipoConsulta').val(nombreTipoConsulta);
	$('#_monto').val(monto);
	desabilitarNavegacion();
	cargando.style.display='block';
	//setTimeout("cargando.style.display='none'", 3000);
}

/**
 * Valida el formulario, en este caso, no es necesario
 * alguna validacion en especifica, pero la plantilla obliga
 * a implementar esta funcion
 * 
 * @return boolean
 */
function validate()
{
	/* el usuario volvio atras */
	if(document.volver)
	{
		document.goBack = 0;
		return true;
	}
		
	//El usuario hizo click en cancelar
	//atencion y luego en cancelar la confirmacion
	if(document._cancelar)
	{
		document._cancelar = 0;
		return false;
	}
	
	return true;
}