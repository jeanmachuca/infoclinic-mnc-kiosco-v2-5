/**
 * Validador de Rut
 * @author Oliver Carrasco
 */

/**
 * Calcula el digito verificador
 * @params string rutSinDv
 * @return string Dv
 */
function calcularDv(rutSinDv)
{
	var suma = 0;
	var largoRut = rutSinDv.length;
	var mutiplicadorActual = 2;
	var modulo = 0;
	var dv = '';
	for(var i = largoRut-1 ; i > -1 ; i--){
		var numeroActual = rutSinDv.charAt(i);
		if(mutiplicadorActual > 7){
			mutiplicadorActual = 2;
		}
		suma += parseInt(numeroActual) * mutiplicadorActual;
		mutiplicadorActual++;
	}
	modulo = suma%11;
	dv = 11 - modulo;

	if(dv == 10){
		dv = 'K';
	}
	else if(dv == 11){
		dv = '0';
	}

	return dv;
 }

/**
 * Verifica si el parametros, es un rut valido
 * @params string rut el texto a verificar
 * @return boolean
 */
function isRut(texto){	
	var rutSinDv = texto.substring(0,texto.length-1);
	var dv = texto.substring(texto.length-1);
	return ( !isNaN(rutSinDv) && calcularDv(rutSinDv) == dv);
}
