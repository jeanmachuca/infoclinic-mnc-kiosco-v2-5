/**
 * Funcion que se ejecuta al inicio del paso3
 * crea un texto no visible el cual se agrega al formulario de envio
 */
 var cargando;

$(document).ready(function(){
    var inputFechaNac = document.createElement('input');
	cargando = document.getElementById('contenedor_cargando');
    inputFechaNac.setAttribute('name','fechaNacimiento');
	inputFechaNac.style.display = 'none';
    inputFechaNac.setAttribute('id','_fechaNacimiento');
    var foo = document.getElementById('formulario');
	foo.appendChild(inputFechaNac);
	$('#btnK').hide();
});

/**
 * Determina si el formulario se debe enviar.
 * en caso del paso3, se validara la fecha.
 * @return boolean
 */
function validate() 
{
	/* el usuario volvio atras */
	if(document.volver)
	{
		document.goBack = 0;
		return true;
	}
	
	if(document._cancelar)
	{
		document._cancelar = 0;
		return false;
	}

	var response = 'Ok';
    var day = document.getElementById('day').value;
    var month = document.getElementById('month').value;
    var year = document.getElementById('year').value;
    
	if(!day){
	   response = 'Ingrese su fecha de nacimiento en formato [DD] [MM] [AAAA]';
	}
	else if(!month){
		response = 'Ingrese su fecha de nacimiento en formato [DD] [MM] [AAAA]';
	} 
	else if(!year){
		response = 'Ingrese su fecha de nacimiento en formato [DD] [MM] [AAAA]';
    }
	else if( !(validate_date(day+'-'+month+'-'+year)) ){
	   response = 'Ingrese su fecha de nacimiento en formato [DD] [MM] [AAAA]'
	}
    
	if( response != 'Ok' ){
	   showAlert(response);
	   return false;
	}
	
	desabilitarNavegacion();
	cargando.style.display='block';
	
	$('#_fechaNacimiento').val(year+'-'+month+'-'+day);
    return true;
}