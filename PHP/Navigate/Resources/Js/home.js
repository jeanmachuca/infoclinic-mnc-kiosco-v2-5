/**
 * Devuelve el dia de hoy.
 * 
 * @return string dia hoy ejemplo LUNES 01 DE ENERO
 */
function hoy(){
	var today = new Date();
	var DAYS = ['DOMINGO','LUNES','MARTES','MIERCOLES','JUEVES','VIERNES','SABADO'];
	var MONTHS = 
	['ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO',
	'SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE'];
	
	return DAYS[today.getDay()]+' '+today.getDate()+' DE '+MONTHS[today.getMonth()];
}

/**
 * Cambia la hora en un elemento con id "hora" 
 * cada 1 segundo con la hora actual en formato hh:mm:ss
 */
function cambiarHora(){
	var hora = $('#hora');
	timer = setInterval(
		function(){
			var hoy      = new Date();
			var horas    = hoy.getHours();
			var minutos  = hoy.getMinutes();
			var segundos = hoy.getSeconds();

			if(segundos < 10){ segundos = '0'+segundos; }
			if(minutos < 10){ minutos = '0'+minutos; }

			hora.html(horas+':'+minutos+':'+segundos);
		},1000
	);
}

/**
 * Evento que se dispara al sacar la pagina.
 * limpia el intervalo "timer".
 */
$(window).unload(function(){
	clearInterval(timer);
});