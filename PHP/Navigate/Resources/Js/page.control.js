//Variable que indica si se debe devolver hacoia atras
document.volver = 0;

//Variable que indica si se debe cancelar la atencion
document._cancelar = 0;

/**
 * Metodo que marca el paso actual con negro
 * y todos los demas con blanco
 * 
 * @param string 
 */
function init(pasoActual)
{
	for ( var i = 1; i < 12; i++)
	{
		var pasos = document.getElementById('paso' + i);
		pasos.style.color = 'white';
	}
	pasoActual = document.getElementById(pasoActual);
	
	try{ 
		pasoActual.style.color = 'black'; 
	} catch(e){ }
}

/**
 * Lanza la alerta en la pantalla con el mensaje dado por parametro
 * @param string mensaje
 */
function showAlert(mensaje)
{
	$('#contenedorAlertTexto').show(600,function(){
		$('#textoAlert').html(mensaje);
	});
}

/**
 * Lanza la alerta en la pantalla con la imagen dada por parametro
 * @param string ruta de la imagen
 */
function showAlertImagen(ruta)
{
	$('#contenedorAlertImagen').show(600,function(){ $(this).css('background','url('+ruta+') no-repeat') });
}

/**
 * Cancela la atencion, llevando el paso
 * en el que se encuentra el usuario, al paso0
 * 
 * @return boolean
 */
function cancelar()
{
	document._cancelar = 1;
	atras('paso0');
	return true;
}

/**
 * Va al paso indicado por parametro, idealmente hacia atras,
 * ya que para retroceder no se requiere validacion.
 *
 * @param string paso
 * @return boolean
 */
function atras(paso)
{	
	$('#op').val(paso);
	document.volver = 1;
	desabilitarNavegacion();
	return true;
}

/**
 * Desabilita la barra de navegacion
 * (oculta los botones de navegacion)
 */
function desabilitarNavegacion()
{
	$('#btnCancelar').hide();
	$('#btnVolver').hide();
	$('#btnContinuar').hide();
}