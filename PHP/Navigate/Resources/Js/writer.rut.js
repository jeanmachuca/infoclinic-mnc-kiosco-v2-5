/**
 * Escibe el valor en el campo txtRut
 * en caso de recibir DELETE, borra un
 * caracter.
 * 
 * @return boolean siempre falso para evitar el submit del formulario 
 * 				   al ingresar un numero en el campo txtRut
 */
function _write(value)
{
	var txtRut = document.getElementById('txtRut');
	var textoRut = txtRut.value;
	
	if( (textoRut.length < 9) && (value != 'Delete') )
	{
		txtRut.value += value;
	}
	else if( (value == 'Delete') && (textoRut.length > 0) )
	{
		txtRut.value = txtRut.value.substring(0,txtRut.value.length - 1);
	}
	return false;
}