var requests = new Array();

function AjaxError(name, description, number)
{
	this.name = name;
	this.description = description;
	this.number = number;

	return this;
}

AjaxError.prototype.toString = function()
{
	return this.name + " " + this.description;
}

if(typeof(XMLHttpRequest) == 'undefined')
var XMLHttpRequest = function()
{
	var request = null;
	try
	{
		request = new ActiveXObject('Msxml2.XMLHTTP');
	}
	catch(e)
	{
		try
		{
			request = new ActiveXObject('Microsoft.XMLHTTP');
		}
		catch(ee)
		{
			alert('Su navegador no soporta Ajax');
		}
	}
	return request;
}

function AjaxStop()
{
	for(var i=0; i<requests.length; i++)
	{
		if(requests[i] != null)
			requests[i].abort();
	}
}

function AjaxCreateRequest(context)
{	
	for(var i=0; i<requests.length; i++)
	{
		if(requests[i].readyState == 4)
		{
			requests[i].abort();
			requests[i].context = null;
			return requests[i];
		}
	}

	var pos = requests.length;
	
	requests[pos] = Object();
	requests[pos].obj = new XMLHttpRequest();
	requests[pos].context = context;
	
	return requests[pos];
}

function AjaxRequest_stream(url, data, callback, context)
{
	var request = AjaxCreateRequest(context);
	var async = typeof(callback) == 'function';
	if(async) request.obj.onreadystatechange = function()
	{
		if(request.obj.readyState == 4) {
			callback(new AjaxResponse(request));
		}
	}
	request.obj.open('PUT', url, async);
	request.obj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
	//request.obj.setRequestHeader('Content-Type', 'application/octet-stream');
	request.obj.setRequestHeader('Language', 'es-ES');
//	alert(data);
	request.obj.send(data);
	if(!async) {
		return new AjaxResponse(request);
	}
}

function AjaxResponse(request)
{
	this.request = request.obj;
	this.error = null;
	this.value = null;
	this.context = request.context;
	if(request.obj.status == 200)
	{
		try
		{
			this.value = ObjectFromJson(request);
			if(this.value && this.value.error)
			{
				this.error = this.value.error;
				this.value = null;
			}
		}
		catch(e)
		{
			alert(e.name + ' , ' + e.description + ' , ' + e.number);
			//this.error = new AjaxError(e.name, e.description, e.number);
		}
	}
	else
	{
		//this.error = new AjaxError('HTTP request failed with status: ' + request.obj.status, request.obj.status);
	}
	return this;
}

function AjaxRequest(url, data, callback, context)
{
	var request = AjaxCreateRequest(context);
	var async = typeof(callback) == 'function';
	if(async) request.obj.onreadystatechange = function()
	{
		if(request.obj.readyState == 4) {
			callback(new AjaxResponse(request));
		}
	}
	request.obj.open('POST', url, async);
	request.obj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	// application/octet-stream
	// para enviar binarios
	// prompt(data, data, data);
	request.obj.send(data);
	
	if(!async) {
		return new AjaxResponse(request);
	}
}


function enc(s)
{
	return s.toString().replace(/\%/g, "%26").replace(/=/g, "%3D");
}

function ObjectFromJson(request) {
	if(request.obj.responseXML != null && request.obj.responseXML.xml != null && request.obj.responseXML.xml != '') {
		return request.obj.responseXML;
	}
	var r = null;	
	//eval('r= ' + request.obj.responseText + ';');
	r = request.obj.responseText;
	return r;
}

function JsonFromObject(o)
{
	if(o == null)
		return 'null';

	switch(typeof(o))
	{
		case 'object':
			if(o.constructor == Array)		// checks if it is an array [,,,]
			{
				var s = '';
				for(var i=0; i<o.length; ++i)
				{
					s += JsonFromObject(o[i]);

					if(i < o.length -1)
						s += ',';
				}

				return '[' + s + ']';
			}
			break;
		case 'string':
			return '"' + o.replace(/(["\\])/g, '\\$1') + '"';
		default:
			return String(o);
	}
}
var ajaxVersion = '5.6.3.4.2';