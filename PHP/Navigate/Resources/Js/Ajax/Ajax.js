var oAjax = {
	url:'/Tools/Ajax/',
	
	Loading: function (mensaje)
	{
	},
	
	UnLoading: function ()
	{
	},
	
	goRequestPage: function(pagedata, callback, context){
		return new AjaxRequest(this.url,pagedata,callback, context);
	},
	
	fillSelectFromArray: function (selectobj, array) {
	
		for(var i = 0; i < array.length; i++) {
			value = array[i]['value'];
			text = array[i]['text'];
			eval('text = ' + text + ";");
			opt = new Option(text, value);
			if (selectobj && value) {
				selectobj.options[selectobj.options.length] = opt;
			}
		}
		first_text = dfirst_text;
	},
	
	// unserializeArray: deserializa el parametro param y devuelve un arreglo hash 
	_unserializeArray: function (param) {
		var i, j, text, value, aux;
		objects = param.split("|");
		var aObjects = new Array();
		for(i = 0; i < objects.length; i++) {
			vars = objects[i].split("&");
			var object = new Array();
			for (j = 0; j < vars.length; j++) {
				aux = vars[j].split("=");
				object[aux[0]] = aux[1];
			}
			aObjects[aObjects.length] = object;
		}
		return aObjects;
	},
	
	selectClear: function (lst){
	  lst.options.length = 0;
	  opt = new Option(''+first_text+'', '0');
	  lst.options[lst.options.length] = opt;
	},
	
	fillObjects: function (data, select) {
		aDatas = this._unserializeArray(data);
		this.selectClear(select);
		this.fillSelectFromArray(select, aDatas);
		return true;
	}
	
}