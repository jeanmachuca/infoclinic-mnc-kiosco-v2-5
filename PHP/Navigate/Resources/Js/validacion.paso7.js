/**
 * Funcion que se ejecuta al comienzo del paso5
 * repara el nombre del medico, ya que el servicio
 * lo entrega con una coma entre el nombre y el apellido
 */
 var cargando;
$(document).ready(function(){
	var fixNombre = $('#nombreMedico').html();
	cargando = document.getElementById('contenedor_cargando');
	fixNombre = fixNombre.replace(new RegExp(',','g'),'');
	$('#nombreMedico').html(fixNombre);
});

function validate()
{
	/* el usuario volvio atras */
	if(document.volver)
	{
		document.goBack = 0;
		return true;
	}
	
	//El usuario hizo click en cancelar
	//atencion y luego en cancelar la confirmacion
	if(document._cancelar)
	{
		document._cancelar = 0;
		return false;
	}

	cargando.style.display='block';
	setTimeout("cargando.style.display='none'", 3000);
	desabilitarNavegacion();
	return true;
}