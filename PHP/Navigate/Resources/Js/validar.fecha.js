/**
 * Valida una fecha en formato dd-mm-aaaa
 * La fecha va a ser validada desde 
 * @return bolean
 */
function validate_date(date)
{    
	var month_with_Thirty0ne_days = [01,03,05,07,08,10,12];
    var month_with_Thirty_days = [04,06,09,11];
    var number = date.split('-');
	var day = number[0];
	var month = number[1];
	var year = number[2];
	
	/** valida que todos los campos sean numericos **/
	if( (isNumber(year)) && (isNumber(month)) && (isNumber(day)) )
	{
	    /*** valida que el dia y el mes y el anio esten dentro de un rango aceptable y que el mes no sea febrero **/
		if( isBetween(day,0,32) && isBetween(month,0,13) && month != 2 && isBetween(year,1900,new Date().getFullYear()))
		{
		    /** check for month of thirty one days **/
	        for(var i in month_with_Thirty0ne_days)
			{
			    if(month == month_with_Thirty0ne_days[i])
				{
				    return (day < 32);
				}
			}
			
			/** check for month of thirty days **/
	        for(var i in month_with_Thirty_days)
			{
			    if(month == month_with_Thirty_days[i])
				{
				    return (day < 31);
				}
			}
			
		}
		else if(month == 2){
		
		    /** probablemente es anio bisiesto **/
			if( (isLeapYear(year)) && (isBetween(day,0,30)) )
			{
			    return true;
			}
			/** no es anio bisiesto asi que debe estar entre 1 y 28 **/
			else if( isBetween(day,0,29) )
			{
			  return true;
			}
		}
	}
	return false;
}

/**
 * Comprueba si el parametro es un numero
 * @return boolean
 */
function isNumber(number){
  return !isNaN(number);
}

/**
 * Comprueba si el parametro esta entre un rango (min & max)
 * @return boolean
 */
function isBetween(data,min,max){
   return ( (data > min) && (data < max) );
}

/**
 * Comprueba si el anio por parametro es bisiesto
 * @return boolean
 */
function isLeapYear(year){
  return (year%4 == 0);
}