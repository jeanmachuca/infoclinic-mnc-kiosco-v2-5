/**
 * Comprueba si el parametro es un email valido
 * @params string texto a evaluar
 * @return boolean
 */
function isEmail(texto)
{
	return /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(texto)
}