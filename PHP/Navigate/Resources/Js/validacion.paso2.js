/**
 * Funcion que se ejecuta al comienzo del paso2
 * crea varios input text invisible el cual se agrega 
 * al formulario con el tipo de prevision, el id de la 
 * prevision y el nombre.
 */
 var cargando;

$(document).ready(function()
{
	var hiddens = ['tipoPrevision','codigoPrevision','nombrePrevision'];
	var frm = document.getElementById('formulario');
	cargando = document.getElementById('contenedor_cargando');
	for(var i in hiddens)
	{
		var input = document.createElement('input');
		input.setAttribute('name',hiddens[i]);
		input.setAttribute('id','_'+hiddens[i]);
		input.style.display = 'none';
		frm.appendChild(input);
	}
    
	$('#btnContinuar').hide();
});

/**
 * Asigna a los inputs creado al comienzo con  id "_tipoPrevision", 
 * "_codigoPrevision" y "_nombrePrevision" los valores por parametro.
 * 
 * @param string tipoPrevision
 * @param string codigoPrevision
 * @param string nombrePrevision
 */
function next(tipoPrevision, codigoPrevision, nombrePrevision)
{
	$('#_tipoPrevision').val(tipoPrevision);
	$('#_codigoPrevision').val(codigoPrevision);
	$('#_nombrePrevision').val(nombrePrevision);
}

/**
 * Valida el formulario, en este caso, no es necesario
 * alguna validacion en especifica, pero la plantilla obliga
 * a implementar esta funcion
 * 
 * @return boolean
 */
function validate()
 {
	
    cargando.style.display='block';
	return true;
}