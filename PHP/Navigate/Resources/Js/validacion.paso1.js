/**
 * funcion que se ejecuta al comienzo.
 * crea un campo en el formulario para enviarlo
 * al modulo.
 */
 var cargando;

$(document).ready(function(){
    var inputRut = document.createElement('input');
    
    inputRut.setAttribute('name','rut');
    //oculto el campo, ya que iexplore no permite crear hidden
	inputRut.style.display = 'none';
    inputRut.setAttribute('id','_rut');
    cargando = document.getElementById('contenedor_cargando');
    var frm = document.getElementById('formulario');
	frm.appendChild(inputRut);
});

/**
 * Valida que el rut haya sido ingresado correctamente
 * o retorna true si el usuario quiere cancelar la atencion.
 * 
 * @return boolean
 */
function validate()
{	
	/* el usuario volvio atras */
	if(document.volver)
	{
		document.goBack = 0;
		return true;
	}
	
	//El usuario hizo click en cancelar
	//atencion y luego en cancelar la confirmacion
	if(document._cancelar)
	{
		document._cancelar = 0;
		return false;
	}

    var rut = $('#txtRut').val();
    
    if(!rut)
    {
	   showAlert('Debe ingresar su Rut.');
	   return false;
	}
	else if(!isRut(rut) || (rut.length < 5))
	{
		showAlert('Ha ingresado su Rut de forma incorrecta.');
	    return false;
	}
	
    $('#_rut').val(formaterRut(rut));
    cargando.style.display='block';
    return true;
}

/**
 * Recibe un texto y lo formatea.
 * al ultimo caracter le antepone un guion
 * 
 * @return string
 */
function formaterRut(texto){
	return (texto.substring(0,texto.length-1) + '-' + texto.substring(texto.length-1));
}