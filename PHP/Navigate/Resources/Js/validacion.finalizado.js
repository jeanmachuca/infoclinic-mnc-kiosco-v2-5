

$(document).ready(function(){
	/* oculta el boton continuar */
	document.getElementById("btnContinuar").style.visibility = "hidden";
	document.getElementById("btnCancelar").style.visibility = "hidden";
	document.getElementById("btnVolver").style.visibility = "hidden";
	
});

/**
* Agrega un campo al formulario:
* solicitudSMS = 1, si el usuario desea recibir un msje de texto a su celular.
* solicitudSMS = 0, si no.
**/
function solicitudSMS(solicitudSMS)
{	
	var op = document.getElementById('op');
	op.value = 'finalizado';
	
	var frm = document.getElementById('formulario');
	var input = document.createElement('input');
	input.type = 'hidden';
	input.name = 'solicitudSMS';
	input.value = solicitudSMS; 
	frm.appendChild(input);
	frm['op'].value='ConfirmarConsulta';

	document.getElementById("boton_si").style.visibility = "hidden";
	document.getElementById("boton_no").style.visibility = "hidden";
}


function validate()
{	
	 
	
	/* el usuario volvio atras */
	if(document.volver)
	{
		document.goBack = 0;
		$('#contenedor_cargando').show(200);
		return true;
	}
	
	//El usuario hizo click en cancelar
	//atencion y luego en cancelar la confirmacion
	if(document._cancelar)
	{
		document._cancelar = 0;
		return false;
	}
	
	$('#contenedor_cargando').show(200);
	return true;
}