/**
 *  Funcion que se ejecuta al comienzo del paso 4.
 *  llama al formateo de fecha de nacimiento, ya que el servicio
 *  lo entrega en formato aaaa-mm-dd y crea los input correspondientes
 *  para enviarlos al modulo
 *  
 */
 var cargando;

$(document).ready(function(){
	
	
	//$('#txtFechaNacimiento').val(formatearFecha($('#fechaNacimiento').val()));
	cargando = document.getElementById('contenedor_cargando');
	var txtCelular = document.getElementById('formulario')['txtCelular'];
	txtCelular.name="";
	var txtFono = document.getElementById('formulario')['txtFono'];
	txtFono.name="";
	var txtNombres = document.getElementById('formulario')['txtNombres'];
	txtNombres.name="";
	var txtFechaNacimiento = document.getElementById('formulario')['txtFechaNacimiento'];
	txtFechaNacimiento.name="";
	var txtApellidoMaterno = document.getElementById('formulario')['txtApellidoMaterno'];
	txtApellidoMaterno.name="";
	var txtApellidoPaterno = document.getElementById('formulario')['txtApellidoPaterno'];
	txtApellidoPaterno.name="";
	var txtSexo = document.getElementById('formulario')['txtSexo'];
	txtSexo.name="";	
	
	var hiddens = ['fonoCelular','fonoFijo','FonoPrefijo','nombre', 'apellidoPaterno', 'apellidoMaterno', 'genero', 'fechaNacimiento'];
	
	var frm = document.getElementById('formulario');
	for(var i in hiddens)
	{
		var input = document.createElement('input');
		input.setAttribute('name',hiddens[i]);
		input.setAttribute('id','_'+hiddens[i]);
		input.style.display = 'none';
		frm.appendChild(input);
	}
});

/**
 * Este script hace el juego con los teclados.
 * Al integrar teclados wosk, favor de eliminar 
 * el metodo que sigue a continuacion
 */
$(function(){
	
	tecladoActivo = 'Normal';
	target = 'txtEmail';
	
	//elimina la k del teclado numerico
	$('#btnK').hide().html('ABC');
	$('#botonera').css('position','absolute');
	$('#botonera').css('top','140px');
	$('#botonera').css('marginLeft','350px');
	$('#botonera').hide();
	
	$('#btnK').click(function(){
		tecladoNormal();
		target = 'txtEmail';
	});
	
	$('#txtCelular').click(function(){
		tecladoNumerico(false);
		target = 'txtCelular';
	});
	
	$('#txtFono').click(function(){ 
		tecladoNumerico(false);
		target = 'txtFono';
	});
	
	$('#txtEmail').click(function(){ 
		tecladoNormal();
	});
	
	$('#txtNombres').click(function(){ 
		tecladoNormal();
		target = 'txtNombres';
	});
	
	$('#txtApellidoPaterno').click(function(){ 
		tecladoNormal();
		target = 'txtApellidoPaterno';
	});
	
	$('#txtApellidoMaterno').click(function(){ 
		tecladoNormal();
		target = 'txtApellidoMaterno';
	});
	
	$('#txtSexo').click(function(){ 
		tecladoNormal();
		target = 'txtSexo';
	});
});


/**
 * Escribe un numero o borra de acuerdo al parametro
 * al objetivo que se seleeciona al momento de hacerle click 
 * Eliminar este metodo al integrar teclado wosk
 * 
 * @param numero a escribir o Delete para borrar una letra
 */
function _write(numero)
{

	seleccion = document.getElementById(target);
	

	if(seleccion.id == 'txtCelular')
	{
		formatearCelular(numero, seleccion);
	}
	else
		{
		if(seleccion.id == 'txtFono')
		{
			formatearFono(numero, seleccion);
		}
			else
			{
			
				if(numero != 'Delete' && numero != 'K')
				{
					seleccion.value += numero;
				}
				
				else if(seleccion.value.length > 0 && numero != 'K')
				{
					seleccion.value = seleccion.value.substring( 0 , seleccion.value.length-1 );
				}
			}
		}
}

function formatearCelular(numero, seleccion)
{
	var largo = seleccion.value.length;
		
	if(largo == 1 && numero != 'Delete')

	{
		seleccion.value = "(+569) " + seleccion.value;
	}
	if(numero != 'Delete' && numero != 'K' && largo <= 15)
	{
		seleccion.value += numero;
	}
	
	/*escribir numero*/
	else if(largo > 0 && numero != 'K')
	{
		seleccion.value = seleccion.value.substring( 0 , seleccion.value.length-1 );
	}
	
	/*al borrar*/
	
	if( numero == 'Delete' && largo <= 8)
	{
		seleccion.value = "";
	}
			
}


function formatearFono(numero, seleccion)
{
	var largo = seleccion.value.length;
	var codigoArea;
	/*Establece codigo de area y separa numero de codigos*/
	if(seleccion.value == '2' && numero != 'Delete')
	{
		seleccion.value = "(+56) " + seleccion.value + " ";
		codigoArea = seleccion.value; // es 2 de Santiago
	}
	if(seleccion.value != '2' && largo == 2 && numero != 'Delete')
	{
		seleccion.value = "(+56) " + seleccion.value + " ";
		codigoArea = seleccion.value;  // es de otras regiones
	}
	
	/*al borrar*/
	if( numero == 'Delete' &&  ( ( codigoArea == 2 && largo <= 8)|| ( codigoArea != 2 && largo <= 7)))
	{
		seleccion.value = "";
	}
	
	if ( codigoArea != 2 && largo == 8 && numero != 'Delete')
	{
		seleccion.value = seleccion.value + " ";	
	}
	
	if ( codigoArea == 2 && largo == 7 && numero != 'Delete')
	{
		seleccion.value = seleccion.value + " ";	
	}
	
	/* al escribir numero */
	
	if( (numero != 'Delete' && numero != 'K' ) && largo < 16 )
	{
		seleccion.value += numero;
	}
	
	else if(seleccion.value.length > 0 && numero != 'K' )
	{
		seleccion.value = seleccion.value.substring( 0 , seleccion.value.length-1 );
	}
		
}

/**
 * Activa el teclado numerico
 * Borra metodo al integrar teclado wosk
 * @param activaNormal indica si se debe activar la opcion de volver a teclado normal
 */
 
function tecladoNumerico(activaNormal)
{
	if(tecladoActivo == 'Normal')
	{
		$('#teclado').hide(600,function(){ $('#botonera').show(600) })
		tecladoActivo = 'Numerico';
	}
	
	(activaNormal)? $('#btnK').show(): $('#btnK').hide();

}

/**
 * Activa el teclado normal con una animacion
 * borrar al integrar teclado wosk
 */
function tecladoNormal()
{
	if(tecladoActivo == 'Numerico')
	{
		$('#botonera').hide(600,function(){ $('#teclado').show(600) })
		tecladoActivo = 'Normal';
	}
}

/**
 * Valida el formulario de ingreso de datos.
 * 
 * @return boolean
 */
function validate()
{
	/* el usuario volvio atras */
	if(document.volver)
	{
		document.goBack = 0;
		return true;
	}
	
	//El usuario hizo click en cancelar y despues cancelo la confirmacion
	if(document._cancelar)
	{
		document._cancelar = 0;
		return false;
	}
	
	
	var celular        = $('#txtCelular').val();
	var email          = $('#txtEmail').val();
	var fono           = $('#txtFono').val();
	var fonoPrefijo    = $('#txtFonoPrefijo').val();
	var nombre			= $('#txtNombres').val();
	var apellidoMaterno = $('#txtApellidoMaterno').val();
	var apellidoPaterno = $('#txtApellidoPaterno').val();
	var genero 			= $('#txtSexo').val();
	var fechaNacimiento = $('#txtFechaNacimiento').val();
	
	/* quitar el formato al enviar */
	
	celular = celular.replace("(+569) ","");
	celular = celular.replace(" ",""); 
	fono = fono.replace("(+56) ",""); 
	fono = fono.replace(" ",""); 
	

	if (celular.length < 8 && celular.length > 0)
	{
		showAlert('Su n&uacute;mero celular debe contener un m&iacute;nimo de 8 d&iacute;gitos');
		return false;
	}

	if (!celular && !fono ) 
	{
		showAlertImagen('Resources/Images/paso4_popup.png');
		return false;
	}
	
	if (!email) 
	{
		showAlertImagen('Resources/Images/paso4_popup.png');
		return false;
	}
	
	if(!isEmail(email))
	{
		showAlert('Su e-Mail debe ser ingresado en formato micuenta@email.com');
		return false;
	}
	
	desabilitarNavegacion();
	 cargando.style.display='block';

	//agrega campos al formulario, en inputs de nombre: txtCelular, txtFono, txtFonoPrefijo
	
	$('#_fonoCelular').val(celular);
	$('#_fonoFijo').val(fono);
	$('#_FonoPrefijo').val(fonoPrefijo);
	$('#_nombre').val(nombre);
	$('#_apellidoMaterno').val(apellidoMaterno);
	$('#_apellidoPaterno').val(apellidoPaterno);
	$('#_genero').val(genero);
	$('#_fechaNacimiento').val(fechaNacimiento);

    return true;
}

/**
 * Reibe una fecha en formato aaaa-mm-dd
 * y la formatea como dd-mm-aaaa
 * 
 * @param string fecha a convertir
 * @return string fecha en formato dd-mm-aaaa
 */
function formatearFecha(fecha)
{
	return fecha.split('-')[2]+'-'+fecha.split('-')[1]+'-'+fecha.split('-')[0];
}