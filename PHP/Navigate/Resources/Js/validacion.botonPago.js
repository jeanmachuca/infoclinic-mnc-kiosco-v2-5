/**
 * Funcion que se ejecuta al comienzo del paso
 * crea varios input text invisible el cual se agrega 
 * al formulario con el tipo de prevision, el id de la 
 * prevision y el nombre.
 */
 var cargando;

$(document).ready(function()
{
	var hiddens = ['BotonPago'];
	var frm = document.getElementById('formulario');
	cargando = document.getElementById('contenedor_cargando');
	for(var i in hiddens)
	{
		var input = document.createElement('input');
		input.setAttribute('name',hiddens[i]);
		input.setAttribute('id','_'+hiddens[i]);
		input.style.display = 'none';
		frm.appendChild(input);
	}
    
	$('#btnContinuar').hide();
});

/**
 * 
 * @param string botonPago
 */
function next(tipoPago)
{
	/*si el paciente selecciona multicaja(tipoPago ==1)  botonPago == 1 Y el bot�n redirige a IMED, 
	de lo contrario, botonPago ==0 y el bot�n redirige a transbank*/
	
	if(tipoPago == 1)
	{
		op = document.getElementById('op');
		op.value = 'IMED'
	}
	else if (tipoPago == 0)
	{
		op = document.getElementById('op');
		op.value = 'Transbank'
	}

	$('#_BotonPago').val(tipoPago);
}

/**
 * Valida el formulario, en este caso, no es necesario
 * alguna validacion en especifica, pero la plantilla obliga
 * a implementar esta funcion
 * 
 * @return boolean
 */

function validate()
{
	/* el usuario volvio atras */
	if(document.volver)
	{
		document.goBack = 0;
		return true;
	}
	
	//El usuario hizo click en cancelar
	//atencion y luego en cancelar la confirmacion
	if(document._cancelar)
	{
		document._cancelar = 0;
		return false;
	}

	cargando.style.display='block';
	setTimeout("cargando.style.display='none'", 3000);
	desabilitarNavegacion();
	return true;
}