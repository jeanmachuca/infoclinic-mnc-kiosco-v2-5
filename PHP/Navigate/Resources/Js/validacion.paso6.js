/**
 * Funcion que se ejecuta al comienzo del paso6
 * crea un input text invisible el cual se agrega 
 * al formulario y oculta el boton siguiente.
 */

var idsintomas = new Array();
var nombres = new Array();
var sintomasMostrar = new Array();
var cargando;
$(document).ready(function(){
	

		var hiddens = ['idSintoma','nombreSintoma', 'sintomasMostrar'];
		var frm = document.getElementById('formulario');
		cargando = document.getElementById('contenedor_cargando');
		for(var i in hiddens)
		{
			var input = document.createElement('input');
			input.setAttribute('name',hiddens[i]);
			input.setAttribute('id','_'+hiddens[i]);
			input.style.display = 'none';
			frm.appendChild(input);
		}
	
});

/**
 * Funcion que agrega un s�ntoma al array y cambia la im�gen del bot�n
 * @param string idSintoma
 * @param string nombre
 * @param string idBoton
 */

function agregar(idSintoma,nombre,idBoton)
{	
	var existe = 0;
	for (i=0;i<idsintomas.length;i++){
		
		if(idSintoma==idsintomas[i]){
			existe=1;
			} 
	}
	
	if (existe==0){
	idsintomas.push(idSintoma);
	nombres.push(nombre);

	$(idBoton).css("background", "url(Resources/Images/btn_estandar_mano.png)");
	} 
}
/**
* Funcion que quita un sintoma del array y cambia la imagen del boton
 * @param string idSintoma
 * @param string nombre
 * @param string idBoton
 */

function quitar(idSintoma,nombre,idBoton)
{	
	for (i=0;i<idsintomas.length;i++){
		
		if(idSintoma==idsintomas[i]){
			delete idsintomas[i];
			delete nombres[i];	
		} 
	}
	$(idBoton).css("background", "url(Resources/Images/btn_estandar.png)");
}

/**
* Agrega o quita sintomas, a partir de la verificacion de la imagen que tiene el boton
* @param string idSintoma
* @param string nombre
*/

function next(idSintoma,nombre){
	
	if(idSintoma != null)
	{
		var idBoton = "#" + idSintoma;
	
		imagen = $(idBoton).css("background-image");
		if(imagen.indexOf("btn_estandar.png")!= -1){
		agregar(idSintoma,nombre, idBoton);
		}else
		{	
		quitar(idSintoma, nombre, idBoton);
		}
	
	}
}

function nextVacunas(idTipoConsulta,nombreTipoConsulta,monto) 
{	

		var hiddens = ['idTipoConsulta','nombreTipoConsulta','monto'];
		var frm = document.getElementById('formulario');
		
		for(var i in hiddens)
		{
			var input = document.createElement('input');
			input.setAttribute('name',hiddens[i]);
			input.setAttribute('id','_'+hiddens[i]);
			input.style.display = 'none';
			frm.appendChild(input);
		}
	
	$('#_idTipoConsulta').val(idTipoConsulta);
	$('#_nombreTipoConsulta').val(nombreTipoConsulta);
	$('#_monto').val(monto);
	desabilitarNavegacion();
	cargando.style.display='block';
}


/*
* Quita nulos o vacios de un array
*/
function cleanArray( actual ){
	  var newArray = new Array();
	  for( var i = 0, j = actual.length; i < j; i++ ){
	      if ( actual[ i ] ){
	        newArray.push( actual[ i ] );
	    }
	  }
	  return newArray;
}

/**
* Valida el formulario y prepara las variables para hacer el submit
* 
* @return boolean
*/

function validate()
{
	desabilitarNavegacion();
	
	/*Agregar sintomas al formulario*/
	
	nombres = cleanArray(nombres);
	idsintomas = cleanArray(idsintomas);
	if(nombres.length >3)
		{
		sintomasMostrar[0] = nombres[0];
		sintomasMostrar[1] = nombres[1];
		sintomasMostrar[2] = nombres[2];
		sm = sintomasMostrar.join(", ");
		$('#_sintomasMostrar').val(sm + "...");
		}
	n = nombres.join(", ");
	s = idsintomas.join(",");
		
	$('#_nombreSintoma').val(n);
	$('#_idSintoma').val(s);
	cargando.style.display="block";
	
	/* el usuario volvio atras */
	if(document.volver)
	{	
		document.goBack = 0;
		desabilitarNavegacion();
		return true;
	}
	
	//El usuario hizo click en cancelar
	//atencion y luego en cancelar la confirmacion
	if(document._cancelar)
	{
		document._cancelar = 0;
		return false;
	}
	
	return true;
}

