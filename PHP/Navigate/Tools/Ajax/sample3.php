<?php

$nblocks = intval($_REQUEST['nblocks']); // number of total blocks 
$iblockW = intval($_REQUEST['iblockW']); // index number of horizontal block (0 to nblocks - 1)
$iblockH = intval($_REQUEST['iblockH']); // index number of vertical block   (0 to nblocks - 1)

$imgfile = "10.jpg";
// Get the original size
list($width, $height) = getimagesize($imgfile);

$cropW      = $width/$nblocks;
$cropH      = $height/$nblocks;
$cropStartX = $cropW*$iblockW;
$cropStartY = $cropH*$iblockH;

// Create two images
$origimg = imagecreatefromjpeg($imgfile);
$cropimg = imagecreatetruecolor($cropW,$cropH);


// Crop
imagecopyresized($cropimg, $origimg, 0, 0, $cropStartX, $cropStartY, $width, $height, $width, $height);

// TODO: write code to save new image
// or, just display it like this:
header("Content-type: image/jpeg");
imagejpeg($cropimg);

// destroy the images
imagedestroy($cropimg);
imagedestroy($origimg);

?>