<?php

class ParamPago{

	public $confirmacion;
	public $codigoAutorizacion;
	public $fechaTransaccion;
	public $idConsulta;
	public $idTransaccionIMed;
	public $identificacion;
	public $tipoIdentificacion;
	public $solicitudSMS;
	public $glosaFallo;
	public $tipoFlujo;

	public function ParamPago(){
	}


}

include_once(dirname(__FILE__).'/'.'../Application/Loader.php');

$archivosConsulta = glob(dirname(__FILE__).'/../Application/Connectivity/WServer/*ConfirmarConsulta.txt');

$cadenaCompleta = "";

foreach ($archivosConsulta as $k=>$nombreLog){
	$cadenaLog = file_get_contents($nombreLog);
	$cadenaLog = str_replace('RESPONDE DATA: :: : : ','RESPONSE DATA: :: : : ',$cadenaLog);
	$cadenaCompleta .= $cadenaLog;
}
$llamadasServicio = explode ('SERVICE INFO',$cadenaCompleta);

print "<table border = 1>";
	print "<tr>";
	print "<th>Confirmacion</th>";
	print "<th>Cod Autorizacion</th>";
	print "<th>Fecha Transaccion</th>";
	print "<th>idConsulta</th>";
	print "<th>idTransaccionIMed</th>";
	print "<th>identificacion</th>";
	print "<th>tipoIdentificacion</th>";
	print "<th>solicitudSMS</th>";
	print "<th>glosaFallo</th>";
	print "<th>tipoFlujo</th>";
	print "</tr>";
	print "<tr>";

foreach ($llamadasServicio as $llamada){
	$aLlamada = explode('<?xml',$llamada);
	if (sizeof($aLlamada)>0){
		$llamada = '<?xml '.$aLlamada[1];
		$aLlamada1 = explode('</soap:Envelope>',$llamada);
		if (sizeof($aLlamada1)>1){
			$llamada = $aLlamada1[0].'</soap:Envelope>';
		}
	}
	$llamada = str_replace('\'','',$llamada);
	$aLlamada2 = explode('RESPONSE DATA: :: : : ',$llamada);

	$entrada = $aLlamada2[0];
	$_xmlEntrada = new _XML2Array($entrada);
	$xmlEntrada  = $_xmlEntrada->parse();

	$salida = $aLlamada2[1];
	$_xmlSalida = new _XML2Array($salida);
	$xmlSalida  = $_xmlSalida->parse();

	$paramPago = $xmlEntrada[0]['children']['0']['children']['0']['children']['0']['children'];

	$objParamPago = new ParamPago();
	foreach ($paramPago as $param){
		$objParamPago->$param['name'] = $param['data'];
	}

	print "<td>".$objParamPago->confirmacion."</td>";
	print "<td>".$objParamPago->codigoAutorizacion."</td>";
	print "<td>".$objParamPago->fechaTransaccion."</td>";
	print "<td>".$objParamPago->idConsulta."</td>";
	print "<td>".$objParamPago->idTransaccionIMed."</td>";
	print "<td>".$objParamPago->identificacion."</td>";
	print "<td>".$objParamPago->tipoIdentificacion."</td>";
	print "<td>".$objParamPago->solicitudSMS."</td>";
	print "<td>".$objParamPago->glosaFallo."</td>";
	print "<td>".$objParamPago->tipoFlujo."</td>";
	print "</tr>";

}

print "</table>";
?>
