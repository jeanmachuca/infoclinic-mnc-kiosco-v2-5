<?php
include_once(dirname(__FILE__).'/'.'../Application/Loader.php');

class MiniclinicConnector extends Main{

	public $oModule;// Declaracion automatica del controlador del modulo
	public $oTemplate;
	
	function MiniclinicConnector(){
		$this->onRender=create_function(null,'return false;');
		parent::Main();
	}
		
	private function TransformTipoFlujo($tipoFlujo){
		$newTipoFlujo = $tipoFlujo;
		switch ($tipoFlujo) {
			case '1':{
				$newTipoFlujo = 'N';
				break;
			}
			case '2':{
				$newTipoFlujo = 'E';
				break;
			}
			
			default:{
				break;
			}
		}
		return $newTipoFlujo;
	}

	public function Home(){
		return $this->oModule->Home();
	}
	public function EstadoImpresora(){
		return $this->oModule->EstadoImpresora();
	}
	public function Motivos( $tipoFlujo, $idKiosco){
		$tipoFlujo = $this->TransformTipoFlujo($tipoFlujo);
		return $this->oModule->Motivos($tipoFlujo,$idKiosco);
	}
	public function Resumen($tipoFlujo, $idKiosco, $idTipoConsulta){
		$tipoFlujo = $this->TransformTipoFlujo($tipoFlujo);
		return $this->oModule->Resumen($tipoFlujo,$idKiosco,$idTipoConsulta);
	}
	public function Identificacion($tipoFlujo, $tipoIdentificacion, $rut, $codigoPrevision)
	{
		$tipoFlujo = $this->TransformTipoFlujo($tipoFlujo);
		return $this->oModule->Identificacion($tipoFlujo,$tipoIdentificacion,$rut,$codigoPrevision);
	}
	public function DatosPersonales($tipoFlujo, $tipoIdentificacion, $idKiosco, $rut, $apellidoPaterno, $apellidoMaterno, $nombre, $fechaNacimiento, $genero, $email, $fonoFijo, $fonoCelular, $idTipoConsulta, $idSintoma, $codigoPrevision, $tipoPrevision, $idMedico)
	{
		$tipoFlujo = $this->TransformTipoFlujo($tipoFlujo);
		return $this->oModule->DatosPersonales($tipoFlujo, $tipoIdentificacion, $idKiosco, $rut, $apellidoPaterno, $apellidoMaterno, $nombre, $fechaNacimiento, $genero, $email, $fonoFijo, $fonoCelular, $idTipoConsulta, $idSintoma, $codigoPrevision, $tipoPrevision, $idMedico);
	}
	public function VentaTransbank($monto, $idConsulta)
	{
		return $this->oModule->VentaTransbank($monto, $idConsulta);
	}
	public function AnularConsulta($tipoFlujo, $idConsulta, $idTransaccionIMED, $rut, $tipoIdentificacion,$codigoAutorizacion,$glosaFallo)
	{
		$tipoFlujo = $this->TransformTipoFlujo($tipoFlujo);
		return $this->oModule->AnularConsulta($tipoFlujo, $idConsulta, $idTransaccionIMED, $rut, $tipoIdentificacion,$codigoAutorizacion,$glosaFallo);
	}
	public function Finalizar(
		$tipoFlujo,
		$idKiosco,  $rut, $apellidoPaterno, $apellidoMaterno, $nombre, $fechaNacimiento,
		$genero, $email, $fonoFijo, $fonoCelular, $idTipoConsulta, $idSintoma, $codigoPrevision, $nombrePrevision, 
		$tipoPrevision, $idMedico, $monto,$formaPago, $idConsulta, $nombreTipoConsulta, $idTransaccionIMED, $numOperacion,
		$solicitudSMS,$nombreSucursal, $direccionSucursal, $tipoIdentificacion, $nombreMedico, $rutMedico)
	{
		$tipoFlujo = $this->TransformTipoFlujo($tipoFlujo);
		
		return $this->oModule->Finalizar(
		$tipoFlujo,
		$idKiosco,  $rut, $apellidoPaterno, $apellidoMaterno, $nombre, $fechaNacimiento,
		$genero, $email, $fonoFijo, $fonoCelular, $idTipoConsulta, $idSintoma, $codigoPrevision, $nombrePrevision, 
		$tipoPrevision, $idMedico, $monto,$formaPago, $idConsulta, $nombreTipoConsulta, $idTransaccionIMED, $numOperacion,
		$solicitudSMS,$nombreSucursal, $direccionSucursal, $tipoIdentificacion, $nombreMedico, $rutMedico);
	}
	
	public function FatalError($errno, $errstr, $errfile, $errline) {
		return $this->oModule->FatalError($errno, $errstr, $errfile, $errline);
	}
	
	public function Logger($message){
		$day = date('Ymd');
		$gestor = fopen("C:/logs/flex/$day.log", 'a+');
		$time = date("Y-m-d H:i:s");
		fwrite($gestor, "[$time] $message");
		fwrite($gestor, chr(10));
		fclose($gestor);
	}
	
	public function __destruct(){
		unset($this);
	}
}

global $oMain;
$oMain = new MiniclinicConnector();

?>
