<?php
define('DIR','C:/logs');
define('FILE_PREFIX', 'php_');
define('LEVEL', 'INFO');

class Logger {
	
	static private $instance = null;
	static private $debugEnabled = false;
	static private $infoEnabled = false;
	static private $warnEnabled = false;
	static private $errorEnabled = false;
	
	private function __construct(){
		switch (LEVEL){
			case 'DEBUG':
				self::$debugEnabled = true;
				self::$infoEnabled = true;
				self::$warnEnabled = true;
				self::$errorEnabled = true;
				break;
			case 'INFO':
				self::$infoEnabled = true;
				self::$warnEnabled = true;
				self::$errorEnabled = true;
				break;
			case 'WARN':
				self::$warnEnabled = true;
				self::$errorEnabled = true;
				break;
			case 'ERROR':
				self::$errorEnabled = true;
				break;
		}
	}
	
	public static function getInstance(){
		if(self::$instance == null){
			self::$instance = new Logger();
		}
		return self::$instance;
	}
	
	public static function getLogger(){
		return self::getInstance();
	}
	
	private function write($level, $message){
		$backtrace = debug_backtrace();
		$cache = $backtrace[1]['file'];
		$file = substr($cache, strrpos($cache, '\\')+1, strlen($cache)-1);
		$line = $backtrace[1]['line'];
		$today = date('Y-m-d');
		$time = date("Y-m-d H:i:s");
		$link = fopen( DIR.'/'.FILE_PREFIX.$today.'.log', 'a+');
		fwrite($link, "[$time][$level] $file:$line $message");
		fwrite($link, chr(10));
		fclose($link);
	}
	
	public function debug($message){
		if (self::$debugEnabled) {
			$this->write('DEBUG', $message);
		}
	}
	
	public function info($message){
		if (self::$infoEnabled) {
			$this->write('INFO', $message);
		}
	}
	
	public function warn($message){
		if (self::$warnEnabled) {
			$this->write('WARNING', $message);
		}
	}
	
	public function error($message){
		if (self::$errorEnabled) {
			$this->write('ERROR', $message);
		}
	}
	
	public function fatal($message){
		$this->write('FATAL', $message);
		exit();
	}
}
?>
