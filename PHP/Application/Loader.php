<?php
setlocale(LC_ALL,'es_ES.ISO-8859-1');
date_default_timezone_set('America/Santiago');
define('_DBTYPE','postgres7');
define('_APPDIR',dirname(__FILE__).'/');
define('_APPCOREDIR',_APPDIR.'Core/');
define('_APPGLOBALDIR',_APPDIR.'Global/');
define('_APPMODULEDIR',_APPDIR.'Module/');
define('_APPCONFIGDIR',_APPDIR.'Config/');
define('_EXTERNALDIR',_APPDIR.'../External/');
define('_NAVIGATEDIR',_APPDIR.'../Navigate/');
define('_UPLOADDIR',_APPDIR.'../Upload/');
define('_APPTPLDIR',_APPDIR.'../Tpl/');
define('_APPWSCLIENTDIR',_APPDIR.'Connectivity/WClient/');
define('_APPWSSERVERDIR',_APPDIR.'Connectivity/WServer/');
define('_APPSTEREOTYPEDIR',_APPCONFIGDIR.'Stereotype/');

class Application {
	public $aConnClientFiles = array();
	public $aConnServerFiles = array();
	public $aGlobalFiles = array();
	public $aCoreFiles = array();
	public $aLibFiles = array();
	public $oEvent;
			
	function Application(){
		include_once(_EXTERNALDIR."external.loader.php");

		$this->aCoreFiles = glob(_APPCOREDIR."core.*.php");
		$this->aGlobalFiles = glob(_APPGLOBALDIR."global.*.php");
		$this->aConnClientFiles = glob(_APPWSCLIENTDIR."client.*.php");
		$this->aConnServerFiles = glob(_APPWSSERVERDIR."server.*.php");
		
		foreach ($this->aCoreFiles as $k=>$file){include_once($file);}
		foreach ($this->aGlobalFiles as $k=>$file){include_once($file);}
			
		foreach ($this->aConnClientFiles as $k=>$file){include_once($file);}
		foreach ($this->aConnServerFiles as $k=>$file){include_once($file);}
		
		$oEvent = new _Event($this);
		$this->oEvent = &$oEvent;
		$this->oEvent->fireEvent('Load',array(&$this));
		return $this;
	}
	
	function &Load (){
		static $oApp;
		if (!is_object($oApp)){
			$oApp = new Application();
		}
		return $oApp;
	}
	
}
global $oApp;
$oApp = Application::Load();

?>
