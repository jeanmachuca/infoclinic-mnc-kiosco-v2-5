<?php
class RegistrarPagoRespuesta extends WSMiniclinic { //este metodo no existe
	
	public $oTemplate;
	public $use_nusoap = true;
    public $CodigoAutorizacion = '';
    public $CodigoBono = '';
    public $FechaTransaccion = '';
    public $IdConsulta = '';
    public $MontoPagado = '';
    public $NumeroTransaccion = '';
    public $RutCliente = '';
    public $TipoPrevision = '';
	
	public $wsdl = "http://miniclinic.azul.gl/miniclinic/services/KioscoWS?wsdl";
	public $soap_func = "RegistrarPago";
	
	
	public function RegistrarPagoRespuesta(){
		
        parent::WSMiniclinic($this->wsdl);
	}
	
	public function fnLoad(){
		$this->InstanceHash($_REQUEST);
		return true;
	}
	
	public function fnPostRequest(){

	    $this->call(array('paramPago'=> 
					array('codigoAutorizacion' => $this->codigoAutorizacion,
						  'codigoBono' => $this->codigoBono,     
						  'fechaTransaccion' => $this->fechaTransaccion,
						  'idConsulta' => $this->idConsulta,
						  'montoPagado' => $this->montoPagado,
						  'numTransaccion' => $this->numeroTransaccion,
						  'rutCliente' => $this->rutCliente,
						  'tipoPrevision' => $this->tipoPrevision
					)));
			
		if ($this->result->return->CodResult == "F"){
			$this->_oEvent->fireEvent('RaiseError',$this->result->return->MsgResult);
		}
	    return true;
	}
	
	public function __destruct(){
		unset($this);
	}
}