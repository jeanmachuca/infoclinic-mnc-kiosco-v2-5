<?php

include_once(dirname(__FILE__).'/'.'../../Loader.php');

define ('INICIALIZACION','INICIALIZACION');
define ('VENTA','VENTA');
define ('ULTIMAVENTA','ULTIMAVENTA');
define ('ANULACION','ANULACION');
define ('CIERRE','CIERRE');
define ('LLAVES','LLAVES');
define ('PRTSTATUS','PRTSTATUS');
define ('PRTFILE','PRTFILE');
define ('TRANSBANKDIR','/VX700/');
define ('ERROR_TRX_FILE',_APPWSSERVERDIR.'/logTRX.txt');

class PagoTransbank extends Main {
	
	public $oTemplate;
	
	public $IdTransaccion = '';
	public $comando = 'PPVX700.exe';
	public $messageFile = "estado.txt";
	public $cachedMessageFile = "cached_estado.txt";
	public $paramString = '';
	public $operacion = '';
	
	/**  return properties **/
	public $CodResult = '';
	public $MsgResulResult = '';
	
	public $result;
	
	public function PagoTransbank($idTransaccion,$operacion){
        $this->IdTransaccion = $idTransaccion;
        $this->operacion = $operacion;
		$this->onRender=create_function(null,'return false;');
        parent::Main();
	}
	
	public function call(){
		$args = func_get_args();
		$this->paramString = implode($args,' ');
		$comando = TRANSBANKDIR.$this->comando.' '.$this->operacion.' '.$this->paramString;
		
		PagoTransbank::logMsg('Ejecutando Comando: '.$comando);
	
		$this->result = @shell_exec($comando);
		
		PagoTransbank::logMsg('Respuesta Comando Ejecutado: '.$this->result);
		
	    return true;
	}

	public function GetCachedMessage(){
		$file = TRANSBANKDIR.$this->cachedMessageFile;
		$content = '';
		PagoTransbank::logMsg('Recuperando mensaje de archivo: '.$file);
		if(!file_exists($file)){
			PagoTransbank::logMsg('El archivo no existe: '.$file);
			return '0=OK'."\n".'500=No pudo leer el archivo de estado'."\n";
		}
		$handler = @fopen($file,'r');
		if ($handler){
			
			PagoTransbank::logMsg('Archivo: '.$file.' abierto correctamente');
			$content = @fread($handler,@filesize($file)-1);
			PagoTransbank::logMsg('Contenido recuperado: '.$content);
			@fclose($handler);			
		} else {
			PagoTransbank::logMsg('El archivo existe pero no se pudo leer: '.$file);
			return '0=OK'."\n".'500=No pudo leer el archivo de estado'."\n";
		}
		PagoTransbank::logMsg('Fin lectura de archivo cache');
		return $content;
	}

	
	public function GetMessage(){
		$file = TRANSBANKDIR.$this->messageFile;
		$cachedFile = TRANSBANKDIR.$this->cachedMessageFile;
		$content = '';
		PagoTransbank::logMsg('Recuperando mensaje de archivo: '.$file);
		if(!file_exists($file)){
			PagoTransbank::logMsg('El archivo no existe: '.$file);
			return '0=OK'."\n".'500=No pudo leer el archivo de estado'."\n";
		}
		$handler = @fopen($file,'r');
		if ($handler){
			
			PagoTransbank::logMsg('Archivo: '.$file.' abierto correctamente');
			$content = @fread($handler,@filesize($file)-1);
			PagoTransbank::logMsg('Contenido recuperado: '.$content);
			@fclose($handler);			
		} else {
			PagoTransbank::logMsg('El archivo existe pero no se pudo leer: '.$file);
			return '0=OK'."\n".'500=No pudo leer el archivo de estado'."\n";
		}
		PagoTransbank::logMsg('Fin lectura de archivo ');
		
		$fp = @fopen($cachedFile,"w");
		@fwrite($fp, $content);
		@fclose($fp);

		return $content;
	}
	
	function logMsg($msg){
		@error_log($msg."\n",3,ERROR_TRX_FILE);
	}	
	
	public function __destruct(){
		unset($this);
	}
}
?>