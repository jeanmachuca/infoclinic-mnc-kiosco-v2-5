<?php
require_once 'client.WSMiniclinic.php';

class IniciarDia extends WSMiniclinic {

	public $idKiosco;
	public $UrlExito = 'http://localhost/PagoIMEDExito.php';
	public $UrlFracaso = 'http://localhost/PagoIMEDError.php';
	
	public function IniciarDia( $idKiosco,$UrlExito,$UrlFracaso){
		$this->UrlExito = $UrlExito;
		$this->UrlFracaso = $UrlFracaso;
		$this->idKiosco = $idKiosco;

		$this->call_params =
        array
		(
		  'idKiosco'=> $this->idKiosco,
		  'urlExito'=> $this->UrlExito,
		  'urlFracaso'=>$this->UrlFracaso
		);
        parent::WSMiniclinic('IniciarDia');
	}

	public function fnLoad()
	{
		$this->call();
//		if($this->hasError)
//		{
//		   //TODO in Error Case
//		}
//		else if($this->hasFault)
//		{
//		   //TODO in Fault Case
//		}
//		else if($this->result->return->CodResult == "F")
//		{
//		   $this->_oEvent->fireEvent('RaiseError',$this->result->Respuesta->MsgResult);
//		}
		return true;
	}
	
	public function __destruct(){
		unset($this);
	}
}