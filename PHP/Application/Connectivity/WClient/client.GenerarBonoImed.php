<?php
class GenerarBonoImed extends WSMiniclinic {
	
	public $oTemplate;
	public $use_nusoap = true;
	
	/** attributes recived by param **/
	public $pagoTransbank;
	public $idConsulta;
	public $idKiosco;
	public $numTransaccion;
	public $numOperacion;
	public $codAutorizacion;
	public $monto;

	public function GenerarBonoImed($params){
        
        $this->pagoTransbank 	= $params['pagoTransbank'];
        $this->idConsulta 		= $params['idConsulta'];
        $this->idKiosco 	  	= $params['idKiosco'];
        $this->numTransaccion 	= $params['numTransaccion'];
        $this->numOperacion 	= $params['numOperacion'];
        $this->codAutorizacion	= $params['codAutorizacion'];
        $this->monto			= $params['monto'];
        
        $this->call_params = array(
			'pagoTransbank'    => $this->pagoTransbank,
			'idConsulta' 	   => $this->idConsulta,
			'idKiosco'    	   => $this->idKiosco,
        	'numTransaccion'   => $this->numTransaccion,
        	'numOperacion'	   => $this->numOperacion,
        	'codAutorizacion'  => $this->codAutorizacion,
        	'monto' 		   => $this->monto
		);
		
		parent::WSMiniclinic("GenerarBonoImed");
	}
}
?>