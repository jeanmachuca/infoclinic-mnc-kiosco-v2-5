<?php
require_once 'client.WSMiniclinic.php';

class ObtenerMedicoHora extends WSMiniclinic
{
    public $oTemplate;
	
	public $idKiosco = '';
	public $TipoConsulta = '';
	public $tipoFlujo = '';
	
	public function ObtenerMedicoHora($tipoFlujo, $idKiosco, $tipoConsulta){
	    //$this->IdKiosco = $this->oConfig->getValue('Kiosco->Id');
	    $this->idKiosco = $idKiosco;
        $this->TipoConsulta = $tipoConsulta;
        $this->tipoFlujo = $tipoFlujo;
		$this->call_params = array(
//				'tipoFlujo' => $this->tipoFlujo,
				'idKiosco' => $this->idKiosco, 
				'tipoConsulta' => $this->TipoConsulta	
				);
		
        parent::WSMiniclinic('ObtenerMedicoHora');
	}
	
	public function fnLoad(){
		$this->call();
		if ($this->result->CodResult == "F"){
			$this->_oEvent->fireEvent('RaiseError',$this->result->MsgResult);
		}
	    return true;
	}
	
	public function __destruct(){
		unset($this);
	}

}
?>