<?php
class ObtenerDatosPersonalesRespuesta extends WSMiniclinic {
	
	public $oTemplate;
	public $use_nusoap = true;
    public $rutCliente = '';
	
	public $wsdl = "http://miniclinic.azul.gl/miniclinic/services/KioscoWS?wsdl";
	public $soap_func = "ObtenerDatosPersonales";
	
	public function RegistrarPagoRespuesta(){
		
        parent::ws_object($this->wsdl);
	}
	
	public function fnLoad(){
		$this->InstanceHash($_REQUEST);
		return true;
	}
	
	public function fnPostRequest(){

	    $this->call(array('rutCliente' => $this->rutCliente));
			
		if ($this->result->return->CodResult == "F"){
			$this->_oEvent->fireEvent('RaiseError',$this->result->return->MsgResult);
		}
	    return true;
	}
	
	public function __destruct(){
		unset($this);
	}
}