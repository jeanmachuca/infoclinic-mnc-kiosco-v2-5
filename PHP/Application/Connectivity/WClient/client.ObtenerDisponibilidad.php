<?php
require_once 'client.WSMiniclinic.php';
 
class ObtenerDisponibilidad extends WSMiniclinic {
	
	public $oTemplate;
	public $MinutosEstimados = 'No data';
	public $debug = false;
	
	public function ObtenerDisponibilidad($idKiosco){
	
		$this->soap_func   = 'ObtenerDisponibilidad';
		$this->debug = false;
		$this->call_params = array('idKiosco' => $idKiosco);
		parent::WSMiniclinic($this->soap_func);
	}

	public function fnLoad(){
	    $this->call();
		if($this->hasError)
		{
		   //TODO in Error Case
		}
		else if($this->hasFault)
		{
		   //TODO in Fault Case
		}
		else
		{
		  $this->MinutosEstimados = 
				"<xml>
				<resultado>1</resultado>
				<tiempo>".$this->result['Respuesta']['rangoEspera']."</tiempo>
				</xml>";
		}
		return true;
	}
	
	public function __destruct(){
		unset($this);
	}
}