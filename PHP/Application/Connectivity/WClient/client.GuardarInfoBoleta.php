<?php 
require_once 'client.WSMiniclinic.php';

class GuardarInfoBoleta extends WSMiniclinic {
	
	public $oTemplate;
	public $use_nusoap = true;

	/*Datos de boleta*/
	public $emisionDate;
	public $rutEmisor;
	public $razonSocialEmisor;
	public $giroEmisor;
	public $codigoSucursalSII;
	public $dirOrigen;
	public $comunaOrigen;
	public $ciudadOrigen;
	public $rutReceptor;
	public $razonSocialReceptor;
	public $direccionReceptor;
	public $montoExento;
	public $montoTotal;
	public $montoItem;
	public $nombreItemDetalle;
	public $descripcionItemDetalle;
	public $tipoCodigo;
	public $valorCodigo;
	public $idKiosco;
	public $qtyItem;
	public $unmdItem;
	public $nombreMedico;
	public $rutMedico;
	public $especialidadMedico;
	public $previsionPaciente;
	public $formaPago;
	public $codigoPrevision;
	public $horaPago;
	public $folio;
	public $pdf417;
	public $resultText;
	/*Datos de venta*/
	public $numTransaccion;
	public $codFinanciador;
	public $rutConvenio;
	public $corrConvenio;
	public $rutTratante;
	public $rutSolicitante;
	public $nomSolic;
	public $codEspecialidad;
	public $rutBenef;
	public $urgencia;
	/*Forma de pago*/
	public $codForPag;
	public $numDoc;
	public $codInst;
	public $codEmi;
	public $emiTarBan;
	public $codAuto;
	public $mtoTransac;


	public function GuardarInfoBoleta($paramsDocumento){
		
		/*Datos de boleta*/
		$this->emisionDate 				= $paramsDocumento['emisionDate'];
		$this->rutEmisor        		= $paramsDocumento['rutEmisor'];
		$this->razonSocialEmisor    	= $paramsDocumento['razonSocialEmisor'];
		$this->giroEmisor             	= $paramsDocumento['giroEmisor'];
		$this->codigoSucursalSII        = $paramsDocumento['codigoSucursalSII'];
		$this->dirOrigen 		  		= $paramsDocumento['dirOrigen'];
		$this->comunaOrigen           	= $paramsDocumento['comunaOrigen'];
		$this->ciudadOrigen  			= $paramsDocumento['ciudadOrigen'];
		$this->rutReceptor	  			= $paramsDocumento['rutReceptor'];
		$this->razonSocialReceptor    	= $paramsDocumento['razonSocialReceptor'];
		$this->direccionReceptor      	= $paramsDocumento['direccionReceptor'];
		$this->montoExento           	= $paramsDocumento['montoExento'];
		$this->montoTotal 		  		= $paramsDocumento['montoTotal'];
		$this->MontoItem 		  		= $paramsDocumento['montoItem'];
		$this->nombreItemDetalle 		= $paramsDocumento['nombreItemDetalle'];
		$this->descripcionItemDetalle 	= $paramsDocumento['descripcionItemDetalle'];
		$this->tipoCodigo 		  		= $paramsDocumento['tipoCodigo'];		
		$this->valorCodigo 		  		= $paramsDocumento['valorCodigo'];		
		$this->idKiosco 		  		= $paramsDocumento['idKiosco'];
		$this->qtyItem 		  			= $paramsDocumento['qtyItem'];
		$this->unmdItem 		  		= $paramsDocumento['unmdItem'];
		$this->nombreMedico 			= $paramsDocumento['nombreMedico'];
		$this->rutMedico	 			= $paramsDocumento['rutMedico'];
		$this->especialidadMedico 		= $paramsDocumento['especialidadMedico'];
		$this->previsionPaciente 		= $paramsDocumento['previsionPaciente'];
		$this->formaPago 				= ($paramsDocumento['formaPago']=='0')?('1'):('0');
		$this->codigoPrevision			= $paramsDocumento['codigoPrevision'];
		$this->horaPago					= $paramsDocumento['horaPago'];
		$this->folio					= $paramsDocumento['folio'];
		$this->pdf417					= $paramsDocumento['pdf417'];
		$this->resultText				= $paramsDocumento['resultText'];
		/*Datos de venta*/
		$this->numTransaccion			= $paramsDocumento['numTransaccion'];
		$this->codFinanciador			= $paramsDocumento['codFinanciador'];
		$this->rutConvenio				= $paramsDocumento['rutConvenio'];
		$this->corrConvenio				= $paramsDocumento['corrConvenio'];
		$this->rutTratante				= $paramsDocumento['rutTratante'];
		$this->rutSolicitante			= $paramsDocumento['rutSolicitante'];
		$this->nomSolic					= $paramsDocumento['nomSolic'];
		$this->codEspecialidad			= $paramsDocumento['codEspecialidad'];
		$this->rutBenef					= $paramsDocumento['rutBenef'];
		$this->urgencia					= $paramsDocumento['urgencia'];
	    /*Forma de pago*/
		$this->codForPag				= $paramsDocumento['codForPag'];
		$this->numDoc					= $paramsDocumento['numDoc'];
		$this->codInst					= $paramsDocumento['codInst'];
		$this->codEmi					= $paramsDocumento['codEmi'];
		$this->emiTarBan				= $paramsDocumento['emiTarBan'];
		$this->codAuto					= $paramsDocumento['codAuto'];
		$this->mtoTransac				= $paramsDocumento['mtoTransac'];

		$this->call_params = 

	
		array('informarERP' =>

		array('emisionDate' 			=> $this->emisionDate,
			  'rutEmisor' 			   	=> $this->rutEmisor,
			  'razonSocialEmisor'    	=> $this->razonSocialEmisor,
			  'giroEmisor' 			   	=> $this->giroEmisor,
			  'codigoSucursalSII' 		=> $this->codigoSucursalSII,
			  'dirOrigen'        		=> $this->dirOrigen,
			  'comunaOrigen'           	=> $this->comunaOrigen,
			  'ciudadOrigen' 			=> $this->ciudadOrigen,
			  'rutReceptor'	   			=> $this->rutReceptor,
			  'razonSocialReceptor'    	=> $this->razonSocialReceptor,
			  'direccionReceptor'      	=> $this->direccionReceptor,
			  'montoExento'           	=> $this->montoExento,
			  'montoTotal'    			=> $this->montoTotal,
			  'nombreItemDetalle'       => $this->nombreItemDetalle,     
			  'descripcionItemDetalle'  => $this->descripcionItemDetalle,
			  'descripcionItemDetalle'  => $this->descripcionItemDetalle,
			  'tipoCodigo'				=> $this->tipoCodigo,
			  'valorCodigo'				=> $this->valorCodigo,
			  'idKiosco'	   			=> $this->idKiosco,
			  'qtyItem'	   				=> $this->qtyItem,
			  'unmdItem'	   			=> $this->unmdItem,
			  'nombreMedico'			=> $this->nombreMedico,
			  'rutMedico'				=> $this->rutMedico,
			  'especialidadMedico'		=> $this->especialidadMedico,
			  'previsionPaciente'		=> $this->previsionPaciente,
			  'formaPago'				=> $this->formaPago,
			  'codigoPrevision'			=> $this->codigoPrevision,
			  'horaPago'				=> $this->horaPago,	 
			  'folio'					=> $this->folio,	
			  'pdf417'					=> $this->pdf417,	
			  'resultText'				=> $this->resultText,
			   /*Datos de venta*/
			  'numTransaccion'			=> $this->numTransaccion,	
			  'codFinanciador'			=> $this->codFinanciador,	
			  'rutConvenio'				=> $this->rutConvenio,	
			  'corrConvenio'			=> $this->corrConvenio,	
			  'rutTratante'				=> $this->rutTratante,	
			  'rutSolicitante'			=> $this->rutSolicitante,	
			  'nomSolic'				=> $this->nomSolic,	
			  'codEspecialidad'			=> $this->codEspecialidad,	
			  'rutBenef'				=> $this->rutBenef,
			  'urgencia'				=> $this->urgencia,	
			 /*Forma de pago*/
			  'codForPag'				=> $this->codForPag,	
			  'numDoc'					=> $this->numDoc,	
			  'codInst'					=> $this->codInst,
			  'codEmi'					=> $this->codEmi,	
			  'emiTarBan'				=> $this->emiTarBan,
			  'codAuto'					=> $this->codAuto,
			  'mtoTransac'				=> $this->mtoTransac,
				  
        )
	
		);
       

	    parent::WSMiniclinic('GuardarInfoBoleta');
        
   	}
	
	public function __destruct(){
		unset($this);
	}
}