<?php


define ('SIGNATURE','/SIGNATURE/');
define ('ERROR_SIG_FILE',_APPWSSERVERDIR.'/logSignature.txt');

class SignatureConnect extends Main {
	
	public $oTemplate;
	
	public $IdTransaccion = '';
	public $comando = 'signature.jar';
	public $messageFile = "estado.txt";
	public $paramString = '';
	public $operacion = '';
	
	/**  return properties **/
	public $CodResult = '';
	public $MsgResulResult = '';
	
	public $result;
	
	public function SignatureConnect($idTransaccion){
        $this->IdTransaccion = $idTransaccion;

        parent::Main();
	}
	
	public function call(){
		$args = func_get_args();
		$this->paramString = implode($args,' ');
		$comando = SIGNATURE.$this->comando.' '.$this->operacion.' '.$this->paramString;
		
		SignatureConnect::logMsg('Ejecutando Comando: '.$comando);
	
		$this->result = @shell_exec($comando);
		
		SignatureConnect::logMsg('Comando Ejecutado: '.$this->result);
		
	    return true;
	}
	
	public function GetMessage(){
		$file = SIGNATURE.$this->messageFile;
		$content = '';
		SignatureConnect::logMsg('Recuperando mensaje de archivo: '.$file);
		if(!file_exists($file)){
			SignatureConnect::logMsg('El archivo no existe: '.$file);
			return '500';
		}
		$handler = @fopen($file,'r');
		if ($handler){
			
			SignatureConnect::logMsg('Archivo: '.$file.' abierto correctamente');
			$content = @fread($handler,@filesize($file)-1);
			SignatureConnect::logMsg('Contenido recuperado: '.$content);
			@fclose($handler);			
		} else {
			SignatureConnect::logMsg('El archivo existe pero no se pudo leer: '.$file);
			return '500';			
		}
		SignatureConnect::logMsg('Fin lectura de archivo ');
		return $content;
	}
	
	function logMsg($msg){
		@error_log($msg."\n",3,ERROR_SIG_FILE);
	}	
	
	public function __destruct(){
		unset($this);
	}
}
?>