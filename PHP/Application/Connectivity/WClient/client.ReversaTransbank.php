<?php
class ReversaTransbank extends WSMiniclinic {
	
	public $oTemplate;
	public $use_nusoap = true;
	
	public $IdConsulta = '';
	public $IdTransaccionTransbank = '';
	
	/**  return properties **/
	public $CodResult = '';
	public $MsgResulResult = '';
	
	public $wsdl = "http://miniclinic.azul.gl/miniclinic/services/KioscoWS?wsdl";
	public $soap_func = "ReversaTransbank";
	
	public function ReversaTransbank($idConsulta,$idTransaccionTransbank){
	    $this->IdConsulta = $idConsulta;
        $this->IdTransaccionTransbank = $idTransaccionTransbank;
        parent::WSMiniclinic($this->wsdl);
	}
	
	public function fnPostRequest(){

	    $this->call($this->soap_func,
		             array('idConsulta'=> $this->IdConsulta,
						   'idTransaccionTransbank'=> $this->IdTransaccionTransbank
					));
		if ($this->result->CodResult == "F"){
			$this->_oEvent->fireEvent('RaiseError',$this->result->MsgResult);
		}
	    return true;
	}
	
	public function __destruct(){
		unset($this);
	}
}
?>