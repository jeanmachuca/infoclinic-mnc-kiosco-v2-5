<?php
require_once 'client.WSMiniclinic.php';

class ConfirmarConsulta extends WSMiniclinic {
	
	public $oTemplate;
	public $use_nusoap = true;
	
	public $Confirmacion = 0;
	public $CodigoAutorizacion = '';
	public $FechaTransaccion = '';
	public $IdConsulta = '';
	public $Identificacion = '';
	public $TipoIdentificacion = '';
	public $GlosaFallo = '';
	public $solicitudSMS = '';
	public $IdTransaccionIMed;
	/**  TransBank Data Unsupport yet **/
	
	public $CodResult = '';
	public $MsgResult = '';
	public $NumeroBoleta = '';
	public $UrlBoleta = '';
	public $tipoFlujo = '';
	
	public function ConfirmarConsulta( $paramsConsulta ){
		$this->tipoFlujo		  = $paramsConsulta['tipoFlujo'];
		$this->Confirmacion       = $paramsConsulta['confirmacion'];
	    $this->CodigoAutorizacion = $paramsConsulta['codigoAutorizacion'];
	    $this->FechaTransaccion   = $paramsConsulta['fechaTransaccion'];
	    $this->IdConsulta 		  = $paramsConsulta['idConsulta'];
	    $this->IdTransaccionIMed  =	$paramsConsulta['idTransaccionIMed'];
	    $this->Identificacion 	  = $paramsConsulta['identificacion'];
	    $this->TipoIdentificacion = $paramsConsulta['tipoIdentificacion'];
	    $this->GlosaFallo         = $paramsConsulta['glosaFallo'];
	    $this->solicitudSMS		  = $paramsConsulta['solicitudSMS'];

		$this->call_params        = 
		array(	'paramPago'=>
		array(	'tipoFlujo' 		 => $this->tipoFlujo,
				'confirmacion'		 => $this->Confirmacion,
				'codigoAutorizacion' => $this->CodigoAutorizacion,
				'fechaTransaccion'   => $this->FechaTransaccion,
				'idConsulta'		 => $this->IdConsulta,
				'idTransaccionIMed'  => $this->IdTransaccionIMed,
				'identificacion'	 => $this->Identificacion,
				'tipoIdentificacion' => $this->TipoIdentificacion,
				'glosaFallo'		 => $this->GlosaFallo,
				'solicitudSMS'		 => $this->solicitudSMS,
				)
		);
		
		parent::WSMiniclinic('ConfirmarConsulta');
	}

	public function fnLoad()
	{
	   
		if($this->hasError)
		{
		   //TODO in Error Case
		}
		else if($this->hasFault)
		{
		   //TODO in Fault Case
		}
		else if($this->result->return->CodResult == "F")
		{
		   $this->_oEvent->fireEvent('RaiseError',$this->result->return->MsgResult);
		}
		else
		{
		  //Set all atributtes for result		  
		}
		return true;
	}
	
	public function fnPostRequest(){

	    
		if ($this->result->CodResult == "F"){
			$this->_oEvent->fireEvent('RaiseError',$this->result->MsgResult);
		}
	    return true;
	}

	public function __destruct(){
		unset($this);
	}
}