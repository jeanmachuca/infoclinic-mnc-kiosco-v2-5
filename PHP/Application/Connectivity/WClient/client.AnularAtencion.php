<?php
require_once 'client.WSMiniclinic.php';

class AnularAtencion extends WSMiniclinic {
	
	public $oTemplate;
	public $IdConsulta = '';
	
	public $CodResult = '';
	public $MsgResult = '';
	public $Resultado;
	
	public function AnularAtencion($idConsulta){
	    $this->IdConsulta = $idConsulta;
		$this->call_params = array('idCliente' => $this->IdConsulta);
		parent::WSMiniclinic('AnularAtencion');
	}
	
	public function fnLoad(){
	    $this->call();
		if($this->hasError)
		{
		   //TODO in Error Case
		}
		else if($this->hasFault)
		{
		   //TODO in Fault Case
		}
		else
		{
		  $this->CodResult = 1; //$this->result['minutosEstimados'];
		}
		return true;
	}
	
	public function fnPostRequest(){

	    $this->call();
		if($this->hasError)
		{
		   //TODO in Error Case
		}
		else if($this->hasFault)
		{
		   //TODO in Fault Case
		}
		else if($this->result->return->CodResult == "F")
		{
		   $this->_oEvent->fireEvent('RaiseError',$this->result->return->MsgResult);
		}
		else
		{
		  //This Response is successful
		}	
	    return true;
	}
	
	public function __destruct(){
		unset($this);
	}
}
