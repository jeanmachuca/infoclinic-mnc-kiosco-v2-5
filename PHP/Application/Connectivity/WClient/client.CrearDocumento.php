<?php 
require_once 'client.WSSignature.php';
class CrearDocumento extends WSSignature {
	
	public $oTemplate;
	public $use_nusoap = true;
	
	public $TipoDTE;
	public $EmisionDate = '';
	public $IndServicio = '';
    public $Folio = '';
	public $RutEmisor = '';
	public $RznSocEmisor = '';
	public $GiroEmisor = '';
	public $codigoSucursalSII = '';
	public $DirOrigen = '';
	public $CmnaOrigen = '';
	public $CiudadOrigen = '';
	public $RutRecep = '';
	public $RznSocRecep = '';
	public $DirRecep = '';
	public $MntNeto;
	public $MntExe;
	public $Iva;
	public $MntTotal;
	public $NroLinDet = '';
    public $TipoCodigo = '';
	public $VlrCodigo = '';
	public $DscItem = '';
	public $QtyItem = '';
	public $UnmdItem = '';
	public $MontoItem = '';
	public $NmbItem= '';
	public $descripcionItemDetalle= '';
	public $idKiosco = '';
	public $NroLinRef = '';
	public $PdfType = '';
	public $PosKey = '';
	public $PosName = '';

	//campos personalizados
	public $nombreMedico = '';
	public $rutMedico = '';
	public $especialidadMedico = '';
	public $previsionPaciente = '';
	public $formaPago  = '';
	public $horaPago  = '';
	
	public function CrearDocumento($paramsDocumento){
	
		$this->EmisionDate 				= $paramsDocumento['emisionDate'];
		$this->RutEmisor        		= $paramsDocumento['rutEmisor'];
		$this->RznSocEmisor    			= $paramsDocumento['rznSocEmisor'];
		$this->GiroEmisor             	= $paramsDocumento['giroEmisor'];
		$this->codigoSucursalSII        = $paramsDocumento['codigoSucursalSII'];
		$this->DirOrigen 		  		= $paramsDocumento['dirOrigen'];
		$this->CmnaOrigen           	= $paramsDocumento['cmnaOrigen'];
		$this->CiudadOrigen  			= $paramsDocumento['ciudadOrigen'];
		$this->RutRecep	  				= $paramsDocumento['rutRecep'];
		$this->RznSocRecep    			= $paramsDocumento['rznSocRecep'];
		$this->DirRecep      			= $paramsDocumento['dirRecep'];
		$this->MntExe           		= $paramsDocumento['mntExe'];
		$this->MntTotal 		  		= $paramsDocumento['mntTotal'];
		$this->MontoItem 		  		= $paramsDocumento['montoItem'];
		$this->NmbItem 		  			= $paramsDocumento['nmbItem'];
		$this->descripcionItemDetalle 	= $paramsDocumento['descripcionItemDetalle'];
		$this->TipoCodigo 		  		= $paramsDocumento['TipoCodigo'];		
		$this->VlrCodigo 		  		= $paramsDocumento['vlrCodigo'];		
		$this->idKiosco 		  		= $paramsDocumento['idKiosco'];
		$this->QtyItem 		  			= $paramsDocumento['qtyItem'];
		$this->UnmdItem 		  		= $paramsDocumento['unmdItem'];

		$this->nombreMedico 			= $paramsDocumento['nombreMedico'];
		$this->rutMedico	 			= $paramsDocumento['rutMedico'];
		$this->especialidadMedico 		= $paramsDocumento['especialidadMedico'];
		$this->previsionPaciente 		= $paramsDocumento['previsionPaciente'];
		$this->formaPago 				= $paramsDocumento['formaPago'];
		$this->horaPago					= $paramsDocumento['horaPago'];

		$this->call_params = 
		array('emisionDate' 			=> $this->EmisionDate,
			  'rutEmisor' 			   	=> $this->RutEmisor,
			  'razonSocialEmisor'    	=> $this->RznSocEmisor,
			  'giroEmisor' 			   	=> $this->GiroEmisor,
			  'codigoSucursalSII' 		=> $this->codigoSucursalSII,
			  'dirOrigen'        		=> $this->DirOrigen,
			  'comunaOrigen'           	=> $this->CmnaOrigen,
			  'ciudadOrigen' 			=> $this->CiudadOrigen,
			  'rutReceptor'	   			=> $this->RutRecep,
			  'razonSocialReceptor'    	=> $this->RznSocRecep,
			  'direccionReceptor'      	=> $this->DirRecep,
			  'mntExe'           		=> $this->MntExe,
			  'mntTotal'    			=> $this->MntTotal,
			  'montoItem'           	=> $this->MontoItem,     
			  'nombreItemDetalle' 		=> $this->NmbItem,
			  'descripcionItemDetalle'  => $this->descripcionItemDetalle,
			  'tpoCodigo'				=> $this->TipoCodigo,
			  'vlrCodigo'				=> $this->VlrCodigo,
			  'idKiosco'	   			=> $this->idKiosco,
			  'qtyItem'	   				=> $this->QtyItem,
			  'unmdItem'	   			=> $this->UnmdItem,
			//Campos personalizados
			  'nombreMedico'			=> $this->nombreMedico,
			  'rutMedico'				=> $this->rutMedico,
			  'especialidadMedico'		=> $this->especialidadMedico,
			  'previsionPaciente'		=> $this->previsionPaciente,
			  'formaPago'				=> $this->formaPago,
			  'horaPago'				=> $this->horaPago,	 
        );
        
       	parent::WSSignature($this->call_params);
        
   	}
	
	public function fnLoad()
	{
	    return true;
	}
	
	public function __destruct(){
		unset($this);
	}
}