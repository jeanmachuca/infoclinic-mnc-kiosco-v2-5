<?php 
require_once 'client.WSMiniclinic.php';

class RegistrarCliente extends WSMiniclinic {
	
	public $oTemplate;
	public $use_nusoap = true;
	
	public $Identificacion = '';
	public $TipoIdentificacion = '';
	public $ApellidoMaterno = '';
    public $ApellidoPaterno = '';
	public $Nombre = '';
	public $FechaNacimiento = '';
	public $Genero = '';
	public $Email = '';
	public $FonoCelular = '';
	public $FonoFijo = '';
	public $TipoConsulta = '';
	public $ListaSintomas = '';
	public $CodigoPrevision = '';
	public $TipoPrevision = '';
	public $Idkiosco = '';
	public $IdMedico = '';
	public $tipoFlujo = '';
	
	public function RegistrarCliente($clientParams) {


		$this->tipoFlujo	  	  = $clientParams['tipoFlujo'];
		$this->Identificacion     = $clientParams['identificacion'];
		$this->TipoIdentificacion = $clientParams['tipoIdentificacion'];
		$this->ApellidoPaterno    = utf8_encode($clientParams['apellidoPaterno']);
		$this->ApellidoMaterno    = utf8_encode($clientParams['apellidoMaterno']);
		$this->Nombre             = utf8_encode($clientParams['nombre']);
		$this->FechaNacimiento    = $clientParams['fechaNacimiento'];
		$this->Genero             = $clientParams['genero'];
		$this->Email 			  = utf8_encode($clientParams['email']);
		$this->FonoCelular 		  = $clientParams['fonoCelular'];
		$this->FonoFijo           = $clientParams['fonoFijo'];
		$this->ListaTipoConsulta  = $clientParams['listaTipoConsultas'];
		$this->ListaSintomas	  = $clientParams['listaSintomas'];
		$this->CodigoPrevision    = $clientParams['codigoPrevision'];
		$this->TipoPrevision      = utf8_encode($clientParams['tipoPrevision']);
		$this->IdMedico           = $clientParams['idMedico'];
		$this->IdKiosco 		  = $clientParams['idKiosco'];



		$this->call_params = 
		array('paramCliente'=>
			array('tipoFlujo'		   => $this->tipoFlujo,
				  'identificacion'     => $this->Identificacion,
				  'tipoIdentificacion' => $this->TipoIdentificacion,
				  'apellidoPaterno'    => $this->ApellidoPaterno,
				  'apellidoMaterno'    => $this->ApellidoMaterno,
				  'nombre' 			   => $this->Nombre,
				  'fechaNacimiento'    => $this->FechaNacimiento,
				  'genero' 			   => $this->Genero,
				  'email' 			   => $this->Email,
				  'fonoCelular'        => $this->FonoCelular,
				  'fonoFijo'           => $this->FonoFijo,
				  'listaTipoConsultas' => $this->ListaTipoConsulta,
				  'listaSintomas'	   => $this->ListaSintomas,
				  'codigoPrevision'    => $this->CodigoPrevision,
				  'tipoPrevision'      => $this->TipoPrevision,
				  'idKiosco'           => $this->IdKiosco,
				  'idMedico'           => $this->IdMedico
			)
		);
        
        parent::WSMiniclinic("RegistrarCliente");
	}
	
	public function fnLoad()
	{
		if ($this->result['RegistrarClienteResult']['CodResult'] == "F")
		{
			// Pa que sirve?
			//$this->_oEvent->fireEvent('RaiseError',$this->result['RegistrarClienteResult']['MsgResult']);
		}
	    return true;
	}
	
	public function __destruct(){
		unset($this);
	}
}