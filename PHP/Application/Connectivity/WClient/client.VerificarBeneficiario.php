<?php
class VerificarBeneficiario extends WSMiniclinic {
	
	public $oTemplate;
	public $use_nusoap = true;
	
	/** attributes recived by param **/
	public $Identificacion = '';
	public $TipoIdentificacion = '';
	public $CodigoPrevision = '';
	public $tipoFlujo = '';

	public function VerificarBeneficiario($tipoFlujo, $identificacion, $tipoIdentificacion, $codigoPrevision){
        
        $this->Identificacion 	  = $identificacion;
        $this->TipoIdentificacion = $tipoIdentificacion;
        $this->CodigoPrevision 	  = $codigoPrevision;
        $this->tipoFlujo 		  = $tipoFlujo;
                
        $this->call_params = 
        array(
        	   'tipoFlujo'		   => $this->tipoFlujo,
			  'identificacion' 	   => $this->Identificacion,
			  'tipoIdentificacion' => $this->TipoIdentificacion,
			  'codigoPrevision'    => $this->CodigoPrevision
		);
		
        parent::WSMiniclinic("VerificarBeneficiario");
	}
	
	public function fnLoad()
	{
		$this->call();
		/*if ($this->result->CodResult == "F"){
			$this->_oEvent->fireEvent('RaiseError',$this->result->MsgResult);
		}*/
	    return true;
	}
	
	public function __destruct(){
		unset($this);
	}
}
?>