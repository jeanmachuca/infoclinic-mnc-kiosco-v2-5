<?php
/*
 * Created on Sep 8, 2011
 *
 */
 
 class ObtenerMotivosPrecios extends WSMiniclinic{
      
	public $oTemplate;
	public $use_nusoap = true;
	public $tipoFlujo = '';
		
	public $soap_func = "ObtenerMotivosPrecios";
	
	public function ObtenerMotivosPrecios($tipoFlujo, $idKiosco){
		
		$this->tipoFlujo = $tipoFlujo;
	    $this->idKiosco = $idKiosco;    
	    
	    $this->call_params = array(
	    		'tipoFlujo' => $this->tipoFlujo,
	    		'idKiosco' => $this->idKiosco		
	    		);
        parent::WSMiniclinic($this->soap_func);
	}
	
	public function fnLoad(){
		$this->call();
		if ($this->result->CodResult == "F"){
			$this->_oEvent->fireEvent('RaiseError',$this->result->MsgResult);
		}
	    return true;
	}
	
	public function __destruct(){
		unset($this);
	}
}
?>
