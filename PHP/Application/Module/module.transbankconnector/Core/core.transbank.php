<?php

class _Transbank {
	public $oModule;
	private $logDir = "error.txt";	
	function _Transbank(&$oModule){
		$this->oModule = &$oModule;
		return $this;
	}
	
	
	function getTitle(){
		return "Miniclinic ";
	}
	
	function &byCero($value){
		$ret = new stdClass();
		$ret->upCero = ($value>0)?($value):(0);
		$ret->downCero = ($value<0)?($value):(0);
		return $ret;
	}


	/**
	 * Leer archivo de comunicación
	 * 
	 * @return string $text respuesta del archivo 
	 */
	function GetMessage($transaccion){
		$trxObject = new PagoTransbank($transaccion, VENTA);
		$content = $trxObject->GetMessage();
		$content = explode("\n",$content);
		$codigoComando = explode("=",substr($content[0], 0, -1));
		$codigoMensaje = explode("=",substr($content[1], 0, -1));
		
		$codigoComando = $codigoComando[0];
		$codigoMensaje = $codigoMensaje[0];
		
		$textoMensajeComando = $codigoComando[1];
		$textoMensaje = $codigoMensaje[1];

		PagoTransbank::logMsg('Codigo Comando: '.$codigoComando);
		PagoTransbank::logMsg('Codigo Mensaje: '.$codigoMensaje);
		PagoTransbank::logMsg('Texto Mensaje Comando: '.$textoMensajeComando);
		PagoTransbank::logMsg('Texto Mensaje: '.$textoMensaje);
		$mensajeRecibido = '';
		
		if ($codigoComando == '0'){
			$codigoRecibido = $codigoMensaje;
			$mensajeRecibido = $textoMensaje;
		} else {
			$codigoRecibido = $codigoComando;
			$mensajeRecibido = $textoMensajeComando;
		}

		$codigosMensajes = array('500','0','1','2','80','81','82','9');
		if (!in_array($codigoRecibido,$codigosMensajes)) {
			$codigoRecibido = '1';
		}

		PagoTransbank::logMsg('Codigo Recibido: '.$codigoRecibido);
		
		$resp =
		array(
				'Codigo'     => $codigoRecibido,
				'Mensaje'	 => $mensajeRecibido,
			);
				
		return $resp;
	}
	

	function Inicializacion($transaccion){
		$trxObject = new PagoTransbank($transaccion, INICIALIZACION);
		$trxObject->call($transaccion);
		return $trxObject->result;
	}
	
	function Venta ( $monto, $ticket, $generaVoucher){
		$trxObject = new PagoTransbank($ticket, VENTA);
		$trxObject->call( $monto, $ticket, $generaVoucher);
		return $trxObject->result;
	}
	
	function UltimaVenta($transaccion, $generaVoucher){
		$trxObject = new PagoTransbank($transaccion, ULTIMAVENTA);
		$trxObject->call($transaccion, $generaVoucher);
		return $trxObject->result;
	}
	
	function AnulacionUltimaVenta($transaccion){
		$trxObject = new PagoTransbank($transaccion, ANULACION);
		$trxObject->call($transaccion);
		return $trxObject->result;
	}
	
	function Cierre($transaccion, $generaVoucher){
		$trxObject = new PagoTransbank($transaccion, CIERRE);
		$trxObject->call($transaccion, $generaVoucher);
		return $trxObject->result;
	}
	
	function CargaLlaves($transaccion){
		$trxObject = new PagoTransbank($transaccion, LLAVES);
		$trxObject->call($transaccion);
		return $trxObject->result;
	}
	
	function EstadoImpresora($transaccion){
		$trxObject = new PagoTransbank($transaccion, PRTSTATUS);
		$trxObject->call($transaccion);
		$content = $trxObject->result;
		$content = explode("=",$content);
		if ($content[0] == '100'){
			$ret = true;
		} else {
			$ret = false;
		}
		return $ret;
	}
	
	function logFatalError($errno, $errstr, $errfile, $errline){
		$fp = fopen($this->logDir,"w");
		fwrite($fp, "Error: ".$errno. ": ".$errstr. " ". $errfile. " ".$errline. " " );
		fclose($fp);
	}
	function ImprimirArchivo($transaccion,$file){
		$trxObject = new PagoTransbank($transaccion, PRTFILE);
		$trxObject->call($file);
		return $trxObject->result;
	}
}
?>
