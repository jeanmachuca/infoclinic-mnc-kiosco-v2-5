<?php
/* vim: set expandtab tabstop=4 shiftwidth=4: */
/**
 * @copyright 2000-2020 Jean Machuca.
 * @author  <Jean Machuca>
 * @package Miniclinic class
 * @subpackage 
 * 
 */

include_once(dirname(__FILE__).'/'.'../../Global/global.Module.php');
include_once(dirname(__FILE__).'/'.'../../Connectivity/WClient/client.PagoTransbank.php');
include_once(dirname(__FILE__).'/'.'Core/core.transbank.php');

class MO_TransbankConnector extends Module {

	public $oTemplate;
	public $_CoreHandler;
	public $UserInfo;
	public $UserPriv;
	public $AccountTotals;
	public $Accounts;
	public $Types;
	
	public $IdTransaccion = 0;
	public $Ticket = 0;
	public $Monto  = 0;
	public $respuesta;

	/**
	 * Constructor
	 */
	public function MO_TransbankConnector(){

		$this->onLoad = array(&$this,'Prepare');
		$this->onRender=create_function(null,'return false;');
		if (isset($_REQUEST['op'])){
			$this->onAction = array(&$this,'at'.ucwords($_REQUEST['op']));
		}
		parent::Module();
	}

	/**
	 * 'onPrepare' Event dispatch method
	 * Commonly assigned as a subevent of onLoad Event
	 */
	public function Prepare(){

		//$this->onError = array(&$this,'FatalError');
		$this->_CoreHandler = new _Transbank($this);
		$this->oTemplate = $this->getTemplate();
		$this->_templateFile = $this->getTemplateFile();
		$this->assign('CoreHandler',$this->_CoreHandler);
		return true;
	}
	

	/*funciones de Transbank*/
	function atPrueba()
	{
		return "prueba";
	}
	
	
	function atGetMessage($transaccion)
	{
		$this->_CoreHandler = new _Transbank($this);
		$resp= $this->_CoreHandler->GetMessage($transaccion);
		
		$this->respuesta = array(
				'CodResult' => $resp['Codigo'],
				'MsgResult'	=> $resp['Mensaje']
				);
				
		return $this->respuesta;

	}
	
	
	
	function atInicializacion($transaccion)
	{
		$this->_CoreHandler = new _Transbank($this);
		$result = $this->_CoreHandler->Inicializacion($transaccion);
		return $result;
	}
	
	function atVenta( $monto, $transaccion, $generaVoucher) // 'S' : genera voucher
	{	
		$this->_CoreHandler = new _Transbank($this);
		$result = $this->_CoreHandler->Venta($monto, $transaccion, $generaVoucher);
		$this->respuesta = array(
				'CodResult' => 'E',
				'MsgResult'	=>	$result
				);
				
			if(empty($result))
			{
				$this->respuesta = array(
				'CodResult' => 'F',
				'MsgResult'	=> "Error en el proceso de venta"
				);
				return $this->respuesta;
			}
		
			
		return $this->respuesta;
	}


	function atUltimaVenta($transaccion, $generaVoucher)
	{
		$this->_CoreHandler = new _Transbank($this);
		$result = $this->_CoreHandler->UltimaVenta($transaccion, $generaVoucher);
		return $result;
	}

	function atAnulacionUltimaVenta($transaccion)
	{
		$this->_CoreHandler = new _Transbank($this);
		$result = $this->_CoreHandler->AnulacionUltimaVenta($transaccion);
		return $result;
	}

	function atCierre($transaccion, $generaVoucher)
	{
		$this->_CoreHandler = new _Transbank($this);
		$result = $this->_CoreHandler->Cierre($transaccion, $generaVoucher);
		return $result;
	}

	function atCargaLlaves($transaccion)
	{
		$this->_CoreHandler = new _Transbank($this);
		$result = $this->_CoreHandler->CargaLlaves($transaccion);
		return $result;
	}

	function atEstadoImpresora($transaccion)
	{
		$this->_CoreHandler = new _Transbank($this);
		$result = $this->_CoreHandler->EstadoImpresora($transaccion);
		return $result;
	}

	function atImprimirArchivo($transaccion,$file)
	{
		$this->_CoreHandler = new _Transbank($this);
		$result = $this->_CoreHandler->ImprimirArchivo($transaccion,$file);
		return $result;
	}
	

	public function FatalError($errno, $errstr, $errfile, $errline) 
	{
		$this->setTemplateFile("fatalError.html");
		parent::setDebug(false);
		$this->_CoreHandler = new _Transbank($this);
		$this->_CoreHandler->logFatalError($errno, $errstr, $errfile, $errline);
		return false;
	}
	
	public function __destruct(){
		unset($this);
	}


	
}

?>