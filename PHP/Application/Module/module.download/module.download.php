<?php
class MO_Download extends Module {
	public $oTemplate;
	public $sFile = "";
	
	public function MO_Download(){
		$this->onLoad = array(&$this,'Prepare');
		$this->onGetRequest = array(&$this,'Process');
		parent::Module();
	}

	public function Prepare(){
		return true;
	}

	public function Process(){
		$this->sFile = _UPLOADDIR.$_REQUEST['File'];
		if (strlen(realpath(dirname($this->sFile)).'/') >= strlen(realpath(_UPLOADDIR).'/')){
			$this->contentType = (isset($_REQUEST['content-type']))?($_REQUEST['content-type']):('application/force-download');
			ob_clean();
		    set_time_limit(0);
			header ("Expires: " . date("D, j M Y H:i:s", time() + (86400 * 30)) . " UTC");
			header ("Cache-Control: Public");
			header ("Pragma: Public");
			header ("Content-Type: $this->contentType");
			header ("Content-Disposition: inline; filename=".basename($this->sFile));
			header ("Content-Length: ".filesize($this->sFile));
		    if($file = fopen($this->sFile, 'rb')){
		    	while( (!feof($file)) && (connection_status()==0) ){
		        	print(fread($file, 1024*8));
		        	flush();
		     	}
		    	fclose($file);
		    }
		}
		return false;
	}

	public function __destruct(){
		unset($this);
	}
}
?>