<?php
class MO_Crypter extends Module {
	public $oTemplate;
	public $encoded = "";
	
	public function MO_Crypter(){
		$this->onLoad = array(&$this,'Prepare');
		$this->onPostRequest = array(&$this,'Process');
		parent::Module();
	}

	public function Prepare(){
		return true;
	}

	public function Process(){
		$this->encoded = _Crypt::encrypt($_REQUEST['string'],$_REQUEST['key']);
		$this->assign('encoded',$this->encoded);
		return true;
	}

	public function __destruct(){
		unset($this);
	}
}
?>