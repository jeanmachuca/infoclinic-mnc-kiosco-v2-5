<?php

class _Transbank {
	public $oModule;
	
	function _Transbank(&$oModule){
		$this->oModule = &$oModule;
		return $this;
	}
	
	
	function getTitle(){
		return "Miniclinic ";
	}
	
	function &byCero($value){
		$ret = new stdClass();
		$ret->upCero = ($value>0)?($value):(0);
		$ret->downCero = ($value<0)?($value):(0);
		return $ret;
	}

	
	function ImprimirArchivo($transaccion,$file){
		$trxObject = new PagoTransbank($transaccion, PRTFILE);
		$trxObject->call($file);
		return $trxObject->result;
	}
	
	function GetCodigoAutorizacion($transaccion){
		$trxObject = new PagoTransbank($transaccion, VENTA);
		$contentCached = $trxObject->GetCachedMessage();
		$content = $trxObject->GetMessage();
		$content = explode("\n",$content);
		$CodigoAutorizacion = substr($content[5], 0, -1);

		// Verificar que el ultimo codigo de autorizacion no sea el mismo que el anterior

		$contentCached = explode("\n",$contentCached);
		$cachedCodigoAutorizacion = substr($contentCached[5], 0, -1);
		if ($CodigoAutorizacion != $cachedCodigoAutorizacion){
			$retCodigoAutorizacion = $CodigoAutorizacion;
		} else {
			$retCodigoAutorizacion = '';
		}
		$retCodigoAutorizacion = $CodigoAutorizacion; // temporal
		return $retCodigoAutorizacion;
	}
	
	function GetTipoTarjeta($transaccion){
			$trxObject = new PagoTransbank($transaccion, VENTA);
			$contentCached = $trxObject->GetCachedMessage();
			$content = $trxObject->GetMessage();
			$content = explode("\n",$content);
			$CodigoAutorizacion = substr($content[12], 0, -1);

			return $CodigoAutorizacion;
		}


	function EstadoImpresora($transaccion){
		$trxObject = new PagoTransbank($transaccion, PRTSTATUS);
		$trxObject->call($transaccion);
		$content = $trxObject->result;
		$content = explode("=",$content);
		if ($content[0] == '100'){
			$ret = true;
		} else {
			$ret = false;
		}
		return $ret;
	}
	

}
?>
