<?php

class _IMed {
	public $oModule;
	
	function _IMed(&$oModule){
		$this->oModule = &$oModule;
		return $this;
	}
	
	
	function getTitle(){
		return "Miniclinic ";
	}
	
	function &byCero($value){
		$ret = new stdClass();
		$ret->upCero = ($value>0)?($value):(0);
		$ret->downCero = ($value<0)?($value):(0);
		return $ret;
	}
	
	/**
	 * Llama a una url predefinida, mediante un HTTP Post
	 */
	function LevantaPagoIMed(){
		
	}
	
	/**
	 * Llama mediante HttpRequest a IMed para indicar pago transbank fallido, IMed har� rollback al bono emitido
	 */
	function PagoTransbankFallido(){
		
	}
	
	/**
	 * Le otorga el control a IMed para seleccionar le medio de pago,
	 * Llama a LevantaPagoImed, que muestra datos de paciente, monto a pagar y opcion de pago
	 * 
	 * 
	 */
	function OtorgarControlIMed(){
		
	}
	
	/**
	 * Llama mediante HttpRequest a IMed para indicar pago exitoso
	 * recibe respuesta si genera bono, si lo genera debe llamar a ConfirmarConsulta
	 * si no lo genera debe llamar a ReversaManual
	 */
	function ConfirmarPagoIMed(){
		
	}
	
	

}
?>
