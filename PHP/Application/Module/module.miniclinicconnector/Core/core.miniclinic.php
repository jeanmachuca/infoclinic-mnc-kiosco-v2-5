<?php
include_once(dirname(__FILE__).'/'.'../../../Loader.php');
define ('BOLETA',_APPWSSERVERDIR.'/Boleta.txt');

class _Miniclinic {
	public $oModule;
	private $tecladoDir;         
	private $logDir;

	function _Miniclinic(&$oModule){
		$this->oModule = &$oModule;
		return $this;
	}
	
	
	function getTitle(){
		return "Miniclinic ";
	}
	
	function &byCero($value){
		$ret = new stdClass();
		$ret->upCero = ($value>0)?($value):(0);
		$ret->downCero = ($value<0)?($value):(0);
		return $ret;
	}
	

	function ImprimirArchivo($idConsulta,$archivo){
		$Transbank->_CoreHandler = new _Transbank($this->oModule);
		$Transbank->_CoreHandler->ImprimirArchivo($idConsulta,$archivo);
	}
	
	function LlenarBoleta($textString,$idConsulta){
		//$textString = implode("\n",$textString);
		//$textString = @iconv("UTF-8","ISO-8859-1", $textString);
		$fp = @fopen(str_replace('Boleta.txt', 'Boletas/Boleta_'.$idConsulta.'.txt', BOLETA),"w");
		$res = @fwrite($fp, $textString."\n");
		@fclose($fp);
		
		$fp = @fopen(BOLETA,"w");
		$res = @fwrite($fp, $textString."\n");
		@fclose($fp);
	}
	
	function ImprimirBoleta($idConsulta){
		$this->ImprimirArchivo($idConsulta,BOLETA);
	}
	                
	function MostrarTeclado()
	{
		/*Se escribe archivo le�do por aplicaci�n que levanta el teclado*/
		//C:\Program Files\Baytex\Kiosco\Miniclinic\Application\Connectivity\WServer
		$this->tecladoDir = substr( _APPWSSERVERDIR, 0, -44)."Teclado/cerrar.txt";
		$fp = fopen($this->tecladoDir,"w");
		fwrite($fp, "01");
		fclose($fp);
	}
	
	
	function OcultarTeclado()
	{
		/*Se escribe archivo leido por aplicacion que oculta el teclado*/
		$this->tecladoDir = substr( _APPWSSERVERDIR, 0, -44).'Teclado/cerrar.txt';
		$fp = fopen($this->tecladoDir,"w");
		fwrite($fp, "00");
		fclose($fp);
	}
	
	
	function logFatalError($errno, $errstr, $errfile, $errline){
		/*
		$this->logDir   = _APPWSSERVERDIR.'/ServicioNoDisponible.txt';
		$fp = @fopen($this->logDir,"w");
		@fwrite($fp, "Error: ".$errno. ": ".$errstr. " ". $errfile. " ".$errline. " " );
		@fclose($fp);
		 */
		$msg = '==Error Informado==\n N: '.$errno.'\n Detalle: '.$errstr.'\n Archivo: '.$errfile.'\n Line: '.$errline;
		@error_log(str_replace('><',">\n<",$msg)."\n",3,_APPWSSERVERDIR.date("Ymd").'_ServicioNoDisponible.log');
	}

	
	
	
}
?>
