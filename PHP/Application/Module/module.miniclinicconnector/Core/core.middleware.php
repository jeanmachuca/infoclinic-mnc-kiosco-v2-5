<?php

class _Middleware {
	public $oModule;
	
	function _Middleware(&$oModule){
		$this->oModule = &$oModule;
		return $this;
	}
	
	
	function getTitle(){
		return "Miniclinic ";
	}
	
	function &byCero($value){
		$ret = new stdClass();
		$ret->upCero = ($value>0)?($value):(0);
		$ret->downCero = ($value<0)?($value):(0);
		return $ret;
	}


	/**
	 * Llama a WSIniciarDia (middleware)
	 * 
	 * @params none, el sistema internamente lo asignara
	 * @return array complexObject Respuesta: 
	 *                                 string CodResult
	 *                                 string MsgResult
	 *                                 string nombre
	 *                                 string direccion
     *  							   array ComplexObject Prevision:
	 *	    						                           		 string nombrePrevision
	 *   													   		 string codigoPrevision
	 *     													   		 int(1) tipoPrevision (0:Particular 1:Fonasa, 2:Isapre)
	 */
	function &ConfiguracionInicial( $idKiosco,$urlExito,$urlFracaso){
		static $cacheConfiguracion;
		if (!is_array($cacheConfiguracion))
		{
			$wsObject = new IniciarDia( $idKiosco,$urlExito,$urlFracaso);
			$cacheConfiguracion = $wsObject->result['Respuesta'];
		}
		return $cacheConfiguracion;
	}
	
	/**
	 * Llama a WSObtenerDisponibilidad (middleware)
	 *
	 * @param nada, el sistema inetrnamente lo asignara
	 * 
	 * @return long minutos menor a cero en caso que no haya disponibilidad
	 */
	function ConfirmaDisponibilidad ($idKiosco){
		$wsObject = new ObtenerDisponibilidad($idKiosco);
		return $wsObject->result['Respuesta'];
	}
	
	/**
	 * Llama a WSVerificarBeneficiario (middleware)
	 * @params string  identificacion, 
	 *         int     tipoIdentificacion, por omicion 1 (el "RUT")
	 *	       string  codigoPrevision
	 * 
	 * @return ComplexObject Respuesta:
	 *                              string CodResult (E: Exito, F: Fracaso)
	 *                              string MsgResult
	 *                              ComplexObject DatosPersonales:
	 *                                                             string   identificacion (numero de rut)
	 *  														   stirng   nombres
	 *															   string   apellidoPaterno
	 *															   string   apellidoMaterno
	 *															   dateTime fechaNacimiento
	 *															   string   fonoCelular
	 *															   int      genero (0: Masculino, 1: Fememnino)
	 *															   string   email
	 *															   ComplexObject DatosCertificacionAfiliacion   --(puede venir vacico incluso siendo exitoso(no hay registro en miniclinic))
	 *															   				 							   string codigoCertificacion (C: Cerfificado distinto de C: rechazado)
	 *															   				 							   string glosaCertificacion
	 *
	 */
	function VerificaBeneficiaro($tipoFlujo, $identificacion, $tipoIdentificacion, $codigoPrevision){
		$wsObject = new VerificarBeneficiario($tipoFlujo, $identificacion, $tipoIdentificacion, $codigoPrevision);
		return $wsObject->result['Respuesta'];
	}
	
	/**
	 * Llama a WSObtenerMotivosPrecios (middleware), idKiosco es dado por la cofiguracion de la maquina
	 * 
	 * @return ComplexObject Respuesta:
	 *                                 string CodResult
	 *								   string MsgResult
	 *                                 Array de ComplexObject ListaTipoConsulta:
	 * 																			ComplexObject TipoConsulta:
	 * 																									   int    idTipoConsulta
	 *                                                                     								   string codigo
	 *                                                                     								   string nombre
	 *                                                                     								   long   valor
	 * 																									   Array ComplexObject ListaSintomas:
	 * 																																		 ComplexObject Sintoma:
	 * 																																							  int idSintoma
	 * 																																							  string nombre
	 * 																	 
     *
	 */
	function ObtenerPrecios($tipoFlujo, $idKiosco){
		$wsObject = new ObtenerMotivosPrecios($tipoFlujo, $idKiosco);
		return $wsObject->result['Respuesta'];
	}
	
	/**
	 * Llama a WSObtenerMedicoHora (middleware)
	 * 
	 * @param int tipoConsulta
	 * 
	 * @return ComplexObject result:
	 *                              string CodResult (E: Exito, F: Fracaso)
	 *                              string MsgResult
	 *                              string horaAtencion ( formato hh:mm )
	 *                              string idMedico
	 *                              string nombreMedico
	 *                              long   valor
	 *                              
	 */
	function SolicitaHoraMedica(  $tipoFlujo, $idKiosco, $idTipoConsulta){
		$wsObject = new ObtenerMedicoHora($tipoFlujo, $idKiosco, $idTipoConsulta);
		return $wsObject->result['Respuesta'];
	}
	
	/**
	 * Llama a WSRegistrarCliente (middleware)
     *
	 * @param Array $clientParams:
	 *        					  string   identificacion (numero de RUT)
     *		  					  int(1)   tipoIdentificacion (Por omicion 1:RUT)
     *		  					  string   apellidoPaterno
     *		  					  string   apellidoMaterno
     *		  					  string   nombre
     *		  					  dateTime fechaNacimiento (Formato yyyy-mm-dd)
     *		  					  long     genero  (0: masculino, 1:femenino)
     *		  					  string   email  
     *		  					  string   fonoCelular
     *		  					  long     tipoConsulta
     *		  					  string   codigoPrevision
     *		  					  long     tipoPrevision
     *		  					  long     idMedico
     *
	 * @return ComplexObject result:
	 *                              string CodResult (E: Exito, F: Fracaso)
	 *                              string MsgResult
	 *                              long   idCliente
	 *                              long   idConsulta
	 *                              long   idTransaccionIMed
	 */
	function RegistrarCliente($clientParams) {
		$wsObject = new RegistrarCliente($clientParams);
		return $wsObject->result['Respuesta'];
	}
	
	/**
	 * Llama a WSConfirmarConsulta (middleware)
	 * 
	 * @param Array $paramsConsulta:
	 *                              int(1)   confirmacion (0: no , 1: Si)
	 *         						string   codigoAutorizacion
	 *         						dateTime fechaTransaccion (en formato yyyy-mm-ddThh:mm:ss)
	 *         						long     idConsulta
	 *         						string   identificacion (Numero de Rut)
	 *         						int(1)   tipoIdentificacion (por omicion 1:Rut)
	 *         						string   glosaFallo (Descripcion mensaje fallo)
	 *         						Complex  DatosTransBank
	 *
	 * @return ComplexObject result:
	 *                              string CodResult (E: Exito, F: Fracaso)
	 *         						string MsgResult
	 *         						int    numeroBoleta (futura ibtegracion con ERP)
	 *         						string urlBoleta
	 */
	function ConfirmarConsulta( $paramsConsulta ){
		$wsObject = new ConfirmarConsulta( $paramsConsulta );
		return $wsObject->result['Respuesta'];
	}
	
	/**
	 * Llama a WSAnularAtencion(middleware)
	 * 
	 * @param long IdConsulta
	 * 
	 * @return ComplexObject result:
	 *                              string CodResult (E: Exito, F: Fracaso)
	 *         						string MsgResult
	 *  							boolean resultado
	 */
    function AnularAtencion($idConsulta){
    	$wsObject = new AnularAtencion($idConsulta);
    	return $wsObject->result;
    }
	
	/**
	 * Llama a WSReversaTransbank (middleware) para indicar solicitud de reversa 
	 * parametros id transbank, id consulta
	 * 
	 * @param long idConsulta
	 * @param long idTransaccionTransBank
	 * 
	 * @return ComplexObject result:
	 *                              string CodResult (E: Exito, F: Fracaso)
	 *                              string MsgResult
	 */
	function ReversaManual( $idConsulta, $idTransaccionTransbank ){
		$wsObject = new ReversaTransbank( $idConsulta, $idTransaccionTransbank );
		return $wsObject->result;
	}
	
     /*** Los metodos de abajo aun no estan especificado ***/
	
	/**
	 * Llama a WSConfirmaConsulta (middleware) con indicador de fallo el pago
	 */
	function InformarFalloPago(){
		
	}

    /**
	 * Llama a WSObtenerPrestacionesPrecios (middleware), con id de consulta
	 * recibe tipos y precios
	 * @param int idConsulta
	 */
	function SolicitaPrestaciones ($idConsulta){
	}
	
    /**
	 *	Llama a InformarERP (middleware), para informar al ERP una lista de datos de la 
	 *  venta y la boleta.
	 */
	function GuardarInfoBoleta ($params){
		$wsObject = new GuardarInfoBoleta($params);
		return $wsObject->result['Respuesta'];
	}

	/**
	 *	Generar Bono Imed
	 */
	function GenerarBonoImed ($params){
		$wsObject = new GenerarBonoImed($params);
		return $wsObject->result;
	}

	/**
	 * Llama a SolicitarFolio (middleware)
	 * 
	 * @param int tipoConsulta
	 * 
	 */
	function SolicitarFolio( $paramsFolio ){
		$wsObject = new CrearDocumento($paramsFolio);
		return $wsObject->result;
	}
	
}
?>
