<?php
/* vim: set expandtab tabstop=4 shiftwidth=4: */
/**
 * @copyright 2000-2020 Jean Machuca.
 * @author  <Jean Machuca>
 * @package Miniclinic class
 * @subpackage 
 * 
 */
include_once(dirname(__FILE__).'/'.'../../Core/core._Config.php');
include_once(dirname(__FILE__).'/'.'../../Global/global.Config.php');
include_once(dirname(__FILE__).'/'.'../../Global/global.Module.php');
include_once(dirname(__FILE__).'/'.'Core/core.middleware.php');
include_once(dirname(__FILE__).'/'.'Core/core.transbank.php');
include_once(dirname(__FILE__).'/'.'Core/core.miniclinic.php');
include_once(dirname(__FILE__).'/'.'../../Logger.php');

class MO_Miniclinicconnector extends Module {
	
	public $oTemplate;
	public $oSession;
	public $_CoreHandler;
	public $onCustomError;
	
	public $tituloPagina ='mini<b>clinic</b>';
	public $subTituloPagina ='';
    public $pasoAnterior = '';
	public $pasoActual = 'paso0';
	public $pasoSiguiente= 'paso1';
	
	/** field in operation **/
	public $rut;
	public $identificacion;
	public $tipoIdentificacion = 1;
	public $codigoPrevision;
	public $fechaNacimiento;
	public $fechaNacOriginal;
	public $esNuevo;
	public $apellidoPaterno;
	public $apellidoMaterno;
	public $nombre;
	public $genero;
	public $email;
	public $fonoCelular;
	public $fonoFijo;
	public $listaTipoConsulta;
	public $listaSintomas;
	public $tipoPrevision;
	public $idMedico;
	public $idTipoConsulta;
	public $nombreTipoConsulta;
	public $idSintoma;
	public $nombreSintoma;
	public $previsiones;
	public $nombrePrevision;
	public $idTransaccionIMED;
	public $solicitudSMS = '';
	public $nombreMedico;
	public $rutMedico;
	public $especialidadMedico;
	public $numeroPacientesEspera;
	public $sintomasMostrar;
	public $formaPago;
	public $preguntaSMS;
	public $preguntaActualizaciones;
	
	/*Segunda versión*/
	public $idBox;
	public $tiempoEspera;
	public $tipoFlujo; /*N: Nacional, E: Extranjero*/
	
	/** IMED **/
	public $NumTransaccion;
	public $NumOperacion;
	public $CodAutorizacion;
	public $AccionTransaccion;
	public $IdTransaccionIMED;
	public $BotonPago;
	
	/** field for Kiosco **/
	public $nombreSucursal;
	public $direccionSucursal;
	public $idKiosco;
	
	/** field for transbank **/
	public $monto;
	public $idConsulta;
	
	public $configName;
	
	public $shutDownHandler;
	
	/**
	 * Constructor
	 */
	
	public function MO_MiniclinicConnector(){
		$this->onRender=create_function(null,'return false;');
		$this->onLoad = array(&$this,'Prepare');
		parent::Module();
	}
	
	private function replaceMetaChars ($str,$upper=true) {
		return $this->quitarTilde($str);
		/*
		try {
			foreach ($str as $charISO) {
				try {
					$charUTF = iconv('ISO-8859-1', 'UTF-8', $charISO);
				}catch (Exception $e){
					$charUTF = $charISO;
				}
				$str = str_replace($char, $replace, $subject);
			}
		} catch (Exception $e){
			// 
		}
	    if (!empty($str)) {
	        $metachars = array(
	            // acentos minuscula
	            array('á', 'a'), array('é', 'e'), array('í', 'i'), array('ó', 'o'), array('ú', 'u'),
	            // acentos mayuscula
	            array('Á', 'A'), array('É', 'E'), array('Í', 'I'), array('Ó', 'O'), array('Ú', 'U'),
	            // enies
	            array('Ñ', 'N'), array('ñ', 'n'),
	            // dieresis minuscula
	            array('ä', 'a'), array('ë', 'e'), array('ï', 'i'), array('ö', 'o'), array('ü', 'u'),
	            // dieresis mayuscula
	            array('Ä', 'A'), array('Ë', 'E'), array('Ï', 'I'), array('Ö', 'O'), array('Ü', 'U'),
	            // acento diacritico minusculas
	            array('à', 'a'), array('è', 'e'), array('ì', 'i'), array('ò', 'o'), array('ù', 'u'),
	            // acento diacritico mayusculas
	            array('À', 'A'), array('È', 'E'), array('Ì', 'I'), array('Ò', 'O'), array('Ù', 'U'),
	        );
	        $new = trim($str);
	        foreach ($metachars as $item) {
	            list($cual, $por) = $item;
	            $new = str_replace($cual, $por, $new);
	        }
			if ($upper){
				$new = strtoupper($new);
			} else {
				$new = strtolower($new);
			}
	        return $new;
	    }
	    return false;
		*/
	}
	
	public function quitarTilde($str){
		$str = str_replace(
			array('À','È','Ì','Ò','Ù','Á','É','Í','Ó','Ú','Ä','Ë','Ï','Ö','Ü','Ñ','à','è','ì','ò','ù','á','é','í','ó','ú','ä','ë','ï','ö','ü','ñ'), 
			array('A','E','I','O','U','A','E','I','O','U','A','E','I','O','U','N','a','e','i','o','u','a','e','i','o','u','a','e','i','o','u','n'), 
		$str);
		
		$str = iconv('ISO-8859-1', 'UTF-8', $str);
		return str_replace( array('I¿Â½','ï¿½'), array('A' ,'E'), $str);
	}
	
	/**
	 * 'onPrepare' Event dispatch method
	 * Commonly assigned as a subevent of onLoad Event
	 */
	public function Prepare() {
		$this->onError = array(&$this,'CustomErrorHandler');
	    $this->moduleName = 'MO_Miniclinicconector';
		$this->_CoreHandler = new _Miniclinic($this);
		return true;
	}
	
	/**
	 * Loads the module config and the module data config that is required 
	 * in the rest of the application
	 */
	protected function LoadConfigData(){
	    $this->moduleName = 'MO_Miniclinicconector';
		define('_CONFIGDIR',_APPCONFIGDIR.'Module/');
		define('_CONFIGDATADIR',_APPCONFIGDIR.'Data/');
		$configDataFileName = _CONFIGDATADIR."config.co_mo_miniclinicconnector.ini";
		if (file_exists($configDataFileName)) {
			$configData = parse_ini_file($configDataFileName,true);
			return $configData;
		}
		return;
	}

	/**
	 Pantalla Inicial
	 * Parámetros de Salida:
	   Configuracion inicial del Kiosco
		    <CodResult></CodResult>
            <MsgResult></MsgResult>
            <nombre></nombre>
            <direccion></direccion>
            <listaPrevision>
               <Prevision>
                  <tipoPrevision></tipoPrevision>
                  <codigoPrevision></codigoPrevision>
                  <nombrePrevision></nombrePrevision>
               </Prevision>
              </listaPrevision>
   	**/
	public function Home(){
	    $this->moduleName = 'MO_Miniclinicconector';
	    /* Consumo de WsIniciarDia */
	   	$configData = $this->LoadConfigData();
	    if (!empty($configData)){
		   	$this->idKiosco = $configData['Kiosco']['id'];
			$urlExito = $configData['Kiosco']['urlExito'];
			$urlFracaso = $configData['Kiosco']['urlFracaso'];
			$this->_CoreHandler = new _Middleware($this);
			$configInicial = $this->_CoreHandler->ConfiguracionInicial($this->idKiosco, $urlExito, $urlFracaso);
			if($configInicial['CodResult'] == 'E' ){
				$configInicial['configData'] = $configData;
				return $configInicial;
			}
			else {
				$respuesta = array(
					'CodResult' => 'F',
					'MsgResult'	=> "No es posible obtener la configuración inicial en este momento",
					'configData' => $configData,
				);
				return $respuesta;
			}
	    } 
		else {
			$respuesta = array(
				'CodResult' => 'F',
				'MsgResult'	=> "No es posible obtener la configuracion del archivo " . _CONFIGDATADIR."config.co_mo_miniclinicconnector.ini",
			);
			return $respuesta;
	    }
	}
	
	/**
	 * Verifica el estado de la impresora, devuelve verdadero o falso
	 * 
	 */
	public function EstadoImpresora(){
		/*
		$this->_CoreHandler = new _Transbank($this);
		$estadoImpresora = $this->_CoreHandler->EstadoImpresora($this);
		return $estadoImpresora;
		*/
		return true;
	}
			
	/**
	 * Action Dispatcher
	 * Paso 1:  Este paso le pide al usuario seleccionar su motivo de consulta
	 * Entrada: idKiosco
	 * Respuesta:
	 Lista de tipos de consulta y su precio
			<CodResult></CodResult>
            <MsgResult></MsgResult>
            <ListaTipoConsultas>
               <TipoConsulta>
                  <idTipoConsulta></idTipoConsulta>
                  <codigo></codigo>
                  <nombre></nombre>
                  <valor></valor>
                  <ListaSintomas>
                     <Sintoma>
                        <idSintoma></idSintoma>
                        <nombre></nombre>
                     </Sintoma>
				  </ListaSintomas>
			</TipoConsulta>
		    <ListaTipoConsultas>
	 */
	public function Motivos( $tipoFlujo, $idKiosco) {
		$this->tipoFlujo = $tipoFlujo;
		$this->idKiosco = $idKiosco;
		
		/* Consumo del WS ObtenerMotivosPrecios */
		$this->_CoreHandler = new _Middleware($this);
		$wsObject = $this->_CoreHandler->ObtenerPrecios($this->tipoFlujo, $this->idKiosco );
		if($wsObject['CodResult'] == 'E') {
			if (isset($wsObject['ListaTipoConsultas']['TipoConsulta']['idTipoConsulta'])) { // Parche para cuando viene 1 solo elemento
				$wsObject['ListaTipoConsultas']['TipoConsulta'] = array($wsObject['ListaTipoConsultas']['TipoConsulta']);
			}
			$wsObject['PasoSiguiente'] = 'Paso2';
			$wsObject['PasoAnterior']  ='Paso0';
			return $wsObject;
		}
		
		if( $wsObject['CodResult'] =='F' ){
			return $wsObject;
		}
		else {
			$respuesta = array(
				'CodResult' => 'F',
				'MsgResult'	=> "Error en la ejecución del servicio ObtenerMotivosPrecios"
			);
			return $respuesta;
		}
	}

	/**Action Dispatcher
	 * Pantalla resumen
	 * Cambios segunda versión: Se agrega idBox y tiempoEspera como parametros de respuesta al servicio obtenerMedicoHora.
	   Respuesta:
		<CodResult></CodResult>
		<MsgResult></MsgResult>
		<horaAtencion></horaAtencion>
		<idBox><idBox>
		<tiempoEspera></tiempoEspera>
		<idMedico></idMedico>
		<numeroPacientesEspera></numeroPacientesEspera>
		<nombreMedico></nombreMedico>
		<rutMedico></rutMedico>
	 */
	public function Resumen($tipoFlujo, $idKiosco, $idTipoConsulta) {
		$this->idTipoConsulta = $idTipoConsulta;
		$this->idKiosco = $idKiosco;
		$this->tipoFlujo = $tipoFlujo;
		
		/* Consumo del WS ObtenerMedicoHora */
		$this->_CoreHandler = new _Middleware($this);
		$wsObject = $this->_CoreHandler->SolicitaHoraMedica($this->tipoFlujo, $this->idKiosco, $this->idTipoConsulta);

		if($wsObject['CodResult'] == 'E'){
			return $wsObject;
		}
		else if($wsObject['CodResult'] == 'F'){
			return $wsObject;
		}
		else {
			$respuesta = array(
				'CodResult' => 'F',
				'MsgResult'	=> "Error en la ejecución del servicio ObtenerMotivosPrecios"
			);
			return $respuesta;
		}
	}


	/** ex paso 4
	 * Action Dispatcher
	 * Este paso le pide al usuario que confirme sus datos, además debe proporcionar numeros telefónicos y un e-mail válido.
	 * Cambios versión 2: Si el paciente es particular, redirige a atBotonPago. Si el paciente no es particular, redirige a atIMED
	 */
	public function Identificacion($tipoFlujo, $tipoIdentificacion, $rut, $codigoPrevision) {
		$this->rut = $rut;
		$this->tipoIdentificacion = $tipoIdentificacion;
		$this->codigoPrevision =  $codigoPrevision;
		$this->tipoFlujo = $tipoFlujo;
		
		/* Consumo del WS VerificarBeneficiario */
		$this->_CoreHandler = new _Middleware($this);
		$wsObject = $this->_CoreHandler->VerificaBeneficiaro($this->tipoFlujo, $this->rut, $this->tipoIdentificacion, $this->codigoPrevision);
		
		//Obtiene todos los datos personales del paciente.
		$datosBeneficiario = $wsObject['datosPersonales'];

		//obtiene la fecha de nacimiento del servicio y la formatea en aaaa-mm-dd
		$this->fechaNacimiento = substr($datosBeneficiario['fechaNacimiento'],0,strrpos($datosBeneficiario['fechaNacimiento'],'T'));
		
		$logger = Logger::getInstance();
		
		/* elimina tildes y caracteres "raros" */
		$wsObject['datosPersonales']['nombre'] = $this->quitarTilde($datosBeneficiario['nombre']);
		$wsObject['datosPersonales']['apellidoPaterno'] = $this->quitarTilde($datosBeneficiario['apellidoPaterno']);
		$wsObject['datosPersonales']['apellidoMaterno'] = $this->quitarTilde($datosBeneficiario['apellidoMaterno']);
		
		$logger->debug('nombre => '.$wsObject['datosPersonales']['nombre']);
		$logger->debug('apellidoPaterno => '.$wsObject['datosPersonales']['apellidoPaterno']);
		$logger->debug('apellidoMaterno => '.$wsObject['datosPersonales']['apellidoMaterno']);
		
		/*Verifica si el usuario se encuentra afiliado al financiador indicado*/
		if($wsObject['CodResult']=='F'){
			return $wsObject;
		}
		
		if(empty($wsObject['CodResult'])){
			$respuesta['CodResult'] = 'F';
			$respuesta['MsgResult'] = 'Error al Verificar Beneficiario';
			return $respuesta;	
		}	
			
		/* Formatear numeros */
		if($datosBeneficiario['fonoCelular']){
			$fonoCelular = "(+569) ".$datosBeneficiario['fonoCelular'];
		}
		
		if($datosBeneficiario['fonoFijo']){
			$fonoFijo = $datosBeneficiario['fonoFijo'];
			
			if($fonoFijo[0] == 2){
				$fonoFijo = "(+56) ".$fonoFijo[0]." ". substr($fonoFijo, 1);
			}
			else {
				$fonoFijo = "(+56) ".$fonoFijo[0].$fonoFijo[1]." ". substr($fonoFijo, 2);
			}
		}
		
		if ($this->codigoPrevision == '0') { //si el paciente es particular
			$pasoSiguiente = 'BotonPago';
		}
		else {
			$pasoSiguiente = 'IMED';
		}
		
		$wsObject['PasoAnterior'] = 'Paso6';
		$wsObject['PasoSiguiente'] = $pasoSiguiente;			
		return $wsObject;
	}

	
	/** 
	 * Action Dispatcher
	 * Versión 2: Se llama este servicio sólo cuando el paciente es particular 
	 * Se registra cliente.
	 * Botón de pago: Si selecciona Particular, paga inmediatamente por transbank.
	 * Si no es particular, se va al botón de pago IMED, paga (Transbank o multicaja) y vuelve.
	 *  //BotonPago = 1, Si la forma de pago es Multicaja y otro, si la forma de pago es Transbank
	 * Respuesta:
		<CodResult></CodResult>
		<MsgResult></MsgResult>
		<idCliente></idCliente>
		<idConsulta></idConsulta>
		<idTransaccionIMed></idTransaccionIMed> 
	 */
	public function DatosPersonales($tipoFlujo, $tipoIdentificacion, $idKiosco, $rut, $apellidoPaterno, $apellidoMaterno, $nombre, $fechaNacimiento, $genero, $email, $fonoFijo, $fonoCelular, $idTipoConsulta, $idSintoma, $codigoPrevision, $tipoPrevision, $idMedico)
	{
	
	/*Registrar Cliente*/
	/*Preparar las variables para el consumo del servicio Registrar Cliente */

	$this->rut =				$rut;
	$this->tipoIdentificacion = $tipoIdentificacion;
	$this->apellidoPaterno =	$this->replaceMetaChars($apellidoPaterno);
	$this->apellidoMaterno =	$this->replaceMetaChars($apellidoMaterno);
	$this->nombre =				$this->replaceMetaChars($nombre);
	$this->fechaNacimiento =	$fechaNacimiento;
	$this->genero =				$genero;
	$this->email =				$this->replaceMetaChars($email,false);
	$this->fonoCelular =		$fonoCelular;
	$this->fonoFijo =			$fonoFijo;
	$this->idTipoConsulta =		$idTipoConsulta;	
	$this->idSintoma =			$idSintoma;	
	$this->codigoPrevision =	$codigoPrevision;
	$this->tipoPrevision =		$tipoPrevision;
	$this->idKiosco =			$idKiosco;
	$this->idMedico =			$idMedico;	
	$this->tipoFlujo =			$tipoFlujo;
	
	$paramCliente = array(
							'tipoFlujo'			 => $this->tipoFlujo,
							'identificacion'     => $this->rut,
							'tipoIdentificacion' => $this->tipoIdentificacion, //1 significa que se identificara con el "RUT"
							'apellidoPaterno'    => $this->apellidoPaterno,
							'apellidoMaterno'    => $this->apellidoMaterno,
							'nombre'             => $this->nombre,
							'fechaNacimiento'    => $this->fechaNacimiento,
							'genero'             => $this->genero,
							'email' 			 => $this->email,
							'fonoCelular' 		 => $this->fonoCelular,
							'fonoFijo'           => $this->fonoFijo,
							'listaTipoConsultas' => $this->idTipoConsulta, 
							'listaSintomas'		 => $this->idSintoma,
							'codigoPrevision'    => $this->codigoPrevision,
							'tipoPrevision'      => $this->tipoPrevision,
							'idMedico'           => $this->idMedico,
							'idKiosco'           => $this->idKiosco
					);
				
		//Consumo del WS RegistrarCliente
		$this->_CoreHandler = new _Middleware($this);
		$wsObject = $this->_CoreHandler->RegistrarCliente($paramCliente);
		
		return $wsObject;
	}
	
	
	/*Función que inicia el módulo transbank y la venta*/
	public function VentaTransbank($monto, $idConsulta){
		if (!isset($idConsulta)){
			// La venta no se va a realizar porque no hay idConsulta
		}
		
		$form = new _Form();
		$form->setAcceptDuplicateKeys(false);
		$form->setExcludeValues("op","PHPSESSID","day","month","year");
		$form->addHidden('idConsulta',$idConsulta);
		$form->addHidden('montocopago',$monto);
		$form->addHidden('monto',$monto);

		$formulario = $form->StartForm(array(
				"name"		=>"formulario",
				"id"		=>"formulario",
				"action"    =>"http://localhost/transbank/VentaTransbank.aspx",
				"method"    =>"POST",
		));
		$formulario .= $form->EndForm();
		print "<html><head></head><body>".$formulario."</body></html>";
		$form->submit();		
	}

	/**
	* Action Dispatcher
	* Se llama después de generar el pago Transbank.
	* generarBonoIMED, con el parámetro pagoTransbank 1  (Nuevo Web Service IMED)
	*/
	private function GenerarBonoImed($idConsulta, $idKiosco, $idTransaccionIMED, $numOperacion, $monto){
		$logger = Logger::getInstance();
		$logger->info("idConsulta => $idConsulta, idKiosco => $idKiosco, idTransaccionIMED => $idTransaccionIMED, numOperacion => $numOperacion, monto => $monto");
		
		$this->idKiosco =			$idKiosco;
		$this->monto =				$monto;
		$this->idTransaccionIMED =	$idTransaccionIMED;
		$this->NumOperacion =		$numOperacion;
		$this->idConsulta =			$idConsulta;
		
		$this->_CoreHandler = new _Transbank($this);
		$CodAutorizacion = $this->_CoreHandler->GetCodigoAutorizacion($this->idConsulta);
		
		/*Se llama al servicio generarBonoImed */
		$params = array (
			'pagoTransbank' 	=>'1',
			'idConsulta' 		=> $this->idConsulta, 
			'idKiosco' 			=> $this->idKiosco,
			'numTransaccion' 	=> $this->idTransaccionIMED,
			'numOperacion' 		=> $this->NumOperacion,
			'codAutorizacion'   => $CodAutorizacion,
			'monto'   			=> $this->monto
		);
		
		$this->_CoreHandler = new _Middleware($this);
		$wsObject = $this->_CoreHandler->GenerarBonoImed($params);
		$codResult = $wsObject['Respuesta']['CodResult'];
		$msgRespuesta = $wsObject['Respuesta']['MsgResult'];
		$logger->info("respuesta bono imed => $msgRespuesta");
		
		if( $codResult  == 'E' or $codResult == 'F' ){
			return $wsObject;
		}
		else {
			$respuesta = array( 'Respuesta' => 
				array(
					'CodResult' => 'F',
					'MsgResult'	=> "Error en la ejecucion del servicio GenerarBonoImed"
				)
			);
			return $respuesta;
		}
	}


	/**
	* Action Dispatcher
	* Se llama después de generar el pago Transbank.
	* generarBonoIMED, con el parámetro pagoTransbank 1  (Nuevo Web Service IMED)
	*/
	private function AnularBonoImed($idConsulta, $idKiosco, $idTransaccionIMED, $numOperacion, $monto) {
		$this->idKiosco =			$idKiosco;
		$this->monto =				$monto;
		$this->idTransaccionIMED =	$idTransaccionIMED;
		$this->NumOperacion =		$numOperacion;
		$this->idConsulta =			$idConsulta;

		$this->_CoreHandler = new _Transbank($this);
		$CodAutorizacion = $this->_CoreHandler->GetCodigoAutorizacion($this->idConsulta);
	
		/* Se llama al servicio generarBonoImed */
		$params = array (
			'pagoTransbank' 	=> '0',
			'idConsulta' 		=> $this->idConsulta,
			'idKiosco' 			=> $this->idKiosco,
			'numTransaccion' 	=> $this->idTransaccionIMED,
			'numOperacion' 		=> $this->NumOperacion,
			'codAutorizacion'   => $CodAutorizacion,
			'monto'   			=> $this->monto
		);
		
		$this->_CoreHandler = new _Middleware($this);
		$wsObject = $this->_CoreHandler->GenerarBonoImed($params);
		
		if( $wsObject['CodResult'] == 'E' ){
			return $wsObject;
		}
		else if( $wsObject['CodResult'] == 'F' ){
			return $wsObject;
		}
		else {
			$respuesta = array(
				'CodResult' => 'F',
				'MsgResult'	=> "Error en la ejecución del servicio GenerarBonoImed"
			);
			return $respuesta;
		}
	}

	/**
	 * Action Dispatcher
	 * Anula la consulta
	 */
	public function AnularConsulta($tipoFlujo, $idConsulta, $idTransaccionIMED, $rut, $tipoIdentificacion,$codigoAutorizacion,$glosaFallo) {
		$this->idConsulta = $idConsulta;
		$this->CodAutorizacion = $codigoAutorizacion;
		$this->idTransaccionIMED =  $idTransaccionIMED;
		$this->rut = $rut;
		$this->tipoIdentificacion = $tipoIdentificacion;
		$this->tipoFlujo = $tipoFlujo;
	
		$paramsConsulta = array(	
			'tipoFlujo'			 => $this->tipoFlujo,
		    'confirmacion'       =>	'0', //Hay error
			'codigoAutorizacion' =>	$this->CodAutorizacion, 
			'fechaTransaccion'   => date('Y-m-d\TH:i:s'),
			'idConsulta'		 => $this->idConsulta,
			'idTransaccionIMed'	 => ($this->idTransaccionIMED!='-1')?($this->idTransaccionIMED):('0'),
			'identificacion'	 => $this->rut,
			'tipoIdentificacion' => $this->tipoIdentificacion,
			'glosaFallo'		 => $this->quitarTilde($glosaFallo),
			'solicitudSMS'		 => '1'
		);
	
		$this->_CoreHandler = new _Middleware($this);
		$wsObject = $this->_CoreHandler->ConfirmarConsulta($paramsConsulta);
		
		if($wsObject['CodResult'] == 'E' ){
			return $wsObject;
		}
		else {
			$respuesta = array(
				'CodResult' => 'E', // Como anula la consulta, debe responder exito siempre
				'MsgResult'	=> "Error en la ejecucion del servicio ConfirmarConsulta"
			);
			return $respuesta;
		}
	}
	
	
	/**
	 * Action Dispatcher
	 * Versión 2: último paso.
	 * 
	 Confirmar consulta.
	 Solicitar Folio
	 Imprimir Boleta
	 GuardarInfoBoleta
	 Si el resultado de todo lo anterior es 'E', Muestra mensaje de éxito.
	*/
	public function Finalizar( $tipoFlujo,$idKiosco,  $rut, $apellidoPaterno, $apellidoMaterno, $nombre, $fechaNacimiento,
		$genero, $email, $fonoFijo, $fonoCelular, $idTipoConsulta, $idSintoma, $codigoPrevision, $nombrePrevision, 
		$tipoPrevision, $idMedico, $monto, $formaPago, $idConsulta, $nombreTipoConsulta, $idTransaccionIMED, $numOperacion,
		$solicitudSMS,$nombreSucursal, $direccionSucursal, $tipoIdentificacion, $nombreMedico, $rutMedico) 
	{
		
		$logger = Logger::getInstance();
		$logger->debug("paciente => $nombre $apellidoPaterno $apellidoMaterno");
		$logger->debug("doc => $nombreMedico");
	
		set_time_limit(300);
		$this->_CoreHandler = new _Miniclinic($this);
	
		$args = func_get_args();
	
		/*Variables para servicios*/
		$this->tipoFlujo = 			$tipoFlujo;
		$this->monto =				$monto;
		$this->idConsulta =			$idConsulta;
		$this->rut =				$rut;
		$this->idTransaccionIMED =	$idTransaccionIMED;
		$this->tipoIdentificacion = $tipoIdentificacion;
		$this->solicitudSMS =		$solicitudSMS;
		$this->apellidoPaterno =	$this->replaceMetaChars($apellidoPaterno);
		$this->apellidoMaterno =	$this->replaceMetaChars($apellidoMaterno); 
		$this->nombre =				$this->replaceMetaChars($nombre);
		$this->fechaNacimiento =	$fechaNacimiento; 
		$this->genero =				$genero;
		$this->email =				$this->replaceMetaChars(email,false);
		$this->fonoCelular =		$fonoCelular;
		$this->fonoFijo =			$fonoFijo;
		$this->idTipoConsulta =		$idTipoConsulta;
		$this->idSintoma =			$idSintoma;	
		$this->codigoPrevision =	$codigoPrevision;
		$this->tipoPrevision =		$tipoPrevision;
		$this->idKiosco =			$idKiosco;
		$this->nombreSucursal =		$this->replaceMetaChars($nombreSucursal);
		$this->direccionSucursal =  $this->replaceMetaChars($direccionSucursal);
		$this->nombreTipoConsulta = $this->replaceMetaChars($nombreTipoConsulta);
		$this->nombreMedico =		$this->replaceMetaChars($nombreMedico);
		$this->rutMedico =			$rutMedico;
		$this->nombrePrevision =    $this->replaceMetaChars($nombrePrevision);

		$FormaPagoTexto = ($formaPago == 1)?'Transbank':'Casa Comercial';
		$esCostoCero = (!isset($this->monto) or ($this->monto == 0));
		$fecha = date('Y-m-d');
		$hora = date('H:i:s');
		$mntNeto = 0;
		$mntExe = $this->monto;
		$iva = $this->monto;
		$mntTotal = $this->monto;
		$rutEmisor = '76123917-1';	  
		$this->_CoreHandler = new _Transbank($this);
		$codAutorizacion = $this->_CoreHandler->GetCodigoAutorizacion($this->idConsulta);
	
		$this->_CoreHandler = new _Transbank($this);
		$emiTarBan = $this->_CoreHandler->GetTipoTarjeta($this->idConsulta);
		
		$result = true; // Parte en Exito, si falla algo, queda en Fracaso
		$gloError = '';
		
		$rutCliente = ($this->tipoIdentificacion != "1")?$rutEmisor:$this->rut;
		
		/* Valida que el id consulta este presente al momento de cerrar la transaccion */
		if (!isset($this->idConsulta) or $this->idConsulta == null or $this->idConsulta == 0){
			$gloError = 'Id de consulta no se encuentra presente';
			$logger->error('Id de consulta no se encuentra presente');
			$result = false;
		}
		else if ($esCostoCero){ // costo cero no pasa por transbank ni bono
			/* Comprobante */
			$this->setTemplateFile("boleta");
			$this->assign('direccionSucursal',$this->direccionSucursal);
			$this->assign('idConsulta',$this->idConsulta);
			$this->assign('nombreTipoConsulta',$this->nombreTipoConsulta);
			$this->assign('idTipoConsulta',$this->idTipoConsulta);
			$this->assign('rutMedico',$this->rutMedico);
			$this->assign('nombreMedico',$this->nombreMedico);
			$this->assign('especialidad',"Medicina General");
			$this->assign('nombre', $this->nombre);
			$this->assign('apellidoPaterno',$this->apellidoPaterno);
			$this->assign('apellidoMaterno',$this->apellidoMaterno);
			$this->assign('rut',$this->rut);
			$this->assign('nombrePrevision',$this->nombrePrevision);
			$this->assign('nombreTipoConsulta',$this->nombreTipoConsulta);
			$this->assign('idTipoConsulta',$this->idTipoConsulta);
			$this->assign('nombrePrevision',$this->nombrePrevision);
			$this->assign('idKiosco',$this->idKiosco);
			$this->assign('fecha',$fecha);
			$this->assign('hora',$hora);
			$boleta = $this->fetch('comprobante_costo_cero');
			
			$this->_CoreHandler = new _Miniclinic($this);
			$this->_CoreHandler->LlenarBoleta($boleta,$this->idConsulta);
			$this->_CoreHandler->ImprimirBoleta($this->idConsulta);
			
			$paramsDocumento = array(
				'emisionDate' 				=> $fecha.'T'.$hora,
				'rutEmisor' 			    => '0',
				'razonSocialEmisor'    		=> utf8_encode ($this->nombreSucursal),
				'giroEmisor' 			    => 'Medicina General', 
				'codigoSucursalSII' 		=> '1',
				'dirOrigen'        			=> utf8_encode($this->direccionSucursal),
				'comunaOrigen'				=> '',
				'ciudadOrigen' 				=> '',
				'rutReceptor'	   			=> $this->rut,
				'razonSocialReceptor'    	=> utf8_encode ("$this->nombre $this->apellidoPaterno $this->apellidoMaterno"),
				'direccionReceptor'      	=> '',
				'montoExento'           	=> $this->monto, 
				'montoTotal'    			=> $this->monto, 
				'montoItem'    				=> $this->monto, 
				'nombreItemDetalle'			=> 'Motivo Consulta',     
				'descripcionItemDetalle'	=> utf8_encode($this->nombreTipoConsulta),
				'tipoCodigo'				=> 'EAN128',
				'valorCodigo'				=> $this->idTipoConsulta,
				'idKiosco'	   				=> $this->idKiosco, 
				'qtyItem'	   				=> '1',
				'unmdItem'	   				=> 'caja',
				'nombreMedico'				=> utf8_encode($this->nombreMedico),
				'rutMedico'					=> $this->rutMedico,
				'especialidadMedico'		=> 'Medicina General',
				'previsionPaciente'			=> utf8_encode($this->nombrePrevision),
				'formaPago'					=> $formaPago,
				'codigoPrevision'			=> $this->codigoPrevision,
				'horaPago'					=> $hora, 
				'folio'						=> '0',	
				'pdf417'					=> '0', 
				'resultText'				=> $resultText,
				'numTransaccion'			=> '0',
				'codFinanciador'			=> '0',
				'rutConvenio'				=> $rutEmisor,	
				'corrConvenio'				=> '0', //??
				'rutTratante'				=> $this->rutMedico,
				'rutSolicitante'			=> $this->rut,
				'nomSolic'					=> utf8_encode($this->nombre.' '.$this->apellidoPaterno.' '.$this->apellidoMaterno),
				'codEspecialidad'			=> '0',
				'rutBenef'					=> $this->rut,
				'urgencia'					=> 'N',
				'codForPag'					=> '0',	
				'numDoc'					=> '0',	
				'codInst'					=> 1,
				'codEmi'					=> -1,	// -1: kiosco, otro valor: Imed
				'emiTarBan'					=> '0', // VI: Visa, MC: MasterCard, !: Otro
				'codAuto'					=> 0,
				'mtoTransac'				=> $this->monto
			);

			try {
				if ($result){
					$this->_CoreHandler = new _Middleware($this);
					$wsResponse = $this->_CoreHandler->GuardarInfoBoleta($paramsDocumento);
					if ($wsResponse['CodResult'] != 'E'){
						$result = false;
					} 
					$gloError = $wsResponse['MsgResult'];
				}
			} catch (Exception $e) {
				$logger->error('Error al invocar GuardarInfoBoleta');
				$result = false;
			}
			
		}
		else if($this->codigoPrevision == '0') { //si es particular 
		
			/*Llama al servicio Solicitar Folio*/
			$paramsFolio = array(
				'emisionDate' 			=> $fecha, //(AAAA-MM-DD) 
				'rutEmisor' 			=> $rutEmisor,
				'rznSocEmisor'    		=> $this->nombreSucursal, 
				'giroEmisor' 			=> 'Medicina General', 
				'codigoSucursalSII'		=> '1',
				'dirOrigen'        		=> $this->direccionSucursal,
				'cmnaOrigen'           	=> "", //Dato opcional
				'ciudadOrigen' 			=> "",//Dato opcional
				'rutRecep'	   			=> $rutCliente,
				'rznSocRecep'    		=> "$this->nombre $this->apellidoPaterno $this->apellidoMaterno",
				'dirRecep'      		=> "", 
				'mntExe'           		=> $this->monto, 
				'mntTotal'           	=> $this->monto,
				'montoItem'           	=> intval($this->monto),
				'nmbItem' 				=> 'Motivo Consulta', 
				'descripcionItemDetalle' => $this->nombreTipoConsulta,
				'TipoCodigo'			=> 'EAN128',
				'vlrCodigo'				=> $this->idTipoConsulta,
				'idKiosco'	   			=> $this->idKiosco, 
				'qtyItem'	   			=> '1', 
				'unmdItem'	   			=> 'caja', 
				// Campos personalizados
				'nombreMedico'			=> $this->nombreMedico,
				'rutMedico'				=> $this->rutMedico,
				'especialidadMedico'	=> 'Medicina General',
				'previsionPaciente'		=> $this->nombrePrevision,
				'formaPago'				=> $formaPago,
				'horaPago'				=> $hora
			);

			try {
				$this->_CoreHandler = new _Middleware($this);
				$wsResponse = $this->_CoreHandler->SolicitarFolio($paramsFolio);
				$resultadoLlamadaSignature = $wsResponse['CrearDocumentoResult']['Resultado'];
				
				$logger->debug("respuesta signature $resultadoLlamadaSignature");
				
				if ($resultadoLlamadaSignature == '1'){
					$result = true;
					$folio = $wsResponse['CrearDocumentoResult']['Folio'];
					$codigoBarras = $wsResponse['CrearDocumentoResult']['Pdf417'];
					$resultText = $wsResponse['CrearDocumentoResult']['Mensaje'];
				}
				else {
					$result = false;
					$gloError = "A ocurrido un Error al solicitar folio idConsulta => $this->idConsulta";
					$logger->error($gloError);
				}
			}
			catch (Exception $e) {
				$result = false;
				$gloError = 'Error fatal al generar folio. Posiblemente timeout';
				$logger->error($gloError);				
			}
			
			if ($result){
				try {
					/*Boleta*/
					$this->setTemplateFile("boleta");
					$this->assign('nombreSucursal',$this->nombreSucursal);
					$this->assign('direccionSucursal',$this->direccionSucursal);
					$this->assign('idConsulta',$this->idConsulta);
					$this->assign('folio',$folio);
					$this->assign('codAutorizacion',$CodAutorizacion);
					$this->assign('nombreTipoConsulta',$this->nombreTipoConsulta);
					$this->assign('idTipoConsulta',$this->idTipoConsulta);
					$this->assign('rutMedico',$this->rutMedico);
					$this->assign('nombreMedico',$this->nombreMedico);
					$this->assign('especialidad',"Medicina General");
					$this->assign('nombre',$this->nombre);
					$this->assign('apellidoPaterno',$this->apellidoPaterno);
					$this->assign('apellidoMaterno',$this->apellidoMaterno);
					$this->assign('rut',$this->rut);
					$this->assign('nombrePrevision',$this->nombrePrevision);
					$this->assign('nombreTipoConsulta',$this->nombreTipoConsulta);
					$this->assign('idTipoConsulta',$this->idTipoConsulta);
					$this->assign('nombrePrevision',$this->nombrePrevision);
					$this->assign('monto',$this->monto);
					$this->assign('formaPago',$formaPagoBoleta);
					$this->assign('idKiosco',$this->idKiosco);
					$this->assign('fecha',$fecha);
					$this->assign('hora',$hora);
					$this->assign('fechaResolucion',$fecha);
					$this->assign('mailContacto',"contacto@miniclinic.cl");
					$this->assign('sitioWeb',"www.miniclinic.cl");
					$this->assign('codigoBarras',$codigoBarras);
					$boleta = $this->fetch('boleta');
					/*imprime la boleta*/
					$this->_CoreHandler = new _Miniclinic($this);
					$this->_CoreHandler->LlenarBoleta($boleta,$this->idConsulta);
					$this->_CoreHandler->ImprimirBoleta($this->idConsulta);
				}
				catch (exception $e){
					$logger->error('Error al imprimir boleta');
					$gloError = 'Error al imprimir la boleta';
					$result = false;
				}
			}
				
			/* Forma de pago */
			if ($formaPago == 1) { //1: Si es por Transbank
				$formaPagoBoleta = 'RedCompra';		// 1: Tarjetas bancarias
				$codForPag = 15;			        // 15: Tarjetas bancarias
			}
			else {	
				$formaPagoBoleta = 'MultiPay'; // 0: Casas Comerciales
				$codForPag = 14;			   // 14: Casas comerciales
			}
			
			$paramsDocumento = array(
				'emisionDate' 				=> $fecha.'T'.$hora,
				'rutEmisor' 			    => $rutEmisor,
				'razonSocialEmisor'    		=> utf8_encode ($this->nombreSucursal),
				'giroEmisor' 			    => 'Medicina General', 
				'codigoSucursalSII' 		=> '1',
				'dirOrigen'        			=> utf8_encode($this->direccionSucursal),
				'comunaOrigen'				=> '',
				'ciudadOrigen' 				=> '',
				'rutReceptor'	   			=> $this->rut,
				'razonSocialReceptor'    	=> utf8_encode ($this->nombre.' '.$this->apellidoPaterno.' '.$this->apellidoMaterno),
				'direccionReceptor'      	=> '',
				'montoExento'           	=> $this->monto, 
				'montoTotal'    			=> $this->monto, 
				'montoItem'    				=> $this->monto, 
				'nombreItemDetalle'			=> 'Motivo Consulta',     
				'descripcionItemDetalle'	=> utf8_encode($this->nombreTipoConsulta),
				'tipoCodigo'				=> 'EAN128',
				'valorCodigo'				=> $this->idTipoConsulta,
				'idKiosco'	   				=> $this->idKiosco, 
				'qtyItem'	   				=> '1',
				'unmdItem'	   				=> 'caja',
				'nombreMedico'				=> utf8_encode($this->nombreMedico),
				'rutMedico'					=> $this->rutMedico,
				'especialidadMedico'		=> 'Medicina General',
				'previsionPaciente'			=> utf8_encode($this->nombrePrevision),
				'formaPago'					=> $formaPago,
				'codigoPrevision'			=> $this->codigoPrevision,
				'horaPago'					=> $hora, 
				'folio'						=> $folio,	
				'pdf417'					=> $codigoBarras, 
				'resultText'				=> $resultText,
				'numTransaccion'			=> $this->idTransaccionIMED,	
				'codFinanciador'			=> '0',
				'rutConvenio'				=> $rutEmisor,	
				'corrConvenio'				=> '0', //??
				'rutTratante'				=> $this->rutMedico,
				'rutSolicitante'			=> $this->rut,
				'nomSolic'					=> utf8_encode($this->nombre.' '.$this->apellidoPaterno.' '.$this->apellidoMaterno),
				'codEspecialidad'			=> '',
				'rutBenef'					=> $this->rut,
				'urgencia'					=> 'N',
				'codForPag'					=> $codForPag,	
				'numDoc'					=> '',	
				'codInst'					=> 1,
				'codEmi'					=> -1,	// -1: kiosco, otro valor: Imed
				'emiTarBan'					=> $emiTarBan,	// VI: Visa, MC: MasterCard, !: Otro
				'codAuto'					=> $codAutorizacion,
				'mtoTransac'				=> $this->monto		  
			);

			try {
				if ($result){
					$this->_CoreHandler = new _Middleware($this);
					$wsResponse = $this->_CoreHandler->GuardarInfoBoleta($paramsDocumento);
					if ($wsResponse['CodResult'] != 'E'){
						$result = false;
					} 
					$gloError = $wsResponse['MsgResult'];
				}
			} catch (Exception $e) {
				$logger->error('Error al guardar info de boleta');
				$result = false;
			}
		}
		else if ($formaPago == 1){ // 1: Fonasa, otro BONO, con pago Transbank (formaPago=1)
			try{
				$logger->info("llamando servicio imed para la venta de bono");
				$wsResponse = $this->GenerarBonoImed($this->idConsulta, $this->idKiosco, $this->idTransaccionIMED, $numOperacion, $this->monto);
				$gloError = $wsResponse['Respuesta']['MsgResult'];
				if ($wsResponse['Respuesta']['CodResult'] != 'E'){
					$logger->error("NO se pudo vender bono => $gloError");
					$result = false;
				}
				else {
					$folioBono = $wsResponse['Respuesta']['folioBono'];
					$logger->info("bono vendido con folio => $folioBono");
				}
			} catch (Exception $e) {
				$gloError = "error al generarBono imed [ idConsulta = $this->idConsulta, idKiosco = $this->idKiosco, idTransaccionImed = $this->idTransaccionIMED, monto = $this->monto]";
				$logger->error($gloError);
				$result = false;
			}
		}
		
		/*ConfirmarConsulta*/
		$confirmacion = ($result)?('1'):('0');
		
		$paramsConsulta = array(
			'tipoFlujo'          => $this->tipoFlujo,
			'confirmacion'       =>	$confirmacion, //'0': Error, '1': Exito
			'codigoAutorizacion' =>	$codAutorizacion, 
			'fechaTransaccion'   => $fecha.'T'.$hora,
			'idConsulta'		 => $this->idConsulta,
			'idTransaccionIMed'	 => ($this->idTransaccionIMED!='-1')?($this->idTransaccionIMED):('0'),
			'identificacion'	 => $this->rut,
			'tipoIdentificacion' => $this->tipoIdentificacion,
			'glosaFallo'		 => $this->quitarTilde($gloError),
			'solicitudSMS'		 => $this->solicitudSMS
		);

		/*Llama al servicio ConfirmarConsulta*/
		try {
			$this->_CoreHandler = new _Middleware($this);
			$wsResponse = $this->_CoreHandler->ConfirmarConsulta($paramsConsulta);
			if ($wsResponse['CodResult'] != 'E'){
				$result = false;
			}
			$gloError = $wsResponse['MsgResult'];
		} 
		catch (Exception $e) {
			$result = false;
		}
		
		$respuesta['CodResult'] =  ($result)?('E'):('F');
		$respuesta['MsgResult'] =  $gloError;
		$respuesta['PasoAnterior'] =  '';
		$respuesta['PasoSiguiente'] = 'Paso0';
		return $respuesta;
	}
	
	public function FatalError($errno, $errstr, $errfile, $errline) {
		$log = Logger::getInstance();
		$log->error("line=> $errline, detail => $errstr");
		return;
	}
	
	
	public function CustomErrorHandler($errno, $errstr, $errfile, $errline){
		return true;
	}
	
	public function __destruct(){
		unset($this);
	}
}
?>
