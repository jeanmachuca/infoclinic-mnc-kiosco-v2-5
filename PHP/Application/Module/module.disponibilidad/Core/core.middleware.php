<?php

class _Middleware {
	public $oModule;
	
	function _Middleware(&$oModule){
		$this->oModule = &$oModule;
		return $this;
	}
	
	
	function getTitle(){
		return "Miniclinic ";
	}
	
	function &byCero($value){
		$ret = new stdClass();
		$ret->upCero = ($value>0)?($value):(0);
		$ret->downCero = ($value<0)?($value):(0);
		return $ret;
	}

	/**
	 * Llama a WSObtenerDisponibilidad (middleware)
	 * recibe los minutos que faltan  para la proxima atencion disponible
	 * 
	 * @return $minutos minutos que faltan para la pr�xima atenci�n
	 */
	function ConfirmaDisponibilidad ($idKiosco){
		$wsObject = new ObtenerDisponibilidad($idKiosco);
		return $wsObject->MinutosEstimados;
	}
	

}
?>
