<?php
/* vim: set expandtab tabstop=4 shiftwidth=4: */
/**
 * @copyright 2000-2020 Jean Machuca.
 * @author  <Jean Machuca>
 * @package Miniclinic class
 * @subpackage 
 * 
 */
class MO_Disponibilidad extends Module {
	public $oTemplate;
	public $_CoreHandler;
	public $UserInfo;
	public $UserPriv;
	public $AccountTotals;
	public $Accounts;
	public $Types;

	/**
	 * Constructor
	 */
	public function MO_Disponibilidad(){
		$this->onLoad = array(&$this,'Prepare');
		if (isset($_REQUEST['op'])){
			$this->onAction = array(&$this,'at'.ucwords($_REQUEST['op']));
		}
		parent::Module();
	}

	/**
	 * 'onPrepare' Event dispatch method
	 * Commonly assigned as a subevent of onLoad Event
	 */
	public function Prepare(){
		$this->_CoreHandler = new _Middleware($this);
		$this->oTemplate = $this->getTemplate();
		$this->_templateFile = $this->getTemplateFile();
		$this->assign('CoreHandler',$this->_CoreHandler);
		$this->LoadConfig();
		$idKiosco = $this->oConfig->Kiosco->id;
		$salida = $this->_CoreHandler->ConfirmaDisponibilidad($idKiosco);
		$this->assign('salida',$salida);
		return true;
	}

	public function __destruct(){
		unset($this);
	}
}
?>