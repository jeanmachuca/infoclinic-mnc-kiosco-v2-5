<?php
/* vim: set expandtab tabstop=4 shiftwidth=4: */
/**
 * @copyright 2000-2020 Jean Machuca.
 * @author  <Jean Machuca>
 * @package Miniclinic class
 * @subpackage 
 * 
 */
class MO_Transbank extends Module {
	public $oTemplate;
	public $_CoreHandler;
	public $UserInfo;
	public $UserPriv;
	public $AccountTotals;
	public $Accounts;
	public $Types;
	
	public $IdTransaccion = 0;
	public $Ticket = 0;
	public $Monto  = 0;

	/**
	 * Constructor
	 */
	public function MO_Transbank(){
		$this->onLoad = array(&$this,'Prepare');
		if (isset($_REQUEST['op'])){
			$this->onAction = array(&$this,'at'.ucwords($_REQUEST['op']));
		}
		parent::Module();
	}

	/**
	 * 'onPrepare' Event dispatch method
	 * Commonly assigned as a subevent of onLoad Event
	 */
	public function Prepare(){

		$this->onError = array(&$this,'FatalError');
		$this->_CoreHandler = new _Transbank($this);
		$this->oTemplate = $this->getTemplate();
		$this->_templateFile = $this->getTemplateFile();
		$this->assign('CoreHandler',$this->_CoreHandler);
		return true;
	}
	
	public function atMensaje($action){
		$salida = $this->_CoreHandler->GetMessage($_REQUEST['transaccion']);
		$this->assign('salida',$salida);
		return true;
	}

	public function atVenta($action){
/*
		$estadoImpresora = $this->_CoreHandler->EstadoImpresora($this);
		if (!$estadoImpresora){
			$this->getEvent()->FireEvent('Error',array('errno'=> 0, 'errstr'=> 'Impresora desconectada', 'errfile'=> 'module.miniclinic.php', 'errline'=> 0));
			return true;
		} else {
			*/
			$salida = '';
			$this->Monto = str_replace('.','',strval($_REQUEST['montocopago']));
			$this->Monto = str_replace('.','',$this->Monto);
			$this->Monto = intval($this->Monto);

			$_REQUEST['monto'] = $this->Monto;
			$this->IdTransaccion = $_REQUEST['idTrx'];
			$this->assign('salida',$salida);

			$file = TRANSBANKDIR.'estado.txt';
			$fp = fopen($file,"w");
			fwrite($fp, "");
			fclose($fp);

			$form = new _Form();
			$form->setAcceptDuplicateKeys(false);
			$form->addMultipleHiddens($_SESSION,$_REQUEST,array("op"=>"transbank"));
			
			$formulario = $form->StartForm(array( "name"=>"_redirect",
									"action"=>"transbank/VentaTransbank.aspx",
									"method"=>"POST"));
			$formulario .= $form->EndForm();
			print "<html><head></head><body>".$formulario."</body></html>";
			$form->submit();
			exit;
			
			return false;
		//}
	}
	public function FatalError($errno, $errstr, $errfile, $errline) 
	{
		$this->setTemplateFile("fatalError.html");
		parent::setDebug(false);
		$this->_CoreHandler = new _Transbank($this);
		$this->_CoreHandler->logFatalError($errno, $errstr, $errfile, $errline);
		return false;
	}
	
	public function __destruct(){
		unset($this);
	}
}
?>