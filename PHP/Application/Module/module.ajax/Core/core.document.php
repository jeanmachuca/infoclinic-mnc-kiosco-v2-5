<?php
class _Document {
	
	public $Location;
	public $Form;
	public $innerHTML;
	
	function _Document(){
		$this->Form = new _Form();
		$this->Location = new _Location();
		return $this;
	}
	
	function Open($filename){
		$this->Close();
/*
	    if (is_file($filename)) {
	        ob_start();
	        include_once($filename);
	        $contents = ob_get_clean();
	        $this->Write($contents);
	        return true;
	    }
*/
   		$scheme = (strtolower($_SERVER['HTTPS']) == "on")?('https'):('http');
		$url = "$scheme://$_SERVER[HTTP_HOST]/$filename";
		$oHTTP = new HttpRequest($url);
		$contents = $oHTTP->DownloadToString();
		if (!empty($contents)) {
			$this->Write($contents);
			return true;
		}
	    return false;
	}
	
	function Write ($sOutput) {
		$this->innerHTML .= $sOutput;
	}
	
	function WriteLn () {
		$this->innerHTML .= "<BR/>".$sOutput;
	}
	
	function Close (){
		$this->innerHTML = '';
	}
	
	function __destruct(){
		unset($this);
	}
}
?>