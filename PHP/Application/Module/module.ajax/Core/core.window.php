<?php
class _Window {
	public $Document;
	public $External;
	
	function _Window(){
		$this->Document = new _Document();
		$this->External = new _External();
		return $this;
	}
	
	public function Open($sHref,$sTitle = '',$sFeatures = ''){
		if ($this->Document->Open($sHref)){
			return $this;
		} else {
			return null;
		}
	}
	
	function __destruct(){
		unset($this);
	}
}
?>