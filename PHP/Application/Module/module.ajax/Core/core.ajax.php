<?php
class _Ajax {
	public $oModule;
	public $aModules = array();
	public $Window;
	public $Form;
	
	function _Ajax(&$oModule){
		$this->oModule = &$oModule;
		$this->Window = new _Window();
		$this->Form = new _Form();
		return $this;
	}
		
	function __destruct(){
		unset($this);
	}
}
?>