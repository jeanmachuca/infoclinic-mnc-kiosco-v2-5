<?php
class MO_Ajax extends Module {
	public $oTemplate;
	public $Dom;
	
	public function MO_Ajax(){
		$this->onLoad = array(&$this,'Prepare');
		$this->onAction = array(&$this,'Process');
		$this->onRender=create_function($null,'return true;');
		parent::Module();
	}

	public function Prepare(){
		$this->Dom = new _Ajax($this);
		return true;
	}

	public function Process(){
		$AjaxFunction = $_REQUEST['fn'];
		$AjaxModule = $_REQUEST['mod'];
		$sHref = "/$AjaxModule.php?";
		unset($_REQUEST['PHPSESSID']);
		foreach ($_REQUEST as $k=>$v) {
			$sHref .= $k.'='.$v.'&';
		}
		$oWnd = $this->Dom->Window->Open($sHref,$sTitle,$sFeatures);
		$this->AddContent($oWnd->Document->innerHTML);
		return true;
	}

	public function __destruct(){
		unset($this);
	}
}
?>