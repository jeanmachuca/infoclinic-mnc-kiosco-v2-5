<?php
class Stereotype extends _XMLArray2Object {

	var $xml2array;
	var $construct;
	var $configuration;
	var $stereotypes;
	
	function Stereotype($xml){
		$this->xml2array = new _XML2Array($xml);
		$this->xml2array->parse();
		$this->construct=true;
		$this->array2class($this->xml2array->stack);
	}

	function &Get($sModule,&$oTemplate, $cache = false){
		static $oStereotype;
		if (!$cache || !is_object($oStereotype[$sModule])){
			$sFile = _APPSTEREOTYPEDIR.$sModule.'/Stereotype.xml';
			if (!is_file($sFile) || !file_exists($sFile)){
				$sFile = _APPSTEREOTYPEDIR.'Stereotype.xml';
			}
			$sXML = $oTemplate->fetch($sFile);
			$oStereotype[$sModule] = new Stereotype($sXML);
		}
		return $oStereotype[$sModule];
	}

	function Dispatch($sModule,$sName,$oTemplate = null,$cache = false){
		if (is_null($oTemplate)){
			$oSmartyTemplate = new Smarty();
			_TTE::setSmartyConf($oSmartyTemplate);
			$oTemplate = new _TTE($oSmartyTemplate);
		}
		$oStereotype = Stereotype::Get($sModule,$oTemplate,$cache);
		eval($oStereotype->stereotypes->$sName);
	}
}
?>