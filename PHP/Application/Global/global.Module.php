<?php
/* vim: set expandtab tabstop=4 shiftwidth=4: */
/**
 * @copyright 2000-2020 Jean Machuca.
 * @author  <Jean Machuca>
 * @package Module class
 * @subpackage 
 * 
 * The Main Module Class for to extend general purpose Main functionalities
 * 
 */
include_once(dirname(__FILE__).'/'.'../Core/core._Main.php');
include_once(dirname(__FILE__).'/'.'../Global/global.Main.php');
include_once(dirname(__FILE__).'/'.'../Loader.php');


 class Module extends _Main {
 	
 	public $content = "";
	public $moduleName = "";
	public $oConfig;
	public $templateFile = "";
	public $aCoreFiles = array();
	
	public $onCustomShutDown;
	
	/**
	 * Module constructor
	 * 
	 */
	public function Module(){
		$this->onDisplay = array(&$this,'Draw');
		$this->onError = array(&$this,'CustomErrorHandler');
		if (isset($this->onShutDown)) $this->onCustomShutDown = $this->onShutDown;
		$this->onShutDown = array(&$this,'CustomShutDown');
		$this->setModuleName();
		$this->LoadConfig();
		$this->LoadCore();
		return parent::_Main();
		
	}
	
	public function setDebug($valor)
	{
		parent::setDebug($valor);
	}
	/**
	 * Sets the module name
	 */	
	public function setModuleName($moduleName = ''){
		if (!empty($moduleName)){
			$this->moduleName = $moduleName;
		} else {
			$this->moduleName = get_class($this);
		}
	}
	
	/**
	 * Assins the main standard template vars 'oModule' and 'contenido'
	 * This vars are used in all of templates 
	 * 
	 */
	public function Draw(){
		/*$this->assign('oModule',$this);
		$this->assign('contenido',$this->content);*/
		return true;
	}
	
	/**
	 * Default Custom Error Handler method, when none 'onError' event is defined
	 * a default onError event is automaticaly defined and this methos is assigned 
	 * for those event
	 */
	public function CustomErrorHandler(){
		/*
		echo "Error en el modulo: ".$this->moduleName;
		$args = func_get_args();
		return $this->oEvent->FireEvent('CustomError',$args);
		**/
		return true;
	}
	
	/**
	 * Fills the standard $contenido var for general purposes 
	 */
	protected function AddContent($content = ""){
		$this->content .= $content;
		return true;
	}
	
	/**
	 * Loads the module config and the module data config that is required 
	 * in the rest of the application
	 */
	protected function LoadConfig(){
		define('_CONFIGDIR',_APPCONFIGDIR.'Module/');
		define('_CONFIGDATADIR',_APPCONFIGDIR.'Data/');
		if (file_exists(_CONFIGDIR.'config.'.strtolower($this->moduleName).'.php')){
			include_once(_CONFIGDIR.'config.'.strtolower($this->moduleName).'.php');
			$configName = 'CO_'.$this->moduleName;
			if (class_exists($configName)){
				$this->oConfig = new $configName();
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Loads the core classes of the module.
	 * This is commonly used in the custom constructor module 
	 */
	protected function LoadCore(){
		define('_COREMODULEDIR',_MODULEDIR.'Core/');
		$this->aCoreFiles = glob(_COREMODULEDIR."core.*.php");
		if (is_array($this->aCoreFiles)){
			foreach ($this->aCoreFiles as $k=>$file){include_once($file);}
		}
	}
	
	/**
	 * Generates and loads the main db connection 
	 * The db connection is loaded only once and may be called anytime 
	 * 
	 */
	protected function &DBConnection(){
		static $oDB;
		if ((!is_object($oDB)) && is_object($this) && is_object($this->oConfig)){
			$dbhost = $this->oConfig->getValue('dbConnection->dbhost');
			$dbuser = $this->oConfig->getValue('dbConnection->dbuser');
			$dbpass = $this->oConfig->getValue('dbConnection->dbpass');
			$persistency = $this->oConfig->getValue('dbConnection->persistency');
			$dbtype = $this->oConfig->getValue('dbConnection->dbtype');
			$dbname = $this->oConfig->getValue('dbConnection->dbname');
//			$oDB = new DBAbstractionLayer($dbhost,$dbuser,$dbpass,$dbname,$persistency,$dbtype);
		}
		return $oDB;
	}
	
	/**
	 * Forces to disconnect the main db connection. This is required when you need interact with more of one databases
	 */
	protected function DBDisconnect(){
		$oDB = $this->DBConnection();
		if (is_object($oDB)) $oDB->sql_close();
		return true;
	}
	
	/**
	 * ShutDown default event dispatcher method. When none custom ShutDown Event is assigned this method is used for standard shutdown control
	 */
	public function CustomShutDown (){
		$ret = $this->getEvent()->fireEvent('CustomShutDown');
		$this->DBDisconnect();
		$this->getSession()->save();
		return $ret;
	}
	
	/** 
	 * Obtain a DBArtifact object with the $artifactName,
	 * if $noCache is true then the object will be created everytime you call this 
	 * else, will be loaded only once 
	 */
	public function getArtifact($artifactName,$noCache = true){
		static $aArtifacts;
		if ($noCache || !isset($aArtifacts[$artifactName]) || !is_object($aArtifacts[$artifactName]) || !is_a($aArtifacts[$artifactName],$artifactName)){
			$db = $this->DBConnection();
			$aArtifacts[$artifactName] = $db->DBArtifact($artifactName);
		}
		return $aArtifacts[$artifactName];
	}
	
	/**
	 * Gets the title of the main document
	 * 
	 */
	public function getTitle (){
		return $this->oConfig->getValue('Module->Title');
	}
	
	/**
	 * Gets the Style Sheet predefined for module personalized styles 
	 * 
	 */
	public function getStyleSheet($media = 'screen'){
		$ThemeName = $this->oConfig->getValue('Theme->name');
		$cssMediaThemeFile = 'Theme/'.$ThemeName.'/Css/Module/css.'.strtolower($this->moduleName).'.'.strtolower($media).'.css';
		$cssThemeFile = 'Theme/'.$ThemeName.'/Css/Module/css.'.strtolower($this->moduleName).'.css';
		$cssMediaResourcesFile = 'Resources/Css/css.'.strtolower($this->moduleName).'.'.strtolower($media).'.css';
		$cssResourcesFile = 'Resources/Css/css.'.strtolower($this->moduleName).'.css';
		$retCssFile = "";

		switch (true) {
			case file_exists(_NAVIGATEDIR.$cssMediaThemeFile):
				$retCssFile = '/'.$cssMediaThemeFile;
				break;
			case file_exists(_NAVIGATEDIR.$cssThemeFile):
				$retCssFile = '/'.$cssThemeFile;
				break;
			case file_exists(_NAVIGATEDIR.$cssMediaResourcesFile):
				$retCssFile = '/'.$cssMediaResourcesFile;
				break;
			case file_exists(_NAVIGATEDIR.$cssResourcesFile):
				$retCssFile = '/'.$cssResourcesFile;
				break;
			default:
				break;
		}
		return $retCssFile;
	}
	
	/**
	 * Sets the theme name 
	 */
	public function setThemeName($ThemeName = ''){
		if (!empty($ThemeName)) {
			$this->oConfig->setValue('Theme->name',$ThemeName);
		}
	}
	
	
	/**
	 * Returns true if the value obtained by creteria of absolute positive is a 
	 * number that is major than cero
	 */
	public function notEmptyButCero(&$byref){
		$isempty = &$byref;
		if (empty($isempty) && "$isempty" != "0"){
		    return false;
		} else {
		    return true;
		}
	}
	
	/** 
	 * Common destructor method for compatibiliy mode
	 */
	public function __destruct(){
		unset($this);
	}
}
?>