<?php
/* vim: set expandtab tabstop=4 shiftwidth=4: */
/**
 * Project: HttpRequest 1.0
 *
 * @author  <Jean Machuca>
 * @package HttpRequest 1.0
 * @subpackage 
 */


class HttpRequest
{
   var $_fp;        // HTTP socket
   var $_url;        // full URL
   var $_host;        // HTTP host
   var $_protocol;    // protocol (HTTP/HTTPS)
   var $_uri;        // request URI
   var $_port;        // port
   var $_method;		// HTTP Method (GET/POST/...)
   var $_data = '';

   // constructor
   function HttpRequest($url,$method = 'GET')
   {
       $this->_url = $url;
	   $this->_method = $method;
       $this->_scan_url();
   }

   // download URL to string
   function DownloadToString()
   {

       $crlf = "\r\n";
	   $usragent = $_SERVER["HTTP_USER_AGENT"];

       // generate request
       $req = $this->_method.' ' . $this->_uri . ' HTTP/1.0' . $crlf
           .    'Host: ' . $this->_host . $crlf
		   .    'Content-type: application/x-www-form- urlencoded' . $crlf
		   .    'Content-length: ' . strlen($this->_data) . $crlf
		   .    'User-Agent: '. $usragent . $crlf
		   .    'Connection: close' . $crlf
		   .$crlf;
	   if ($this->_method == 'POST'){
	   	   $req .= $this->_data;
	   }

       // fetch
       $this->_fp = fsockopen(($this->_protocol == 'https' ? 'ssl://' : '') . $this->_host, $this->_port);
       fwrite($this->_fp, $req);
       while(is_resource($this->_fp) && $this->_fp && !feof($this->_fp))
           $response .= fread($this->_fp, 1024);
       fclose($this->_fp);

       // split header and body
       $pos = strpos($response, $crlf . $crlf);
       if($pos === false)
           return($response);
       $header = substr($response, 0, $pos);
       $body = substr($response, $pos + 2 * strlen($crlf));

       // parse headers
       $headers = array();
       $lines = explode($crlf, $header);
       foreach($lines as $line)
           if(($pos = strpos($line, ':')) !== false)
               $headers[strtolower(trim(substr($line, 0, $pos)))] = trim(substr($line, $pos+1));

       // redirection?
       if(isset($headers['location']))
       {
           $http = new HttpRequest($headers['location']);
           return($http->DownloadToString($http));
       }
       else
       {
           return($body);
       }
   }

   // scan url
   function _scan_url()
   {
       $req = $this->_url;

       $pos = strpos($req, '://');
       $this->_protocol = strtolower(substr($req, 0, $pos));

       $req = substr($req, $pos+3);
       $pos = strpos($req, '/');
       if($pos === false)
           $pos = strlen($req);
       $host = substr($req, 0, $pos);

       if(strpos($host, ':') !== false)
       {
           list($this->_host, $this->_port) = explode(':', $host);
       }
       else
       {
           $this->_host = $host;
           $this->_port = ($this->_protocol == 'https') ? 443 : 80;
       }

       $this->_uri = substr($req, $pos);
       if($this->_uri == '')
           $this->_uri = '/';
   }

   function __destruct() {
	   unset($this);
   }
}
?>