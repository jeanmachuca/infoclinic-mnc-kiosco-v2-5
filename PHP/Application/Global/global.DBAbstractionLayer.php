<?php
//error_reporting(E_ALL);
/************************************************************************/
/* Portal_v265 (APWA): Automation Platform for Web Aplications          */
/* ============================================================         */
/*                                                                      */
/* Copyright (c) 2003-2010 by Jean Machuca                              */
/*                                                                      */
/*                                                                      */
/* Last Rev.: jan 01 2006                                               */
/*                                                                      */
/*                                                                      */
/************************************************************************/

$include_db['MySQL'] = _APPDIR."Db/db.mysql.php";
$include_db['mysql4'] = _APPDIR."Db/db.mysql4.php";
$include_db['postgres7'] = _APPDIR."Db/db.postgres7.php";
$include_db['mssql'] = _APPDIR."Db/db.mssql.php";
$include_db['oracle'] = _APPDIR."Db/db.oracle.php";
$include_db['msaccess'] = _APPDIR."Db/db.msaccess.php";
$include_db['db2'] = _APPDIR."Db/db.db2.php";
include_once($include_db[_DBTYPE]);
/**
*
* @abstract class DBAbstractionLayer
* @name DBAbstractionLayer
*
*/
class DBAbstractionLayer extends sql_db {

	var $fieldnames = array();
	var $sqlfieldnames = array();
	var $db_connect_id = false;
	var $dbname = "";
	var $dbtable = "";
	var $sqltext = "";
	var $sqltype = "SELECT";
	var $sqlfields = '*';
	var $records_object = array();
	var $dbtype = "MySQL";
	var $db_sqlfieldstr = array();
	var $db_sqltablestr = array();
	var $oTemplate;
	var $oEvent;
	
	var $onLoad;
	var $onArtifactLoad;
	var $onConnect;
	var $onDisconnect;
	var $onQuery;
	var $onExcecuteQuery;
	var $onPager;
	var $onSQLBuild;
	var $onInsert;
	var $onUpdate;
	var $onDelete;

	function &DBAbstractionLayer($sqlserver = '', $sqluser = '', $sqlpassword = '', $database = '', $persistency = null,$dbtype = 'MySQL')
	{
		$oEvent = new _Event($this);
		$this->oEvent = &$oEvent;
		
	    $this->persistency = ($persistency == null)?(("$this->persistency" != "")?($this->persistency):(true)):($persistency);
		$this->user = ("$sqluser" == "")?(("$this->user" != "")?($this->user):('root')):($sqluser);
		$this->password = ("$sqlpassword" == "")?(("$this->password" != "")?($this->password):('')):($sqlpassword);
		$this->server = ("$sqlserver" == "")?(("$this->server" != "")?($this->server):('localhost')):($sqlserver);
		$this->dbname = ("$database" == "")?(("$this->dbname" != "")?($this->dbname):('test')):($database);
		$this->dbtype = $dbtype;
  		if (parent::sql_db($this->server, $this->user, $this->password, $this->dbname, $this->persistency) && $this->db_connect_id>0){
  			$this->oEvent->fireEvent('Connect');} else{$this->oEvent->fireEvent('ConnectError');}
  		return $this;
	}
	/**
	* Return first element of first row of sql statement. Recordset is disposed
	* for you.
	*
	* @param sql			SQL statement
	* @param [inputarr]		input bind array
	*/
	function getOne($sqltext,$inputarr=false)
	{
		$ret = false;
		$rs = $this->getRow($sqltext,$inputarr);
		if (is_array($rs)) {
			$ret = $rs[0];
		}

		return $ret;
	}

	/**
	 * Execute SQL
	 *
	 * @param $sql		SQL statement to execute, or possibly an array holding prepared statement ($sql[0] will hold sql text)
	 * @param [$inputarr[]]	holds the input data to bind to. Null elements will be set to null.
	 * @param [$arg3]	reserved for john lim for future use
	 * @return array		RecordSet or false
	 */
	function Execute($sqltext,$inputarr=false)
	{
	    static $elements,$row;
		$result = $this->sql_query($sqltext);
		if ($inputarr != false){
		    $elements = $inputarr;
		} else {
			$elements = array();
		}
		while($row = $this->sql_fetch_object($result)){
		    $elements[] = $row;
		}
		return $elements;
	}
	
	/**
	*
	* ExecuteArray: Execute an sql statement and return the result as bidimentional array
	* @param string $sqltext text of the sql statement
	* @param array [$inputarr[]]	holds the input data to bind to. Null elements will be set to null.
	*/ 
	function ExecuteArray($sqltext,$inputarr=false)
	{
		$result = $this->sql_query($sqltext);
		if ($inputarr != false){
		    $elements = $inputarr;
		} else {
			$elements = array();
		}
		while($row = $this->sql_fetchrow($result)){
		    $elements[] = $row;
		}
		return $elements;
	}

	function getRow($sqltext,$inputarr=false)
	{
		$result = $this->sql_query($sqltext);
		$row = $this->sql_fetchrow($result);
		if ($inputarr != false){
		    $inputarr[] = $row;
		} else {
		    $inputarr = $row;
		}
		return $inputarr;
	}
	
	function getObject($sqltext,$inputarr=false)
	{
		$result = $this->sql_query($sqltext);
		$row = $this->sql_fetch_object($result);
		if ($inputarr != false){
		    $inputarr[] = $row;
		} else {
		    $inputarr = $row;
		}
		return $inputarr;
	}

	function sql_fetch_object($res, $nr=0)
	{
	    static $row_dbi_o2 = null;
	    $object = null;
	    $nf = $this->sql_numfields($res);
	    if ($row_dbi_o2 = $this->sql_fetchrow($res)){
	    	$ClassName = $this->dbtable;
	    	DBAbstractionLayer::declareRecordClass($ClassName);
	    	$object = new $ClassName($this);
	    	/*
		    for($count=0; $count < $nf; $count++) {
		    	$field_name = $this->sql_fieldname($count, $res);
		    	$field_value = $row_dbi_o2[$field_name];
		    	$object->$field_name = $field_value;
		    }
		    */
	    	$aFieldNames = $this->fieldnames;
	    	foreach ($aFieldNames as $field_name) {
		    	$field_value = $row_dbi_o2[$field_name];
		    	$object->$field_name = $field_value;
	    	}
		}
	    $this->fetch_object=(is_object($object))?($object):($this->fetch_object);
	    return (is_object($object))?($object):(false);
	}
	
	function declareRecordClass($ClassName)
	{
		static $dArtifacts;
		if (class_exists($ClassName)){$dArtifacts[$ClassName] = true;}
		if (empty($dArtifacts[$ClassName])){
			$oSmartyTemplate = new Smarty();
			_TTE::setSmartyConf($oSmartyTemplate);
			$oTemplate = new _TTE($oSmartyTemplate,'assign','display');
			$oTemplate->assign('ClassName',$ClassName);
			if (!in_array($ClassName,get_declared_classes())) {
				Stereotype::Dispatch('_','oRecord',$oTemplate);
			}
			$dArtifacts[$ClassName] = true;
		}
	}

	function getSqlFieldNames($sqltext)
	{
	    $res = $this->sql_query($sqltext);
	    $row_dbi_o2 = array();
	    $row_dbi_o2 = $this->sql_fetchrow($res);
	    $nf = $this->sql_numfields($res);
    	$this->sqlfieldnames = array();
	    for($count=0; $count < $nf; $count++) {
	    	$field_name = $this->sql_fieldname($count, $res);
	    	$this->sqlfieldnames[] = $field_name;
	    }
	    return $this->sqlfieldnames;
	}
	
	function getDatabaseTables(){
	    $this->databaseTables = $this->ExecuteArray($this->db_sqltablestr[$this->dbtype]);
	    return $this->databaseTables;
	}
	
	function fnLoad(){return true;}
	function fnArtifactLoad(){return true;}
	function fnConnect(){return true;}
	function fnDisconnect(){return true;}
	function fnQuery(){return true;}
	function fnExcecuteQuery(){return true;}
	function fnPager(){return true;}
	function fnSQLBuild(){return true;}
	function fnInsert(){return true;}
	function fnUpdate(){return true;}
	function fnDelete(){return true;}

	
	
	
	/******       AQUI COMIENZA EL ARTEFACTO                     *******/
	
	/**
	* 
	* Gets the artifact object as a table instance
	* 
	* @param string [$dbtable] name of the table that contains the data
	* @return object 
	*
	*/ 
	function &DBArtifact($dbtable = "")
	{
	    $this->dbtable = (empty($dbtable))?($this->dbtable):($dbtable);
		$this->sqlfilter = "";
		$this->db_sqlfieldstr['MySQL'] = "SHOW FIELDS FROM $this->dbtable";
		$this->db_sqlfieldstr['mysql4'] = "";
		$this->db_sqlfieldstr['postgres7'] = "SELECT 
				a.attname AS Field, 
				t.typname  || '(' || 
				CASE WHEN a.attlen<1 THEN a.atttypmod-4 ELSE a.attlen END || ')' AS Type, 
				CASE WHEN a.attnotnull THEN '' ELSE 'YES' END AS Null
			FROM pg_class c, pg_attribute a, pg_type t 
			WHERE c.relname = '$this->dbtable' and a.attnum > 0 and a.attrelid = c.oid and a.atttypid = t.oid
			ORDER BY a.attnum,a.attname";
		$this->db_sqlfieldstr['mssql'] = "";
		$this->db_sqlfieldstr['oracle'] = "";
		$this->db_sqlfieldstr['msaccess'] = "";
		$this->db_sqlfieldstr['db2'] = "";
		
		$this->db_sqltablestr['MySQL'] = "SHOW TABLES";
		$this->db_sqltablestr['mysql4'] = "";
		$this->db_sqltablestr['postgres7'] = "SELECT tablename as \"Tables_in_$this->dbname\" FROM pg_tables WHERE tablename NOT LIKE 'sql%' AND tablename NOT LIKE 'pg_%' ORDER BY tablename";
		$this->db_sqltablestr['mssql'] = "";
		$this->db_sqltablestr['oracle'] = "";
		$this->db_sqltablestr['msaccess'] = "";
		$this->db_sqltablestr['db2'] = "";
	    $this->sqlfields = implode(",",$this->getFieldsNames());
	    $this->getFieldsObject();
	    $this->getFieldsArray();
	    $this->getPrimaryKey();
	    $this->pager = new _Pager($this);
	    $this->oEvent->fireEvent('ArtifactLoad',$this);
	    return $this;
	}
	
	function &setTemplateEngine(&$oTemplate,$tteAssignMethod = null,$tteShowMethod = null){
		$this->oTemplate = new _TTE($oTemplate,$tteAssignMethod,$tteShowMethod);
		return $this->oTemplate;
	}
	
	
	function getRecord($r = -1){
		$this->getRecords();
	    if ($r < 0){$r = sizeof($this->records_object)-$r;}
		return (!empty($this->records_object))?($this->records_object[$r]):(false);
	}
	
	function getRecords(){
//		error_log(print "<pre>".$this->SQLBuild()."</pre>",3,"sqlSelect.log");
	    $this->records_object = $this->Execute($this->SQLBuild());
		return $this->records_object;
	}

	function getFieldsObject()
	{
		$this->fields = $this->Execute($this->db_sqlfieldstr[$this->dbtype]);
		$tt = sizeof($this->fields);
		for ($i=0;$i<$tt;$i++){
			if (isset($this->fields[$i]) && isset($this->fields[$i]->Field)){
				$fn = $this->fields[$i]->Field;
				$fields->$fn = $this->fields[$i];
				unset($this->fields[$i]);
			}
		}
		$this->fields = $fields;
		return $this->fields;
	}
	
	function getFieldsArray()
	{
		$this->fields_array = $this->ExecuteArray($this->db_sqlfieldstr[$this->dbtype]);
		$tt = sizeof($this->fields_array);
		for ($i=0;$i<$tt;$i++){
			$fieldname = (!empty($this->fields_array[$i]['Field']))?($this->fields_array[$i]['Field']):($this->fields_array[$i]['field']);
			$this->fields_array[$fieldname]=$this->fields_array[$i];
			unset($this->fields_array[$i]);
		}
		return $this->fields_array;
	}
	
	function getFieldsNames()
	{
		$fields = $this->getFieldsArray();
    	$this->fieldnames = array();
    	foreach($fields as $field){
	    	$field_name = (!empty($field['Field']))?($field['Field']):($field['field']);
	    	$this->fieldnames[] = $field_name;
	    }
		return $this->fieldnames;
	}
	
	function getHeaderFields(){
		$this->headerfields = $this->getSqlFieldNames($this->db_sqlfieldstr[$this->dbtype]);
		return $this->headerfields;
	}
	
	function getFieldAttributes($fieldname = '')
	{
	    if (!empty($fieldname)){
	    	$attributes = $this->fields->$fieldname;
	    	return $attributes;
		}else{ return false;}
	}
	
	function getPrimaryKey()
	{
		$fields = $this->getFieldsObject();
    	$this->fieldnames = array();
    	if (is_array($fields)){
	    	foreach($fields as $field){
				if (!empty($field->Key)){
		    		$this->primaryKey = $field;
		    	}
					return $this->primaryKey;
		    }
    	}
    	return null;
	}
	
	function setFieldsObjectValues(&$row,$fields){
		$values = $this->getArrayFieldValues($row,$fields);
		
	}
	
	function getArrayFieldValues($row,$fields,$onlyNonEmpty = false){
		$values = array();
		foreach($fields as $field){
			if ((!$onlyNonEmpty) || (!empty($row[$field]))){
            	$values[$field] = $row[$field];
			}
		}
		return $values;
	}
	
	function implodedUpdateValues($row,$fields,$onlyNonEmpty = false,$delim = ','){
		$fieldvalues = $this->getArrayFieldValues($row,$fields,$onlyNonEmpty);
		$fieldnames = $fields;
		$values = array();
		foreach ($fieldnames as $k=>$fieldname){
			if (empty($fieldvalues[$fieldname])){ // tratamiento especial para el valor nulo en update 
				$strfvalue = "$fieldname = ".$this->SpecialNull($fieldvalues[$fieldname])."";
			} else {
				$strfvalue = "$fieldname = '".$fieldvalues[$fieldname]."'";
			}
			$values[] = $strfvalue;
		}
		$values = implode($delim,$values);
		return $values;
	}
	
	function SpecialNull($ffv)
	{
		$aSpecialNull = array();
		$aSpecialNull['MySQL'] = "";
		$aSpecialNull['mysql4'] = "";
		$aSpecialNull['postgres7'] = "NULL";
		$aSpecialNull['mssql'] = "";
		$aSpecialNull['oracle'] = "NULL";
		$aSpecialNull['msaccess'] = "";
		$aSpecialNull['db2'] = "";
		$ffv = (empty($ffv))?($aSpecialNull[_DBTYPE]):($ffv);
		return $ffv;
	}	

	function SQLBuild($sqltype = 'SELECT',$newtable = '',$aFieldValues = null)
	{
	    $this->sqltype = $sqltype;
    	$this->sqlfields = (empty($this->sqlfields))?(implode(",",$this->getFieldsNames())):($this->sqlfields);
    	$this->newdbtable = ($newtable == '')?($this->dbtable):($newtable);
	    switch (strtoupper("$this->sqltype")){
	        case "SELECT":
	        $sqlfields = (empty($this->sqlfields))?("*"):($this->sqlfields);
	    	$this->sqltext = "SELECT $sqlfields FROM $this->dbtable ";
			if (!empty($this->sqlfilter))$this->sqltext	.=" WHERE $this->sqlfilter";
			$this->sqltext	.=" $this->sqlorderfields $this->sqldirorder";
	    	break;
	    	case "INSERT":
			$aValues = $this->getArrayFieldValues(($aFieldValues == null)?($this->getRow($this->SQLBuild())):($aFieldValues),$this->getFieldsNames());
	    	$this->sqlfieldvalues = "'".implode("','",$aValues)."'";
	    	/* Aplicar tratamiento especial para el valor nulo deshabilitado en el insert
	    	foreach ($aValues as $vv) {
	    		if (is_null($vv)){
	    			$this->sqlfieldvalues = str_replace("'".$vv."'",$this->SpecialNull($vv),$this->sqlfieldvalues);
	    		}
	    	}*/
	    	$this->sqltext = "INSERT INTO $this->newdbtable ($this->sqlfields) VALUES ($this->sqlfieldvalues) ";
	    	break;
	    	case "UPDATE":
	    	$updatevalues = $this->implodedUpdateValues(($aFieldValues == null)?($this->getRow($this->SQLBuild())):($aFieldValues),$this->getFieldsNames());
	    	$this->sqltext = " UPDATE $this->newdbtable SET $updatevalues ";
			if (!empty($this->sqlfilter))$this->sqltext	.=" WHERE $this->sqlfilter";
	    	break;
	    	case "DELETE":
	    	$this->sqltext = " DELETE FROM $this->newdbtable ";
			if (!empty($this->sqlfilter))$this->sqltext	.=" WHERE $this->sqlfilter";
	    	break;
	    }
	    return $this->sqltext;
	}
	/******       AQUI TERMINA EL ARTEFACTO                     *******/

	function __destruct(){
		unset($this);
	}
}
?>