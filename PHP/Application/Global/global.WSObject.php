<?php
/* vim: set expandtab tabstop=4 shiftwidth=4: */
 /**
  * @package Five Multipurpose Framework
  * @subpackage WSObject Class
  * 
  * @version "3.23.09.2011"
  * 
  * @copyright 2000-2020 Jean Machuca.
  * @author  "Jean Machuca" <jean@baytex.net>, "Baytex S.A." <info@baytex.net>
  * @license http://en.wikipedia.org/wiki/GNU_General_Public_License GPL
  * 
  * Represents a sigle WSObject with the Web Service information 
  * for an object oriented programming with a simple SOA implementation 
  * 
  * NOTE: This version 3.23.09.2011 is designed only for Nusoap compatible implementation, not SoapClient, in major of php versions, PHP5 inclusive
  * 
  */

$ini = ini_set("soap.wsdl_cache_enabled","0"); 
define ('ERROR_FILE',_APPWSSERVERDIR.'/logWS'.date("Ymd"));

class WSObject extends _Main {
	
	/**
	 * Determines if use the Nusoap lib for WS Client 
	 */
	var $use_nusoap = true;
	
	/**
	 * Determines if nusoap client uses a wsdl
	 */
	var $nusoap_wsdl = true;
	
	/**
	 * Indicates the path for the nusoap lib main file.
	 */
	var $nusoap_includefile = 'nusoap.php';
	
	/**
	 * Security var for to determine if the WSObject class is in instanciated or static mode.  
	 */
	var $construct;
	
	/**
	 * Indicates url of the wsdl document 
	 */
	var $wsdl;
	
	/**
	 * Indicates the endpoint url for to force this in case of the wsdl have problems with this.
	 */
	var $endpoint;

	/**
	 * Indicates the namespace of WS 
	 */
	var $namespace;

	
	/**
	 * Array with params for to call the web service operation  
	 */
	var $call_params = array();
	
	/**
	 * For compatibility mode 
	 */
	var $wsdl_params = array();
	
	/**
	 * Indicates the name of the web service operation 
	 */
	var $soap_func = '';
	
	/**
	 * For compatibility mode
	 */
	var $soap_params = array();
	
	/**
	 * Is the result object with the response var of the web service 
	 */
	var $result;
	
	/**
	 * Flag that indicates if the call process have errors 
	 */
	var $hasError = false;
	
	/**
	 * String with the last call errors 
	 */
	var $error_str;
	
	/**
	 * Flag indicates if the las operation call was fault 
	 */
	var $hasFault = false;
	
	/**
	 * String with the fault code of the last call 
	 */
	var $faultcode = '';
	
	/**
	 * String with the las fault description
	 */
	var $faultstring = '';
	
	/**
	 * Object with the last request of service   
	 */
	var $last_request;
	
	/**
	 * Object with the last response of service
	 */
	var $last_response;

	var $debug = true;
	
	/**
	 * Constructor
	 */
	function WSObject($wsdl,$wsdl_params = null,$soap_func = null,$call_params = null){
		$this->construct = true;
		$this->wsdl = $wsdl;
		$this->wsdl_params = $wsdl_params;
		$this->soap_func = $soap_func;
		$this->call_params = $call_params;
		$this->call();
		//print("Constructor WSObject <br>");
		parent::_Main();
	}

	function call(){
		if ($this->use_nusoap){
			//print("Llamada nusoap <br>");
			$this->nusoap_call();
		} else {
			//print("Llamada pear_soap <br>");
			$this->pear_soap_call();
		}
	}

	function pear_soap_call(){
		//print("Dentro pear_soap <br>");
		$soap_client = new SOAP_Client($this->wsdl);
		$this->result = $soap_client->call($this->soap_func,$this->call_params);		
	}
	
	/**
	 * Operation Call method 
	 */
	function nusoap_call()
		{
		$soap_client = new nusoap_client($this->wsdl,$this->nusoap_wsdl);
		$soap_client->setDebugLevel(0);
		//Indica que la codificacion es UTF-8
		$soap_client->soap_defencoding = 'UTF-8';
        $soap_client->decode_utf8 = true;
		
		$soap_client->endpoint = $this->endpoint;

		$tempResult = $soap_client->call($this->soap_func,$this->call_params,$this->namespace);

		$this->last_request = $soap_client->request;
		$this->last_response = $soap_client->responseData;
		
		$this->result = $tempResult; 
		
		WSObject::logMsg($this->result,get_class($this),$this->debug);
			
		WSObject::logMsg("SERVICE INFO: ",get_class($this),$this->debug);

		WSObject::logMsg($this->wsdl,get_class($this),$this->debug);
		WSObject::logMsg($this->soap_func,get_class($this),$this->debug);
		WSObject::logMsg($this->call_params,get_class($this),$this->debug);
		WSObject::logMsg($this->endpoint,get_class($this),$this->debug);
		WSObject::logMsg(var_export($soap_client->request,true),get_class($this),$this->debug);
		WSObject::logMsg("RESPONDE DATA: :: : : ".var_export($soap_client->responseData,true),get_class($this),$this->debug);
		WSObject::logMsg($soap_client->debug_str,get_class($this),$this->debug);
		
		
		// Check for a fault
		if ($soap_client->fault){
			//this line is executed when there is(are) a(some) fault(s)
			$this->hasFault    = true;
			$this->faultcode   = $tempResult['faultcode'];
			$this->faultstring = $tempResult['faultstring'];
			$this->error_str = $this->soap_func.": ".$this->faultcode." - ".$this->faultstring;
			WSObject::logMsg($this->soap_func.": ".$this->faultcode." - ".$this->faultstring,get_class($this),$this->debug);
		} else if ($soap_client->getError()){
			$err = $soap_client->getError();
			$this->hasError  = true;
			$this->error_str = $err;
			WSObject::logMsg($this->error_str,get_class($this),$this->debug);
		}
		
	}
		
	function hasSuccess(){
		if ($this->hasError or $this->hasFault){
			$result = false;
		} else {
			$result = true;
		}
		return $result;
	}
	
	/**
	 * Gets the acummulated error string 
	 */
	function getError(){
		if($this->error_str != ''){
			WSObject::logMsg($this->soap_func.": ".$this->error_str);
			return $this->error_str;
		}
		return false;
	}
	
	/**
	 * Gets the last request  
	 */
	function getLastRequest(){
		return $this->last_request;
	}
	
	/**
	 * Gets the last response object 
	 */
	function getLastResponse(){
		return $this->last_response;
	}
	
	/**
	 * For compatibility with Macromedia XML Array Standard and XML2Array Class 
	 */
	function array2class($XMLArray,$objectpath = "\$this->"){
		if (!$this->construct) return false;
		foreach ($XMLArray as $item){
			$child = $item['children'];
			$k_property = $item['name'];
			if (is_array($child)) {
				$this->array2class($child,$objectpath.$k_property."->");
			}
			if (isset($item['data']) && !empty($item['data'])){
				$v_property = $item['data'];
				$property = "$objectpath"."$k_property = \$v_property;";
				$valid_prop = str_replace("->","",$objectpath.$k_property);
				if (preg_match("/^[a-z0-9 ]*/",$valid_prop)){
					echo $property."<br>";
					eval($property);
				}
			}
		}
	}

	/**
	* Debug Log Messages
	*/
	function logMsg($msg,$className = null,$debug=true){
		if (is_null($className)){
			$error_file = ERROR_FILE;
		} else {
			$error_file = ERROR_FILE.$className;
		}
		$error_file = $error_file.".txt";

		if ($debug){
			@error_log(str_replace('><',">\n<",$msg)."\n",3,$error_file);
		}
	}
	
}

?>