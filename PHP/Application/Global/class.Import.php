<?PHP
/**
 * @package ExcelData Generico (Clase)
 * @desc Importa y Exporta datos en excel. (Reemplaza el uso de la class cargador y export)
 * @creation_date 24/07/2006
 * @version 2.0.20060724
 * @author  Jean Machuca.
 * @example 
 * 
 * 		// Una clase v�lida ser�a:
 * 		class tabla extends manten {
 * 			... 
 * 			// objeto cargador
 * 			var $cargador;
 * 			// titulo de la clase 
 * 			var $classTitle = 'Demo de carga de datos';
 * 
 * 			// guarda la instancia en la db
 * 			function save(){
 * 				...
 * 				return true;
 * 			}
 * 			// obtener la instancia de un registro
 * 			function get($id){
 * 				...
 * 				return $object;
 * 			}
 * 			// callback para realizar operaciones antes de guardar el registro. si devuelve false no se guarda el registro
 * 			function callbackExcelImport(&$class,&$line){
 * 				return true;
 * 			}
 * 
 * 			//invoca al cargador y realiza la carga de datos
 * 			function import(){
 * 				$this->cargador = new IMImport();
 * 				$this->cargador->import(get_class($this));
 * 			}
 * 		}
 * 
 */

if (!defined('INCLUDE_PATH')) {
	define('INCLUDE_PATH', '');
}

if (!defined('INCLUDE_ADMIN')) {
	define('INCLUDE_ADMIN', '../admin/');
}

require_once(INCLUDE_PATH."global/class.ExcelReader.php");	// PEAR Spreadsheet_Excel_Reader Algorithm Based
require_once(INCLUDE_PATH."global/class.ExcelData.php");	// ExcelData Generico (Clase)

class Import extends _ExcelData {

	/**
	 * Instancia de la clase controladora de la tabla
	 *
	 * @var stdClass $class
	 * @access static
	 */
	var $class;
	
	/**
	 * Nombre de la clase padre. La clase padre debe contener como propiedad el objecto $db 
	 *
	 * @var string $pclassname
	 * @access public
	 */
    var $pclassname = 'manten';
    
    /**
     * Nombre del m�todo que guarda la instancia en la base de datos.
     * 
     * @var string $saveMethod
     */
    var $saveMethod = 'save';
    
    /**
     * Nombre del m�todo que obtiene la instancia de un registro desde la base de datos.
     *
     * @var string $getMethod
     */
    var $getMethod = 'get';
    
    /**
     * Nombre del m�todo callback. Este m�todo decide si se guarda el registro o no.
     *
     * @var string $callbackMethod
     */
    var $callbackMethod = 'callbackExcelImport';
	
	/**
    * Funcion llamada al incio de la carga de la p�gina con el proposito de ejecutar alguna acci�n
    *
    * @param string cadena que define la acci�n a ejecutar
    * @access   public
    */
    function action (&$opc) {
		$this->clear_error(); // limpiar el estado de errores y mensajes
		switch ($opc) {
			
		    case "import":
				$this->setTable($_REQUEST["tablename"]);
				$classname = strtolower(trim($this->tableName));
			    $classprefix = substr($classname,0,4);
			    include_once(INCLUDE_PATH."class/class.$classname.php");
			    $this->class = new $classname();
			    $this->class->cargador = &$this;
                $this->class->import();
				$opc = "new";
                break;
        }

    }

    /**
    * Define lo que se muestra en el cuerpo del documento, llamada luego de 'action'
    *
    * @param string cadena que define el cuerpo a mostrar
    * @access   public
    */
    function body (&$opc) {
        switch ($opc) {
			case "new":
               $this->in_form();
               break;
            default:
			   $this->in_form();
                break;
        }
    }

	/**
    * Muestra un formulario de ingreso
    *
    * @access   public
    */
    function in_form () {
    	$tablas = $this->getClasses();
        $template = &App::getTemplate();
		$this->init_template($template);
		$template->assign('tablas',$tablas);
		$template->display('IMImport.tpl.html');
    }
}

/**
 * Database engine controller. busca independizar la mazcara de control de base de datos.
 * NOTA: nombre utilizado es EEDb
 */
class _EEDb {
	var $db;
	var $result;
	
	function EEDb(){
		include_once(INCLUDE_PATH."global/class.DB.php");
		$this->db = DB::getConnection();
		if (is_object($this->db)){
			return $this;
		} else {
			return null;
		}
	}
	
	function getRow($sql){
		if (is_object($this->db)){		
			return $this->db->getRow($sql);
		}else{
			return null;
		}
	}
	function query($sql){
		if (is_object($this->db)){		
			$this->result = $this->db->query($sql);
			return $this;
		}else{
			return null;
		}
	}
	function fetchRow(){
		if (is_object($this->db)){		
			return $this->result->fetchRow();
		}else{
			return null;
		}
	}
	
	function getOne($sql){
		return $this->db->getOne($sql);
	}
	
	function __destruct(){
		unset($this);
	}
}
?>