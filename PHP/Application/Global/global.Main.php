<?php
class Main extends _Main {
	public $content = "";
	public $oModule;
	
	public function Main(){
		$this->onDisplay = array(&$this,'Draw');
		$this->onError = array(&$this,'CustomErrorHandler');
		$this->LoadModule();
		parent::_Main();
	}
		
	public function Draw(){
		$this->assign('contenido',$this->content);
		return true;
	}
	
	public function CustomErrorHandler($errno = null, $errstr = null, $errfile = null, $errline = null){
//		if ($this->DEBUG){
			$msg = '==ERROR NIVEL 1==\n N: '.$errno.'\n Message: '.$errstr.'\n File: '.$errfile.'\n Line: '.$errline;
			@error_log(str_replace('><',">\n<",$msg)."\n",3,_APPWSSERVERDIR.date("Ymd").'_errorNivel_1.log');
//		}
		return true;
	}
	
	protected function AddContent($content = ""){
		$this->content .= $content;
	}
		
	protected function LoadModule(){
		define('_MODULEDIR',_APPMODULEDIR.'module.'.strtolower(get_class($this)).'/');
		if (file_exists(_MODULEDIR.'module.'.strtolower(get_class($this)).'.php')){
			include_once(_MODULEDIR.'module.'.strtolower(get_class($this)).'.php');
			$moduleName = 'MO_'.get_class($this);
			if (class_exists($moduleName)){
		 		$this->oModule = new $moduleName();
				return true;
			}
		}
		return false;
	}
	
	public function __destruct(){
		unset($this);
	}
}
?>