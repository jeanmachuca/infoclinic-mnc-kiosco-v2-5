<?php
//error_reporting(E_ALL);
/************************************************************************/
/* Portal_v265 (APWA): Automation Platform for Web Aplications          */
/* ============================================================         */
/*                                                                      */
/* Copyright (c) 2003-2010 by Jean Machuca                              */
/*                                                                      */
/*                                                                      */
/* Last Rev.: jan 01 2006                                               */
/*                                                                      */
/*                                                                      */
/************************************************************************/

/**
*
* @abstract class DBReccord
* @name DBReccord
*
*/
class DBRecord {
	
	// properties
	var $_oArtifact = null;
	var $_nullArtifact = null;
	var $_oXML2Form;
	var $_originalFilter = "";

	// events
	var $_oEvent;
	var $_onSave;
	var $_onInsert;
	var $_onUpdate;
	var $_onDelete;
	var $_onList;
	var $_onRender;
	var $_onPager;
	var $_onRefresh;
	var $_onRaiseError;

	function DBRecord($oArtifact)
	{
		$oEvent = new _Event($this,'_on');
		$this->_oEvent = &$oEvent;
		$this->_oArtifact = &$oArtifact;
		$this->_originalFilter = $this->_oArtifact->sqlfilter;
		$this->SetNull();
	}
	
	function fnRaiseError($msg = '')
	{
		static $eMsg;
		$eMsg .= $msg;
		return $eMsg;
	}
	
	function SetNull()
	{
		$aFieldNames = $this->_oArtifact->getFieldsNames();
		foreach ($aFieldNames as $aV) {$oRecord->$aV = null;}
	}

	function Save()
	{
		if (is_object($this)){
			if (($this->Update() == 0) && ($this->Insert() < 0)){
				$this->_oEvent->fireEvent('RaiseError','Save Error');
			}
		} else {
			$this->_oEvent->fireEvent('RaiseError','No object constructor called');
		}
		
	}
	
	function Insert()
	{
		if (is_object($this)){
			$q = $this->_oArtifact->SQLBuild('INSERT',null,$this->HashInstance());
			$this->_oArtifact->getOne($q);
			$ret = $this->_oArtifact->sql_affectedrows();
			return $ret;
		} else {
			$this->_oEvent->fireEvent('RaiseError','No object constructor called');
		}
		
	}
	
	function Update()
	{
		if (is_object($this)){
			$q = $this->_oArtifact->SQLBuild('UPDATE',null,$this->HashInstance());
//			print "<pre>".$q."</pre>";
			$this->_oArtifact->getOne($q);
			$ret = $this->_oArtifact->sql_affectedrows();
			return $ret;
		} else {
			$this->_oEvent->fireEvent('RaiseError','No object constructor called');
		}
		
	}
		
	function Delete()
	{
		if (is_object($this)){
			$q = $this->_oArtifact->SQLBuild('DELETE',null,$this->HashInstance());
			$this->_oArtifact->getOne($q);
			$ret = $this->_oArtifact->sql_affectedrows();
			return $ret;
		} else {
			$this->_oEvent->fireEvent('RaiseError','No object constructor called');
		}
		
	}	
	
	function Render()
	{
		if (is_object($this)){
			
		} else {
			$this->_oEvent->fireEvent('RaiseError','No object constructor called');
		}
		
	}
	
	function Pager()
	{
		if (is_object($this)){
			
		} else {
			$this->_oEvent->fireEvent('RaiseError','No object constructor called');
		}
		
	}
	
	function Refresh($sA = '')
	{
		if (is_object($this)){
			$sArtifact = ($sA == '')?($this->_oArtifact->dbtable):($sA);
			$this->_oArtifact = $this->_oArtifact->DBArtifact($sArtifact);
			$this->_oArtifact->sqlfilter = $this->_originalFilter;
		} else {
			$this->_oEvent->fireEvent('RaiseError','No object constructor called');
		}
		
	}	
	
	function HashInstance()
	{
		$hash = array();
		if (is_object($this)){
			foreach ($this as $fk=>$fv) {if (substr($fk,0,1) != '_'){$hash[$fk] = $this->$fk;}}
		} else {
			$this->_oEvent->fireEvent('RaiseError','No object constructor called');
		}
		return $hash;
	}
	
	function InstanceHash($hash)
	{
		if (is_object($this)){
			foreach ($hash as $fk=>$fv) {if (substr($fk,0,1) != '_'){$this->$fk = $hash[$fk];}}
		} else {
			$this->_oEvent->fireEvent('RaiseError','No object constructor called');
		}
	}
	
	function getElements($sArtifact)
	{
		if (is_object($this)){
			$oArt = $this->_oArtifact->DBArtifact($sArtifact);
			$aArts = $oArt->getRecords();
			$aA = array();
			foreach ($aArts as $aO){
				$aA[] = $aO->HashInstance();
			}
			$this->Refresh();
			return $aA;
		} else {
			$this->_oEvent->fireEvent('RaiseError','No object constructor called');
		}
	}
	
	function XMLFields2Template(&$template,$plantilla = null,$render = true){
   		$this->_oXML2Form = new _XML2Form($this,$template,$plantilla);
    	if (empty($this->_oXML2Form->XMLErrorStr)){
    		$this->_oXML2Form->process($render);
    		return true;
    	} else {
    		echo ("Se ha encontrado el siguiente error en la definición del formulario: <br/>".$this->_oXML2Form->XMLErrorStr);
    		exit;
    		return false;
    	}
	}
	
	function getFieldHTML($fKey){
		if (is_object($this)){
			return $this->_oXML2Form->getFieldHTML($fKey);
		} else {
			$this->_oEvent->fireEvent('RaiseError','No object constructor called');
		}
	}
	
	function getFieldObj($fKey){
		if (is_object($this)){
			return $this->_oXML2Form->getField($fKey);
		} else {
			$this->_oEvent->fireEvent('RaiseError','No object constructor called');
		}
	}
	
	function getErrorMsg(){
		return $this->fnRaiseError();
	}
	
	function setFieldFilter($sFilter,$delim = " AND ",$onlyReturn = false){
		$aFieldFilter = array();
		$aHash = $this->HashInstance();
		foreach ($aHash as $k=>$v){
			$oField = $this->_oXML2Form->getField($k);
			if (isset($oField->FieldFilter) && $oField->FieldFilter == $sFilter) { // filter = 'primary'
				$aFieldFilter[$k] = $v;
			}
		}
		$fieldFilter = $this->_oArtifact->implodedUpdateValues($aFieldFilter,array_keys($aFieldFilter),false,$delim);
		if (!$onlyReturn) {$this->_oArtifact->sqlfilter = $fieldFilter;}
		return $fieldFilter;
	}

	function __destruct(){
		unset($this);
	}
}
?>