<?php

/**
 * oXML XML Object Handler (JMA: 20061114)
 * (Experimental)
 *
 */
class oXML {
	var $XMLString;
	var $XMLFile;
	var $XMLClass;
	var $xml2array;
	var $XMLErrorStr;
	var $XMLErrorLine;
	var $oXMLClass;
	
	function oXML(&$oClass){
		$this->XMLClass = &$oClass;
	}
	
	function _buildProcess($XMLClass){
		$array2obj = new _Array2Obj();
		$array2obj->reverse($this->oXMLClass);
		$XMLString = '';
		foreach ($XMLClass as $CHILDName=> $XMLChild) {
			if (is_object($XMLChild)){
				$XMLString .= $this->_buildProcess($XMLChild);
			} else {
				$XMLString .= '<'.$CHILDName.' >';
			}
		}
	}
	
	function reverse(){
		$this->xml2array = new _XML2Array($this->XMLString);
		$this->xml2array->onerror = XML2ARRAY_ONERROR_CANCEL;
		$this->xml2array->input_encodding = "ISO-8859-1";
		$this->xml2array->parse();
		$this->XMLErrorStr = $this->xml2array->error_str;
		$this->XMLErrorLine = $this->xml2array->error_line;
		$array2obj = new _Array2Obj();
		$this->oXMLClass = $array2obj->process($this->xml2array->stack);
	}
}

//END
?>