<?php
class Auth extends _Main {
	public $oTemplate;
	public $sLogin;
	public $sPassword;
	public $vLogin;
	public $vPassword;
	public $sTable;
	private $oLogin;
	private $oModule;
	
	public $onLogin;
	public $onLogout;
	
	public function Auth($oModule,$sTable,$sLogin,$sPassword){
		$this->onRender = create_function(null,'return false;');
		if (!isset($this->oModule->onLogin)){
			$this->onLogin=create_function($null,'return true;');
		} else {
			$this->onLogin = &$this->oModule->onLogin;
		}
		if (!isset($this->oModule->onLogout)){
			$this->onLogout=create_function($null,'return true;');
		} else {
			$this->onLogout = &$this->oModule->onLogout;
		}
		$this->sLogin = $sLogin;
		$this->sPassword = $sPassword;
		$this->sTable = $sTable;
		$this->oModule = $oModule;
		$this->onLoad = array($this,'formLogin');
		$this->onPostRequest = array($this,'requestLogin');
		$this->onAction = array($this,'actionLogin');
		parent::_Main();
	}
	
	public function &Login($vLogin,$vPassword){
		if (!empty($vLogin) && !empty($vPassword)){
			$vPassword = $this->CryptPassword($vPassword);
			$oArtifact = $this->oModule->getArtifact($this->sTable);
			$oArtifact->sqlfilter = " $this->sLogin = '$vLogin' AND $this->sPassword = '$vPassword' ";
			$o = $oArtifact->getRecord(0);
			if (is_object($o) && $this->getEvent()->fireEvent('Login',$o)){
				$oResult = $o->HashInstance();
				$_SESSION['ISLOGON'][$this->sTable] = true;
				$_SESSION['Auth'][$this->sTable] = &$oResult;
			} else {
				$o = false;
			}
		} else {
			$o = $this->Current();
		}
		return $o;
	}
	
	public function formLogin(){
		$this->oLogin = $this->Current();
		return true;
	}
	
	public function actionLogin($action){
		if ($action != 'PostRequest'){
//			_Form::forceRedirect("#?auth=login",'POST');
			$this->requestLogin('login');
		}
		return true;
	}
	
	public function requestLogin($auth = null){
		$this->oLogin = $this->Login($_REQUEST[$this->sLogin],$_REQUEST[$this->sPassword]);
		if ($_REQUEST['auth'] == 'logout'){$this->Logout();}
		if (!is_object($this->oLogin)){
			if (is_null($auth)) {$_REQUEST['auth'];}
			switch ($auth){
				case 'login':
					if (isset($_REQUEST[$this->sLogin])){
//						_Form::forceRedirect("#?auth=noLogin&".$this->sLogin."=".$_REQUEST[$this->sLogin],'POST');
						$this->requestLogin('noLogin');
					}
					if (!is_object($this->oLogin)){
						$this->oModule->setTemplateFile('Login.html');
					}
					break;
				case 'noLogin':
					$this->oModule->assign('errorLoginMsg','Datos de acceso incorrectos!');
					$this->oModule->setTemplateFile('Login.html');
					break;
				default:
//				_Form::forceRedirect("#?auth=login",'POST');
				$this->requestLogin('login');
			}
		}
		return true;
	}
	
	public function Logout(){
		if ($this->getEvent()->fireEvent('Logout',$oResult)){
			unset($_SESSION['ISLOGON'][$this->sTable]);
			unset($_SESSION['Auth'][$this->sTable]);
			_Form::forceRedirect("#");
		}
	}
	
	public function CryptPassword($vPassword){
		return _Crypt::encrypt($vPassword);
	}
	
	public function &Current(){
		$ClassName = $this->sTable;
    	DBAbstractionLayer::declareRecordClass($ClassName);
		$oResult = &$_SESSION['Auth'][$this->sTable];
		if (is_array($oResult)) {
			$oArtifact = $this->oModule->getArtifact($this->sTable,true);
	    	$o = new $ClassName($oArtifact);
	    	$o->InstanceHash($oResult);
	    	$vLogin = $oResult[$this->sLogin];
	    	$vPassword = $oResult[$this->sPassword];
	    	$o->_oArtifact->sqlfilter = " $this->sLogin = '$vLogin' AND $this->sPassword = '$vPassword' ";
		} else {
			$o = false;
		}
		return $o;
	}
	
	public function __destruct(){
		unset($this->oArtifact);
		unset($this);
	}
	
	public function fnLogin(){return true;}
	public function fnLogout(){return true;}
}
?>