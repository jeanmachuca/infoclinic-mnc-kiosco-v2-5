<?php
/**
 * Instancia de configuración principal del módulo
 * @internal  
 *
 */
class CO_MO_Miniclinic extends Config {
	function CO_MO_Miniclinic(){
		$this->Theme->name = 'default';
		return parent::Config();
	}
	function __destruct(){
		unset($this);
	}
}
?>