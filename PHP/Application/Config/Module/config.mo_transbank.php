<?php
/**
 * Instancia de configuración principal del módulo
 * @internal  
 *
 */
class CO_MO_Transbank extends Config {
	function CO_MO_Transbank(){
		$this->Theme->name = 'default';
		return parent::Config();
	}
	function __destruct(){
		unset($this);
	}
}
?>