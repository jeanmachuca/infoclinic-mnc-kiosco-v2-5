<?php
/* vim: set expandtab tabstop=4 shiftwidth=4: */
/**
 * Project: XML2Array 1.0
 *
 * @author  <Jean Machuca>
 * @package XML2Array 1.0
 * @subpackage 
 */

if (!defined('XML2ARRAY_ONERROR_IGNORE')) {
	define('XML2ARRAY_ONERROR_IGNORE', 0);
}

if (!defined('XML2ARRAY_ONERROR_CANCEL')) {
	define('XML2ARRAY_ONERROR_CANCEL', 1);
}

if (!defined('XML2ARRAY_ONERROR_EXIT')) {
	define('XML2ARRAY_ONERROR_EXIT', 2);
}

class _XML2Array {
	var $construct = false;
	var $file = "";
	var $stack = array();
	var $xml_parser;
	var $i = 0;
	var $parser_file;
	var $fp;
	var $string_data = array();
	var $is_file;
	var $nodename;
	var $error_str;
	var $error_line = 0;
	var $onerror = true;
	
	function _XML2Array($file){
		$this->file = $file;
		$this->is_file = (@is_file($this->file) && file_exists($this->file))?(true):(false);
		$this->construct = true;
	}
	
	function readline(){
		if ($this->is_file) {
			$data = fread($this->fp, 4096);
		}else {
			$data = $this->string_data[$this->i++];
		}
		return $data;
	}
	
	function isEof(){
		if ($this->is_file){
			$ret = feof($this->fp);
		} else {
			$ret = ($this->i >= sizeof($this->string_data));
		}
		return $ret;
	}
	
	function parse(){
		if (!$this->construct) return false;
		
		if (!$this->is_file){$this->string_data = $this->split_string($this->file,4096);}
		
		if (!(list($this->xml_parser, $this->fp) = $this->new_xml_parser())) {
		   die("No se pudo procesar el XML");
		}
		
		while ($data = $this->readline()) {
		   if (!xml_parse($this->xml_parser, $data, $this->isEof())) {
		   	$this->error_line = xml_get_current_line_number($this->xml_parser);
		    $this->error_str = sprintf("XML ERROR: %s en linea %d\n ",
		                   xml_error_string(xml_get_error_code($this->xml_parser)),
		                   $this->error_line);
		       switch ($this->onerror){
		       	case XML2ARRAY_ONERROR_CANCEL:
		       		return $this->stack;
		       		break;
		       	case XML2ARRAY_ONERROR_EXIT:
		       		return false;
		       		break;
		       	case XML2ARRAY_ONERROR_IGNORE:
		       	default:
		       		break;
		       }
		   }
		}
		xml_parser_free($this->xml_parser);
		return $this->stack;
	}
	
	function startElement($parser, $name, $attribs)
	{
		if (!$this->construct) return false;
	   $countdata = count($this->stack)-1;
	   $parent_name = (isset($this->stack[$countdata]['name']))?($this->stack[$countdata]['name']):('');
	   $tag=array("name"=>$name,"attrs"=>$attribs,"parent_name"=>$parent_name); 
	   array_push($this->stack,$tag);
	}
	
	function endElement($parser, $name)
	{
		if (!$this->construct) return false;

	   $countdata = count($this->stack)-1;
	   $this->stack[$countdata-1]['children'][] = $this->stack[$countdata];
       $this->stack[$countdata]['parent_name'] = (isset($this->stack[$countdata-1]['name']))?($this->stack[$countdata-1]['name']):('');
	   array_pop($this->stack);
	}
	
	function characterData($parser, $data)
	{
		if (!$this->construct) return false;
	   
	   
	   if(trim($data))
	   {  
	   			 $countdata = count($this->stack)-1;
	             if (isset($this->stack[$countdata]['data'])) {$this->stack[$countdata]['data'] .= $data;} else {$this->stack[$countdata]['data'] = $data;}
	             $this->stack[$countdata]['parent_name'] = $this->stack[$countdata-1]['name'];
	   }
	}
	
	function PIHandler($parser, $target, $data)
	{
		if (!$this->construct) return false;
	   if ((strtolower($target)) == "php") {
	         $text .= '<?php '.$data.' ?>';  
	         ob_start();
	           eval ('?>' . $text);
	           $text = ob_get_clean();
	           $this->stack[count($this->stack)-1]['data'] .= $text;
	   }
	}
	
	function defaultHandler($parser, $data)
	{
	   if (!$this->construct) return false;
	}
	
	function externalEntityRefHandler($parser, $openEntityNames, $base, $systemId,$publicId) {
	   if (!$this->construct) return false;
	   if ($systemId) {
	       if (!list($parser, $fp) = new_xml_parser($systemId)) {
	           printf("No se pudo abrir la entrada %s en %s\n", $openEntityNames,
	                   $systemId);
	           return false;
	       }
	       while ($data = fread($fp, 4096)) {
	           if (!xml_parse($parser, $data, feof($fp))) {
	               printf("XML ERROR: %s en l�nea %d mientras se procesaba la entrada %s\n",
	                       xml_error_string(xml_get_error_code($parser)),
	                       xml_get_current_line_number($parser), $openEntityNames);
	               xml_parser_free($parser);
	               return false;
	           }
	       }
	       xml_parser_free($parser);
	       return true;
	   }
	   return false;
	}
	
	function new_xml_parser()
	{
		if (!$this->construct) return false;
	   $this->xml_parser = xml_parser_create();
       xml_set_object($this->xml_parser, $this);
	   
	   xml_parser_set_option($this->xml_parser, XML_OPTION_CASE_FOLDING, 0);
	   
	   xml_set_processing_instruction_handler($this->xml_parser, "PIHandler");
	   
	   xml_set_element_handler($this->xml_parser, "startElement", "endElement");
	   
	   xml_set_character_data_handler($this->xml_parser, "characterData"); 
	   
	   xml_set_default_handler($this->xml_parser, "defaultHandler");
	   
	   xml_set_external_entity_ref_handler($this->xml_parser, "externalEntityRefHandler");
	  
	   if ($this->is_file && !($this->fp = @fopen($this->file, "r"))) {
	       return false;
	   }
	   if (!is_array($this->parser_file)) {
	       settype($this->parser_file, "array");
	   }
	   $this->parser_file[$this->xml_parser] = $this->file;
	   return array($this->xml_parser, $this->fp);
	}
	
   function split_string($string,$split_length=1){ 
       $count = strlen($string);  
       if($split_length < 1){ 
           return false;  
       } elseif($split_length > $count){ 
           return array($string); 
       } else { 
           $num = (int)ceil($count/$split_length);  
           $ret = array();  
           for($i=0;$i<$num;$i++){  
               $ret[] = substr($string,$i*$split_length,$split_length);  
           }  
           return $ret; 
       }      
   }

	function subarray($xmla,$node){
		foreach ($xmla as  $item) {
			if ($item['name'] == $node){
				return  $item;
			} else if (is_array($item)){
	           return $this->subarray($item,$node);
			}
		}
		return $ret;
	}
}

?>