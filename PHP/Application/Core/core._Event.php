<?php
/**
* @package Event class
* @author Jean Machuca
* @version 1.0.20060729
*
*/
class _Event {
	private $aEvents = array();
	private $eventPrefix = 'on';
	private $functionPrefix = 'fn';
	
	public function _Event(&$oClass,$eventPrefix = 'on',$functionPrefix = 'fn'){
		$this->eventPrefix = $eventPrefix;
		$this->functionPrefix = $functionPrefix;
		if ($this->SetDefaultEvents($oClass) && $this->CatchEventAll($oClass)){
			return $this;
		} else {
			return null;
		}
	}
	
	/**
	* FireEvent the indicated event
	* 
	* @param string $eventName Name of the event
	* @param array $eventParams array Event params. 
	*/
	public function &FireEvent($eventName,$eventParams = array()){
		$UserCall = false;
		if (is_object($this) && isset($this->aEvents[$eventName]) && is_callable($this->aEvents[$eventName])){
			$UserCall = call_user_func_array($this->aEvents[$eventName],$eventParams);
		}
		return $UserCall;
	}
	
	/**
	* CatchEvents a user call for an indicated event
	* 
	* @param string $eventName Name of the event
	* @param string,array $userMethod string or array of the method call
	*/
	private function CatchEvent($eventName,&$userMethod){
		if (is_object($this) && strtolower(gettype($eventName)) == 'string' && $eventName != ""){
			$this->aEvents[$eventName] = &$userMethod;
			return true;
		} else {
			return false;
		}
	}
	
	/**
	* 
	* Automatic CatchEvents the object events in extended object of event class or return the event object with the CatchEvented events
	* @param object $oClass instance of a class
	* @param string $eventPrefix prefix of the class var name with contains the event
	* @return object instance of event class with the CatchEvented events
	*/
	private function CatchEventAll(&$oClass){
		if (is_object($this)){
			$aClassVars = get_class_vars(get_class($oClass));
			$aClassVarNames = array_keys($aClassVars);
			foreach ($aClassVarNames as $varName){
				if (!is_numeric($varName) && (substr($varName,0,strlen($this->eventPrefix)) == $this->eventPrefix)){
					$eventName = substr($varName,strlen($this->eventPrefix));
					$this->CatchEvent($eventName,$oClass->$varName);
				}
			}
			return $oClass;
		} else {
			return false;
		}
	}

	/**
	* 
	* Automatic set the prefixEvent type events to Event method of the given reference class instance
	* @param object $oClass instance of a reference class
	* @param string $eventPrefix prefix of the class var name with contains the event
	* @return object instance of event class with the CatchEvented events
	*/
	private function SetDefaultEvents(&$oClass){
		if (is_object($this)){
			$aClassVars = get_class_vars(get_class($oClass));
			$aClassVarNames = array_keys($aClassVars);
			foreach ($aClassVarNames as $varName){
				if (!is_numeric($varName) && (substr($varName,0,strlen($this->eventPrefix)) == $this->eventPrefix)){
					$eventName = substr($varName,strlen($this->eventPrefix));
					if ($oClass->$varName == null) $oClass->$varName = array(&$oClass,$this->functionPrefix.$eventName);
				}
			}
			return $oClass;
		} else {
			return false;
		}
	}
	
	public function __destruct(){
		unset($this);
	}
}
?>