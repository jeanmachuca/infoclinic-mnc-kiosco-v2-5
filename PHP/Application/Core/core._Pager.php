<?php
class _Pager
{
    public $_id;
    public $_maxRows = 2;
    public $_jumps = 7;
    public $_currentPage = 1;
    public $_sql;
    public $_rows;
    public $_pages;
    public $_hiddens;
    public $_acceptDuplicateKeys = false;
    public $_autoHiddenList = false;
	
	public $artifact;
	public $tblattr = '';
	public $headerarray = array();
	public $elements = array();
	public $sqlListBuild = '';
	public $template;
	public $output = "";
	public $paginator = "";
	public $_method = "GET";
	public $_staticColumnList = array();
	
	function &_Pager($artifact,$maxrows = 2){
	    $this->artifact = $artifact;
		$this->_hiddens = array();
        $this->setMaxRows($maxrows);
        $this->setAcceptDuplicateKeys();
		$this->setAutoHiddenList();
	    $this->_id = $this->getNewId();
        $this->_currentPage = (int) $_REQUEST["page".$this->_id] + empty($_REQUEST["page".$this->_id]);
//		$this->addHidden("_sArtifact",$this->artifact->dbtable);
		$this->Reload();
		return $this;
	}
	
	function Reload(){
	    $this->sqlListBuild = $this->artifact->SQLBuild();
		$this->process($this->sqlListBuild);
		$this->headerarray = $this->artifact->getSqlFieldNames($this->sqlListBuild);
		$this->elements = $this->artifact->Execute($this->sqlListBuild);
	}
	
	function saveGrid(){
		$oArtifact = $this->artifact;
		$sArtifact = $oArtifact->dbtable;
		$aList = $_REQUEST['list'];
    	DBAbstractionLayer::declareRecordClass($sArtifact);
		$oRecord = new $sArtifact($this->artifact);
		if (is_array($aList)){
			foreach ($aList as $recordLine => $HashInstance){
				$oXML2Form = $oRecord->_oXML2Form;
				$oRecord = new $sArtifact($oArtifact);
				$oRecord->InstanceHash($HashInstance);
				$oRecord->XMLFields2Template($this->template,'list',false);
				$oRecord->Refresh($sArtifact);
				$oRecord->setFieldFilter('primary');
				$oRecord->Save();
			}
		}
	}
	
	function &setTemplateEngine(&$template,$tteAssignMethod = null,$tteShowMethod = null){
		$this->template = new _TTE($template,$tteAssignMethod,$tteShowMethod);
		return $this->template;
	}
	
	function setFormMethod($method = 'GET'){
		$this->_method = $method;
	}
	
	function StartForm($attrs = null){
		$sAttrs = "";
		foreach ($attrs as $attr => $vattr){
			$sAttrs .= " ".$attr."=\"".$vattr."\"";
		}
		$sForm = "<FORM$sAttrs >";
		foreach($this->_hiddens as $hidden) {
				$hname = $hidden['name'];
				$hvalue = $hidden['value'];
	            $sForm .= "<input type=\"hidden\" name=\"$hname\" value=\"$hvalue\" />";
	    }		
		return $sForm;
	}
	
	function EndForm (){
		return "</FORM>";
	}
	
	
	function Render($outputname = '',$footerpage = '', $show = false)
	{
		$gHtml = $this->getHTML($this->elements);
		$this->addHidden("page$this->_id",$_REQUEST["page".$this->_id]);
		$startForm = $this->StartForm(array('name'=>'grid'.$this->_id,'action'=>'#','enctype'=>'multipart/form-data','method'=>$this->_method));
		$endForm = $this->EndForm();
		$this->output = $startForm.$gHtml.$endForm;
		/* pages */
		$this->paginator = "";
      	if ($this->isDisplay()){
      		$this->paginator .= "<SPAN class=\"grid\" id=\"pagerLoadding\" name=\"pagerLoadding\" style=\"display:none\"><TABLE class=\"grid\" ><TR><TD >CARGANDO</TD></TR></TABLE></SPAN>";
			$this->paginator .= "<script>function goPage(id, page) {document.body.style.alpha=1;pagerLoadding.style.display = 'block';pagerLoadding.style.height='500px';form = document.forms['paginator' + id];form.page$this->_id.value = page;form.submit();return false;}</script>";
			$this->paginator .= $this->StartForm(array('class'=>'paginator'.$this->_id,'method'=>$this->_method,'name'=>'paginator'.$this->_id));
			$range = $this->pageRange();
			$firstPage = $this->getPage('FIRST');
			$lastPage = $this->getPage('LAST');
	  		$this->paginator .= "<a onClick=\"return goPage($this->_id, ".strval($firstPage).");\" href=\"javascript:;\" class=\"grid\">".htmlentities("<<")."</a> |";
  			foreach($range as $page) {
			    if($page->number==$_REQUEST["page".$this->_id]) {
					$numbe1 = "<b>".$page->number."</b>";
			    } else {$numbe1 = $page->number;}
		  		$this->paginator .= "<a onClick=\"return goPage($this->_id, ".strval($page->number).");\" href=\"javascript:;\" class=\"grid\">$numbe1</a> |";
  			}
	  		$this->paginator .= "<a onClick=\"return goPage($this->_id, ".strval($lastPage).");\" href=\"javascript:;\" class=\"grid\"> ".htmlentities(">>")." </a>";
  			$this->paginator .= $this->EndForm();
      	}
      	/* end pages */
		
	    if (!empty($outputname)){
	    	$this->template->assign($outputname,$this->output);
	    	$this->template->assign($footerpage,$this->paginator);
		    if ($show && !$this->template->show()){
		    	print $this->output; // not template show possible
			}
	    }
	    return $this->output;
	}
	
	function addStaticColumn($cname,$ctitle,$sText){
		$this->_staticColumnList[$cname] = array();
		$this->_staticColumnList[$cname]['title'] = $ctitle;
		$this->_staticColumnList[$cname]['body'] = $sText;
	}
	
	function getHTML(&$elements,$ztabhtml='',$ztrhtml='',$zthhtml='',$ztdhtml='')
	{
	
	    $ztabhtml = $this->tblattr;
	    $zheaderarray = $this->headerarray;

		if (!$ztabhtml) $ztabhtml = 'BORDER=1';

		$s = "<TABLE class=\"grid\" $ztabhtml>";
		for ($i=0; $i<sizeof($elements) && $i<1; $i++) {
			$s .= "<TR $ztrhtml >";
			$oRecord = $elements[$i];
			if (is_object($oRecord)){
				$oRecord->XMLFields2Template($this->template,'list');
				$aRecord = $oRecord->HashInstance();
				foreach($aRecord as $sKey=>$oValue){
					$oField = $oRecord->getFieldObj($sKey);
					if (is_object($oField) && (empty($oField->FieldVisibility) || strtolower($oField->FieldVisibility) == 'visible')){
						$oField->DisplayLabel = htmlentities(iconv('utf-8','iso-8859-1',$oField->DisplayLabel));
						$s .= "	<TH >{$oField->DisplayLabel}</TH>\n";
				    }
				}
				foreach ($this->_staticColumnList as $cname=>$cc) {
					$s.="<TH>".$cc['title']."</TH>";
				}
				
			}
			$s .= "\n</TR>\n";
		}
		for ($i=0; $i<sizeof($elements); $i++) {
			$s .= "<TR $ztrhtml >";
			$oRecord = $elements[$i];
			if (is_object($oRecord)){
				$oRecord->XMLFields2Template($this->template,'list');
				$aRecord = $oRecord->HashInstance();
				$RecordLine = intval(($this->_maxRows * $this->_currentPage) - $this->_maxRows + $i);
				foreach($aRecord as $sKey=>$oValue){
					$oField = $oRecord->getFieldObj($sKey);
					if (is_object($oField)){
						$oField->RecordLine = $RecordLine;
					    $s .= $oField->getHTML();
						if ($this->_autoHiddenList && $oField->FieldVisibility == 'hidden'){
							$this->addHidden("list[".$RecordLine."][".$oField->FieldName."]",$oField->FieldValue);
						}
					}
				}
				foreach ($this->_staticColumnList as $cname=>$cc) {
					$cc['body'] = str_replace('#RecordLine#',$RecordLine,$cc['body']);
					$s.="<TH>".$cc['body']."</TH>";
				}
			}else if ($oRecord) {
				$s .=  "	<TD $ztdhtml>".$oRecord."</TD>\n";
			} else {$s .= "	<TD $ztdhtml>&nbsp;</TD>\n";}
			
			$s .= "\n</TR>\n";
		}
		$s .= '</TABLE>';
		return $s;
	}
	
	function setAutoHiddenList($autoHiddenList = true){
		$this->_autoHiddenList = ($autoHiddenList == true)?(true):(false);
	}
	
	/**** funciones propias del paginado*****/

	function addHidden($name, $value = null) {
		if (is_null($value) && isset($_REQUEST[$name])) {$value = $_REQUEST[$name];}
		if ($this->_acceptDuplicateKeys) {
			$this->_hiddens[] = array("name" => $name, "value" => $value);
		} else {
			$this->_hiddens[$name] = array("name" => $name, "value" => $value);
		}
	}

	function setAcceptDuplicateKeys($acceptDuplicateKeys = true) {
		$this->_acceptDuplicateKeys = ($acceptDuplicateKeys == true)?(true):(false);
	}
    
    function getNewId() {
		static $count;
		$count = intval($count);
		return $count++;
	}
    
	function setMaxRows($maxRows) {
	    $this->_maxRows = (int) $maxRows;
	}
	
    function process(&$sql) {
		$this->_sql = &$sql;
		$this->_process();
		$this->_refreshSQL();
	}
	
	function _process() {
        if ($result = $this->artifact->sql_query($this->_sql)) {
			$this->_rows = $this->artifact->sql_numrows($result);
			$this->_pages = $this->rows2Pages($this->_rows, $this->_maxRows);
		    if ($this->_currentPage > $this->_pages) {
				$this->_currentPage = $this->_pages;
			}
			if ($this->_currentPage < 1){
				$this->_currentPage = 1;
			}
		}
	}
	
	function rows2Pages ($rows, $maxRows) {
		$maxRows = (intval($maxRows) > 0)?(intval($maxRows)):(1);
		return intval($rows) / intval($maxRows);
    }
	
    function pageRange() {
        $pages = array();
        if ($this->_pages >= 1) {
            if ($this->_currentPage > (intval($this->_jumps)/2)) {
                $init = $this->_currentPage - (intval($this->_jumps)/2);
            } else {
                $init = 0;
            }
        }
        for ($i = $init; $i <= ($init + $this->_jumps - 1) && $i < $this->_pages; $i++) {
               $pages[] = new Page($this,($i+1));
        }
        return $pages;
    }
	
	function isDisplay() {		
		return $this->isPrevPage()||$this->isNextPage();
	}

	function isPrevPage() {
		if ($this->_currentPage > 1) {
			return true;
		} else {
			return false;
		}
	}

	function isNextPage() {
		if ($this->_currentPage < $this->_pages) {
			return true;
		} else {
			return false;
		}
	}
	

	function getPage($required) {
		if ($required == 'PREV') {
			$numberPage = $this->_currentPage - 1;
		} elseif ($required == 'NEXT') {
			$numberPage = $this->_currentPage + 1;
		} elseif ($required == 'FIRST') {
			$numberPage = 1;		
		} else {
			$numberPage = $this->_pages;
		}
		return $numberPage;
	}

    
	function _refreshSQL() {
		if (!eregi("LIMIT", $this->_sql)) {
			if ($this->_currentPage > 0) {
				$mod = ($this->_rows % $this->_maxRows);
				$init = (($this->_currentPage-1) * $this->_maxRows);
				$init += (($this->_currentPage >= $this->_pages) && $mod > 0)?($this->_maxRows - $mod):(0);
			} else {
				$init = 0;
			}
			// calculo de final para escalabilidad a otros motores en los que sea necesario este calculo
			$end = $init++ + $this->_maxRows;
			if ($end > $this->_rows) {
				$end = $this->_rows;
			}

			$this->_sql = "$this->_sql LIMIT $this->_maxRows OFFSET ". abs($init);
		}
        
	}
	
	function getRowCount()  {
		return $this->_rows;
	}
	
	function getLength() {
		$lenght = count($this->pageRange());
		return $lenght;
	}	
	
	
	
	function __destruct(){
		unset($this);
	}
}

class Page {
	
	var $paginator;
	var $number;
	
	function Page(&$paginator, $number) {
		$this->paginator = &$paginator;
		$this->number  = (int) $number;
	}
	
	function isLastPage() {
		return ($this->number == $this->paginator->_pages)?(true):(false);
	}
	
	function isFirstPage() {
		return ($this->number <= 1)?(true):(false);
	}
	
}

?>