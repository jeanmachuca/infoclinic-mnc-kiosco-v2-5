<?php
/**
* @package _Main Class
* @author Jean Machuca
* @version 1.0.20060903
*/
class _Form{
	
	private $_hiddens = array();
	private $_acceptDuplicateKeys = false;
	private $_formName = 'defaultForm';
	private $_excludedValues = array();
	
	function _Form(){
        $this->setAcceptDuplicateKeys();		
	}
	
	public function forceRedirect($url,$method = 'GET'){
		static $formID;
		if (!is_numeric($formID)){$formID = 0;}
		$formID = $formID+1;
		$parsed_url = explode("?",$url);
		$action = $parsed_url[0];
		$parsed_url = $parsed_url[1];
		$parsed_url = explode("&",$parsed_url);
		$ret = "";
		$ret .= "<html><body><form id=\"redirect_form_$formID\" method=\"$method\" name=\"redirect_form_$formID\" action=\"$action\">";
		foreach ($parsed_url as $k =>$v){
			$kv = explode("=",$v);
			$name = $kv[0];
			$value = $kv[1];
			$ret .= "<input type=\"hidden\" name=\"$name\" value=\"$value\"/>";
		}
		$ret .= "</form>";
		$ret .= "<script type=\"text/javascript\" language=\"JavaScript\">document.forms['redirect_form_$formID'].submit();</script></body></html>";
		ob_clean();
		print $ret;
		ob_flush();
		exit;
	}
	
	function submit($withScriptTag = true){
		$ret = "";
		if ($withScriptTag){$ret .= "<script type=\"text/javascript\" language=\"JavaScript\">";}
		$ret .= "document.forms['".$this->_formName."'].submit();";
		if ($withScriptTag){$ret .= "</script>";}
		print $ret;
		return true;
	}
	
	function StartForm($attrs = null){
		$sAttrs = "";
		foreach ($attrs as $attr => $vattr){
			if (strtolower(trim($attr)) == 'name'){$this->_formName = $vattr;}
			$sAttrs .= " ".$attr."=\"".$vattr."\"";
		}
		$sForm = "<FORM$sAttrs >";

		@error_log("<pre>".var_export($this->_hiddens,true)."</pre>",3,_APPWSSERVERDIR.'/logPOSTSent.txt');

		foreach($this->_hiddens as $hidden) {
				$hname = $hidden['name'];
				$hvalue = $hidden['value'];
	            $sForm .= "<input type=\"hidden\" name=\"$hname\" value=\"$hvalue\" />";
	    }		
		return $sForm;
	}
	
	function EndForm (){
		return "</FORM>";
	}
	
	function setExcludeValues(){
		$args = func_get_args();
		$this->_excludedValues = $args;
	}
	
	function addHidden($name, $value = null) {
		if (!is_array($value) && !in_array($name,$this->_excludedValues)){
			if (is_null($value) && isset($_REQUEST[$name])) {$value = $_REQUEST[$name];}
			if ($this->_acceptDuplicateKeys) {
				$this->_hiddens[] = array("name" => $name, "value" => $value);
			} else {
				$this->_hiddens[$name] = array("name" => $name, "value" => $value);
			}
		}
	}
	
	function setAcceptDuplicateKeys($acceptDuplicateKeys = true) {
		$this->_acceptDuplicateKeys = ($acceptDuplicateKeys == true)?(true):(false);
	}
	
	function addMultipleHiddens(){
		$args = func_get_args();
		if (sizeof($args)>0){
			foreach($args as $hiddens) {
				if (sizeof($hiddens)>0){
					foreach($hiddens as $name => $value){
						$this->addHidden($name,$value);
					}
				}
			}
		}
	}
	
}
?>