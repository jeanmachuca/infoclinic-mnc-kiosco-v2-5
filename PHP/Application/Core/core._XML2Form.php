<?php
/**
 * _XML2Form DATAPACKET 2.0 format compatible
 * @author Jean Machuca.
 * @version 1.0.20061016
 *
 */
class _XML2Form {

	var $xml2array;
	var $xmlobj;
	var $construct;
	var $fields;
	var $_template;
	var $_templateFile;
	var $XMLErrorStr = "";
	var $XMLErrorLine = 0;
	var $oClass;
	var $dinamicForm = 'dinamicForm';
		
	function _XML2Form($oClass,&$template, $dinamicForm = 'dinamicForm'){
		$this->oClass = $oClass;
		$ClassName = get_class($this->oClass);
		if (is_a($template,'_TTE')) {
			$this->_template = &$template;
		} else {
			$oSmartyTemplate = new Smarty();
			_TTE::setSmartyConf($oSmartyTemplate);
			$this->_template = new _TTE($oSmartyTemplate,'assign','display');
		}
		$this->dinamicForm = $dinamicForm;
		$file = _MODULETPLDIR."dinamicForms/form.".$this->dinamicForm."/controls.".$ClassName.".xml";
//		print $file."<br>";
	//	print $ClassName."<br>";
		$this->setTemplateFile($file);
		$this->_template->assign(strtoupper($ClassName),$this->oClass);
		$this->_template->assign('XML2Form',$this);
		$xml = $this->fetch();

		$this->xml2array = new _XML2Array($xml);
		$this->xml2array->onerror = XML2ARRAY_ONERROR_EXIT;
		$this->xml2array->input_encodding = "ISO-8859-1";
		$this->xml2array->parse();
		$this->XMLErrorStr = $this->xml2array->error_str;
		$this->XMLErrorLine = $this->xml2array->error_line;
		$this->construct=true;
		if (empty($this->xml2array->stack)){$this->XMLErrorStr = 'El archivo '.$file.' est� vac�o o no tiene informaci�n XML V�lida';}
		if (empty($xml)){$this->XMLErrorStr = 'El archivo '.$file.' est� vac�o o no existe';}
		$_Array2Obj = new _Array2Obj();
		$this->xmlobj = $_Array2Obj->process($this->xml2array->stack);
//		echo "<pre>".var_export($this->xml2array,true)."</pre>";
		return $this;
	}
	
	/**
	 * Procesa el xml le�do y asigna los campos de formulario
	 *
	 * @return unknown
	 */
	function process($render = true){
		$oFields = $this->xmlobj->item0->children->item0->children->item0->children;
		$this->fields = new stdClass();
		foreach ($oFields as $iField) {
			$fieldName = $iField->attrs->FieldName;
			$FAttrs = $iField->attrs;
			$this->fields->$fieldName = new TField($this->oClass,$this->dinamicForm);
			$TField = &$this->fields->$fieldName;
			foreach ($TField as $kF=>$iFAttr) {
				if (isset($FAttrs->$kF)){
					$TField->$kF = $FAttrs->$kF;
				}
			}
			$TField->setValue($this->oClass,$iField->data);
		}
		
		// mostrar controles
		if ($render){
			foreach ($this->fields as $k=>$v){
				$this->fields->$k->catchTemplate($this->_template);
				$this->_template->assign(strtoupper($this->fields->$k->FieldForm).'_'.strtoupper($k),$this->getFieldHTML($k));
			}
		}
		return true;
	}
	
	function reverse(){
		
	}
	
	/**
	 * Devuelve las opciones en formato del tipo "valor:texto;"
	 * El array de entrada es un arreglo de objetos cuya primera propiedad es la que contiene "valor" y la segunda contiene "texto"
	 *
	 * @param stdClass[] $aOptions
	 * @return string
	 */
	function getOptions($aOptions){
		$_Array2Obj = new _Array2Obj();
		$asOptions = array();
		foreach ($aOptions as $oOption) {
			$aOption = $_Array2Obj->reverse($oOption);
			$kOption = array_keys($aOption);
			$kValue = $kOption[0];
			$kText = $kOption[1];
			$vValue = $aOption[$kValue];
			$vText = $aOption[$kText];
			$asOptions[] = $vValue.":".$vText;
		}
		$sOptions = implode(";",$asOptions);
		return $sOptions;
	}
	
	function Obligatory($sTitle){
		return "<B> * ".$sTitle."</B>";
	}
	
	function setTemplateFile($file){
		$this->_templateFile = $file;
	}
	function fetch() {
		return $this->_template->fetch($this->_templateFile);
	}
	
	function &getField($k){
		return $this->fields->$k;
	}
	
	function getFieldHTML($k){
		$oField = $this->getField($k);
		return $oField->getHTML();
	}

}

class TOption {
	var $Text;
	var $Value;
	var $Selected = false;
	
	function TOption($aFopt){
		$this->Value = $aFopt[0];
		$this->Text = $aFopt[1];
		return $this;
	}
}

class TField{
	var $FieldName = '';
	var $DisplayLabel = '';
	var $FieldType = 'String';
	var $FieldClass = 'TField';
	var $FieldForm = 'dinamicForm';
	var $FieldControl = 'input.type=text';
	var $FieldControlOptions = '0:Seleccione';
	var $FieldVisibility = 'visible';
	var $FieldHelpMessage = '';
	var $FieldFilter = 'full';
	var $FieldValue = '';
	
	function TField(&$oClass,$dinamicForm){
		$this->FieldForm = $dinamicForm;
		$this->FieldClass = get_class($oClass);
		return $this;
	}
	
	function setValue(&$oClass,$data = ''){
		$FieldName = $this->FieldName;
		$this->FieldValue = (isset($oClass->$FieldName))?($oClass->$FieldName):((!empty($this->FieldValue))?($this->FieldValue):($data));
		$aFieldControlOptions = array();
		if (!empty($this->FieldControlOptions)){
			$findEqual = strpos($this->FieldControlOptions,';');
			if (empty($findEqual)){
				$findEqual2 = strpos($this->FieldControlOptions,':');
				if (empty($findEqual2)){
					$aFieldControlOptions[] = $this->FieldControlOptions;
				}else{
					$aFopt = explode(":",$this->FieldControlOptions);
					$oFopt = new TOption($aFopt);
					if ($oFopt->Value == $this->FieldValue){
						$oFopt->Selected = true;
					}
					$aFieldControlOptions[] = $oFopt;
				}
			} else {
				$aFieldControlOpts = explode(";",$this->FieldControlOptions);
				foreach ($aFieldControlOpts as $sFopt) {
					$findEqual2 = strpos($sFopt,':');
					if (empty($findEqual2)){
						$aFieldControlOptions[] = $sFopt;
					}else{
						$aFopt = explode(":",$sFopt);
						$oFopt = new TOption($aFopt);
						if ($oFopt->Value == $this->FieldValue){
							$oFopt->Selected = true;
						}
						$aFieldControlOptions[] = $oFopt;
					}
				}
			}
		}
		$this->FieldControlOptions = $aFieldControlOptions;
		$this->DisplayLabel = str_replace("[B]","<B>",$this->DisplayLabel);
		$this->DisplayLabel = str_replace("[/B]","</B>",$this->DisplayLabel);
		$this->DisplayLabel = str_replace("[b]","<B>",$this->DisplayLabel);
		$this->DisplayLabel = str_replace("[/b]","</B>",$this->DisplayLabel);
	}
	
	function getHTML(){
		$controlAttrs = explode('.',$this->FieldControl);
		$controlName = $controlAttrs[0];
		$controlAttrs = array_slice($controlAttrs,1);
		$controlAtNames = array();
		$controlAtValues = array();
		$sAtts = "";
		foreach ($controlAttrs as $controlAttr) {
			$findEqual = strpos($controlAttr,'=');
			if (empty($findEqual)){
				$cn = $controlAttr;
				if (trim(strtoupper($cn)) <> 'VALUE'){
					$controlAtNames[] = $cn;
					$sAtts .= " ".$cn." ";
				}
			} else {
				$controlAttrV = explode('=',$controlAttr);
				$cn = $controlAttrV[0];
				$cv = $controlAttrV[1];
				if (trim(strtoupper($cn)) <> 'VALUE'){
					$controlAtNames[] = $cn;
					$controlAtValues[$cn] = $cv;
					$sAtts .= " ".$cn."=\"".$cv."\" ";
				}
			}
		}
		if (empty($this->FieldVisibility) || strtolower($this->FieldVisibility) == 'visible'){
			$fieldHTML = $this->loadControlTPL($controlName,$sAtts);
		}
		return $fieldHTML;
	}
	
	function &catchTemplate($template = null){
		static $oTemplate;
		if (is_object($template)){
			$oTemplate = $template;
		}
		return $oTemplate;
	}
	
	function loadControlTPL($controlName,$Attrs){
		$template = $this->catchTemplate();
		if (is_null($template)) {$template = new Smarty();}
		$file = _MODULETPLDIR."dinamicForms/form.".$this->FieldForm."/controls/".$controlName.".control.tpl.htm";
		$template->assign('FieldAttrs',$Attrs);
		$template->assign('oField',$this);
		$ret = $template->fetch($file);
		return $ret;
	}
}
?>