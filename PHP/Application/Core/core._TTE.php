<?php
/**
* Tag Template Engine Mask
*
*/
class _TTE {
	public $textstring = "";
	public $onShow;
	public $onAssign;
	public $template;
	public $_templateFile = "";
	public $_templateFetch = "";
	public $oEvent;
	
	function &_TTE(&$template,$assignMethod = 'assign',$showMethod = 'display',$assignByRef = 'assign_by_ref'){
		$this->template = &$template;
		$this->onAssign = array(&$template,$assignMethod);
		$this->onAssignByRef = array(&$template,$assignByRef);
		$this->onShow = array(&$template,$showMethod);
		$oEvent = new _Event($this);
		$this->oEvent = &$oEvent;
		return $this;
	}
	
	function assign($assignVar,$assignValue){
		$this->aAssigns[$assignVar] = $assignValue;
		$args = func_get_args();
		return $this->oEvent->fireEvent('Assign',$args);
	}
	
	function assign_by_ref($assignVar,$assignValue){
		$this->aAssigns[$assignVar] = $assignValue;
		$args = func_get_args();
		return $this->oEvent->fireEvent('AssignByRef',$args);
	}
	
	function show(){
		$this->_templateFetch = $this->oEvent->fireEvent('Show',array($this->_templateFile));
		return $this->_templateFetch;
	}
	
	function fetch($absoluteFile){
		$ret = $this->template->fetch($absoluteFile);
		return $ret;
	}
	
	function setTemplateFile($templateFile = ""){
		$this->_templateFile = $templateFile;
	}
	
	function setSmartyConf(&$oTemplate){
		$oTemplate->left_delimiter = "{#";
		$oTemplate->right_delimiter = "#}";
		$oTemplate->compile_dir = _EXTERNALDIR.'/Smarty/templates_c/';
		$oTemplate->cache_dir = _EXTERNALDIR.'/Smarty/libs/cache/';
	}
	
	function __destruct(){
		unset($this);
	}
}
?>