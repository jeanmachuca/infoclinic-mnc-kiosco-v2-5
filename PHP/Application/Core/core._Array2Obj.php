<?php

/* vim: set expandtab tabstop=4 shiftwidth=4: */
/**
 * @author  <Jean Machuca>
 * @package PWAGLibs
 * @version 1.0
 * @subpackage Array2Obj
 * @example 
 * 
 * $array2obj = new _Array2Obj();
 * $obj = $array2obj->process($array); // convierte $array en un objeto
 * $aObj = $array2obj->reverse($o); // convierte $o en array
 * 
 */

class _Array2Obj {
	var $construct = false;
	var $stack;
	var $input;
	
	/**
	 * Constructor simple
	 *
	 * @return _Array2Obj
	 */
	function _Array2Obj(){
		$this->input = array();
		$this->stack = new stdClass();
		$this->construct = true;
	}
	
	/**
	 * Convierte un array en un objeto 
	 *
	 * @param array[] $array
	 * @return stdClass
	 */
	function process($array){
		$this->stack = $this->_buildObj($array,$this->stack);
		return $this->stack;
	}
	
	/**
	 * Convierte un objeto en un array
	 *
	 * @param stdClass $obj
	 * @return array[]
	 */
	function reverse($obj){
		$this->input = $this->_buildArray($obj,$this->input);
		return $this->input;
	}
	
	function _buildObj ($array,&$obj){
		foreach ($array as $key=>$value) {
			if (is_numeric($key)){$key = 'item'.$key;}
			$key = $this->_replaceMetaChars($key);
			if (!is_array($value)){
				$obj->$key = $value;
			} else {
				$obj->$key = new stdClass();
				$obj->$key = $this->_buildObj($value,$obj->$key);
			}
		}
		return $obj;
	}
	
	function _buildArray($obj,&$array){
		foreach ($obj as $key=>$value) {
			$key = $this->_replaceMetaChars($key);
			if (is_numeric($key)){$key = 'item'.$key;}
			if (!is_object($value)){
				$array[$key] = $value;
			} else {
				$array[$key] = array();
				$array[$key] = $this->_buildArray($value,$array[$key]);
			}
		}
		return $array;
	}
	
	function _replaceMetaChars ($str) {
	    if (!empty($str)) {
	        $metachars = array(
	            // acentos minuscula
	            array('�', 'a'), array('�', 'e'), array('�', 'i'), array('�', 'o'), array('�', 'u'),
	            // acentos mayuscula
	            array('�', 'A'), array('�', 'E'), array('�', 'I'), array('�', 'O'), array('�', 'U'),
	            // enies
	            array('�', 'N'), array('�', 'n'),
	            // dieresis minuscula
	            array('�', 'a'), array('�', 'e'), array('�', 'i'), array('�', 'o'), array('�', 'u'),
	            // dieresis mayuscula
	            array('�', 'A'), array('�', 'E'), array('�', 'I'), array('�', 'O'), array('�', 'U'),
	            // acento diacritico minusculas
	            array('�', 'a'), array('�', 'e'), array('�', 'i'), array('�', 'o'), array('�', 'u'),
	            // acento diacritico mayusculas
	            array('�', 'A'), array('�', 'E'), array('�', 'I'), array('�', 'O'), array('�', 'U'),
	            // otros caractere
	            array('\'', '_'), array(' ', '_'), array('$', 'X'), array('#', 'X'), array('@', 'X'),
	            array('|', 'X'), array('\\', '_'), array('%', 'X'), array('/', 'X'), array('(', 'X'),
	            array(')', 'X'), array('[', '_'), array(']', '_'), array('{', 'X'), array('}', 'X'),
	            array('-', '_'), array('+', '_'), array('*', '_'), array('&', 'X'), array('?', 'X'),
	            array('�', '_'), array('�', '_'), array('!', '_'), array('�', 'X'), array('�', 'X'),
	            array('<', '_'), array('>', '_'), array('.', '_'), array(',', 'X'), array(';', 'X'),
	            array('�', '_'), array('�', '_'), array('`', '_'), array('~', 'X'), array('�', 'c'),
	            array('�', 'C'), array("\t", "_"), array(":","_")
	        );
	        $new = trim($str);
	        foreach ($metachars as $item) {
	            list($cual, $por) = $item;
	            $new = str_replace($cual, $por, $new);
	        }
	        return $new;
	    }
	    return false;
	}
	
}

?>