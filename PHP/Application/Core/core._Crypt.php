<?php
/* vim: set expandtab tabstop=4 shiftwidth=4: */
/**
 * @copyright 2000-2010 Jean Machuca.
 * @author  <Jean Machuca>
 * @package _Crypt class
 * @subpackage 
 * @example 
 * Ejemplo (1):
 * $string = new _Crypt('texto',md5('key'));
 * echo $string->_encrypt();
 * 
 * echo $string->_decrypt(); // desencripta el ultimo texto encriptado
 * 
 * Ejemplo (2):
 * echo _Crypt::encrypt('texto',md5('key'));
 * echo _Crypt::decrypt('texto-encriptado',md5('key'));
 * 
 */

class _Crypt {
	var $last_string = "";
	var $last_key = "";
	var $construct;
	
	/**
     * Constructor
     *
     * @access   public
     * @return   null.
     */
	
	function _Crypt($string,$key = ''){
		$key = ("$key" == "")?($_SERVER['HTTP_HOST']):($key);
		$this->last_key = $key;
		$this->last_string = $string;
		$this->construct = true;
	}
	
	/**
     * Encripta el �ltimo string procesado
     *
     * @access   private
     * @return   string cadena encriptada.
     */
	
	function _encrypt() {
		if (!$this->construct) return false;
		$string = $this->last_string;
		$key = $this->last_key;
		$result = '';
		for($i=0; $i<strlen($string); $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)+ord($keychar));
			$result.=$char;
		}
		$this->last_string = base64_encode($result);
		return $this->last_string;
	}

	/**
     * Desencripta el �ltimo string procesado
     *
     * @access   private
     * @return   string cadena encriptada.
     */
	function _decrypt() {
		if (!$this->construct) return false;
		$string = $this->last_string;
		$key = $this->last_key;
		$result = '';
		$string = base64_decode($string);

		for($i=0; $i<strlen($string); $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)-ord($keychar));
			$result.=$char;
		}
		$this->last_string = $result;
		return $this->last_string;
	}
  
	/**
     * Encripta el string con el key dado
     *
     * @access   public
     * @return   string cadena encriptada.
     */
	function encrypt($string = '',$key = '') {
		if (empty($string) && empty($key) && !$this->construct) return false;
		$string = new _Crypt($string,$key);
		return $string->_encrypt();
	}
	/**
     * Desencripta el string con el key dado
     *
     * @access   public
     * @return   string cadena encriptada.
     */
	function decrypt($string = '',$key = '') {
		if (empty($string) && empty($key) && !$this->construct) return false;
		$string = new _Crypt($string,$key);
		return $string->_decrypt();
	}
	
	function __destruct(){
		unset($this);
	}
	
}
?>