<?php
class _Config {
	public $dbConnection;
	public $Theme;
	public $configDataFileName;
	function _Config(){
		$this->dbConnection = (is_object($this->dbConnection))?($this->dbConnection):(new stdClass());
		$this->Theme = (is_object($this->Theme))?($this->Theme):(new stdClass());
		$this->configDataFileName = _CONFIGDATADIR."config.".strtolower(get_class($this)).".ini";
		if (file_exists($this->configDataFileName)) {
			$configFile = parse_ini_file($this->configDataFileName,true);
			$array2obj = new _Array2Obj();
			$configVars = $array2obj->process($configFile);
			foreach ($configVars as $kvar=>$var){
				$this->$kvar = $var;
			}
			//echo "<pre>".var_export($this,true)."</pre>";
		}
		return $this;
	}
	
	private function catchConfig($pConf = null){
		static $Conf;
		$configName = get_class($this);
		if (empty($Conf) && is_object($pConf) && is_a($pConf,$configName)){
			$Conf = $pConf;
		}
		if (empty($Conf)){
			$Conf = new $configName();
		}
		return $Conf;
	}
	
	function getValue($var){
		$Conf = $this->catchConfig();
		$newvar = "\$ret = \$Conf->$var;";
		eval($newvar);
		return $ret;
	}
	
	function setValue($var,$value){
		$Conf = $this->catchConfig();
		$newvar = "\$Conf->$var = \$value ;";
		eval($newvar);
		$this->catchConfig($Conf);
		return true;
	}
}
?>