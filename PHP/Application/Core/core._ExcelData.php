<?PHP
/**
 * @package ExcelData Generico (Clase)
 * @desc Importa y Exporta datos en excel. (Reemplaza el uso de la class cargador y export)
 * @creation_date 24/07/2006
 * @version 2.0.20060724
 * @author  Jean Machuca.
 * @example No use esta clase directamente. Use IMImport.
 *  
 */

class _ExcelData {

    // Declaracion de Variables (desde la tabla)
	
    var $fileLoad;
    var $filePath;
    var $numrows;
    var $numcols;
    var $tableName;
    var $tipo;
    var $aliases = false;
    var $array_aliases;
    var $resultsave = true;
    var $errorList = array();
    var $db;
    var $dataExcel;
    var $error_message = "";
    var $error_code = 0;
    var $pclassname = 'manten';
    var $saveMethod = 'save';
    var $getMethod = 'get';
    var $callbackMethod = 'callbackExcelImport';
    
    /**
     * Constructor de la clase
     *
     * @access public
     */
    function _ExcelData() {
    	$this->resultsave = true;
    	$this->dataExcel = new ExcelReader();
    	return $this;
    }

	/**
	 * Importa los datos desde un archivo excel. La ruta del archivo puede venir desde un formulario o desde el disco.
	 * Para el caso de que la ruta provenga desde un formulario el campo debe llamarse fileLoad. Ej.: <input type=file name=fileLoad>
	 *
	 * @param string $tablename Nombre de la tabla a cargar
	 * @param bool $from_file Verdadero si la ruta del archivo es relativa al disco, falso si la ruta viene desde un formulario.
	 * @return bool True si todo sale bien, False de lo contrario.
	 */
    function import($tablename = false, $from_file = false) {
		$this->setTable((!$tablename)?($_REQUEST["tablename"]):($tablename));
		$classname = strtolower(trim($this->tableName));
	    $classprefix = substr($classname,0,4);
	    include_once(INCLUDE_PATH."class/class.$classname.php");
		return $this->uploadFile($from_file);
	}
	
	function setTable($tableName) {
		$this->tableName = $tableName;
	}
	
	function validClass($classname){
		$a1_methods = get_class_methods($classname);
		$a1_property = get_class_vars($classname);
		return (get_parent_class($classname) == $this->pclassname && 
				array_key_exists('cargador',$a1_property) && 
				array_key_exists('classTitle',$a1_property) && 
				in_array($this->saveMethod,$a1_methods) && 
				in_array($this->getMethod,$a1_methods) && 
				(in_array(strtolower($this->callbackMethod),$a1_methods) || in_array($this->callbackMethod,$a1_methods)) && 
				in_array('import',$a1_methods)
				);
	}
	
	function getClasses(){
		$classfiles = glob(INCLUDE_PATH."class/class.*.php");
		$classes = array();
		$a1_classes = get_declared_classes();
		foreach ($classfiles as $classfile){
			$a_class = explode(".",$classfile);
			$classname = $a_class[1];
			if(!in_array($classname,$a1_classes)){
				include_once(INCLUDE_PATH."class/class.$classname.php");
			}
			if ($this->validClass($classname)){
				$cls = new $classname();
				$el = new stdClass();
				if (!empty($cls->classTitle)){
					$el->text = $cls->classTitle;
					$el->value = $classname;
					$classes[] = $el;
				}
				unset($cls);
			}
		}
		return $classes;
	}
	
	function getPrimaryKeyName(){
		
	}
	
	function getColumns(){
		
	}
    
	function uploadFile($from_file = false){
		if ($from_file) {
			$this->filePath = $from_file;
		} else {
			$this->processFile('fileLoad', $this->fileLoad);
			$this->filePath = UPLOAD_DIR.$this->fileLoad;
		}
		if (strtolower(substr($this->filePath,-3)) == 'xls') {
			return $this->loadDB();
		} else {
			$this->raise_error('El formato del archivo no es v�lido');
			return false;
		}
	}
	
	function checkColumns ($cnames){
		return true;
	}

	function loadDB(){
		if ($this->validClass($this->tableName)){
			$this->dataExcel->setRowColOffset(0);
			if (file_exists($this->filePath)){
				$this->dataExcel->read($this->filePath);
				$this->numrows = $this->dataExcel->sheets[0]['numRows'];
				$this->numcols = $this->dataExcel->sheets[0]['numCols'];
				$cnames = array();
			    $cnames = $this->dataExcel->sheets[0]['cells'][0];
			    if ($this->checkColumns($cnames)){
			    	//el ciclo comienza en 1 ya que en 0 estan los nombres de los campos	
					for($i=1; $i < $this->numrows; $i++){
				    	$rowexcel = $this->dataExcel->sheets[0]['cells'][$i];
				    	$this->chargeRow($rowexcel,$i, $cnames);
					}
			    	$verdetalle = "";
				    if(!$this->resultsave){
						$strerror="Listado de errores:<br> ".implode("<br>",$this->errorList);
						$verdetalle = "<p><b>Hubo ".sizeof($this->errorList)." errores durante el proceso de carga:</b></p>$strerror";
					}
					$datos = $i-1;
					$this->raise_error("Finalizado. Se procesaron $datos filas en la tabla <b> $this->tableName</b>$verdetalle");
					if(!@unlink($this->filePath)){
						$this->raise_error("Ocurri� un error, el archivo no pudo ser borrado");
					}
				} else {
					$this->raise_error("El formato del archivo no es v�lido");
				}
			}else{
				$this->raise_error("Error, no se encontr� el archivo");
			}
			return $this->resultsave;
		}else{
			$this->raise_error("Error Interno, La definici�n de la clase a cargar no es v�lida");
			return false;
		}

	}
	
	function chargeRow(&$row,$line, $cnames = array()) {
		
		if(!empty($this->tableName)){
				$classname = strtolower(trim($this->tableName));
			    $classprefix = substr($classname,0,4);
			    $class = new $classname();
			    $i = 0;
			    // define las tablas en las que el id es un entero definido por la carga y no autoincrement
			    $array_isint = array();
		    
			    $id_isint = (in_array($classname,$array_isint))?(true):(false);
	
			    if ($id_isint){
					$sql = "SELECT COUNT(*) FROM $classname";
					$countregs = $this->db->getOne($sql);
			        if (!empty($countregs)) {
			    		$row["$classname"."_id"] = $countregs;
					} else {
						$row["$classname"."_id"] = 1;
					}
					$row["$classprefix"."_codigo"] = $row["$classname"."_id"];
			    }
				if (in_array($classname.'_id',$cnames)){
					$idfieldname="$classname"."_id";
				} else if (in_array($classprefix.'._codigo',$cnames)){
					$idfieldname = "$classprefix"."_codigo";
				} else if (isset($_REQUEST["idFieldName"])){
					// en caso de haber un id pasado por form
					$idfieldname = $_REQUEST["idFieldName"];
				} else {
					$idfieldname = false;
				}
				if ($idfieldname){
					$valfieldname = $row[(in_array($idfieldname,$cnames)-1)];
					$valfieldname = intval($valfieldname);
					$sql = "SELECT COUNT(*) FROM $classname WHERE $idfieldname = ".$valfieldname;
					$countregs = $this->db->getOne($sql);
				} else {
				   	$countregs = 0;
				}
			    $newreg = (empty($countregs) || $countregs < 1)?(true):(false);
			    if ($newreg){
			    }else{
			    	$getMethod = $this->getMethod;
				    $class = $class->$getMethod($row[(in_array($idfieldname,$cnames)-1)]);
			    }
			    $a1_classes = get_declared_classes();
			    $class->db = (in_array('EEDb',$a1_classes) || in_array('eedb',$a1_classes))?(new EEDb()):(DB::getConnection());
		    	unset($this->cargador->class->cargador);// bucle de pila
			    $class->cargador = &$this;

			    for($i=0;$i<sizeof($cnames);$i++){
			    	//si existe un alias para el campo, referenciarlo a este, sino tomarlo como el nombre del campo
			    	$rowaliaskey = $cnames[$i];
			    	$alias = $this->array_aliases[$cnames[$i]];
		    		$rowkey = (empty($alias))?($cnames[$i]):($alias);
		    		
		    		$row[$rowkey] = $row[$i];
		    		
			        if ($id_isint || "$rowkey" != "$classname"."_id"){
				    	if (!is_numeric($rowkey) && isset($row[$rowkey])){
					    	$class->$rowkey = $row[$rowkey];
				    	}
			        }
			    }
			    
				if ($newreg){
					$classprefix_estado = $classprefix."_estado";
					$classprefix_orden = $classprefix."_orden";
				    $class->$classprefix_estado = 'A';
				    $class->$classprefix_orden = 10;
				    $callbackMethod = $this->callbackMethod;
				    $oldErrorReporting = error_reporting();
				    error_reporting(0);
					if ($class->$callbackMethod($class,$line)){
						// por efecto inducido de operador && si hay un error 
						$saveMethod = $this->saveMethod;
					    $this->resultsave = ($this->resultsave && $class->$saveMethod());
					}
					error_reporting($oldErrorReporting);
				} else {
					// En caso de existir modificar 
			    	if (!is_numeric($idfieldname) && isset($row[$idfieldname])){
				    	$class->$idfieldname = $row[$idfieldname];
					    $callbackMethod = $this->callbackMethod;
					    $oldErrorReporting = error_reporting();
					    error_reporting(0);
						if ($class->$callbackMethod($class,$line)){
							// por efecto inducido de operador && si hay un error 
							$saveMethod = $this->saveMethod;
						    $this->resultsave = ($this->resultsave && $class->$saveMethod());
						}
						error_reporting($oldErrorReporting);
			    	}
				}
				if (!$this->resultsave){
					$errorId = (!empty($valfieldname))?(" ID: ".$valfieldname):("No tiene identificador.");
					$this->errorList[] = "Error al cargar la fila: $line. $errorId";
				}

		}else{
			return false;
		}
		return true;
	}
	
 	function loadRow(&$row) {
	    $property_array = array_keys($row);
	    for ($i=0;$i<sizeof($property_array);$i++){
	        $rowkey = $property_array[$i];
	    	if (!is_numeric($rowkey) && isset($row[$rowkey])){
	    		$this->$rowkey = $row[$rowkey];
	    	}
	    }
	    return null;
	}
	
	function getClassVars(&$class){return get_class_vars(get_class($class));}
	function raise_error ($error_message, $error_code=-1) {$this->error_message=$error_message;$this->error_code=$error_code;if (!empty($err)) {return false;} else {return true;}}
	function clear_error () {$this->error_message="";$this->error_code=0;}
	function init_template (&$template) {$this->show_error($template);$this->show_error_carga($template, "msgerror");}
	function show_error(&$template, $tag="message") {if ($this->error_code!=0) {$template->assign($tag, $this->error_message);}}
	function show_error_carga(&$template, $tag="message") {if ($this->error_code_carga!=0) {$template->assign($tag, $this->error_message_carga);}}
	
    function processFile($name, &$file) {
		if ($_POST['eliminar_'.$name] == 'S') {
			if (!empty($file)) {
				@unlink(UPLOAD_DIR.$file);
				$file = NULL;
			}
		} else {
		    if (is_uploaded_file($_FILES[$name]["tmp_name"])) {
				if (!empty($file)) {
				    @unlink(UPLOAD_DIR.$file);
				}
				$fileName = uniqid("file_").strrchr($_FILES[$name]["name"], ".");
		        if (move_uploaded_file($_FILES[$name]["tmp_name"], UPLOAD_DIR.$fileName)) {
			        $file = $fileName;
					if (file_exists(UPLOAD_DIR.$file)) {
					   $group = Parameter::get("appSettings:group");
					   umask(0);
					   chmod(UPLOAD_DIR.$file, 0777);
					   @chgrp(UPLOAD_DIR.$file, $group);
					}
	            }

	        }
	    }
    }
    
	function __destruct(){
		unset($this);
	}
	
}

?>