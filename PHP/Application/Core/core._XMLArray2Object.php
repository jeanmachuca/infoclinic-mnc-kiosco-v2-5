<?php

class _XMLArray2Object {
	
	var $output_encodding = "ISO-8859-1";
	var $input_encodding = "UTF-8";
	
	function _XMLArray2Object(){
		return $this;
	}
	
	function lastIndexOf($haystack, $needle) {
			$index = strpos(strrev($haystack), strrev($needle));
			$index = strlen($haystack) - strlen($index) - $index;
			return $index;
	}
	
	function k_property_name($k_property,$objpath,$i = null){
			$thisobj = null;
			$ii = intval($i)+1;
			if (!empty($k_property)){
				@eval("\$thisobj = (isset(".$objpath.$k_property."))?(".$objpath.$k_property."):(null);");
			}
			$k_prop = (empty($thisobj)
							)?($k_property):(
							$this->k_property_name(
									(empty($i))?($k_property):(substr($k_property,0,$this->lastIndexOf($k_property,"".$i.""))).$ii
							,$objpath,$ii));
			return $k_prop;
	}
	
	function array2class($XMLArray,$objectpath = "\$this->"){
		if (!$this->construct) return false;
		foreach ($XMLArray as $item){
			$child = (isset($item['children']))?($item['children']):(null);
			$k_property = $this->replaceMetaChars($item['name']);
			$k_property = $this->k_property_name($k_property,$objectpath);
			$pk_property = $objectpath.$k_property;
			if (is_array($child)) {
				$this->array2class($child,$pk_property."->");
			}
			if (isset($item['data']) && !empty($item['data'])){
				$v_property = $item['data'];
				if ("string" == gettype($v_property)){$v_property = @iconv($this->input_encodding,$this->output_encodding, $v_property);}
		   		$property = $pk_property." = \$v_property;";
		   		@eval($property);
			}
		}
		return $this;
	}
	
	function replaceMetaChars ($str) {
	    if (!empty($str)) {
	        $metachars = array(
	            // acentos minuscula
	            array('�', 'a'), array('�', 'e'), array('�', 'i'), array('�', 'o'), array('�', 'u'),
	            // acentos mayuscula
	            array('�', 'A'), array('�', 'E'), array('�', 'I'), array('�', 'O'), array('�', 'U'),
	            // enies
	            array('�', 'N'), array('�', 'n'),
	            // dieresis minuscula
	            array('�', 'a'), array('�', 'e'), array('�', 'i'), array('�', 'o'), array('�', 'u'),
	            // dieresis mayuscula
	            array('�', 'A'), array('�', 'E'), array('�', 'I'), array('�', 'O'), array('�', 'U'),
	            // acento diacritico minusculas
	            array('�', 'a'), array('�', 'e'), array('�', 'i'), array('�', 'o'), array('�', 'u'),
	            // acento diacritico mayusculas
	            array('�', 'A'), array('�', 'E'), array('�', 'I'), array('�', 'O'), array('�', 'U'),
	            // otros caractere
	            array('\'', '_'), array(' ', '_'), array('$', 'X'), array('#', 'X'), array('@', 'X'),
	            array('|', 'X'), array('\\', '_'), array('%', 'X'), array('/', 'X'), array('(', 'X'),
	            array(')', 'X'), array('[', '_'), array(']', '_'), array('{', 'X'), array('}', 'X'),
	            array('-', '_'), array('+', '_'), array('*', '_'), array('&', 'X'), array('?', 'X'),
	            array('�', '_'), array('�', '_'), array('!', '_'), array('�', 'X'), array('�', 'X'),
	            array('<', '_'), array('>', '_'), array('.', '_'), array(',', 'X'), array(';', 'X'),
	            array('�', '_'), array('�', '_'), array('`', '_'), array('~', 'X'), array('�', 'c'),
	            array('�', 'C'), array("\t", "_"), array(":","_")
	        );
	        $new = trim($str);
	        foreach ($metachars as $item) {
	            list($cual, $por) = $item;
	            $new = str_replace($cual, $por, $new);
	        }
	        return $new;
	    }
	    return false;
	}
	
}
?>