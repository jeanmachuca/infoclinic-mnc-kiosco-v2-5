<?php
/**
* @package _Main Class
* @author Jean Machuca
* @version 1.0.20060731
*/
include_once(dirname(__FILE__).'/'.'../Loader.php');
include_once(dirname(__FILE__).'/'.'../../External/Smarty/libs/Smarty.class.php');


class _Main {
	private $oTemplate;
	private $_templateFile = "";
	private $errorHandler;
	private $sessionHandler;
	private $shutDownHandler;
	private $extensionTFile = ".html";
	public $moduleName = "main";
	
	// Events
	public $onLoad;
	public $onRender;
	public $onDisplay;
	public $onPostRequest;
	public $onGetRequest;
	public $onHeadRequest;
	public $onPutRequest;
	public $onNoRequest;
	public $onReadyStateChange;
	public $onError;
	public $onCustomError;
	public $onShutDown;
	public $onAction;
	public $onRedirect;
	
	//Session Vars
	private $oSession;

	public $DEBUG;
	
	
	public function _Main(){

		$this->errorHandler = array(&$this,'_Error');
		set_error_handler($this->errorHandler); 
		$this->shutDownHandler = array(&$this,'_ShutDown');
		register_shutdown_function($this->shutDownHandler);
		$this->_Load();
		$this->_Render();
		
		$oEvent = $this->getEvent();
		if ($this->_Load() 
			&& $this->_Request()
			&& $this->_Render()
		){
			$oEvent->FireEvent('ReadyStateChange',array('request'=>'complete','percent'=>100));
			return $this;
		} else {
			return null;
		}
	}
	
	public function setDebug($valor)
	{
		$this->DEBUG = $valor;
	}
	
	public function _Load(){
		$this->oSession = _Session::getSession();
		$this->oTemplate = new Smarty();
		_TTE::setSmartyConf($this->oTemplate);
		define('_MODULETPLDIR',_APPDIR."/Tpl/tpl.".strtolower($this->moduleName)."/");
		$this->setTemplateFile(strtolower($this->moduleName).$this->extensionTFile);
		$this->getEvent()->FireEvent('ReadyStateChange',array('request'=>'begin','percent'=>15));
		return $this->getEvent()->FireEvent('Load');
	}
	
	private function _Request(){
		$this->getEvent()->FireEvent('ReadyStateChange',array('request'=>'progress','percent'=>35));
		if (!empty($_SERVER['REQUEST_METHOD'])){
			switch (strtoupper($_SERVER['REQUEST_METHOD'])){
				case 'GET':
					$action = 'GetRequest';
				break;
				case 'HEAD':
					$action = 'HeadRequest';
				break;
				case 'POST':
					$action = 'PostRequest';
				break;
				case 'PUT':
					$action = 'PutRequest';
				break;
				default:
					$action = 'NoRequest';
				break;
			}
			if ($this->getEvent()->FireEvent($action)){
				return $this->getEvent()->FireEvent('Action',array('action'=>$action));
			}
		}
		return false;
	}	
	private function _Render(){
		$this->getEvent()->FireEvent('ReadyStateChange',array('request'=>'progress','percent'=>70));
		$render = $this->getEvent()->FireEvent('Render');
		if ($render){
			$header = $this->oTemplate->fetch(_MODULETPLDIR."/tpl.header$this->extensionTFile");
			$leftblocks = $this->oTemplate->fetch(_MODULETPLDIR."/tpl.leftblocks$this->extensionTFile");
			$rightblocks = $this->oTemplate->fetch(_MODULETPLDIR."/tpl.rightblocks$this->extensionTFile");
			$footer = $this->oTemplate->fetch(_MODULETPLDIR."/tpl.footer$this->extensionTFile");
			$this->oTemplate->assign('cabecera',$header);
			$this->oTemplate->assign('pie',$footer);
			$this->oTemplate->assign('bloque_izquierdo',$leftblocks);
			$this->oTemplate->assign('bloque_derecho',$rightblocks);
			$this->getEvent()->FireEvent('ReadyStateChange',array('request'=>'progress','percent'=>90));
			$display = $this->getEvent()->FireEvent('Display');
			if ($display){
				$this->oTemplate->display($this->_templateFile);
			} else {
				$this->getEvent()->FireEvent('ReadyStateChange',array('request'=>'progress','percent'=>100));
				return $render;
			}
		}
		$this->getEvent()->FireEvent('ReadyStateChange',array('request'=>'progress','percent'=>100));
		return $render;
	}
	
	public function _Error($errno, $errstr, $errfile, $errline){
		if (is_object($this)){
			$args = func_get_args();
			if ($errno == E_NOTICE && substr($errstr, 0, 17) == "Undefined index: ") return true;
			if ($errno == E_NOTICE && substr($errstr, 0, 20) == "Undefined property: ") return true;
			$aErrors = array(
				E_ERROR=>'Run-Time Error',
				E_WARNING=>'Run-Time Warning',
				E_PARSE=>'Compile-Time Parse Error',
				E_NOTICE=>'Run-Time Notice',
				E_CORE_ERROR=>'Fatal Startup Error',
				E_CORE_WARNING=>'Warning',
				E_COMPILE_ERROR=>'Fatal compile-time Error',
				E_COMPILE_WARNING=>'Compile-Time Warning',
				E_USER_ERROR=>'User-generated Error',
				E_USER_WARNING=>'User-generated Warning',
				E_USER_NOTICE=>'User-generated Notice');
			$aNoDisplayErrors = array(E_NOTICE);
			$aFatalErrors = array(E_ERROR,E_PARSE,E_CORE_ERROR,E_COMPILE_ERROR);
			if (!in_array($errno,array_keys($aErrors))) return true; // unrecognized error no referenced pass true
			if ((!in_array($errno,$aNoDisplayErrors)) && $this->getEvent()->FireEvent('Error',$args) && $this->DEBUG){
				if(!function_exists('debug_backtrace'))
				{
					echo 'function debug_backtrace does not exists'."\r\n";
					return false;
				}		
					echo '<pre>';
					echo "\r\n".'--:: '.$aErrors[$errno].': '.$errstr.' ::--'."\r\n";
					echo "\r\n".'----------------'."\r\n";
					echo 'Debug Back Trace:'."\r\n";
					echo '----------------'."\r\n";
				$backtrace = debug_backtrace();
				if (sizeof($backtrace)>2){$backtrace = array_slice($backtrace,1);}
				$backtrace = array_reverse($backtrace);
				foreach($backtrace as $t)
				{
					echo "\t" . '@ ';
				if(isset($t['file'])) {echo basename($t['file']) . ':' . $t['line'];}
				else
				{
					// if file was not set, I assumed the functioncall
					// was from PHP compiled source (ie XML-callbacks).
					echo '<PHP inner-code>';
				}
					echo ' => ';
				if(isset($t['class'])) echo $t['class'] . $t['type'];
					echo $t['function'];
					if(isset($t['args']) && sizeof($t['args']) > 0) echo '(...)';
					else echo '()';
					echo "\r\n";
				}
				echo '</pre>';
				if (in_array($errno,$aFatalErrors)) exit;
				return false;
			}
			return true;
		} else {
			echo "Error Handling is not possible there";
			exit;
			return false;
		}
	}
	public function &getEvent(){
		static $oEvent;
		if ((!is_object($oEvent)) && is_object($this)){
			$oEvent = new _Event($this);
		}
		return $oEvent;
	}
	public function _ShutDown(){
		return $this->getEvent()->FireEvent('ShutDown');
	}
	
	public function setTemplateFile($templateFile = ""){
		$this->_setTemplateFile(_MODULETPLDIR."tpl.".$templateFile);
	}
	private function _setTemplateFile($templateFile = ""){
		$this->_templateFile = $templateFile;
	}
	
	public function __destruct(){
		unset($this);
	}
	
	public function &getTemplate(){
		return $this->oTemplate;
	}
	
	public function &getSession(){
		return $this->oSession;
	}	
	
	public function &getTemplateFile(){
		return $this->_templateFile;
	}
	
	public function assign($vari,$value=null){
		$this->oTemplate->assign($vari,$value);
	}
	
	public function fetch($templateFile){
		return $this->oTemplate->fetch(_MODULETPLDIR."/tpl.".$templateFile.$this->extensionTFile);		
	}

    public function uploadFile($name, &$file) {
		if (is_uploaded_file($_FILES[$name]["tmp_name"])) {
			if (!empty($file)) {
				@unlink(_UPLOADDIR.$file);
			}
			$fileName = uniqid("file_").strrchr($_FILES[$name]["name"], ".");
			if (move_uploaded_file($_FILES[$name]["tmp_name"], _UPLOADDIR.$fileName)) {
				$file = $fileName;
				if (file_exists(_UPLOADDIR.$file)) {
				   umask(0);
				   chmod(_UPLOADDIR.$file, 0777);
				   @chgrp(_UPLOADDIR.$file, 0777);
				}
			}

		}
    }
 
	function InstanceHash()
	{
		if (is_object($this)){
			$args = func_get_args();
			if (sizeof($args)>0){
				foreach($args as $hash){
					$aClassVars = get_class_vars(get_class($this)); 
//					print "<pre>".var_export($aClassVars,true)."</pre>";
					if (sizeof($hash)>0){//si hay algo en las variables
						foreach ($hash as $fk=>$fv) {if (substr($fk,0,1) != '_' 
							&& array_key_exists($fk,$aClassVars)
							&& array_key_exists($fk,$hash)){$this->$fk = $hash[$fk];}}
					}
				}
			}
		} else {
			$this->_oEvent->fireEvent('RaiseError','No object constructor called');
		}
	}

	function HashInstance()
	{
		$hash = array();
		if (is_object($this)){
			$aClassVars = get_class_vars(get_class($this));
			if (sizeof($aClassVars)>0){
				foreach ($aClassVars as $fk=>$fv) {if (substr($fk,0,1) != '_'){$hash[$fk] = $this->$fk;}}
			}
		} else {
			$this->_oEvent->fireEvent('RaiseError','No object constructor called');
		}
		return $hash;
	}

	
	/*
	* Default Events Methods
	* If an Event haven't a default method declared here, then the event definition of $this->onEvent type is obligatory
	*
	*/
	public function fnLoad (){return true;}
	public function fnRender (){return true;}
	public function fnDisplay (){return true;}
	public function fnPostRequest (){return true;}
	public function fnGetRequest (){return true;}
	public function fnHeadRequest (){return true;}
	public function fnPutRequest (){return true;}
	public function fnNoRequest (){return true;}
	public function fnReadyStateChange (){return true;}
	public function fnError (){return true;}
	public function fnShutDown (){return true;}
	public function fnAction (){return true;}
	public function fnRedirect (){return true;}
	
}
?>