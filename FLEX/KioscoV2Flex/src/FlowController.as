package
{
	import mx.controls.Alert;
	import mx.core.FlexGlobals;

	public class FlowController
	{
		public function FlowController()
		{
		}
		
		protected static function getPreviousIndexExtranjero(currentIndex:Number, paso:String):Number{
			var newIndex:Number;
			
			switch(paso)
			{
				case Constants.HOME:{
					newIndex = Constants.HOME_INDEX;
					break;
				}
				case Constants.EXTRANJERO_MOTIVOS_CONSULTA_NAME:{
					newIndex = Constants.HOME_INDEX;
					break;
				}
				case Constants.EXTRANJERO_SUBMOTIVOS_NAME:{
					newIndex = Constants.EXTRANJERO_MOTIVOS_CONSULTA_INDEX;
					break;
				}
				case Constants.EXTRANJERO_RESUMEN_NAME:{
					newIndex = Constants.EXTRANJERO_SUBMOTIVOS_INDEX;
					break;
					
				}
				case Constants.EXTRANJERO_IDENTIFICACION_NAME:{
					newIndex = Constants.EXTRANJERO_RESUMEN_INDEX;
					break;
					
				}
				case Constants.EXTRANJERO_FECHA_NAME:{
					newIndex = Constants.EXTRANJERO_IDENTIFICACION_INDEX;
					break;
				}
				case Constants.EXTRANJERO_DATOS_PERSONALES_NAME:{
					newIndex = Constants.EXTRANJERO_FECHA_INDEX;
					break;
					
				}
				case Constants.EXTRANJERO_PAGO_PARTICULAR_NAME:{
					newIndex = Constants.EXTRANJERO_DATOS_PERSONALES_INDEX;
					break;
					
				}
				default:{
					newIndex = currentIndex;
					break;
					
				}
			}
			
			return newIndex;
			
		}

		protected static function getNextIndexExtranjero(currentIndex:Number, paso:String):Number{
			var newIndex:Number;
			
			switch(paso)
			{
				case Constants.HOME:{
					newIndex = Constants.EXTRANJERO_MOTIVOS_CONSULTA_INDEX;
					break;
				}
				case Constants.EXTRANJERO_MOTIVOS_CONSULTA_NAME:{
					newIndex = Constants.EXTRANJERO_SUBMOTIVOS_INDEX;
					break;
					
				}
				case Constants.EXTRANJERO_SUBMOTIVOS_NAME:{
					newIndex = Constants.EXTRANJERO_RESUMEN_INDEX;
					break;
					
				}
				case Constants.EXTRANJERO_RESUMEN_NAME:{
					newIndex = Constants.EXTRANJERO_IDENTIFICACION_INDEX;
					break;
					
				}
				case Constants.EXTRANJERO_IDENTIFICACION_NAME:{
					newIndex = Constants.EXTRANJERO_FECHA_INDEX;
					break;
					
				}
				case Constants.EXTRANJERO_FECHA_NAME:{
					newIndex = Constants.EXTRANJERO_DATOS_PERSONALES_INDEX;
					break;
					
				}
				case Constants.EXTRANJERO_DATOS_PERSONALES_NAME:{
					FlexGlobals.topLevelApplication.consulta.botonPagoIMED = 1;
					FlexGlobals.topLevelApplication.consulta.accionTransaccion = "P";
					FlexGlobals.topLevelApplication.consulta.activarAutentia = false;
					newIndex = Constants.EXTRANJERO_PAGO_PARTICULAR_INDEX;
					break;
					
				}
				case Constants.EXTRANJERO_PAGO_PARTICULAR_NAME:{
					newIndex = Constants.EXTRANJERO_EXITO_INDEX;
					break;
					
				}
				case Constants.EXTRANJERO_EXITO_NAME:{
					newIndex = Constants.HOME_INDEX;
					break;
					
				}
				default:{
					newIndex = currentIndex;
					break;
					
				}
			}
			
			return newIndex;
			
		}

		protected static function getPreviousIndexNacional(currentIndex:Number, paso:String):Number{
			var newIndex:Number;
			
			switch(paso)
			{
				case Constants.HOME:{
					break;
				}
				case Constants.NACIONAL_MOTIVOS_CONSULTA_NAME:{
					newIndex = Constants.HOME_INDEX;
					break;
				}
				case Constants.NACIONAL_SUBMOTIVOS_NAME:{
					newIndex = Constants.NACIONAL_MOTIVOS_CONSULTA_INDEX;
					break;
				}
				case Constants.NACIONAL_PREVISION_NAME:{
					newIndex = Constants.NACIONAL_MOTIVOS_CONSULTA_INDEX;
					break;
				}
				case Constants.NACIONAL_RESUMEN_NAME:{
					newIndex = Constants.NACIONAL_PREVISION_INDEX;
					break;
				}
				case Constants.NACIONAL_RUT_NAME:{
					newIndex = Constants.NACIONAL_RESUMEN_INDEX;
					break;
				}
				/*
				case Constants.NACIONAL_TICKET_NAME:{
					newIndex = Constants.NACIONAL_RUT_INDEX;
					break;
				}
				case Constants.NACIONAL_FECHA_NAME:{
					newIndex = Constants.NACIONAL_TICKET_INDEX;
					break;
				}
				*/
				/* aca */
				case Constants.NACIONAL_FECHA_NAME:{
					newIndex = Constants.NACIONAL_RUT_INDEX;
					break;
				}
				case Constants.NACIONAL_DATOS_PERSONALES_NAME:{
					newIndex = Constants.NACIONAL_FECHA_INDEX;
					break;
				}
				case Constants.NACIONAL_BOTON_PAGO_PARTICULAR_NAME:{
					newIndex = Constants.NACIONAL_DATOS_PERSONALES_INDEX;
					break;
				}
				case Constants.NACIONAL_BOTON_PAGO_BONO_NAME:{
					newIndex = Constants.NACIONAL_DATOS_PERSONALES_INDEX;
					break;
				}
				default:{
					newIndex = currentIndex;
					break;
				}
			}
			
			return newIndex;
			
		}
		
		protected static function getNextIndexNacional(currentIndex:Number, paso:String):Number{
			var newIndex:Number;
			
			switch(paso)
			{
				case Constants.HOME:{
					newIndex = Constants.NACIONAL_MOTIVOS_CONSULTA_INDEX;
					break;
					
				}
				case Constants.NACIONAL_MOTIVOS_CONSULTA_NAME:{
					newIndex = Constants.NACIONAL_SUBMOTIVOS_INDEX;
					break;
					
				}
				case Constants.NACIONAL_SUBMOTIVOS_NAME:{
					newIndex = Constants.NACIONAL_PREVISION_INDEX;
					break;
					
				}
				case Constants.NACIONAL_PREVISION_NAME:{
					newIndex = Constants.NACIONAL_RESUMEN_INDEX;
					break;
					
				}
				case Constants.NACIONAL_RESUMEN_NAME:{
					newIndex = Constants.NACIONAL_RUT_INDEX;
					break;
					
				}
				/* aca */
				case Constants.NACIONAL_RUT_NAME:{
					newIndex = Constants.NACIONAL_FECHA_INDEX;
					break;
					
				}
				/*
				case Constants.NACIONAL_RUT_NAME:{
					newIndex = Constants.NACIONAL_TICKET_INDEX;
					break;
					
				}
				case Constants.NACIONAL_TICKET_NAME:{
					newIndex = Constants.NACIONAL_FECHA_INDEX;
					break;
				}
				*/
				case Constants.NACIONAL_FECHA_NAME:{
					newIndex = Constants.NACIONAL_DATOS_PERSONALES_INDEX;
					break;
					
				}
				case Constants.NACIONAL_DATOS_PERSONALES_NAME:{
					switch(FlexGlobals.topLevelApplication.consulta.prevision.tipoPrevision)
					{
						case Constants.TIPO_PARTICULAR:
						{
							//particular
							FlexGlobals.topLevelApplication.consulta.accionTransaccion = "P";
							FlexGlobals.topLevelApplication.consulta.botonPagoIMED = 1;
							FlexGlobals.topLevelApplication.consulta.activarAutentia = false;
							newIndex = Constants.NACIONAL_BOTON_PAGO_PARTICULAR_INDEX;
							break;
						}
						case Constants.TIPO_FONASA:
						{
							//fonasa
							FlexGlobals.topLevelApplication.consulta.accionTransaccion = "E";
							FlexGlobals.topLevelApplication.consulta.botonPagoIMED = 0;
							FlexGlobals.topLevelApplication.consulta.activarAutentia = true;
							newIndex = Constants.NACIONAL_BOTON_PAGO_BONO_INDEX;
							break;
						}
							
						default:
						{
							//isapre 
							FlexGlobals.topLevelApplication.consulta.accionTransaccion = "E";
							FlexGlobals.topLevelApplication.consulta.botonPagoIMED = 0;
							FlexGlobals.topLevelApplication.consulta.activarAutentia = true;
							newIndex = Constants.NACIONAL_BOTON_PAGO_BONO_INDEX;
							break;
						}
					}
					break;
					
				}
				case Constants.NACIONAL_BOTON_PAGO_PARTICULAR_NAME:{
					switch(FlexGlobals.topLevelApplication.consulta.estadoTransaccion)
					{
						case Constants.ESTADO_TRANSACCION_EN_CURSO:
						{
							if (FlexGlobals.topLevelApplication.consulta.formaPago == Constants.FORMA_PAGO_MULTIPAY){
								newIndex = Constants.NACIONAL_BOTON_PAGO_BONO_INDEX;
							} else {
								newIndex = currentIndex;
							}
							break;
						}
						case Constants.ESTADO_TRANSACCION_EXITOSA:
						{
							newIndex = Constants.EXITO_INDEX;
							break;
						}
						case Constants.ESTADO_TRANSACCION_FRACASADA:
						{
							newIndex = Constants.FRACASO_INDEX;
							break;
						}
							
						default:
						{
							newIndex = currentIndex;
							break;
						}
					}
					break;
					
				}
				case Constants.NACIONAL_BOTON_PAGO_BONO_NAME:{
					switch(FlexGlobals.topLevelApplication.consulta.estadoTransaccion)
					{
						case Constants.ESTADO_TRANSACCION_EN_CURSO:
						{
							// pago transbank desde imed
							break;
						}
						case Constants.ESTADO_TRANSACCION_EXITOSA:
						{
							newIndex = Constants.EXITO_INDEX;
							break;
						}
						case Constants.ESTADO_TRANSACCION_FRACASADA:
						{
							newIndex = Constants.FRACASO_INDEX;
							trace("transaccion fracasada");
							break;
						}
							
						default:
						{
							newIndex=currentIndex;
							break;
						}
					}
					break;
					
				}
				default:{
					newIndex = currentIndex;
					break;
					
				}
			}
			
			return newIndex;
			
		}
		
		public static function getNextIndex(currentIndex:Number, paso:String):Number
		{
			var newIndex:Number;
			
			switch(FlexGlobals.topLevelApplication.consulta.tipoFlujo)
			{
				case Constants.TIPO_FLUJO_NACIONAL:
				{
					newIndex = getNextIndexNacional(currentIndex,paso);
					break;
				}
					
				case Constants.TIPO_FLUJO_EXTRANJERO:
				{
					newIndex = getNextIndexExtranjero(currentIndex,paso);
					break;
				}
					
				default:
				{
					newIndex = currentIndex;
					break;
				}
			}
			return newIndex;
		}
		
		public static function getPreviousIndex(currentIndex:Number, paso:String):Number
		{
			var newIndex:Number;
			
			switch(FlexGlobals.topLevelApplication.consulta.tipoFlujo)
			{
				case Constants.TIPO_FLUJO_NACIONAL:
				{
					newIndex = getPreviousIndexNacional(currentIndex,paso);
					break;
				}
					
				case Constants.TIPO_FLUJO_EXTRANJERO:
				{
					newIndex = getPreviousIndexExtranjero(currentIndex,paso);
					break;
				}
					
				default:
				{
					break;
				}
			}
			
			
			return newIndex;
		}
	}
}