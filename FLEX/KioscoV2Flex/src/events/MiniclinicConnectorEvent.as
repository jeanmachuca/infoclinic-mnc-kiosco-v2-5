package events
{
	import flash.events.Event;
	
	public class MiniclinicConnectorEvent extends Event
	{

		public static const ValorizarResultExito:String = 'ValorizarResultExito';
		public static const ValorizarResultFracaso:String = 'ValorizarResultFracaso';
		public static const EstadoImpresoraResultExito:String = 'EstadoImpresoraResultExito';
		public static const EstadoImpresoraResultFracaso:String = 'EstadoImpresoraResultFracaso';
		public static const HomeResultExito:String = 'HomeResultExito';
		public static const HomeResultFracaso:String = 'HomeResultFracaso';
		public static const MotivosResultExito:String = 'MotivosResultExito';
		public static const MotivosResultFracaso:String = 'MotivosResultFracaso';
		public static const ResumenResultExito:String = 'ResumenResultExito';
		public static const ResumenResultFracaso:String = 'ResumenResultFracaso';
		public static const IdentificacionResultExito:String = 'IdentificacionResultExito';
		public static const IdentificacionResultFracaso:String = 'IdentificacionResultFracaso';
		public static const DatosPersonalesResultExito:String = 'DatosPersonalesResultExito';
		public static const DatosPersonalesResultFracaso:String = 'DatosPersonalesResultFracaso';
		public static const FinalizarResultExito:String = 'FinalizarResultExito';
		public static const FinalizarResultFracaso:String = 'FinalizarResultFracaso';
		public static const AnularConsultaResultExito:String = 'AnularConsultaResultExito';
		public static const AnularConsultaResultFracaso:String = 'AnularConsultaResultFracaso';
		public static const FatalErrorResult:String = 'FatalErrorResult';
		
		
		public function MiniclinicConnectorEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(type, bubbles, cancelable);
		}
	}
}