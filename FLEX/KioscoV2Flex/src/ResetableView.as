package
{
	public interface ResetableView
	{
		function load(oldIndex:int):void;
		function reset(oldIndex:int):void;
		function loaded(secureIndex:int):void;
	}
}