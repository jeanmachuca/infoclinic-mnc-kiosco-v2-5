/* vim: set expandtab tabstop=4 shiftwidth=4: */
/**
 * @package Five Multipurpose Framework
 * @subpackage NativeProcessClientEvent Class
 * 
 * @version "3.23.09.2011"
 * 
 * @copyright 2000-2020 Jean Machuca.
 * @author  "Jean Machuca" <correojean@gmail.com>
 * @license http://en.wikipedia.org/wiki/GNU_General_Public_License GPL
 * 
 * Simple Native Process Call for AIR 3.2
 * 
 */
package five.connectivity.client
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;

	/**
	 * 
	 * 
	 */
	public class NativeProcessClientEvent extends Event 
	{
		/**
		 * 
		 */
		public static const OUTPUT_DATA:String = "dataOK";
		public static const ERROR_DATA:String = "errorData";
		public static const EXIT:String = "exit";
		public static const OUTPUT_IO_ERROR:String = "output_IO_Error";
		public static const ERROR_IO_ERROR:String = "IO_Error";
		

		[Bindable]
		public var result:String;
		
		public function NativeProcessClientEvent(type:String="None", bubbles:Boolean=false, cancelable:Boolean=false)
		{

			super(type, bubbles, cancelable);
		}
		

	}
}