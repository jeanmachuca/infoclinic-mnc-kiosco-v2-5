/* vim: set expandtab tabstop=4 shiftwidth=4: */
/**
 * @package Five Multipurpose Framework
 * @subpackage NativeProcessClient Class
 * 
 * @version "3.23.09.2011"
 * 
 * @copyright 2000-2020 Jean Machuca.
 * @author  "Jean Machuca" <correojean@gmail.com>
 * @license http://en.wikipedia.org/wiki/GNU_General_Public_License GPL
 * 
 * Simple Native Process Call for AIR 3.2
 * 
 */
package five.connectivity.client
{/*
	import flash.desktop.NativeProcess;
	import flash.desktop.NativeProcessStartupInfo;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.NativeProcessExitEvent;
	import flash.events.ProgressEvent;
	import flash.filesystem.File;*/
	
	import mx.core.Container;
	
	/**
	 * Dispatched when the process call is done with valid output data
	 * 
	 * @eventType five.connectivity.client.NativeProcessClientEvent.OUTPUT_DATA
	 */
	[Event(name="dataOK", type="five.connectivity.client.NativeProcessClientEvent")]
	
	/**
	 * Dispatched when the process call is done with errors
	 * 
	 * @eventType five.connectivity.client.NativeProcessClientEvent.ERROR_DATA
	 */
	[Event(name="errorData", type="five.connectivity.client.NativeProcessClientEvent")]
	
	/**
	 * Dispatched when the process call is interrupted
	 * 
	 * @eventType five.connectivity.client.NativeProcessClientEvent.EXIT
	 */
	[Event(name="exit", type="five.connectivity.client.NativeProcessClientEvent")]
	
	/**
	 * Dispatched when the process call had an IO error
	 * 
	 * @eventType five.connectivity.client.NativeProcessClientEvent.OUTPUT_IO_ERROR
	 */
	[Event(name="output_IO_Error", type="five.connectivity.client.NativeProcessClientEvent")]
	
	/**
	 * Dispatched when the process call had a Uncaugh IO Error
	 * 
	 * @eventType five.connectivity.client.NativeProcessClientEvent.ERROR_IO_ERROR
	 */
	[Event(name="IO_Error", type="five.connectivity.client.NativeProcessClientEvent")]
	
	/**
	 * @see NativeProcess
	 * 
	 */
	public class NativeProcessClient extends Sprite
	{
		
		
		public const SUCCESS_CODE:String = "E";
		public const FAULT_CODE:String = "F";
		
		
		//public var process:NativeProcess;
		
		public var MAIN_DIR:String = '/';

		private var _command:String = 'run.exe';
		private var _paramString:String = '';
		private var _operation:String = '';
		
		/**  return properties **/
		public var CodResult:String = '';
		public var MsgResulResult:String = '';
		
		public var result:String;
		
		
		/**
		 * 
		 * @param command
		 * @param operation
		 * @param paramString
		 * 
		 */
		public function NativeProcessClient()
		{
		}
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function call():void{
			if(NativeProcess.isSupported)
			{
				setupAndLaunch();
			}
			else
			{
				trace("NativeProcess not supported.");
			}
			
		}
		
		/**
		 * 
		 * 
		 */
		protected function setupAndLaunch():void
		{     
			process = new NativeProcess();
			
			var nativeProcessStartupInfo:NativeProcessStartupInfo = new NativeProcessStartupInfo();
			var file:File = File.applicationDirectory.resolvePath(MAIN_DIR+command);
			nativeProcessStartupInfo.executable = file;
			
			var processArgs:Vector.<String> = new Vector.<String>();
			processArgs[0] = operation;
			for each (var param:String in paramString.split(" ")){
				processArgs.push(param);
			}
			nativeProcessStartupInfo.arguments = processArgs;
			
			process.start(nativeProcessStartupInfo);
			
			process.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, ondataOK);
			process.addEventListener(ProgressEvent.STANDARD_ERROR_DATA, onErrorData);
			process.addEventListener(NativeProcessExitEvent.EXIT, onExit);
			process.addEventListener(IOErrorEvent.STANDARD_OUTPUT_IO_ERROR, onIOError);
			process.addEventListener(IOErrorEvent.STANDARD_ERROR_IO_ERROR, onIOError);
		}
		
		/**
		 * 
		 * @param event
		 * 
		 */
		protected function ondataOK(event:ProgressEvent):void
		{
			result = process.standardOutput.readUTFBytes(process.standardOutput.bytesAvailable);
			CodResult = SUCCESS_CODE;
			trace("Got: ", result);
			var evt:NativeProcessClientEvent = new NativeProcessClientEvent(NativeProcessClientEvent.OUTPUT_DATA);
			evt.result = result;
			dispatchEvent(evt);
		}
		
		/**
		 * 
		 * @param event
		 * 
		 */
		protected function onErrorData(event:ProgressEvent):void
		{
			result = process.standardError.readUTFBytes(process.standardError.bytesAvailable);
			CodResult = FAULT_CODE;
			trace("ERROR -", result);
			var evt:NativeProcessClientEvent = new NativeProcessClientEvent(NativeProcessClientEvent.ERROR_DATA);
			evt.result = result;
			dispatchEvent(evt);
		}
		
		/**
		 * 
		 * @param event
		 * 
		 */
		protected function onExit(event:NativeProcessExitEvent):void
		{
			CodResult = SUCCESS_CODE;
			result = event.exitCode as String;
			trace("Process exited with ", CodResult);
			var evt:NativeProcessClientEvent = new NativeProcessClientEvent(NativeProcessClientEvent.EXIT);
			evt.result = result;
			dispatchEvent(evt);
		}
		
		/**
		 * 
		 * @param event
		 * 
		 */
		protected function onIOError(event:IOErrorEvent):void
		{
			result = event.toString();
			CodResult = FAULT_CODE;
			trace(result);
			var evt:NativeProcessClientEvent = new NativeProcessClientEvent(NativeProcessClientEvent.ERROR_IO_ERROR);
			evt.result = result;
			dispatchEvent(evt);
		}

		[Bindable(event="commandChange")]
		public function get command():String
		{
			return _command;
		}

		public function set command(value:String):void
		{
			if( _command !== value)
			{
				_command = value;
				dispatchEvent(new Event("commandChange"));
			}
		}

		[Bindable(event="paramStringChange")]
		public function get paramString():String
		{
			return _paramString;
		}

		public function set paramString(value:String):void
		{
			if( _paramString !== value)
			{
				_paramString = value;
				dispatchEvent(new Event("paramStringChange"));
			}
		}

		[Bindable(event="operationChange")]
		public function get operation():String
		{
			return _operation;
		}

		public function set operation(value:String):void
		{
			if( _operation !== value)
			{
				_operation = value;
				dispatchEvent(new Event("operationChange"));
			}
		}

	}
}
