package
{
	import flash.events.EventDispatcher;
	
	import views.component.Effects;

	public class Constants extends EventDispatcher
	{
		//public static const ID_KIOSCO:Number = 1;
		
		public static const ESTADO_TRANSACCION_EN_CURSO:int = 0;
		public static const ESTADO_TRANSACCION_EXITOSA:int = 1;
		public static const ESTADO_TRANSACCION_FRACASADA:int = 2;
		
		public static const SERVER_REQUEST_TIMEOUT:String = "Client.Error.RequestTimeout";
		public static const SERVER_REQUEST_SUCCESS:String= "E";
		public static const SERVER_REQUEST_FAULT:String= "F";
		
		
		public static const HOME:String = 'HOME';
		public static const HOME_INDEX:int = 0;
		public static const NACIONAL:String = 'NACIONAL';
		public static const NACIONAL_PARTICULAR:String = 'NACIONAL_PARTICULAR';
		public static const NACIONAL_PARTICULAR_TBK:String = 'NACIONAL_PARTICULAR_TBK';
		public static const NACIONAL_PARTICULAR_MP:String = 'NACIONAL_PARTICULAR_MP';
		public static const NACIONAL_PREVISION:String = 'NACIONAL_PREVISION';
		public static const NACIONAL_PREVISION_TBK:String = 'NACIONAL_PREVISION';
		public static const NACIONAL_PREVISION_MP:String = 'NACIONAL_PREVISION';
		

		public static const NACIONAL_MOTIVOS_CONSULTA_NAME:String = 'NACIONAL_MOTIVOS_CONSULTA';
		public static const NACIONAL_MOTIVOS_CONSULTA_INDEX:int = 1;
		public static const NACIONAL_SUBMOTIVOS_NAME:String = 'NACIONAL_SUBMOTIVOS';
		public static const NACIONAL_SUBMOTIVOS_INDEX:int = 2;
		public static const NACIONAL_PREVISION_NAME:String = 'NACIONAL_PREVISION';
		public static const NACIONAL_PREVISION_INDEX:int = 3;
		public static const NACIONAL_RESUMEN_NAME:String = 'NACIONAL_RESUMEN';
		public static const NACIONAL_RESUMEN_INDEX:int = 4;
		public static const NACIONAL_RUT_NAME:String = 'NACIONAL_RUT';
		public static const NACIONAL_RUT_INDEX:int = 5;
		public static const NACIONAL_FECHA_NAME:String = 'NACIONAL_FECHA';
		public static const NACIONAL_FECHA_INDEX:int = 6;
		public static const NACIONAL_DATOS_PERSONALES_NAME:String = "NACIONAL_DATOS_PERSONALES";
		public static const NACIONAL_DATOS_PERSONALES_INDEX:int = 7;

		public static const NACIONAL_BOTON_PAGO_PARTICULAR_INDEX:int = 8;
		public static const NACIONAL_BOTON_PAGO_PARTICULAR_NAME:String = "NACIONAL_BOTON_PAGO_PARTICULAR";
		
		public static const NACIONAL_BOTON_PAGO_BONO_INDEX:int = 9;
		public static const NACIONAL_BOTON_PAGO_BONO_NAME:String = "NACIONAL_BOTON_PAGO_BONO";
		
		public static const EXTRANJERO:String = 'EXTRANJERO';
		public static const EXTRANJERO_MOTIVOS_CONSULTA_NAME:String = 'EXTRANJERO_MOTIVOS_CONSULTA';
		public static const EXTRANJERO_MOTIVOS_CONSULTA_INDEX:int = 12;
		public static const EXTRANJERO_SUBMOTIVOS_NAME:String = 'EXTRANJERO_SUBMOTIVOS';
		public static const EXTRANJERO_SUBMOTIVOS_INDEX:int = 13;
		public static const EXTRANJERO_RESUMEN_NAME:String = 'EXTRANJERO_RESUMEN';
		public static const EXTRANJERO_RESUMEN_INDEX:int = 14;
		public static const EXTRANJERO_IDENTIFICACION_NAME:String = 'EXTRANJERO_IDENTIFICACION';
		public static const EXTRANJERO_IDENTIFICACION_INDEX:int = 15;
		public static const EXTRANJERO_FECHA_NAME:String = 'EXTRANJERO_FECHA';
		public static const EXTRANJERO_FECHA_INDEX:int = 16;
		public static const EXTRANJERO_DATOS_PERSONALES_NAME:String = 'EXTRANJERO_DATOS_PERSONALES';
		public static const EXTRANJERO_DATOS_PERSONALES_INDEX:int = 17;
		public static const EXTRANJERO_PAGO_PARTICULAR_NAME:String = 'EXTRANJERO_PAGO_PARTICULAR';
		public static const EXTRANJERO_PAGO_PARTICULAR_INDEX:int = 18;
		public static const EXTRANJERO_EXITO_NAME:String = 'EXTRANJERO_EXITO';
		public static const EXTRANJERO_EXITO_INDEX:int = 19;
		public static const EXTRANJERO_FRACASO_NAME:String = 'EXTRANJERO_EXITO';
		public static const EXTRANJERO_FRACASO_INDEX:int = 20;
		
		public static const NACIONAL_TICKET_INDEX:int = 21;
		public static const NACIONAL_TICKET_NAME:String = 'NACIONAL_TICKET';
		
		
		public static const EXITO_INDEX:int = 10;
		public static const FRACASO_INDEX:int = 11;
		
		public static const TIPO_PARTICULAR:String = "0";
		public static const TIPO_FONASA:String = "1";
		public static const TIPO_ISAPRE:String = "2";
		
		public static const TIPO_FLUJO_NACIONAL:int = 1;
		public static const TIPO_FLUJO_EXTRANJERO:int = 2;
		public static const FORMA_PAGO_TRANSBANK:int = 1;
		public static const FORMA_PAGO_MULTIPAY:int = 2;
		public static const TIPO_IDENTIFICACION_NACIONAL:int = 1;
		public static const TIPO_IDENTIFICACION_EXTRANJERO:int = 2;
		
		public static const GENERIC_ERROR_MESSAGE:String = "Error Desconocido";
		
		public static const EFFECTS:Effects = new Effects();
		
		public function Constants() {}
	}		
}