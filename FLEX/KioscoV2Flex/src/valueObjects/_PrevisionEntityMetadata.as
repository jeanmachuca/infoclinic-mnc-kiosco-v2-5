
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.styles.StyleValidator;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import mx.events.ValidationResultEvent;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _PrevisionEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("codigoPrevision", "tipoPrevision", "nombrePrevision");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array("codigoPrevision", "tipoPrevision", "nombrePrevision");
    model_internal static var allAlwaysAvailableProperties:Array = new Array("codigoPrevision", "tipoPrevision", "nombrePrevision");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("codigoPrevision", "tipoPrevision", "nombrePrevision");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("codigoPrevision", "tipoPrevision", "nombrePrevision");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array();
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "Prevision";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;

    
    model_internal var _codigoPrevisionIsValid:Boolean;
    model_internal var _codigoPrevisionValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _codigoPrevisionIsValidCacheInitialized:Boolean = false;
    model_internal var _codigoPrevisionValidationFailureMessages:Array;
    
    model_internal var _tipoPrevisionIsValid:Boolean;
    model_internal var _tipoPrevisionValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _tipoPrevisionIsValidCacheInitialized:Boolean = false;
    model_internal var _tipoPrevisionValidationFailureMessages:Array;
    
    model_internal var _nombrePrevisionIsValid:Boolean;
    model_internal var _nombrePrevisionValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _nombrePrevisionIsValidCacheInitialized:Boolean = false;
    model_internal var _nombrePrevisionValidationFailureMessages:Array;

    model_internal var _instance:_Super_Prevision;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _PrevisionEntityMetadata(value : _Super_Prevision)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["codigoPrevision"] = new Array();
            model_internal::dependentsOnMap["tipoPrevision"] = new Array();
            model_internal::dependentsOnMap["nombrePrevision"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["codigoPrevision"] = "String";
        model_internal::propertyTypeMap["tipoPrevision"] = "String";
        model_internal::propertyTypeMap["nombrePrevision"] = "String";

        model_internal::_instance = value;
        model_internal::_codigoPrevisionValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForCodigoPrevision);
        model_internal::_codigoPrevisionValidator.required = true;
        model_internal::_codigoPrevisionValidator.requiredFieldError = "codigoPrevision is required";
        //model_internal::_codigoPrevisionValidator.source = model_internal::_instance;
        //model_internal::_codigoPrevisionValidator.property = "codigoPrevision";
        model_internal::_tipoPrevisionValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForTipoPrevision);
        model_internal::_tipoPrevisionValidator.required = true;
        model_internal::_tipoPrevisionValidator.requiredFieldError = "tipoPrevision is required";
        //model_internal::_tipoPrevisionValidator.source = model_internal::_instance;
        //model_internal::_tipoPrevisionValidator.property = "tipoPrevision";
        model_internal::_nombrePrevisionValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForNombrePrevision);
        model_internal::_nombrePrevisionValidator.required = true;
        model_internal::_nombrePrevisionValidator.requiredFieldError = "nombrePrevision is required";
        //model_internal::_nombrePrevisionValidator.source = model_internal::_instance;
        //model_internal::_nombrePrevisionValidator.property = "nombrePrevision";
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity Prevision");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity Prevision");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of Prevision");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity Prevision");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity Prevision");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity Prevision");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isCodigoPrevisionAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTipoPrevisionAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isNombrePrevisionAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */
    public function invalidateDependentOnCodigoPrevision():void
    {
        if (model_internal::_codigoPrevisionIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfCodigoPrevision = null;
            model_internal::calculateCodigoPrevisionIsValid();
        }
    }
    public function invalidateDependentOnTipoPrevision():void
    {
        if (model_internal::_tipoPrevisionIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfTipoPrevision = null;
            model_internal::calculateTipoPrevisionIsValid();
        }
    }
    public function invalidateDependentOnNombrePrevision():void
    {
        if (model_internal::_nombrePrevisionIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfNombrePrevision = null;
            model_internal::calculateNombrePrevisionIsValid();
        }
    }

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get codigoPrevisionStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get codigoPrevisionValidator() : StyleValidator
    {
        return model_internal::_codigoPrevisionValidator;
    }

    model_internal function set _codigoPrevisionIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_codigoPrevisionIsValid;         
        if (oldValue !== value)
        {
            model_internal::_codigoPrevisionIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "codigoPrevisionIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get codigoPrevisionIsValid():Boolean
    {
        if (!model_internal::_codigoPrevisionIsValidCacheInitialized)
        {
            model_internal::calculateCodigoPrevisionIsValid();
        }

        return model_internal::_codigoPrevisionIsValid;
    }

    model_internal function calculateCodigoPrevisionIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_codigoPrevisionValidator.validate(model_internal::_instance.codigoPrevision)
        model_internal::_codigoPrevisionIsValid_der = (valRes.results == null);
        model_internal::_codigoPrevisionIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::codigoPrevisionValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::codigoPrevisionValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get codigoPrevisionValidationFailureMessages():Array
    {
        if (model_internal::_codigoPrevisionValidationFailureMessages == null)
            model_internal::calculateCodigoPrevisionIsValid();

        return _codigoPrevisionValidationFailureMessages;
    }

    model_internal function set codigoPrevisionValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_codigoPrevisionValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_codigoPrevisionValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "codigoPrevisionValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get tipoPrevisionStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get tipoPrevisionValidator() : StyleValidator
    {
        return model_internal::_tipoPrevisionValidator;
    }

    model_internal function set _tipoPrevisionIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_tipoPrevisionIsValid;         
        if (oldValue !== value)
        {
            model_internal::_tipoPrevisionIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "tipoPrevisionIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get tipoPrevisionIsValid():Boolean
    {
        if (!model_internal::_tipoPrevisionIsValidCacheInitialized)
        {
            model_internal::calculateTipoPrevisionIsValid();
        }

        return model_internal::_tipoPrevisionIsValid;
    }

    model_internal function calculateTipoPrevisionIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_tipoPrevisionValidator.validate(model_internal::_instance.tipoPrevision)
        model_internal::_tipoPrevisionIsValid_der = (valRes.results == null);
        model_internal::_tipoPrevisionIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::tipoPrevisionValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::tipoPrevisionValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get tipoPrevisionValidationFailureMessages():Array
    {
        if (model_internal::_tipoPrevisionValidationFailureMessages == null)
            model_internal::calculateTipoPrevisionIsValid();

        return _tipoPrevisionValidationFailureMessages;
    }

    model_internal function set tipoPrevisionValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_tipoPrevisionValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_tipoPrevisionValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "tipoPrevisionValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get nombrePrevisionStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get nombrePrevisionValidator() : StyleValidator
    {
        return model_internal::_nombrePrevisionValidator;
    }

    model_internal function set _nombrePrevisionIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_nombrePrevisionIsValid;         
        if (oldValue !== value)
        {
            model_internal::_nombrePrevisionIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "nombrePrevisionIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get nombrePrevisionIsValid():Boolean
    {
        if (!model_internal::_nombrePrevisionIsValidCacheInitialized)
        {
            model_internal::calculateNombrePrevisionIsValid();
        }

        return model_internal::_nombrePrevisionIsValid;
    }

    model_internal function calculateNombrePrevisionIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_nombrePrevisionValidator.validate(model_internal::_instance.nombrePrevision)
        model_internal::_nombrePrevisionIsValid_der = (valRes.results == null);
        model_internal::_nombrePrevisionIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::nombrePrevisionValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::nombrePrevisionValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get nombrePrevisionValidationFailureMessages():Array
    {
        if (model_internal::_nombrePrevisionValidationFailureMessages == null)
            model_internal::calculateNombrePrevisionIsValid();

        return _nombrePrevisionValidationFailureMessages;
    }

    model_internal function set nombrePrevisionValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_nombrePrevisionValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_nombrePrevisionValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "nombrePrevisionValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            case("codigoPrevision"):
            {
                return codigoPrevisionValidationFailureMessages;
            }
            case("tipoPrevision"):
            {
                return tipoPrevisionValidationFailureMessages;
            }
            case("nombrePrevision"):
            {
                return nombrePrevisionValidationFailureMessages;
            }
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
