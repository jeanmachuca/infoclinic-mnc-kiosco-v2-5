
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.styles.StyleValidator;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import mx.events.ValidationResultEvent;
import valueObjects.ListaSintomas;
import valueObjects.ListaVacunas;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _TipoConsultaEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("idPadre", "nombre", "codigo", "ListaVacunas", "ListaSintomas", "valor", "esPrecio", "padre", "idTipoConsulta");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array("nombre", "codigo", "ListaVacunas", "ListaSintomas", "valor", "esPrecio", "padre", "idTipoConsulta");
    model_internal static var allAlwaysAvailableProperties:Array = new Array("idPadre", "nombre", "codigo", "ListaVacunas", "ListaSintomas", "valor", "esPrecio", "padre", "idTipoConsulta");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("idPadre", "nombre", "codigo", "ListaVacunas", "ListaSintomas", "valor", "esPrecio", "padre", "idTipoConsulta");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("idPadre", "nombre", "codigo", "ListaVacunas", "ListaSintomas", "valor", "esPrecio", "padre", "idTipoConsulta");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array();
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "TipoConsulta";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;

    
    model_internal var _nombreIsValid:Boolean;
    model_internal var _nombreValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _nombreIsValidCacheInitialized:Boolean = false;
    model_internal var _nombreValidationFailureMessages:Array;
    
    model_internal var _codigoIsValid:Boolean;
    model_internal var _codigoValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _codigoIsValidCacheInitialized:Boolean = false;
    model_internal var _codigoValidationFailureMessages:Array;
    
    model_internal var _ListaVacunasIsValid:Boolean;
    model_internal var _ListaVacunasValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _ListaVacunasIsValidCacheInitialized:Boolean = false;
    model_internal var _ListaVacunasValidationFailureMessages:Array;
    
    model_internal var _ListaSintomasIsValid:Boolean;
    model_internal var _ListaSintomasValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _ListaSintomasIsValidCacheInitialized:Boolean = false;
    model_internal var _ListaSintomasValidationFailureMessages:Array;
    
    model_internal var _valorIsValid:Boolean;
    model_internal var _valorValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _valorIsValidCacheInitialized:Boolean = false;
    model_internal var _valorValidationFailureMessages:Array;
    
    model_internal var _esPrecioIsValid:Boolean;
    model_internal var _esPrecioValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _esPrecioIsValidCacheInitialized:Boolean = false;
    model_internal var _esPrecioValidationFailureMessages:Array;
    
    model_internal var _padreIsValid:Boolean;
    model_internal var _padreValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _padreIsValidCacheInitialized:Boolean = false;
    model_internal var _padreValidationFailureMessages:Array;
    
    model_internal var _idTipoConsultaIsValid:Boolean;
    model_internal var _idTipoConsultaValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _idTipoConsultaIsValidCacheInitialized:Boolean = false;
    model_internal var _idTipoConsultaValidationFailureMessages:Array;

    model_internal var _instance:_Super_TipoConsulta;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _TipoConsultaEntityMetadata(value : _Super_TipoConsulta)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["idPadre"] = new Array();
            model_internal::dependentsOnMap["nombre"] = new Array();
            model_internal::dependentsOnMap["codigo"] = new Array();
            model_internal::dependentsOnMap["ListaVacunas"] = new Array();
            model_internal::dependentsOnMap["ListaSintomas"] = new Array();
            model_internal::dependentsOnMap["valor"] = new Array();
            model_internal::dependentsOnMap["esPrecio"] = new Array();
            model_internal::dependentsOnMap["padre"] = new Array();
            model_internal::dependentsOnMap["idTipoConsulta"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["idPadre"] = "int";
        model_internal::propertyTypeMap["nombre"] = "String";
        model_internal::propertyTypeMap["codigo"] = "String";
        model_internal::propertyTypeMap["ListaVacunas"] = "valueObjects.ListaVacunas";
        model_internal::propertyTypeMap["ListaSintomas"] = "valueObjects.ListaSintomas";
        model_internal::propertyTypeMap["valor"] = "String";
        model_internal::propertyTypeMap["esPrecio"] = "String";
        model_internal::propertyTypeMap["padre"] = "String";
        model_internal::propertyTypeMap["idTipoConsulta"] = "String";

        model_internal::_instance = value;
        model_internal::_nombreValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForNombre);
        model_internal::_nombreValidator.required = true;
        model_internal::_nombreValidator.requiredFieldError = "nombre is required";
        //model_internal::_nombreValidator.source = model_internal::_instance;
        //model_internal::_nombreValidator.property = "nombre";
        model_internal::_codigoValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForCodigo);
        model_internal::_codigoValidator.required = true;
        model_internal::_codigoValidator.requiredFieldError = "codigo is required";
        //model_internal::_codigoValidator.source = model_internal::_instance;
        //model_internal::_codigoValidator.property = "codigo";
        model_internal::_ListaVacunasValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForListaVacunas);
        model_internal::_ListaVacunasValidator.required = true;
        model_internal::_ListaVacunasValidator.requiredFieldError = "ListaVacunas is required";
        //model_internal::_ListaVacunasValidator.source = model_internal::_instance;
        //model_internal::_ListaVacunasValidator.property = "ListaVacunas";
        model_internal::_ListaSintomasValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForListaSintomas);
        model_internal::_ListaSintomasValidator.required = true;
        model_internal::_ListaSintomasValidator.requiredFieldError = "ListaSintomas is required";
        //model_internal::_ListaSintomasValidator.source = model_internal::_instance;
        //model_internal::_ListaSintomasValidator.property = "ListaSintomas";
        model_internal::_valorValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForValor);
        model_internal::_valorValidator.required = true;
        model_internal::_valorValidator.requiredFieldError = "valor is required";
        //model_internal::_valorValidator.source = model_internal::_instance;
        //model_internal::_valorValidator.property = "valor";
        model_internal::_esPrecioValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForEsPrecio);
        model_internal::_esPrecioValidator.required = true;
        model_internal::_esPrecioValidator.requiredFieldError = "esPrecio is required";
        //model_internal::_esPrecioValidator.source = model_internal::_instance;
        //model_internal::_esPrecioValidator.property = "esPrecio";
        model_internal::_padreValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForPadre);
        model_internal::_padreValidator.required = true;
        model_internal::_padreValidator.requiredFieldError = "padre is required";
        //model_internal::_padreValidator.source = model_internal::_instance;
        //model_internal::_padreValidator.property = "padre";
        model_internal::_idTipoConsultaValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForIdTipoConsulta);
        model_internal::_idTipoConsultaValidator.required = true;
        model_internal::_idTipoConsultaValidator.requiredFieldError = "idTipoConsulta is required";
        //model_internal::_idTipoConsultaValidator.source = model_internal::_instance;
        //model_internal::_idTipoConsultaValidator.property = "idTipoConsulta";
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity TipoConsulta");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity TipoConsulta");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of TipoConsulta");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity TipoConsulta");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity TipoConsulta");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity TipoConsulta");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isIdPadreAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isNombreAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCodigoAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isListaVacunasAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isListaSintomasAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isValorAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEsPrecioAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPadreAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIdTipoConsultaAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */
    public function invalidateDependentOnNombre():void
    {
        if (model_internal::_nombreIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfNombre = null;
            model_internal::calculateNombreIsValid();
        }
    }
    public function invalidateDependentOnCodigo():void
    {
        if (model_internal::_codigoIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfCodigo = null;
            model_internal::calculateCodigoIsValid();
        }
    }
    public function invalidateDependentOnListaVacunas():void
    {
        if (model_internal::_ListaVacunasIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfListaVacunas = null;
            model_internal::calculateListaVacunasIsValid();
        }
    }
    public function invalidateDependentOnListaSintomas():void
    {
        if (model_internal::_ListaSintomasIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfListaSintomas = null;
            model_internal::calculateListaSintomasIsValid();
        }
    }
    public function invalidateDependentOnValor():void
    {
        if (model_internal::_valorIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfValor = null;
            model_internal::calculateValorIsValid();
        }
    }
    public function invalidateDependentOnEsPrecio():void
    {
        if (model_internal::_esPrecioIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfEsPrecio = null;
            model_internal::calculateEsPrecioIsValid();
        }
    }
    public function invalidateDependentOnPadre():void
    {
        if (model_internal::_padreIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfPadre = null;
            model_internal::calculatePadreIsValid();
        }
    }
    public function invalidateDependentOnIdTipoConsulta():void
    {
        if (model_internal::_idTipoConsultaIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfIdTipoConsulta = null;
            model_internal::calculateIdTipoConsultaIsValid();
        }
    }

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get idPadreStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get nombreStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get nombreValidator() : StyleValidator
    {
        return model_internal::_nombreValidator;
    }

    model_internal function set _nombreIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_nombreIsValid;         
        if (oldValue !== value)
        {
            model_internal::_nombreIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "nombreIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get nombreIsValid():Boolean
    {
        if (!model_internal::_nombreIsValidCacheInitialized)
        {
            model_internal::calculateNombreIsValid();
        }

        return model_internal::_nombreIsValid;
    }

    model_internal function calculateNombreIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_nombreValidator.validate(model_internal::_instance.nombre)
        model_internal::_nombreIsValid_der = (valRes.results == null);
        model_internal::_nombreIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::nombreValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::nombreValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get nombreValidationFailureMessages():Array
    {
        if (model_internal::_nombreValidationFailureMessages == null)
            model_internal::calculateNombreIsValid();

        return _nombreValidationFailureMessages;
    }

    model_internal function set nombreValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_nombreValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_nombreValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "nombreValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get codigoStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get codigoValidator() : StyleValidator
    {
        return model_internal::_codigoValidator;
    }

    model_internal function set _codigoIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_codigoIsValid;         
        if (oldValue !== value)
        {
            model_internal::_codigoIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "codigoIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get codigoIsValid():Boolean
    {
        if (!model_internal::_codigoIsValidCacheInitialized)
        {
            model_internal::calculateCodigoIsValid();
        }

        return model_internal::_codigoIsValid;
    }

    model_internal function calculateCodigoIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_codigoValidator.validate(model_internal::_instance.codigo)
        model_internal::_codigoIsValid_der = (valRes.results == null);
        model_internal::_codigoIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::codigoValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::codigoValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get codigoValidationFailureMessages():Array
    {
        if (model_internal::_codigoValidationFailureMessages == null)
            model_internal::calculateCodigoIsValid();

        return _codigoValidationFailureMessages;
    }

    model_internal function set codigoValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_codigoValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_codigoValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "codigoValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get ListaVacunasStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get ListaVacunasValidator() : StyleValidator
    {
        return model_internal::_ListaVacunasValidator;
    }

    model_internal function set _ListaVacunasIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_ListaVacunasIsValid;         
        if (oldValue !== value)
        {
            model_internal::_ListaVacunasIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ListaVacunasIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get ListaVacunasIsValid():Boolean
    {
        if (!model_internal::_ListaVacunasIsValidCacheInitialized)
        {
            model_internal::calculateListaVacunasIsValid();
        }

        return model_internal::_ListaVacunasIsValid;
    }

    model_internal function calculateListaVacunasIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_ListaVacunasValidator.validate(model_internal::_instance.ListaVacunas)
        model_internal::_ListaVacunasIsValid_der = (valRes.results == null);
        model_internal::_ListaVacunasIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::ListaVacunasValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::ListaVacunasValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get ListaVacunasValidationFailureMessages():Array
    {
        if (model_internal::_ListaVacunasValidationFailureMessages == null)
            model_internal::calculateListaVacunasIsValid();

        return _ListaVacunasValidationFailureMessages;
    }

    model_internal function set ListaVacunasValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_ListaVacunasValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_ListaVacunasValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ListaVacunasValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get ListaSintomasStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get ListaSintomasValidator() : StyleValidator
    {
        return model_internal::_ListaSintomasValidator;
    }

    model_internal function set _ListaSintomasIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_ListaSintomasIsValid;         
        if (oldValue !== value)
        {
            model_internal::_ListaSintomasIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ListaSintomasIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get ListaSintomasIsValid():Boolean
    {
        if (!model_internal::_ListaSintomasIsValidCacheInitialized)
        {
            model_internal::calculateListaSintomasIsValid();
        }

        return model_internal::_ListaSintomasIsValid;
    }

    model_internal function calculateListaSintomasIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_ListaSintomasValidator.validate(model_internal::_instance.ListaSintomas)
        model_internal::_ListaSintomasIsValid_der = (valRes.results == null);
        model_internal::_ListaSintomasIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::ListaSintomasValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::ListaSintomasValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get ListaSintomasValidationFailureMessages():Array
    {
        if (model_internal::_ListaSintomasValidationFailureMessages == null)
            model_internal::calculateListaSintomasIsValid();

        return _ListaSintomasValidationFailureMessages;
    }

    model_internal function set ListaSintomasValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_ListaSintomasValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_ListaSintomasValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ListaSintomasValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get valorStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get valorValidator() : StyleValidator
    {
        return model_internal::_valorValidator;
    }

    model_internal function set _valorIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_valorIsValid;         
        if (oldValue !== value)
        {
            model_internal::_valorIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "valorIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get valorIsValid():Boolean
    {
        if (!model_internal::_valorIsValidCacheInitialized)
        {
            model_internal::calculateValorIsValid();
        }

        return model_internal::_valorIsValid;
    }

    model_internal function calculateValorIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_valorValidator.validate(model_internal::_instance.valor)
        model_internal::_valorIsValid_der = (valRes.results == null);
        model_internal::_valorIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::valorValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::valorValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get valorValidationFailureMessages():Array
    {
        if (model_internal::_valorValidationFailureMessages == null)
            model_internal::calculateValorIsValid();

        return _valorValidationFailureMessages;
    }

    model_internal function set valorValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_valorValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_valorValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "valorValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get esPrecioStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get esPrecioValidator() : StyleValidator
    {
        return model_internal::_esPrecioValidator;
    }

    model_internal function set _esPrecioIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_esPrecioIsValid;         
        if (oldValue !== value)
        {
            model_internal::_esPrecioIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "esPrecioIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get esPrecioIsValid():Boolean
    {
        if (!model_internal::_esPrecioIsValidCacheInitialized)
        {
            model_internal::calculateEsPrecioIsValid();
        }

        return model_internal::_esPrecioIsValid;
    }

    model_internal function calculateEsPrecioIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_esPrecioValidator.validate(model_internal::_instance.esPrecio)
        model_internal::_esPrecioIsValid_der = (valRes.results == null);
        model_internal::_esPrecioIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::esPrecioValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::esPrecioValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get esPrecioValidationFailureMessages():Array
    {
        if (model_internal::_esPrecioValidationFailureMessages == null)
            model_internal::calculateEsPrecioIsValid();

        return _esPrecioValidationFailureMessages;
    }

    model_internal function set esPrecioValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_esPrecioValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_esPrecioValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "esPrecioValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get padreStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get padreValidator() : StyleValidator
    {
        return model_internal::_padreValidator;
    }

    model_internal function set _padreIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_padreIsValid;         
        if (oldValue !== value)
        {
            model_internal::_padreIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "padreIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get padreIsValid():Boolean
    {
        if (!model_internal::_padreIsValidCacheInitialized)
        {
            model_internal::calculatePadreIsValid();
        }

        return model_internal::_padreIsValid;
    }

    model_internal function calculatePadreIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_padreValidator.validate(model_internal::_instance.padre)
        model_internal::_padreIsValid_der = (valRes.results == null);
        model_internal::_padreIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::padreValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::padreValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get padreValidationFailureMessages():Array
    {
        if (model_internal::_padreValidationFailureMessages == null)
            model_internal::calculatePadreIsValid();

        return _padreValidationFailureMessages;
    }

    model_internal function set padreValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_padreValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_padreValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "padreValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get idTipoConsultaStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get idTipoConsultaValidator() : StyleValidator
    {
        return model_internal::_idTipoConsultaValidator;
    }

    model_internal function set _idTipoConsultaIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_idTipoConsultaIsValid;         
        if (oldValue !== value)
        {
            model_internal::_idTipoConsultaIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "idTipoConsultaIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get idTipoConsultaIsValid():Boolean
    {
        if (!model_internal::_idTipoConsultaIsValidCacheInitialized)
        {
            model_internal::calculateIdTipoConsultaIsValid();
        }

        return model_internal::_idTipoConsultaIsValid;
    }

    model_internal function calculateIdTipoConsultaIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_idTipoConsultaValidator.validate(model_internal::_instance.idTipoConsulta)
        model_internal::_idTipoConsultaIsValid_der = (valRes.results == null);
        model_internal::_idTipoConsultaIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::idTipoConsultaValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::idTipoConsultaValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get idTipoConsultaValidationFailureMessages():Array
    {
        if (model_internal::_idTipoConsultaValidationFailureMessages == null)
            model_internal::calculateIdTipoConsultaIsValid();

        return _idTipoConsultaValidationFailureMessages;
    }

    model_internal function set idTipoConsultaValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_idTipoConsultaValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_idTipoConsultaValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "idTipoConsultaValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            case("nombre"):
            {
                return nombreValidationFailureMessages;
            }
            case("codigo"):
            {
                return codigoValidationFailureMessages;
            }
            case("ListaVacunas"):
            {
                return ListaVacunasValidationFailureMessages;
            }
            case("ListaSintomas"):
            {
                return ListaSintomasValidationFailureMessages;
            }
            case("valor"):
            {
                return valorValidationFailureMessages;
            }
            case("esPrecio"):
            {
                return esPrecioValidationFailureMessages;
            }
            case("padre"):
            {
                return padreValidationFailureMessages;
            }
            case("idTipoConsulta"):
            {
                return idTipoConsultaValidationFailureMessages;
            }
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
