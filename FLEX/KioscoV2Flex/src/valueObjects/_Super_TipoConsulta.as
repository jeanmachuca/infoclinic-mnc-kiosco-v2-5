/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - TipoConsulta.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.util.FiberUtils;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.Event;
import flash.events.EventDispatcher;
import mx.binding.utils.ChangeWatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import mx.validators.ValidationResult;
import valueObjects.ListaSintomas;
import valueObjects.ListaVacunas;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_TipoConsulta extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.ListaVacunas.initRemoteClassAliasSingleChild();
        valueObjects.TipoConsulta.initRemoteClassAliasSingleChild();
        valueObjects.ListaSintomas.initRemoteClassAliasSingleChild();
        valueObjects.Sintoma.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _TipoConsultaEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_idPadre : int;
    private var _internal_nombre : String;
    private var _internal_codigo : String;
    private var _internal_ListaVacunas : valueObjects.ListaVacunas;
    private var _internal_ListaSintomas : valueObjects.ListaSintomas;
    private var _internal_valor : String;
    private var _internal_esPrecio : String;
    private var _internal_padre : String;
    private var _internal_idTipoConsulta : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_TipoConsulta()
    {
        _model = new _TipoConsultaEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "nombre", model_internal::setterListenerNombre));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "codigo", model_internal::setterListenerCodigo));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "ListaVacunas", model_internal::setterListenerListaVacunas));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "ListaSintomas", model_internal::setterListenerListaSintomas));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "valor", model_internal::setterListenerValor));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "esPrecio", model_internal::setterListenerEsPrecio));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "padre", model_internal::setterListenerPadre));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "idTipoConsulta", model_internal::setterListenerIdTipoConsulta));

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get idPadre() : int
    {
        return _internal_idPadre;
    }

    [Bindable(event="propertyChange")]
    public function get nombre() : String
    {
        return _internal_nombre;
    }

    [Bindable(event="propertyChange")]
    public function get codigo() : String
    {
        return _internal_codigo;
    }

    [Bindable(event="propertyChange")]
    public function get ListaVacunas() : valueObjects.ListaVacunas
    {
        return _internal_ListaVacunas;
    }

    [Bindable(event="propertyChange")]
    public function get ListaSintomas() : valueObjects.ListaSintomas
    {
        return _internal_ListaSintomas;
    }

    [Bindable(event="propertyChange")]
    public function get valor() : String
    {
        return _internal_valor;
    }

    [Bindable(event="propertyChange")]
    public function get esPrecio() : String
    {
        return _internal_esPrecio;
    }

    [Bindable(event="propertyChange")]
    public function get padre() : String
    {
        return _internal_padre;
    }

    [Bindable(event="propertyChange")]
    public function get idTipoConsulta() : String
    {
        return _internal_idTipoConsulta;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set idPadre(value:int) : void
    {
        var oldValue:int = _internal_idPadre;
        if (oldValue !== value)
        {
            _internal_idPadre = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "idPadre", oldValue, _internal_idPadre));
        }
    }

    public function set nombre(value:String) : void
    {
        var oldValue:String = _internal_nombre;
        if (oldValue !== value)
        {
            _internal_nombre = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "nombre", oldValue, _internal_nombre));
        }
    }

    public function set codigo(value:String) : void
    {
        var oldValue:String = _internal_codigo;
        if (oldValue !== value)
        {
            _internal_codigo = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "codigo", oldValue, _internal_codigo));
        }
    }

    public function set ListaVacunas(value:valueObjects.ListaVacunas) : void
    {
        var oldValue:valueObjects.ListaVacunas = _internal_ListaVacunas;
        if (oldValue !== value)
        {
            _internal_ListaVacunas = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ListaVacunas", oldValue, _internal_ListaVacunas));
        }
    }

    public function set ListaSintomas(value:valueObjects.ListaSintomas) : void
    {
        var oldValue:valueObjects.ListaSintomas = _internal_ListaSintomas;
        if (oldValue !== value)
        {
            _internal_ListaSintomas = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ListaSintomas", oldValue, _internal_ListaSintomas));
        }
    }

    public function set valor(value:String) : void
    {
        var oldValue:String = _internal_valor;
        if (oldValue !== value)
        {
            _internal_valor = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "valor", oldValue, _internal_valor));
        }
    }

    public function set esPrecio(value:String) : void
    {
        var oldValue:String = _internal_esPrecio;
        if (oldValue !== value)
        {
            _internal_esPrecio = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "esPrecio", oldValue, _internal_esPrecio));
        }
    }

    public function set padre(value:String) : void
    {
        var oldValue:String = _internal_padre;
        if (oldValue !== value)
        {
            _internal_padre = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "padre", oldValue, _internal_padre));
        }
    }

    public function set idTipoConsulta(value:String) : void
    {
        var oldValue:String = _internal_idTipoConsulta;
        if (oldValue !== value)
        {
            _internal_idTipoConsulta = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "idTipoConsulta", oldValue, _internal_idTipoConsulta));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */

    model_internal function setterListenerNombre(value:flash.events.Event):void
    {
        _model.invalidateDependentOnNombre();
    }

    model_internal function setterListenerCodigo(value:flash.events.Event):void
    {
        _model.invalidateDependentOnCodigo();
    }

    model_internal function setterListenerListaVacunas(value:flash.events.Event):void
    {
        _model.invalidateDependentOnListaVacunas();
    }

    model_internal function setterListenerListaSintomas(value:flash.events.Event):void
    {
        _model.invalidateDependentOnListaSintomas();
    }

    model_internal function setterListenerValor(value:flash.events.Event):void
    {
        _model.invalidateDependentOnValor();
    }

    model_internal function setterListenerEsPrecio(value:flash.events.Event):void
    {
        _model.invalidateDependentOnEsPrecio();
    }

    model_internal function setterListenerPadre(value:flash.events.Event):void
    {
        _model.invalidateDependentOnPadre();
    }

    model_internal function setterListenerIdTipoConsulta(value:flash.events.Event):void
    {
        _model.invalidateDependentOnIdTipoConsulta();
    }


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;
        if (!_model.nombreIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_nombreValidationFailureMessages);
        }
        if (!_model.codigoIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_codigoValidationFailureMessages);
        }
        if (!_model.ListaVacunasIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_ListaVacunasValidationFailureMessages);
        }
        if (!_model.ListaSintomasIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_ListaSintomasValidationFailureMessages);
        }
        if (!_model.valorIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_valorValidationFailureMessages);
        }
        if (!_model.esPrecioIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_esPrecioValidationFailureMessages);
        }
        if (!_model.padreIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_padreValidationFailureMessages);
        }
        if (!_model.idTipoConsultaIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_idTipoConsultaValidationFailureMessages);
        }

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _TipoConsultaEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _TipoConsultaEntityMetadata) : void
    {
        var oldValue : _TipoConsultaEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }

    model_internal var _doValidationCacheOfNombre : Array = null;
    model_internal var _doValidationLastValOfNombre : String;

    model_internal function _doValidationForNombre(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfNombre != null && model_internal::_doValidationLastValOfNombre == value)
           return model_internal::_doValidationCacheOfNombre ;

        _model.model_internal::_nombreIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isNombreAvailable && _internal_nombre == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "nombre is required"));
        }

        model_internal::_doValidationCacheOfNombre = validationFailures;
        model_internal::_doValidationLastValOfNombre = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfCodigo : Array = null;
    model_internal var _doValidationLastValOfCodigo : String;

    model_internal function _doValidationForCodigo(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfCodigo != null && model_internal::_doValidationLastValOfCodigo == value)
           return model_internal::_doValidationCacheOfCodigo ;

        _model.model_internal::_codigoIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isCodigoAvailable && _internal_codigo == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "codigo is required"));
        }

        model_internal::_doValidationCacheOfCodigo = validationFailures;
        model_internal::_doValidationLastValOfCodigo = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfListaVacunas : Array = null;
    model_internal var _doValidationLastValOfListaVacunas : valueObjects.ListaVacunas;

    model_internal function _doValidationForListaVacunas(valueIn:Object):Array
    {
        var value : valueObjects.ListaVacunas = valueIn as valueObjects.ListaVacunas;

        if (model_internal::_doValidationCacheOfListaVacunas != null && model_internal::_doValidationLastValOfListaVacunas == value)
           return model_internal::_doValidationCacheOfListaVacunas ;

        _model.model_internal::_ListaVacunasIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isListaVacunasAvailable && _internal_ListaVacunas == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "ListaVacunas is required"));
        }

        model_internal::_doValidationCacheOfListaVacunas = validationFailures;
        model_internal::_doValidationLastValOfListaVacunas = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfListaSintomas : Array = null;
    model_internal var _doValidationLastValOfListaSintomas : valueObjects.ListaSintomas;

    model_internal function _doValidationForListaSintomas(valueIn:Object):Array
    {
        var value : valueObjects.ListaSintomas = valueIn as valueObjects.ListaSintomas;

        if (model_internal::_doValidationCacheOfListaSintomas != null && model_internal::_doValidationLastValOfListaSintomas == value)
           return model_internal::_doValidationCacheOfListaSintomas ;

        _model.model_internal::_ListaSintomasIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isListaSintomasAvailable && _internal_ListaSintomas == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "ListaSintomas is required"));
        }

        model_internal::_doValidationCacheOfListaSintomas = validationFailures;
        model_internal::_doValidationLastValOfListaSintomas = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfValor : Array = null;
    model_internal var _doValidationLastValOfValor : String;

    model_internal function _doValidationForValor(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfValor != null && model_internal::_doValidationLastValOfValor == value)
           return model_internal::_doValidationCacheOfValor ;

        _model.model_internal::_valorIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isValorAvailable && _internal_valor == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "valor is required"));
        }

        model_internal::_doValidationCacheOfValor = validationFailures;
        model_internal::_doValidationLastValOfValor = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfEsPrecio : Array = null;
    model_internal var _doValidationLastValOfEsPrecio : String;

    model_internal function _doValidationForEsPrecio(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfEsPrecio != null && model_internal::_doValidationLastValOfEsPrecio == value)
           return model_internal::_doValidationCacheOfEsPrecio ;

        _model.model_internal::_esPrecioIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isEsPrecioAvailable && _internal_esPrecio == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "esPrecio is required"));
        }

        model_internal::_doValidationCacheOfEsPrecio = validationFailures;
        model_internal::_doValidationLastValOfEsPrecio = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfPadre : Array = null;
    model_internal var _doValidationLastValOfPadre : String;

    model_internal function _doValidationForPadre(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfPadre != null && model_internal::_doValidationLastValOfPadre == value)
           return model_internal::_doValidationCacheOfPadre ;

        _model.model_internal::_padreIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isPadreAvailable && _internal_padre == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "padre is required"));
        }

        model_internal::_doValidationCacheOfPadre = validationFailures;
        model_internal::_doValidationLastValOfPadre = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfIdTipoConsulta : Array = null;
    model_internal var _doValidationLastValOfIdTipoConsulta : String;

    model_internal function _doValidationForIdTipoConsulta(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfIdTipoConsulta != null && model_internal::_doValidationLastValOfIdTipoConsulta == value)
           return model_internal::_doValidationCacheOfIdTipoConsulta ;

        _model.model_internal::_idTipoConsultaIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isIdTipoConsultaAvailable && _internal_idTipoConsulta == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "idTipoConsulta is required"));
        }

        model_internal::_doValidationCacheOfIdTipoConsulta = validationFailures;
        model_internal::_doValidationLastValOfIdTipoConsulta = value;

        return validationFailures;
    }
    

}

}
