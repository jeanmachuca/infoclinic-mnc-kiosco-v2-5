/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - Kiosco.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.util.FiberUtils;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.Event;
import flash.events.EventDispatcher;
import mx.binding.utils.ChangeWatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import mx.validators.ValidationResult;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_Kiosco extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _KioscoEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_nombre : String;
    private var _internal_id : String;
    private var _internal_powered : String;
    private var _internal_preguntaSMS : String;
    private var _internal_preguntaActualizaciones : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_Kiosco()
    {
        _model = new _KioscoEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "nombre", model_internal::setterListenerNombre));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "id", model_internal::setterListenerId));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "powered", model_internal::setterListenerPowered));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "preguntaSMS", model_internal::setterListenerPreguntaSMS));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "preguntaActualizaciones", model_internal::setterListenerPreguntaActualizaciones));

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get nombre() : String
    {
        return _internal_nombre;
    }

    [Bindable(event="propertyChange")]
    public function get id() : String
    {
        return _internal_id;
    }

    [Bindable(event="propertyChange")]
    public function get powered() : String
    {
        return _internal_powered;
    }

    [Bindable(event="propertyChange")]
    public function get preguntaSMS() : String
    {
        return _internal_preguntaSMS;
    }

    [Bindable(event="propertyChange")]
    public function get preguntaActualizaciones() : String
    {
        return _internal_preguntaActualizaciones;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set nombre(value:String) : void
    {
        var oldValue:String = _internal_nombre;
        if (oldValue !== value)
        {
            _internal_nombre = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "nombre", oldValue, _internal_nombre));
        }
    }

    public function set id(value:String) : void
    {
        var oldValue:String = _internal_id;
        if (oldValue !== value)
        {
            _internal_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "id", oldValue, _internal_id));
        }
    }

    public function set powered(value:String) : void
    {
        var oldValue:String = _internal_powered;
        if (oldValue !== value)
        {
            _internal_powered = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "powered", oldValue, _internal_powered));
        }
    }

    public function set preguntaSMS(value:String) : void
    {
        var oldValue:String = _internal_preguntaSMS;
        if (oldValue !== value)
        {
            _internal_preguntaSMS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "preguntaSMS", oldValue, _internal_preguntaSMS));
        }
    }

    public function set preguntaActualizaciones(value:String) : void
    {
        var oldValue:String = _internal_preguntaActualizaciones;
        if (oldValue !== value)
        {
            _internal_preguntaActualizaciones = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "preguntaActualizaciones", oldValue, _internal_preguntaActualizaciones));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */

    model_internal function setterListenerNombre(value:flash.events.Event):void
    {
        _model.invalidateDependentOnNombre();
    }

    model_internal function setterListenerId(value:flash.events.Event):void
    {
        _model.invalidateDependentOnId();
    }

    model_internal function setterListenerPowered(value:flash.events.Event):void
    {
        _model.invalidateDependentOnPowered();
    }

    model_internal function setterListenerPreguntaSMS(value:flash.events.Event):void
    {
        _model.invalidateDependentOnPreguntaSMS();
    }

    model_internal function setterListenerPreguntaActualizaciones(value:flash.events.Event):void
    {
        _model.invalidateDependentOnPreguntaActualizaciones();
    }


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;
        if (!_model.nombreIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_nombreValidationFailureMessages);
        }
        if (!_model.idIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_idValidationFailureMessages);
        }
        if (!_model.poweredIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_poweredValidationFailureMessages);
        }
        if (!_model.preguntaSMSIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_preguntaSMSValidationFailureMessages);
        }
        if (!_model.preguntaActualizacionesIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_preguntaActualizacionesValidationFailureMessages);
        }

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _KioscoEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _KioscoEntityMetadata) : void
    {
        var oldValue : _KioscoEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }

    model_internal var _doValidationCacheOfNombre : Array = null;
    model_internal var _doValidationLastValOfNombre : String;

    model_internal function _doValidationForNombre(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfNombre != null && model_internal::_doValidationLastValOfNombre == value)
           return model_internal::_doValidationCacheOfNombre ;

        _model.model_internal::_nombreIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isNombreAvailable && _internal_nombre == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "nombre is required"));
        }

        model_internal::_doValidationCacheOfNombre = validationFailures;
        model_internal::_doValidationLastValOfNombre = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfId : Array = null;
    model_internal var _doValidationLastValOfId : String;

    model_internal function _doValidationForId(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfId != null && model_internal::_doValidationLastValOfId == value)
           return model_internal::_doValidationCacheOfId ;

        _model.model_internal::_idIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isIdAvailable && _internal_id == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "id is required"));
        }

        model_internal::_doValidationCacheOfId = validationFailures;
        model_internal::_doValidationLastValOfId = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfPowered : Array = null;
    model_internal var _doValidationLastValOfPowered : String;

    model_internal function _doValidationForPowered(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfPowered != null && model_internal::_doValidationLastValOfPowered == value)
           return model_internal::_doValidationCacheOfPowered ;

        _model.model_internal::_poweredIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isPoweredAvailable && _internal_powered == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "powered is required"));
        }

        model_internal::_doValidationCacheOfPowered = validationFailures;
        model_internal::_doValidationLastValOfPowered = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfPreguntaSMS : Array = null;
    model_internal var _doValidationLastValOfPreguntaSMS : String;

    model_internal function _doValidationForPreguntaSMS(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfPreguntaSMS != null && model_internal::_doValidationLastValOfPreguntaSMS == value)
           return model_internal::_doValidationCacheOfPreguntaSMS ;

        _model.model_internal::_preguntaSMSIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isPreguntaSMSAvailable && _internal_preguntaSMS == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "preguntaSMS is required"));
        }

        model_internal::_doValidationCacheOfPreguntaSMS = validationFailures;
        model_internal::_doValidationLastValOfPreguntaSMS = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfPreguntaActualizaciones : Array = null;
    model_internal var _doValidationLastValOfPreguntaActualizaciones : String;

    model_internal function _doValidationForPreguntaActualizaciones(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfPreguntaActualizaciones != null && model_internal::_doValidationLastValOfPreguntaActualizaciones == value)
           return model_internal::_doValidationCacheOfPreguntaActualizaciones ;

        _model.model_internal::_preguntaActualizacionesIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isPreguntaActualizacionesAvailable && _internal_preguntaActualizaciones == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "preguntaActualizaciones is required"));
        }

        model_internal::_doValidationCacheOfPreguntaActualizaciones = validationFailures;
        model_internal::_doValidationLastValOfPreguntaActualizaciones = value;

        return validationFailures;
    }
    

}

}
