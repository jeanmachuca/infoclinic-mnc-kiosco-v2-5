
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.styles.StyleValidator;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import mx.events.ValidationResultEvent;
import valueObjects.Boleta;
import valueObjects.ImedConfig;
import valueObjects.Kiosco;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _ConfigDataEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("Boleta", "Kiosco", "Theme", "oArtifacts", "Imed");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array("Boleta", "Kiosco", "Theme", "oArtifacts", "Imed");
    model_internal static var allAlwaysAvailableProperties:Array = new Array("Boleta", "Kiosco", "Theme", "oArtifacts", "Imed");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("Boleta", "Kiosco", "Theme", "oArtifacts", "Imed");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("Boleta", "Kiosco", "Theme", "oArtifacts", "Imed");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array();
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "ConfigData";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;

    
    model_internal var _BoletaIsValid:Boolean;
    model_internal var _BoletaValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _BoletaIsValidCacheInitialized:Boolean = false;
    model_internal var _BoletaValidationFailureMessages:Array;
    
    model_internal var _KioscoIsValid:Boolean;
    model_internal var _KioscoValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _KioscoIsValidCacheInitialized:Boolean = false;
    model_internal var _KioscoValidationFailureMessages:Array;
    
    model_internal var _ThemeIsValid:Boolean;
    model_internal var _ThemeValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _ThemeIsValidCacheInitialized:Boolean = false;
    model_internal var _ThemeValidationFailureMessages:Array;
    
    model_internal var _oArtifactsIsValid:Boolean;
    model_internal var _oArtifactsValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _oArtifactsIsValidCacheInitialized:Boolean = false;
    model_internal var _oArtifactsValidationFailureMessages:Array;
    
    model_internal var _ImedIsValid:Boolean;
    model_internal var _ImedValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _ImedIsValidCacheInitialized:Boolean = false;
    model_internal var _ImedValidationFailureMessages:Array;

    model_internal var _instance:_Super_ConfigData;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _ConfigDataEntityMetadata(value : _Super_ConfigData)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["Boleta"] = new Array();
            model_internal::dependentsOnMap["Kiosco"] = new Array();
            model_internal::dependentsOnMap["Theme"] = new Array();
            model_internal::dependentsOnMap["oArtifacts"] = new Array();
            model_internal::dependentsOnMap["Imed"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["Boleta"] = "valueObjects.Boleta";
        model_internal::propertyTypeMap["Kiosco"] = "valueObjects.Kiosco";
        model_internal::propertyTypeMap["Theme"] = "Object";
        model_internal::propertyTypeMap["oArtifacts"] = "Object";
        model_internal::propertyTypeMap["Imed"] = "valueObjects.ImedConfig";

        model_internal::_instance = value;
        model_internal::_BoletaValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForBoleta);
        model_internal::_BoletaValidator.required = true;
        model_internal::_BoletaValidator.requiredFieldError = "Boleta is required";
        //model_internal::_BoletaValidator.source = model_internal::_instance;
        //model_internal::_BoletaValidator.property = "Boleta";
        model_internal::_KioscoValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForKiosco);
        model_internal::_KioscoValidator.required = true;
        model_internal::_KioscoValidator.requiredFieldError = "Kiosco is required";
        //model_internal::_KioscoValidator.source = model_internal::_instance;
        //model_internal::_KioscoValidator.property = "Kiosco";
        model_internal::_ThemeValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForTheme);
        model_internal::_ThemeValidator.required = true;
        model_internal::_ThemeValidator.requiredFieldError = "Theme is required";
        //model_internal::_ThemeValidator.source = model_internal::_instance;
        //model_internal::_ThemeValidator.property = "Theme";
        model_internal::_oArtifactsValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForOArtifacts);
        model_internal::_oArtifactsValidator.required = true;
        model_internal::_oArtifactsValidator.requiredFieldError = "oArtifacts is required";
        //model_internal::_oArtifactsValidator.source = model_internal::_instance;
        //model_internal::_oArtifactsValidator.property = "oArtifacts";
        model_internal::_ImedValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForImed);
        model_internal::_ImedValidator.required = true;
        model_internal::_ImedValidator.requiredFieldError = "Imed is required";
        //model_internal::_ImedValidator.source = model_internal::_instance;
        //model_internal::_ImedValidator.property = "Imed";
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity ConfigData");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity ConfigData");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of ConfigData");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity ConfigData");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity ConfigData");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity ConfigData");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isBoletaAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isKioscoAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isThemeAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isOArtifactsAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isImedAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */
    public function invalidateDependentOnBoleta():void
    {
        if (model_internal::_BoletaIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfBoleta = null;
            model_internal::calculateBoletaIsValid();
        }
    }
    public function invalidateDependentOnKiosco():void
    {
        if (model_internal::_KioscoIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfKiosco = null;
            model_internal::calculateKioscoIsValid();
        }
    }
    public function invalidateDependentOnTheme():void
    {
        if (model_internal::_ThemeIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfTheme = null;
            model_internal::calculateThemeIsValid();
        }
    }
    public function invalidateDependentOnOArtifacts():void
    {
        if (model_internal::_oArtifactsIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfOArtifacts = null;
            model_internal::calculateOArtifactsIsValid();
        }
    }
    public function invalidateDependentOnImed():void
    {
        if (model_internal::_ImedIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfImed = null;
            model_internal::calculateImedIsValid();
        }
    }

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get BoletaStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get BoletaValidator() : StyleValidator
    {
        return model_internal::_BoletaValidator;
    }

    model_internal function set _BoletaIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_BoletaIsValid;         
        if (oldValue !== value)
        {
            model_internal::_BoletaIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "BoletaIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get BoletaIsValid():Boolean
    {
        if (!model_internal::_BoletaIsValidCacheInitialized)
        {
            model_internal::calculateBoletaIsValid();
        }

        return model_internal::_BoletaIsValid;
    }

    model_internal function calculateBoletaIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_BoletaValidator.validate(model_internal::_instance.Boleta)
        model_internal::_BoletaIsValid_der = (valRes.results == null);
        model_internal::_BoletaIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::BoletaValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::BoletaValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get BoletaValidationFailureMessages():Array
    {
        if (model_internal::_BoletaValidationFailureMessages == null)
            model_internal::calculateBoletaIsValid();

        return _BoletaValidationFailureMessages;
    }

    model_internal function set BoletaValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_BoletaValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_BoletaValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "BoletaValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get KioscoStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get KioscoValidator() : StyleValidator
    {
        return model_internal::_KioscoValidator;
    }

    model_internal function set _KioscoIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_KioscoIsValid;         
        if (oldValue !== value)
        {
            model_internal::_KioscoIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "KioscoIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get KioscoIsValid():Boolean
    {
        if (!model_internal::_KioscoIsValidCacheInitialized)
        {
            model_internal::calculateKioscoIsValid();
        }

        return model_internal::_KioscoIsValid;
    }

    model_internal function calculateKioscoIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_KioscoValidator.validate(model_internal::_instance.Kiosco)
        model_internal::_KioscoIsValid_der = (valRes.results == null);
        model_internal::_KioscoIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::KioscoValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::KioscoValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get KioscoValidationFailureMessages():Array
    {
        if (model_internal::_KioscoValidationFailureMessages == null)
            model_internal::calculateKioscoIsValid();

        return _KioscoValidationFailureMessages;
    }

    model_internal function set KioscoValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_KioscoValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_KioscoValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "KioscoValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get ThemeStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get ThemeValidator() : StyleValidator
    {
        return model_internal::_ThemeValidator;
    }

    model_internal function set _ThemeIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_ThemeIsValid;         
        if (oldValue !== value)
        {
            model_internal::_ThemeIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ThemeIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get ThemeIsValid():Boolean
    {
        if (!model_internal::_ThemeIsValidCacheInitialized)
        {
            model_internal::calculateThemeIsValid();
        }

        return model_internal::_ThemeIsValid;
    }

    model_internal function calculateThemeIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_ThemeValidator.validate(model_internal::_instance.Theme)
        model_internal::_ThemeIsValid_der = (valRes.results == null);
        model_internal::_ThemeIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::ThemeValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::ThemeValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get ThemeValidationFailureMessages():Array
    {
        if (model_internal::_ThemeValidationFailureMessages == null)
            model_internal::calculateThemeIsValid();

        return _ThemeValidationFailureMessages;
    }

    model_internal function set ThemeValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_ThemeValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_ThemeValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ThemeValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get oArtifactsStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get oArtifactsValidator() : StyleValidator
    {
        return model_internal::_oArtifactsValidator;
    }

    model_internal function set _oArtifactsIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_oArtifactsIsValid;         
        if (oldValue !== value)
        {
            model_internal::_oArtifactsIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "oArtifactsIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get oArtifactsIsValid():Boolean
    {
        if (!model_internal::_oArtifactsIsValidCacheInitialized)
        {
            model_internal::calculateOArtifactsIsValid();
        }

        return model_internal::_oArtifactsIsValid;
    }

    model_internal function calculateOArtifactsIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_oArtifactsValidator.validate(model_internal::_instance.oArtifacts)
        model_internal::_oArtifactsIsValid_der = (valRes.results == null);
        model_internal::_oArtifactsIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::oArtifactsValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::oArtifactsValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get oArtifactsValidationFailureMessages():Array
    {
        if (model_internal::_oArtifactsValidationFailureMessages == null)
            model_internal::calculateOArtifactsIsValid();

        return _oArtifactsValidationFailureMessages;
    }

    model_internal function set oArtifactsValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_oArtifactsValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_oArtifactsValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "oArtifactsValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get ImedStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get ImedValidator() : StyleValidator
    {
        return model_internal::_ImedValidator;
    }

    model_internal function set _ImedIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_ImedIsValid;         
        if (oldValue !== value)
        {
            model_internal::_ImedIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ImedIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get ImedIsValid():Boolean
    {
        if (!model_internal::_ImedIsValidCacheInitialized)
        {
            model_internal::calculateImedIsValid();
        }

        return model_internal::_ImedIsValid;
    }

    model_internal function calculateImedIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_ImedValidator.validate(model_internal::_instance.Imed)
        model_internal::_ImedIsValid_der = (valRes.results == null);
        model_internal::_ImedIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::ImedValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::ImedValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get ImedValidationFailureMessages():Array
    {
        if (model_internal::_ImedValidationFailureMessages == null)
            model_internal::calculateImedIsValid();

        return _ImedValidationFailureMessages;
    }

    model_internal function set ImedValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_ImedValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_ImedValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ImedValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            case("Boleta"):
            {
                return BoletaValidationFailureMessages;
            }
            case("Kiosco"):
            {
                return KioscoValidationFailureMessages;
            }
            case("Theme"):
            {
                return ThemeValidationFailureMessages;
            }
            case("oArtifacts"):
            {
                return oArtifactsValidationFailureMessages;
            }
            case("Imed"):
            {
                return ImedValidationFailureMessages;
            }
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
