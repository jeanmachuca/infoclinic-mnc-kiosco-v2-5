/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - DatosPaciente.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.util.FiberUtils;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.Event;
import flash.events.EventDispatcher;
import mx.binding.utils.ChangeWatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import mx.validators.ValidationResult;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_DatosPaciente extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _DatosPacienteEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_CodResult : String;
    private var _internal_MsgResult : String;
    private var _internal_datosCertificacionAfiliacion : Object;
    private var _internal_datosPersonales : Object;
    private var _internal_estaEnrolado : String;
    private var _internal_nombre : String;
    private var _internal_identificacion : String;
    private var _internal_genero : String;
    private var _internal_email : String;
    private var _internal_fonoCelular : String;
    private var _internal_apellidoMaterno : String;
    private var _internal_apellidoPaterno : String;
    private var _internal_fonoFijo : String;
    private var _internal_fechaNacimiento : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_DatosPaciente()
    {
        _model = new _DatosPacienteEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "CodResult", model_internal::setterListenerCodResult));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "MsgResult", model_internal::setterListenerMsgResult));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "datosCertificacionAfiliacion", model_internal::setterListenerDatosCertificacionAfiliacion));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "datosPersonales", model_internal::setterListenerDatosPersonales));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "estaEnrolado", model_internal::setterListenerEstaEnrolado));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "nombre", model_internal::setterListenerNombre));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "identificacion", model_internal::setterListenerIdentificacion));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "genero", model_internal::setterListenerGenero));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "email", model_internal::setterListenerEmail));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "fonoCelular", model_internal::setterListenerFonoCelular));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "apellidoMaterno", model_internal::setterListenerApellidoMaterno));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "apellidoPaterno", model_internal::setterListenerApellidoPaterno));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "fonoFijo", model_internal::setterListenerFonoFijo));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "fechaNacimiento", model_internal::setterListenerFechaNacimiento));

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get CodResult() : String
    {
        return _internal_CodResult;
    }

    [Bindable(event="propertyChange")]
    public function get MsgResult() : String
    {
        return _internal_MsgResult;
    }

    [Bindable(event="propertyChange")]
    public function get datosCertificacionAfiliacion() : Object
    {
        return _internal_datosCertificacionAfiliacion;
    }

    [Bindable(event="propertyChange")]
    public function get datosPersonales() : Object
    {
        return _internal_datosPersonales;
    }

    [Bindable(event="propertyChange")]
    public function get estaEnrolado() : String
    {
        return _internal_estaEnrolado;
    }

    [Bindable(event="propertyChange")]
    public function get nombre() : String
    {
        return _internal_nombre;
    }

    [Bindable(event="propertyChange")]
    public function get identificacion() : String
    {
        return _internal_identificacion;
    }

    [Bindable(event="propertyChange")]
    public function get genero() : String
    {
        return _internal_genero;
    }

    [Bindable(event="propertyChange")]
    public function get email() : String
    {
        return _internal_email;
    }

    [Bindable(event="propertyChange")]
    public function get fonoCelular() : String
    {
        return _internal_fonoCelular;
    }

    [Bindable(event="propertyChange")]
    public function get apellidoMaterno() : String
    {
        return _internal_apellidoMaterno;
    }

    [Bindable(event="propertyChange")]
    public function get apellidoPaterno() : String
    {
        return _internal_apellidoPaterno;
    }

    [Bindable(event="propertyChange")]
    public function get fonoFijo() : String
    {
        return _internal_fonoFijo;
    }

    [Bindable(event="propertyChange")]
    public function get fechaNacimiento() : String
    {
        return _internal_fechaNacimiento;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set CodResult(value:String) : void
    {
        var oldValue:String = _internal_CodResult;
        if (oldValue !== value)
        {
            _internal_CodResult = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CodResult", oldValue, _internal_CodResult));
        }
    }

    public function set MsgResult(value:String) : void
    {
        var oldValue:String = _internal_MsgResult;
        if (oldValue !== value)
        {
            _internal_MsgResult = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MsgResult", oldValue, _internal_MsgResult));
        }
    }

    public function set datosCertificacionAfiliacion(value:Object) : void
    {
        var oldValue:Object = _internal_datosCertificacionAfiliacion;
        if (oldValue !== value)
        {
            _internal_datosCertificacionAfiliacion = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "datosCertificacionAfiliacion", oldValue, _internal_datosCertificacionAfiliacion));
        }
    }

    public function set datosPersonales(value:Object) : void
    {
        var oldValue:Object = _internal_datosPersonales;
        if (oldValue !== value)
        {
            _internal_datosPersonales = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "datosPersonales", oldValue, _internal_datosPersonales));
        }
    }

    public function set estaEnrolado(value:String) : void
    {
        var oldValue:String = _internal_estaEnrolado;
        if (oldValue !== value)
        {
            _internal_estaEnrolado = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "estaEnrolado", oldValue, _internal_estaEnrolado));
        }
    }

    public function set nombre(value:String) : void
    {
        var oldValue:String = _internal_nombre;
        if (oldValue !== value)
        {
            _internal_nombre = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "nombre", oldValue, _internal_nombre));
        }
    }

    public function set identificacion(value:String) : void
    {
        var oldValue:String = _internal_identificacion;
        if (oldValue !== value)
        {
            _internal_identificacion = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "identificacion", oldValue, _internal_identificacion));
        }
    }

    public function set genero(value:String) : void
    {
        var oldValue:String = _internal_genero;
        if (oldValue !== value)
        {
            _internal_genero = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "genero", oldValue, _internal_genero));
        }
    }

    public function set email(value:String) : void
    {
        var oldValue:String = _internal_email;
        if (oldValue !== value)
        {
            _internal_email = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "email", oldValue, _internal_email));
        }
    }

    public function set fonoCelular(value:String) : void
    {
        var oldValue:String = _internal_fonoCelular;
        if (oldValue !== value)
        {
            _internal_fonoCelular = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "fonoCelular", oldValue, _internal_fonoCelular));
        }
    }

    public function set apellidoMaterno(value:String) : void
    {
        var oldValue:String = _internal_apellidoMaterno;
        if (oldValue !== value)
        {
            _internal_apellidoMaterno = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "apellidoMaterno", oldValue, _internal_apellidoMaterno));
        }
    }

    public function set apellidoPaterno(value:String) : void
    {
        var oldValue:String = _internal_apellidoPaterno;
        if (oldValue !== value)
        {
            _internal_apellidoPaterno = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "apellidoPaterno", oldValue, _internal_apellidoPaterno));
        }
    }

    public function set fonoFijo(value:String) : void
    {
        var oldValue:String = _internal_fonoFijo;
        if (oldValue !== value)
        {
            _internal_fonoFijo = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "fonoFijo", oldValue, _internal_fonoFijo));
        }
    }

    public function set fechaNacimiento(value:String) : void
    {
        var oldValue:String = _internal_fechaNacimiento;
        if (oldValue !== value)
        {
            _internal_fechaNacimiento = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "fechaNacimiento", oldValue, _internal_fechaNacimiento));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */

    model_internal function setterListenerCodResult(value:flash.events.Event):void
    {
        _model.invalidateDependentOnCodResult();
    }

    model_internal function setterListenerMsgResult(value:flash.events.Event):void
    {
        _model.invalidateDependentOnMsgResult();
    }

    model_internal function setterListenerDatosCertificacionAfiliacion(value:flash.events.Event):void
    {
        _model.invalidateDependentOnDatosCertificacionAfiliacion();
    }

    model_internal function setterListenerDatosPersonales(value:flash.events.Event):void
    {
        _model.invalidateDependentOnDatosPersonales();
    }

    model_internal function setterListenerEstaEnrolado(value:flash.events.Event):void
    {
        _model.invalidateDependentOnEstaEnrolado();
    }

    model_internal function setterListenerNombre(value:flash.events.Event):void
    {
        _model.invalidateDependentOnNombre();
    }

    model_internal function setterListenerIdentificacion(value:flash.events.Event):void
    {
        _model.invalidateDependentOnIdentificacion();
    }

    model_internal function setterListenerGenero(value:flash.events.Event):void
    {
        _model.invalidateDependentOnGenero();
    }

    model_internal function setterListenerEmail(value:flash.events.Event):void
    {
        _model.invalidateDependentOnEmail();
    }

    model_internal function setterListenerFonoCelular(value:flash.events.Event):void
    {
        _model.invalidateDependentOnFonoCelular();
    }

    model_internal function setterListenerApellidoMaterno(value:flash.events.Event):void
    {
        _model.invalidateDependentOnApellidoMaterno();
    }

    model_internal function setterListenerApellidoPaterno(value:flash.events.Event):void
    {
        _model.invalidateDependentOnApellidoPaterno();
    }

    model_internal function setterListenerFonoFijo(value:flash.events.Event):void
    {
        _model.invalidateDependentOnFonoFijo();
    }

    model_internal function setterListenerFechaNacimiento(value:flash.events.Event):void
    {
        _model.invalidateDependentOnFechaNacimiento();
    }


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;
        if (!_model.CodResultIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_CodResultValidationFailureMessages);
        }
        if (!_model.MsgResultIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_MsgResultValidationFailureMessages);
        }
        if (!_model.datosCertificacionAfiliacionIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_datosCertificacionAfiliacionValidationFailureMessages);
        }
        if (!_model.datosPersonalesIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_datosPersonalesValidationFailureMessages);
        }
        if (!_model.estaEnroladoIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_estaEnroladoValidationFailureMessages);
        }
        if (!_model.nombreIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_nombreValidationFailureMessages);
        }
        if (!_model.identificacionIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_identificacionValidationFailureMessages);
        }
        if (!_model.generoIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_generoValidationFailureMessages);
        }
        if (!_model.emailIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_emailValidationFailureMessages);
        }
        if (!_model.fonoCelularIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_fonoCelularValidationFailureMessages);
        }
        if (!_model.apellidoMaternoIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_apellidoMaternoValidationFailureMessages);
        }
        if (!_model.apellidoPaternoIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_apellidoPaternoValidationFailureMessages);
        }
        if (!_model.fonoFijoIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_fonoFijoValidationFailureMessages);
        }
        if (!_model.fechaNacimientoIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_fechaNacimientoValidationFailureMessages);
        }

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _DatosPacienteEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _DatosPacienteEntityMetadata) : void
    {
        var oldValue : _DatosPacienteEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }

    model_internal var _doValidationCacheOfCodResult : Array = null;
    model_internal var _doValidationLastValOfCodResult : String;

    model_internal function _doValidationForCodResult(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfCodResult != null && model_internal::_doValidationLastValOfCodResult == value)
           return model_internal::_doValidationCacheOfCodResult ;

        _model.model_internal::_CodResultIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isCodResultAvailable && _internal_CodResult == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "CodResult is required"));
        }

        model_internal::_doValidationCacheOfCodResult = validationFailures;
        model_internal::_doValidationLastValOfCodResult = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfMsgResult : Array = null;
    model_internal var _doValidationLastValOfMsgResult : String;

    model_internal function _doValidationForMsgResult(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfMsgResult != null && model_internal::_doValidationLastValOfMsgResult == value)
           return model_internal::_doValidationCacheOfMsgResult ;

        _model.model_internal::_MsgResultIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isMsgResultAvailable && _internal_MsgResult == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "MsgResult is required"));
        }

        model_internal::_doValidationCacheOfMsgResult = validationFailures;
        model_internal::_doValidationLastValOfMsgResult = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfDatosCertificacionAfiliacion : Array = null;
    model_internal var _doValidationLastValOfDatosCertificacionAfiliacion : Object;

    model_internal function _doValidationForDatosCertificacionAfiliacion(valueIn:Object):Array
    {
        var value : Object = valueIn as Object;

        if (model_internal::_doValidationCacheOfDatosCertificacionAfiliacion != null && model_internal::_doValidationLastValOfDatosCertificacionAfiliacion == value)
           return model_internal::_doValidationCacheOfDatosCertificacionAfiliacion ;

        _model.model_internal::_datosCertificacionAfiliacionIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isDatosCertificacionAfiliacionAvailable && _internal_datosCertificacionAfiliacion == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "datosCertificacionAfiliacion is required"));
        }

        model_internal::_doValidationCacheOfDatosCertificacionAfiliacion = validationFailures;
        model_internal::_doValidationLastValOfDatosCertificacionAfiliacion = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfDatosPersonales : Array = null;
    model_internal var _doValidationLastValOfDatosPersonales : Object;

    model_internal function _doValidationForDatosPersonales(valueIn:Object):Array
    {
        var value : Object = valueIn as Object;

        if (model_internal::_doValidationCacheOfDatosPersonales != null && model_internal::_doValidationLastValOfDatosPersonales == value)
           return model_internal::_doValidationCacheOfDatosPersonales ;

        _model.model_internal::_datosPersonalesIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isDatosPersonalesAvailable && _internal_datosPersonales == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "datosPersonales is required"));
        }

        model_internal::_doValidationCacheOfDatosPersonales = validationFailures;
        model_internal::_doValidationLastValOfDatosPersonales = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfEstaEnrolado : Array = null;
    model_internal var _doValidationLastValOfEstaEnrolado : String;

    model_internal function _doValidationForEstaEnrolado(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfEstaEnrolado != null && model_internal::_doValidationLastValOfEstaEnrolado == value)
           return model_internal::_doValidationCacheOfEstaEnrolado ;

        _model.model_internal::_estaEnroladoIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isEstaEnroladoAvailable && _internal_estaEnrolado == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "estaEnrolado is required"));
        }

        model_internal::_doValidationCacheOfEstaEnrolado = validationFailures;
        model_internal::_doValidationLastValOfEstaEnrolado = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfNombre : Array = null;
    model_internal var _doValidationLastValOfNombre : String;

    model_internal function _doValidationForNombre(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfNombre != null && model_internal::_doValidationLastValOfNombre == value)
           return model_internal::_doValidationCacheOfNombre ;

        _model.model_internal::_nombreIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isNombreAvailable && _internal_nombre == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "nombre is required"));
        }

        model_internal::_doValidationCacheOfNombre = validationFailures;
        model_internal::_doValidationLastValOfNombre = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfIdentificacion : Array = null;
    model_internal var _doValidationLastValOfIdentificacion : String;

    model_internal function _doValidationForIdentificacion(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfIdentificacion != null && model_internal::_doValidationLastValOfIdentificacion == value)
           return model_internal::_doValidationCacheOfIdentificacion ;

        _model.model_internal::_identificacionIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isIdentificacionAvailable && _internal_identificacion == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "identificacion is required"));
        }

        model_internal::_doValidationCacheOfIdentificacion = validationFailures;
        model_internal::_doValidationLastValOfIdentificacion = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfGenero : Array = null;
    model_internal var _doValidationLastValOfGenero : String;

    model_internal function _doValidationForGenero(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfGenero != null && model_internal::_doValidationLastValOfGenero == value)
           return model_internal::_doValidationCacheOfGenero ;

        _model.model_internal::_generoIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isGeneroAvailable && _internal_genero == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "genero is required"));
        }

        model_internal::_doValidationCacheOfGenero = validationFailures;
        model_internal::_doValidationLastValOfGenero = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfEmail : Array = null;
    model_internal var _doValidationLastValOfEmail : String;

    model_internal function _doValidationForEmail(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfEmail != null && model_internal::_doValidationLastValOfEmail == value)
           return model_internal::_doValidationCacheOfEmail ;

        _model.model_internal::_emailIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isEmailAvailable && _internal_email == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "email is required"));
        }

        model_internal::_doValidationCacheOfEmail = validationFailures;
        model_internal::_doValidationLastValOfEmail = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfFonoCelular : Array = null;
    model_internal var _doValidationLastValOfFonoCelular : String;

    model_internal function _doValidationForFonoCelular(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfFonoCelular != null && model_internal::_doValidationLastValOfFonoCelular == value)
           return model_internal::_doValidationCacheOfFonoCelular ;

        _model.model_internal::_fonoCelularIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isFonoCelularAvailable && _internal_fonoCelular == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "fonoCelular is required"));
        }

        model_internal::_doValidationCacheOfFonoCelular = validationFailures;
        model_internal::_doValidationLastValOfFonoCelular = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfApellidoMaterno : Array = null;
    model_internal var _doValidationLastValOfApellidoMaterno : String;

    model_internal function _doValidationForApellidoMaterno(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfApellidoMaterno != null && model_internal::_doValidationLastValOfApellidoMaterno == value)
           return model_internal::_doValidationCacheOfApellidoMaterno ;

        _model.model_internal::_apellidoMaternoIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isApellidoMaternoAvailable && _internal_apellidoMaterno == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "apellidoMaterno is required"));
        }

        model_internal::_doValidationCacheOfApellidoMaterno = validationFailures;
        model_internal::_doValidationLastValOfApellidoMaterno = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfApellidoPaterno : Array = null;
    model_internal var _doValidationLastValOfApellidoPaterno : String;

    model_internal function _doValidationForApellidoPaterno(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfApellidoPaterno != null && model_internal::_doValidationLastValOfApellidoPaterno == value)
           return model_internal::_doValidationCacheOfApellidoPaterno ;

        _model.model_internal::_apellidoPaternoIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isApellidoPaternoAvailable && _internal_apellidoPaterno == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "apellidoPaterno is required"));
        }

        model_internal::_doValidationCacheOfApellidoPaterno = validationFailures;
        model_internal::_doValidationLastValOfApellidoPaterno = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfFonoFijo : Array = null;
    model_internal var _doValidationLastValOfFonoFijo : String;

    model_internal function _doValidationForFonoFijo(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfFonoFijo != null && model_internal::_doValidationLastValOfFonoFijo == value)
           return model_internal::_doValidationCacheOfFonoFijo ;

        _model.model_internal::_fonoFijoIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isFonoFijoAvailable && _internal_fonoFijo == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "fonoFijo is required"));
        }

        model_internal::_doValidationCacheOfFonoFijo = validationFailures;
        model_internal::_doValidationLastValOfFonoFijo = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfFechaNacimiento : Array = null;
    model_internal var _doValidationLastValOfFechaNacimiento : String;

    model_internal function _doValidationForFechaNacimiento(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfFechaNacimiento != null && model_internal::_doValidationLastValOfFechaNacimiento == value)
           return model_internal::_doValidationCacheOfFechaNacimiento ;

        _model.model_internal::_fechaNacimientoIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isFechaNacimientoAvailable && _internal_fechaNacimiento == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "fechaNacimiento is required"));
        }

        model_internal::_doValidationCacheOfFechaNacimiento = validationFailures;
        model_internal::_doValidationLastValOfFechaNacimiento = value;

        return validationFailures;
    }
    

}

}
