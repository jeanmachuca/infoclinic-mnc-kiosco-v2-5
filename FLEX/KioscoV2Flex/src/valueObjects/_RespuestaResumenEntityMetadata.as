
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.styles.StyleValidator;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import mx.events.ValidationResultEvent;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _RespuestaResumenEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("CodResult", "MsgResult", "rutMedico", "numeroPacientesEspera", "idBox", "tiempoEspera", "nombreMedico", "idMedico", "horaAtencion");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array("CodResult", "MsgResult", "rutMedico", "numeroPacientesEspera", "idBox", "tiempoEspera", "nombreMedico", "idMedico", "horaAtencion");
    model_internal static var allAlwaysAvailableProperties:Array = new Array("CodResult", "MsgResult", "rutMedico", "numeroPacientesEspera", "idBox", "tiempoEspera", "nombreMedico", "idMedico", "horaAtencion");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("CodResult", "MsgResult", "rutMedico", "numeroPacientesEspera", "idBox", "tiempoEspera", "nombreMedico", "idMedico", "horaAtencion");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("CodResult", "MsgResult", "rutMedico", "numeroPacientesEspera", "idBox", "tiempoEspera", "nombreMedico", "idMedico", "horaAtencion");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array();
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "RespuestaResumen";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;

    
    model_internal var _CodResultIsValid:Boolean;
    model_internal var _CodResultValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _CodResultIsValidCacheInitialized:Boolean = false;
    model_internal var _CodResultValidationFailureMessages:Array;
    
    model_internal var _MsgResultIsValid:Boolean;
    model_internal var _MsgResultValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _MsgResultIsValidCacheInitialized:Boolean = false;
    model_internal var _MsgResultValidationFailureMessages:Array;
    
    model_internal var _rutMedicoIsValid:Boolean;
    model_internal var _rutMedicoValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _rutMedicoIsValidCacheInitialized:Boolean = false;
    model_internal var _rutMedicoValidationFailureMessages:Array;
    
    model_internal var _numeroPacientesEsperaIsValid:Boolean;
    model_internal var _numeroPacientesEsperaValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _numeroPacientesEsperaIsValidCacheInitialized:Boolean = false;
    model_internal var _numeroPacientesEsperaValidationFailureMessages:Array;
    
    model_internal var _idBoxIsValid:Boolean;
    model_internal var _idBoxValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _idBoxIsValidCacheInitialized:Boolean = false;
    model_internal var _idBoxValidationFailureMessages:Array;
    
    model_internal var _tiempoEsperaIsValid:Boolean;
    model_internal var _tiempoEsperaValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _tiempoEsperaIsValidCacheInitialized:Boolean = false;
    model_internal var _tiempoEsperaValidationFailureMessages:Array;
    
    model_internal var _nombreMedicoIsValid:Boolean;
    model_internal var _nombreMedicoValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _nombreMedicoIsValidCacheInitialized:Boolean = false;
    model_internal var _nombreMedicoValidationFailureMessages:Array;
    
    model_internal var _idMedicoIsValid:Boolean;
    model_internal var _idMedicoValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _idMedicoIsValidCacheInitialized:Boolean = false;
    model_internal var _idMedicoValidationFailureMessages:Array;
    
    model_internal var _horaAtencionIsValid:Boolean;
    model_internal var _horaAtencionValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _horaAtencionIsValidCacheInitialized:Boolean = false;
    model_internal var _horaAtencionValidationFailureMessages:Array;

    model_internal var _instance:_Super_RespuestaResumen;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _RespuestaResumenEntityMetadata(value : _Super_RespuestaResumen)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["CodResult"] = new Array();
            model_internal::dependentsOnMap["MsgResult"] = new Array();
            model_internal::dependentsOnMap["rutMedico"] = new Array();
            model_internal::dependentsOnMap["numeroPacientesEspera"] = new Array();
            model_internal::dependentsOnMap["idBox"] = new Array();
            model_internal::dependentsOnMap["tiempoEspera"] = new Array();
            model_internal::dependentsOnMap["nombreMedico"] = new Array();
            model_internal::dependentsOnMap["idMedico"] = new Array();
            model_internal::dependentsOnMap["horaAtencion"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["CodResult"] = "String";
        model_internal::propertyTypeMap["MsgResult"] = "String";
        model_internal::propertyTypeMap["rutMedico"] = "String";
        model_internal::propertyTypeMap["numeroPacientesEspera"] = "String";
        model_internal::propertyTypeMap["idBox"] = "String";
        model_internal::propertyTypeMap["tiempoEspera"] = "String";
        model_internal::propertyTypeMap["nombreMedico"] = "String";
        model_internal::propertyTypeMap["idMedico"] = "String";
        model_internal::propertyTypeMap["horaAtencion"] = "String";

        model_internal::_instance = value;
        model_internal::_CodResultValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForCodResult);
        model_internal::_CodResultValidator.required = true;
        model_internal::_CodResultValidator.requiredFieldError = "CodResult is required";
        //model_internal::_CodResultValidator.source = model_internal::_instance;
        //model_internal::_CodResultValidator.property = "CodResult";
        model_internal::_MsgResultValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForMsgResult);
        model_internal::_MsgResultValidator.required = true;
        model_internal::_MsgResultValidator.requiredFieldError = "MsgResult is required";
        //model_internal::_MsgResultValidator.source = model_internal::_instance;
        //model_internal::_MsgResultValidator.property = "MsgResult";
        model_internal::_rutMedicoValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForRutMedico);
        model_internal::_rutMedicoValidator.required = true;
        model_internal::_rutMedicoValidator.requiredFieldError = "rutMedico is required";
        //model_internal::_rutMedicoValidator.source = model_internal::_instance;
        //model_internal::_rutMedicoValidator.property = "rutMedico";
        model_internal::_numeroPacientesEsperaValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForNumeroPacientesEspera);
        model_internal::_numeroPacientesEsperaValidator.required = true;
        model_internal::_numeroPacientesEsperaValidator.requiredFieldError = "numeroPacientesEspera is required";
        //model_internal::_numeroPacientesEsperaValidator.source = model_internal::_instance;
        //model_internal::_numeroPacientesEsperaValidator.property = "numeroPacientesEspera";
        model_internal::_idBoxValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForIdBox);
        model_internal::_idBoxValidator.required = true;
        model_internal::_idBoxValidator.requiredFieldError = "idBox is required";
        //model_internal::_idBoxValidator.source = model_internal::_instance;
        //model_internal::_idBoxValidator.property = "idBox";
        model_internal::_tiempoEsperaValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForTiempoEspera);
        model_internal::_tiempoEsperaValidator.required = true;
        model_internal::_tiempoEsperaValidator.requiredFieldError = "tiempoEspera is required";
        //model_internal::_tiempoEsperaValidator.source = model_internal::_instance;
        //model_internal::_tiempoEsperaValidator.property = "tiempoEspera";
        model_internal::_nombreMedicoValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForNombreMedico);
        model_internal::_nombreMedicoValidator.required = true;
        model_internal::_nombreMedicoValidator.requiredFieldError = "nombreMedico is required";
        //model_internal::_nombreMedicoValidator.source = model_internal::_instance;
        //model_internal::_nombreMedicoValidator.property = "nombreMedico";
        model_internal::_idMedicoValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForIdMedico);
        model_internal::_idMedicoValidator.required = true;
        model_internal::_idMedicoValidator.requiredFieldError = "idMedico is required";
        //model_internal::_idMedicoValidator.source = model_internal::_instance;
        //model_internal::_idMedicoValidator.property = "idMedico";
        model_internal::_horaAtencionValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForHoraAtencion);
        model_internal::_horaAtencionValidator.required = true;
        model_internal::_horaAtencionValidator.requiredFieldError = "horaAtencion is required";
        //model_internal::_horaAtencionValidator.source = model_internal::_instance;
        //model_internal::_horaAtencionValidator.property = "horaAtencion";
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity RespuestaResumen");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity RespuestaResumen");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of RespuestaResumen");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity RespuestaResumen");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity RespuestaResumen");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity RespuestaResumen");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isCodResultAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMsgResultAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isRutMedicoAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isNumeroPacientesEsperaAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIdBoxAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTiempoEsperaAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isNombreMedicoAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIdMedicoAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHoraAtencionAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */
    public function invalidateDependentOnCodResult():void
    {
        if (model_internal::_CodResultIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfCodResult = null;
            model_internal::calculateCodResultIsValid();
        }
    }
    public function invalidateDependentOnMsgResult():void
    {
        if (model_internal::_MsgResultIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfMsgResult = null;
            model_internal::calculateMsgResultIsValid();
        }
    }
    public function invalidateDependentOnRutMedico():void
    {
        if (model_internal::_rutMedicoIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfRutMedico = null;
            model_internal::calculateRutMedicoIsValid();
        }
    }
    public function invalidateDependentOnNumeroPacientesEspera():void
    {
        if (model_internal::_numeroPacientesEsperaIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfNumeroPacientesEspera = null;
            model_internal::calculateNumeroPacientesEsperaIsValid();
        }
    }
    public function invalidateDependentOnIdBox():void
    {
        if (model_internal::_idBoxIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfIdBox = null;
            model_internal::calculateIdBoxIsValid();
        }
    }
    public function invalidateDependentOnTiempoEspera():void
    {
        if (model_internal::_tiempoEsperaIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfTiempoEspera = null;
            model_internal::calculateTiempoEsperaIsValid();
        }
    }
    public function invalidateDependentOnNombreMedico():void
    {
        if (model_internal::_nombreMedicoIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfNombreMedico = null;
            model_internal::calculateNombreMedicoIsValid();
        }
    }
    public function invalidateDependentOnIdMedico():void
    {
        if (model_internal::_idMedicoIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfIdMedico = null;
            model_internal::calculateIdMedicoIsValid();
        }
    }
    public function invalidateDependentOnHoraAtencion():void
    {
        if (model_internal::_horaAtencionIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfHoraAtencion = null;
            model_internal::calculateHoraAtencionIsValid();
        }
    }

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get CodResultStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get CodResultValidator() : StyleValidator
    {
        return model_internal::_CodResultValidator;
    }

    model_internal function set _CodResultIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_CodResultIsValid;         
        if (oldValue !== value)
        {
            model_internal::_CodResultIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CodResultIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get CodResultIsValid():Boolean
    {
        if (!model_internal::_CodResultIsValidCacheInitialized)
        {
            model_internal::calculateCodResultIsValid();
        }

        return model_internal::_CodResultIsValid;
    }

    model_internal function calculateCodResultIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_CodResultValidator.validate(model_internal::_instance.CodResult)
        model_internal::_CodResultIsValid_der = (valRes.results == null);
        model_internal::_CodResultIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::CodResultValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::CodResultValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get CodResultValidationFailureMessages():Array
    {
        if (model_internal::_CodResultValidationFailureMessages == null)
            model_internal::calculateCodResultIsValid();

        return _CodResultValidationFailureMessages;
    }

    model_internal function set CodResultValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_CodResultValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_CodResultValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CodResultValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get MsgResultStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get MsgResultValidator() : StyleValidator
    {
        return model_internal::_MsgResultValidator;
    }

    model_internal function set _MsgResultIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_MsgResultIsValid;         
        if (oldValue !== value)
        {
            model_internal::_MsgResultIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MsgResultIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get MsgResultIsValid():Boolean
    {
        if (!model_internal::_MsgResultIsValidCacheInitialized)
        {
            model_internal::calculateMsgResultIsValid();
        }

        return model_internal::_MsgResultIsValid;
    }

    model_internal function calculateMsgResultIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_MsgResultValidator.validate(model_internal::_instance.MsgResult)
        model_internal::_MsgResultIsValid_der = (valRes.results == null);
        model_internal::_MsgResultIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::MsgResultValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::MsgResultValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get MsgResultValidationFailureMessages():Array
    {
        if (model_internal::_MsgResultValidationFailureMessages == null)
            model_internal::calculateMsgResultIsValid();

        return _MsgResultValidationFailureMessages;
    }

    model_internal function set MsgResultValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_MsgResultValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_MsgResultValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MsgResultValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get rutMedicoStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get rutMedicoValidator() : StyleValidator
    {
        return model_internal::_rutMedicoValidator;
    }

    model_internal function set _rutMedicoIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_rutMedicoIsValid;         
        if (oldValue !== value)
        {
            model_internal::_rutMedicoIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "rutMedicoIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get rutMedicoIsValid():Boolean
    {
        if (!model_internal::_rutMedicoIsValidCacheInitialized)
        {
            model_internal::calculateRutMedicoIsValid();
        }

        return model_internal::_rutMedicoIsValid;
    }

    model_internal function calculateRutMedicoIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_rutMedicoValidator.validate(model_internal::_instance.rutMedico)
        model_internal::_rutMedicoIsValid_der = (valRes.results == null);
        model_internal::_rutMedicoIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::rutMedicoValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::rutMedicoValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get rutMedicoValidationFailureMessages():Array
    {
        if (model_internal::_rutMedicoValidationFailureMessages == null)
            model_internal::calculateRutMedicoIsValid();

        return _rutMedicoValidationFailureMessages;
    }

    model_internal function set rutMedicoValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_rutMedicoValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_rutMedicoValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "rutMedicoValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get numeroPacientesEsperaStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get numeroPacientesEsperaValidator() : StyleValidator
    {
        return model_internal::_numeroPacientesEsperaValidator;
    }

    model_internal function set _numeroPacientesEsperaIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_numeroPacientesEsperaIsValid;         
        if (oldValue !== value)
        {
            model_internal::_numeroPacientesEsperaIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "numeroPacientesEsperaIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get numeroPacientesEsperaIsValid():Boolean
    {
        if (!model_internal::_numeroPacientesEsperaIsValidCacheInitialized)
        {
            model_internal::calculateNumeroPacientesEsperaIsValid();
        }

        return model_internal::_numeroPacientesEsperaIsValid;
    }

    model_internal function calculateNumeroPacientesEsperaIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_numeroPacientesEsperaValidator.validate(model_internal::_instance.numeroPacientesEspera)
        model_internal::_numeroPacientesEsperaIsValid_der = (valRes.results == null);
        model_internal::_numeroPacientesEsperaIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::numeroPacientesEsperaValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::numeroPacientesEsperaValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get numeroPacientesEsperaValidationFailureMessages():Array
    {
        if (model_internal::_numeroPacientesEsperaValidationFailureMessages == null)
            model_internal::calculateNumeroPacientesEsperaIsValid();

        return _numeroPacientesEsperaValidationFailureMessages;
    }

    model_internal function set numeroPacientesEsperaValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_numeroPacientesEsperaValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_numeroPacientesEsperaValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "numeroPacientesEsperaValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get idBoxStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get idBoxValidator() : StyleValidator
    {
        return model_internal::_idBoxValidator;
    }

    model_internal function set _idBoxIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_idBoxIsValid;         
        if (oldValue !== value)
        {
            model_internal::_idBoxIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "idBoxIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get idBoxIsValid():Boolean
    {
        if (!model_internal::_idBoxIsValidCacheInitialized)
        {
            model_internal::calculateIdBoxIsValid();
        }

        return model_internal::_idBoxIsValid;
    }

    model_internal function calculateIdBoxIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_idBoxValidator.validate(model_internal::_instance.idBox)
        model_internal::_idBoxIsValid_der = (valRes.results == null);
        model_internal::_idBoxIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::idBoxValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::idBoxValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get idBoxValidationFailureMessages():Array
    {
        if (model_internal::_idBoxValidationFailureMessages == null)
            model_internal::calculateIdBoxIsValid();

        return _idBoxValidationFailureMessages;
    }

    model_internal function set idBoxValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_idBoxValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_idBoxValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "idBoxValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get tiempoEsperaStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get tiempoEsperaValidator() : StyleValidator
    {
        return model_internal::_tiempoEsperaValidator;
    }

    model_internal function set _tiempoEsperaIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_tiempoEsperaIsValid;         
        if (oldValue !== value)
        {
            model_internal::_tiempoEsperaIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "tiempoEsperaIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get tiempoEsperaIsValid():Boolean
    {
        if (!model_internal::_tiempoEsperaIsValidCacheInitialized)
        {
            model_internal::calculateTiempoEsperaIsValid();
        }

        return model_internal::_tiempoEsperaIsValid;
    }

    model_internal function calculateTiempoEsperaIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_tiempoEsperaValidator.validate(model_internal::_instance.tiempoEspera)
        model_internal::_tiempoEsperaIsValid_der = (valRes.results == null);
        model_internal::_tiempoEsperaIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::tiempoEsperaValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::tiempoEsperaValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get tiempoEsperaValidationFailureMessages():Array
    {
        if (model_internal::_tiempoEsperaValidationFailureMessages == null)
            model_internal::calculateTiempoEsperaIsValid();

        return _tiempoEsperaValidationFailureMessages;
    }

    model_internal function set tiempoEsperaValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_tiempoEsperaValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_tiempoEsperaValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "tiempoEsperaValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get nombreMedicoStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get nombreMedicoValidator() : StyleValidator
    {
        return model_internal::_nombreMedicoValidator;
    }

    model_internal function set _nombreMedicoIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_nombreMedicoIsValid;         
        if (oldValue !== value)
        {
            model_internal::_nombreMedicoIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "nombreMedicoIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get nombreMedicoIsValid():Boolean
    {
        if (!model_internal::_nombreMedicoIsValidCacheInitialized)
        {
            model_internal::calculateNombreMedicoIsValid();
        }

        return model_internal::_nombreMedicoIsValid;
    }

    model_internal function calculateNombreMedicoIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_nombreMedicoValidator.validate(model_internal::_instance.nombreMedico)
        model_internal::_nombreMedicoIsValid_der = (valRes.results == null);
        model_internal::_nombreMedicoIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::nombreMedicoValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::nombreMedicoValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get nombreMedicoValidationFailureMessages():Array
    {
        if (model_internal::_nombreMedicoValidationFailureMessages == null)
            model_internal::calculateNombreMedicoIsValid();

        return _nombreMedicoValidationFailureMessages;
    }

    model_internal function set nombreMedicoValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_nombreMedicoValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_nombreMedicoValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "nombreMedicoValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get idMedicoStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get idMedicoValidator() : StyleValidator
    {
        return model_internal::_idMedicoValidator;
    }

    model_internal function set _idMedicoIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_idMedicoIsValid;         
        if (oldValue !== value)
        {
            model_internal::_idMedicoIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "idMedicoIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get idMedicoIsValid():Boolean
    {
        if (!model_internal::_idMedicoIsValidCacheInitialized)
        {
            model_internal::calculateIdMedicoIsValid();
        }

        return model_internal::_idMedicoIsValid;
    }

    model_internal function calculateIdMedicoIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_idMedicoValidator.validate(model_internal::_instance.idMedico)
        model_internal::_idMedicoIsValid_der = (valRes.results == null);
        model_internal::_idMedicoIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::idMedicoValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::idMedicoValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get idMedicoValidationFailureMessages():Array
    {
        if (model_internal::_idMedicoValidationFailureMessages == null)
            model_internal::calculateIdMedicoIsValid();

        return _idMedicoValidationFailureMessages;
    }

    model_internal function set idMedicoValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_idMedicoValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_idMedicoValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "idMedicoValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get horaAtencionStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get horaAtencionValidator() : StyleValidator
    {
        return model_internal::_horaAtencionValidator;
    }

    model_internal function set _horaAtencionIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_horaAtencionIsValid;         
        if (oldValue !== value)
        {
            model_internal::_horaAtencionIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "horaAtencionIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get horaAtencionIsValid():Boolean
    {
        if (!model_internal::_horaAtencionIsValidCacheInitialized)
        {
            model_internal::calculateHoraAtencionIsValid();
        }

        return model_internal::_horaAtencionIsValid;
    }

    model_internal function calculateHoraAtencionIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_horaAtencionValidator.validate(model_internal::_instance.horaAtencion)
        model_internal::_horaAtencionIsValid_der = (valRes.results == null);
        model_internal::_horaAtencionIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::horaAtencionValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::horaAtencionValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get horaAtencionValidationFailureMessages():Array
    {
        if (model_internal::_horaAtencionValidationFailureMessages == null)
            model_internal::calculateHoraAtencionIsValid();

        return _horaAtencionValidationFailureMessages;
    }

    model_internal function set horaAtencionValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_horaAtencionValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_horaAtencionValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "horaAtencionValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            case("CodResult"):
            {
                return CodResultValidationFailureMessages;
            }
            case("MsgResult"):
            {
                return MsgResultValidationFailureMessages;
            }
            case("rutMedico"):
            {
                return rutMedicoValidationFailureMessages;
            }
            case("numeroPacientesEspera"):
            {
                return numeroPacientesEsperaValidationFailureMessages;
            }
            case("idBox"):
            {
                return idBoxValidationFailureMessages;
            }
            case("tiempoEspera"):
            {
                return tiempoEsperaValidationFailureMessages;
            }
            case("nombreMedico"):
            {
                return nombreMedicoValidationFailureMessages;
            }
            case("idMedico"):
            {
                return idMedicoValidationFailureMessages;
            }
            case("horaAtencion"):
            {
                return horaAtencionValidationFailureMessages;
            }
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
