/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - Sucursal.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.util.FiberUtils;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.Event;
import flash.events.EventDispatcher;
import mx.binding.utils.ChangeWatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import mx.validators.ValidationResult;
import valueObjects.ListaPrevision;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_Sucursal extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.ListaPrevision.initRemoteClassAliasSingleChild();
        valueObjects.Prevision.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _SucursalEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_nombre : String;
    private var _internal_direccion : String;
    private var _internal_CodResult : String;
    private var _internal_MsgResult : String;
    private var _internal_listaPrevision : valueObjects.ListaPrevision;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_Sucursal()
    {
        _model = new _SucursalEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "nombre", model_internal::setterListenerNombre));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "direccion", model_internal::setterListenerDireccion));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "CodResult", model_internal::setterListenerCodResult));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "MsgResult", model_internal::setterListenerMsgResult));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "listaPrevision", model_internal::setterListenerListaPrevision));

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get nombre() : String
    {
        return _internal_nombre;
    }

    [Bindable(event="propertyChange")]
    public function get direccion() : String
    {
        return _internal_direccion;
    }

    [Bindable(event="propertyChange")]
    public function get CodResult() : String
    {
        return _internal_CodResult;
    }

    [Bindable(event="propertyChange")]
    public function get MsgResult() : String
    {
        return _internal_MsgResult;
    }

    [Bindable(event="propertyChange")]
    public function get listaPrevision() : valueObjects.ListaPrevision
    {
        return _internal_listaPrevision;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set nombre(value:String) : void
    {
        var oldValue:String = _internal_nombre;
        if (oldValue !== value)
        {
            _internal_nombre = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "nombre", oldValue, _internal_nombre));
        }
    }

    public function set direccion(value:String) : void
    {
        var oldValue:String = _internal_direccion;
        if (oldValue !== value)
        {
            _internal_direccion = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "direccion", oldValue, _internal_direccion));
        }
    }

    public function set CodResult(value:String) : void
    {
        var oldValue:String = _internal_CodResult;
        if (oldValue !== value)
        {
            _internal_CodResult = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CodResult", oldValue, _internal_CodResult));
        }
    }

    public function set MsgResult(value:String) : void
    {
        var oldValue:String = _internal_MsgResult;
        if (oldValue !== value)
        {
            _internal_MsgResult = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MsgResult", oldValue, _internal_MsgResult));
        }
    }

    public function set listaPrevision(value:valueObjects.ListaPrevision) : void
    {
        var oldValue:valueObjects.ListaPrevision = _internal_listaPrevision;
        if (oldValue !== value)
        {
            _internal_listaPrevision = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "listaPrevision", oldValue, _internal_listaPrevision));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */

    model_internal function setterListenerNombre(value:flash.events.Event):void
    {
        _model.invalidateDependentOnNombre();
    }

    model_internal function setterListenerDireccion(value:flash.events.Event):void
    {
        _model.invalidateDependentOnDireccion();
    }

    model_internal function setterListenerCodResult(value:flash.events.Event):void
    {
        _model.invalidateDependentOnCodResult();
    }

    model_internal function setterListenerMsgResult(value:flash.events.Event):void
    {
        _model.invalidateDependentOnMsgResult();
    }

    model_internal function setterListenerListaPrevision(value:flash.events.Event):void
    {
        _model.invalidateDependentOnListaPrevision();
    }


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;
        if (!_model.nombreIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_nombreValidationFailureMessages);
        }
        if (!_model.direccionIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_direccionValidationFailureMessages);
        }
        if (!_model.CodResultIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_CodResultValidationFailureMessages);
        }
        if (!_model.MsgResultIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_MsgResultValidationFailureMessages);
        }
        if (!_model.listaPrevisionIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_listaPrevisionValidationFailureMessages);
        }

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _SucursalEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _SucursalEntityMetadata) : void
    {
        var oldValue : _SucursalEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }

    model_internal var _doValidationCacheOfNombre : Array = null;
    model_internal var _doValidationLastValOfNombre : String;

    model_internal function _doValidationForNombre(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfNombre != null && model_internal::_doValidationLastValOfNombre == value)
           return model_internal::_doValidationCacheOfNombre ;

        _model.model_internal::_nombreIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isNombreAvailable && _internal_nombre == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "nombre is required"));
        }

        model_internal::_doValidationCacheOfNombre = validationFailures;
        model_internal::_doValidationLastValOfNombre = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfDireccion : Array = null;
    model_internal var _doValidationLastValOfDireccion : String;

    model_internal function _doValidationForDireccion(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfDireccion != null && model_internal::_doValidationLastValOfDireccion == value)
           return model_internal::_doValidationCacheOfDireccion ;

        _model.model_internal::_direccionIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isDireccionAvailable && _internal_direccion == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "direccion is required"));
        }

        model_internal::_doValidationCacheOfDireccion = validationFailures;
        model_internal::_doValidationLastValOfDireccion = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfCodResult : Array = null;
    model_internal var _doValidationLastValOfCodResult : String;

    model_internal function _doValidationForCodResult(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfCodResult != null && model_internal::_doValidationLastValOfCodResult == value)
           return model_internal::_doValidationCacheOfCodResult ;

        _model.model_internal::_CodResultIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isCodResultAvailable && _internal_CodResult == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "CodResult is required"));
        }

        model_internal::_doValidationCacheOfCodResult = validationFailures;
        model_internal::_doValidationLastValOfCodResult = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfMsgResult : Array = null;
    model_internal var _doValidationLastValOfMsgResult : String;

    model_internal function _doValidationForMsgResult(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfMsgResult != null && model_internal::_doValidationLastValOfMsgResult == value)
           return model_internal::_doValidationCacheOfMsgResult ;

        _model.model_internal::_MsgResultIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isMsgResultAvailable && _internal_MsgResult == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "MsgResult is required"));
        }

        model_internal::_doValidationCacheOfMsgResult = validationFailures;
        model_internal::_doValidationLastValOfMsgResult = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfListaPrevision : Array = null;
    model_internal var _doValidationLastValOfListaPrevision : valueObjects.ListaPrevision;

    model_internal function _doValidationForListaPrevision(valueIn:Object):Array
    {
        var value : valueObjects.ListaPrevision = valueIn as valueObjects.ListaPrevision;

        if (model_internal::_doValidationCacheOfListaPrevision != null && model_internal::_doValidationLastValOfListaPrevision == value)
           return model_internal::_doValidationCacheOfListaPrevision ;

        _model.model_internal::_listaPrevisionIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isListaPrevisionAvailable && _internal_listaPrevision == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "listaPrevision is required"));
        }

        model_internal::_doValidationCacheOfListaPrevision = validationFailures;
        model_internal::_doValidationLastValOfListaPrevision = value;

        return validationFailures;
    }
    

}

}
