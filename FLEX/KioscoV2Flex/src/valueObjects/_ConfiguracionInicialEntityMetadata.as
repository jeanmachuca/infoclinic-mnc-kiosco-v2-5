
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.styles.StyleValidator;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import mx.events.ValidationResultEvent;
import valueObjects.ConfigData;
import valueObjects.ListaPrevision;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _ConfiguracionInicialEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("nombre", "configData", "direccion", "CodResult", "MsgResult", "listaPrevision");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array("nombre", "configData", "direccion", "CodResult", "MsgResult", "listaPrevision");
    model_internal static var allAlwaysAvailableProperties:Array = new Array("nombre", "configData", "direccion", "CodResult", "MsgResult", "listaPrevision");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("nombre", "configData", "direccion", "CodResult", "MsgResult", "listaPrevision");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("nombre", "configData", "direccion", "CodResult", "MsgResult", "listaPrevision");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array();
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "ConfiguracionInicial";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;

    
    model_internal var _nombreIsValid:Boolean;
    model_internal var _nombreValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _nombreIsValidCacheInitialized:Boolean = false;
    model_internal var _nombreValidationFailureMessages:Array;
    
    model_internal var _configDataIsValid:Boolean;
    model_internal var _configDataValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _configDataIsValidCacheInitialized:Boolean = false;
    model_internal var _configDataValidationFailureMessages:Array;
    
    model_internal var _direccionIsValid:Boolean;
    model_internal var _direccionValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _direccionIsValidCacheInitialized:Boolean = false;
    model_internal var _direccionValidationFailureMessages:Array;
    
    model_internal var _CodResultIsValid:Boolean;
    model_internal var _CodResultValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _CodResultIsValidCacheInitialized:Boolean = false;
    model_internal var _CodResultValidationFailureMessages:Array;
    
    model_internal var _MsgResultIsValid:Boolean;
    model_internal var _MsgResultValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _MsgResultIsValidCacheInitialized:Boolean = false;
    model_internal var _MsgResultValidationFailureMessages:Array;
    
    model_internal var _listaPrevisionIsValid:Boolean;
    model_internal var _listaPrevisionValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _listaPrevisionIsValidCacheInitialized:Boolean = false;
    model_internal var _listaPrevisionValidationFailureMessages:Array;

    model_internal var _instance:_Super_ConfiguracionInicial;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _ConfiguracionInicialEntityMetadata(value : _Super_ConfiguracionInicial)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["nombre"] = new Array();
            model_internal::dependentsOnMap["configData"] = new Array();
            model_internal::dependentsOnMap["direccion"] = new Array();
            model_internal::dependentsOnMap["CodResult"] = new Array();
            model_internal::dependentsOnMap["MsgResult"] = new Array();
            model_internal::dependentsOnMap["listaPrevision"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["nombre"] = "String";
        model_internal::propertyTypeMap["configData"] = "valueObjects.ConfigData";
        model_internal::propertyTypeMap["direccion"] = "String";
        model_internal::propertyTypeMap["CodResult"] = "String";
        model_internal::propertyTypeMap["MsgResult"] = "String";
        model_internal::propertyTypeMap["listaPrevision"] = "valueObjects.ListaPrevision";

        model_internal::_instance = value;
        model_internal::_nombreValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForNombre);
        model_internal::_nombreValidator.required = true;
        model_internal::_nombreValidator.requiredFieldError = "nombre is required";
        //model_internal::_nombreValidator.source = model_internal::_instance;
        //model_internal::_nombreValidator.property = "nombre";
        model_internal::_configDataValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForConfigData);
        model_internal::_configDataValidator.required = true;
        model_internal::_configDataValidator.requiredFieldError = "configData is required";
        //model_internal::_configDataValidator.source = model_internal::_instance;
        //model_internal::_configDataValidator.property = "configData";
        model_internal::_direccionValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForDireccion);
        model_internal::_direccionValidator.required = true;
        model_internal::_direccionValidator.requiredFieldError = "direccion is required";
        //model_internal::_direccionValidator.source = model_internal::_instance;
        //model_internal::_direccionValidator.property = "direccion";
        model_internal::_CodResultValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForCodResult);
        model_internal::_CodResultValidator.required = true;
        model_internal::_CodResultValidator.requiredFieldError = "CodResult is required";
        //model_internal::_CodResultValidator.source = model_internal::_instance;
        //model_internal::_CodResultValidator.property = "CodResult";
        model_internal::_MsgResultValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForMsgResult);
        model_internal::_MsgResultValidator.required = true;
        model_internal::_MsgResultValidator.requiredFieldError = "MsgResult is required";
        //model_internal::_MsgResultValidator.source = model_internal::_instance;
        //model_internal::_MsgResultValidator.property = "MsgResult";
        model_internal::_listaPrevisionValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForListaPrevision);
        model_internal::_listaPrevisionValidator.required = true;
        model_internal::_listaPrevisionValidator.requiredFieldError = "listaPrevision is required";
        //model_internal::_listaPrevisionValidator.source = model_internal::_instance;
        //model_internal::_listaPrevisionValidator.property = "listaPrevision";
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity ConfiguracionInicial");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity ConfiguracionInicial");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of ConfiguracionInicial");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity ConfiguracionInicial");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity ConfiguracionInicial");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity ConfiguracionInicial");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isNombreAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isConfigDataAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDireccionAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCodResultAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMsgResultAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isListaPrevisionAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */
    public function invalidateDependentOnNombre():void
    {
        if (model_internal::_nombreIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfNombre = null;
            model_internal::calculateNombreIsValid();
        }
    }
    public function invalidateDependentOnConfigData():void
    {
        if (model_internal::_configDataIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfConfigData = null;
            model_internal::calculateConfigDataIsValid();
        }
    }
    public function invalidateDependentOnDireccion():void
    {
        if (model_internal::_direccionIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfDireccion = null;
            model_internal::calculateDireccionIsValid();
        }
    }
    public function invalidateDependentOnCodResult():void
    {
        if (model_internal::_CodResultIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfCodResult = null;
            model_internal::calculateCodResultIsValid();
        }
    }
    public function invalidateDependentOnMsgResult():void
    {
        if (model_internal::_MsgResultIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfMsgResult = null;
            model_internal::calculateMsgResultIsValid();
        }
    }
    public function invalidateDependentOnListaPrevision():void
    {
        if (model_internal::_listaPrevisionIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfListaPrevision = null;
            model_internal::calculateListaPrevisionIsValid();
        }
    }

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get nombreStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get nombreValidator() : StyleValidator
    {
        return model_internal::_nombreValidator;
    }

    model_internal function set _nombreIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_nombreIsValid;         
        if (oldValue !== value)
        {
            model_internal::_nombreIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "nombreIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get nombreIsValid():Boolean
    {
        if (!model_internal::_nombreIsValidCacheInitialized)
        {
            model_internal::calculateNombreIsValid();
        }

        return model_internal::_nombreIsValid;
    }

    model_internal function calculateNombreIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_nombreValidator.validate(model_internal::_instance.nombre)
        model_internal::_nombreIsValid_der = (valRes.results == null);
        model_internal::_nombreIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::nombreValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::nombreValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get nombreValidationFailureMessages():Array
    {
        if (model_internal::_nombreValidationFailureMessages == null)
            model_internal::calculateNombreIsValid();

        return _nombreValidationFailureMessages;
    }

    model_internal function set nombreValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_nombreValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_nombreValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "nombreValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get configDataStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get configDataValidator() : StyleValidator
    {
        return model_internal::_configDataValidator;
    }

    model_internal function set _configDataIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_configDataIsValid;         
        if (oldValue !== value)
        {
            model_internal::_configDataIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "configDataIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get configDataIsValid():Boolean
    {
        if (!model_internal::_configDataIsValidCacheInitialized)
        {
            model_internal::calculateConfigDataIsValid();
        }

        return model_internal::_configDataIsValid;
    }

    model_internal function calculateConfigDataIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_configDataValidator.validate(model_internal::_instance.configData)
        model_internal::_configDataIsValid_der = (valRes.results == null);
        model_internal::_configDataIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::configDataValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::configDataValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get configDataValidationFailureMessages():Array
    {
        if (model_internal::_configDataValidationFailureMessages == null)
            model_internal::calculateConfigDataIsValid();

        return _configDataValidationFailureMessages;
    }

    model_internal function set configDataValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_configDataValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_configDataValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "configDataValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get direccionStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get direccionValidator() : StyleValidator
    {
        return model_internal::_direccionValidator;
    }

    model_internal function set _direccionIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_direccionIsValid;         
        if (oldValue !== value)
        {
            model_internal::_direccionIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "direccionIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get direccionIsValid():Boolean
    {
        if (!model_internal::_direccionIsValidCacheInitialized)
        {
            model_internal::calculateDireccionIsValid();
        }

        return model_internal::_direccionIsValid;
    }

    model_internal function calculateDireccionIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_direccionValidator.validate(model_internal::_instance.direccion)
        model_internal::_direccionIsValid_der = (valRes.results == null);
        model_internal::_direccionIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::direccionValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::direccionValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get direccionValidationFailureMessages():Array
    {
        if (model_internal::_direccionValidationFailureMessages == null)
            model_internal::calculateDireccionIsValid();

        return _direccionValidationFailureMessages;
    }

    model_internal function set direccionValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_direccionValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_direccionValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "direccionValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get CodResultStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get CodResultValidator() : StyleValidator
    {
        return model_internal::_CodResultValidator;
    }

    model_internal function set _CodResultIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_CodResultIsValid;         
        if (oldValue !== value)
        {
            model_internal::_CodResultIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CodResultIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get CodResultIsValid():Boolean
    {
        if (!model_internal::_CodResultIsValidCacheInitialized)
        {
            model_internal::calculateCodResultIsValid();
        }

        return model_internal::_CodResultIsValid;
    }

    model_internal function calculateCodResultIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_CodResultValidator.validate(model_internal::_instance.CodResult)
        model_internal::_CodResultIsValid_der = (valRes.results == null);
        model_internal::_CodResultIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::CodResultValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::CodResultValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get CodResultValidationFailureMessages():Array
    {
        if (model_internal::_CodResultValidationFailureMessages == null)
            model_internal::calculateCodResultIsValid();

        return _CodResultValidationFailureMessages;
    }

    model_internal function set CodResultValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_CodResultValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_CodResultValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CodResultValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get MsgResultStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get MsgResultValidator() : StyleValidator
    {
        return model_internal::_MsgResultValidator;
    }

    model_internal function set _MsgResultIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_MsgResultIsValid;         
        if (oldValue !== value)
        {
            model_internal::_MsgResultIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MsgResultIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get MsgResultIsValid():Boolean
    {
        if (!model_internal::_MsgResultIsValidCacheInitialized)
        {
            model_internal::calculateMsgResultIsValid();
        }

        return model_internal::_MsgResultIsValid;
    }

    model_internal function calculateMsgResultIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_MsgResultValidator.validate(model_internal::_instance.MsgResult)
        model_internal::_MsgResultIsValid_der = (valRes.results == null);
        model_internal::_MsgResultIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::MsgResultValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::MsgResultValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get MsgResultValidationFailureMessages():Array
    {
        if (model_internal::_MsgResultValidationFailureMessages == null)
            model_internal::calculateMsgResultIsValid();

        return _MsgResultValidationFailureMessages;
    }

    model_internal function set MsgResultValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_MsgResultValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_MsgResultValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MsgResultValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get listaPrevisionStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get listaPrevisionValidator() : StyleValidator
    {
        return model_internal::_listaPrevisionValidator;
    }

    model_internal function set _listaPrevisionIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_listaPrevisionIsValid;         
        if (oldValue !== value)
        {
            model_internal::_listaPrevisionIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "listaPrevisionIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get listaPrevisionIsValid():Boolean
    {
        if (!model_internal::_listaPrevisionIsValidCacheInitialized)
        {
            model_internal::calculateListaPrevisionIsValid();
        }

        return model_internal::_listaPrevisionIsValid;
    }

    model_internal function calculateListaPrevisionIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_listaPrevisionValidator.validate(model_internal::_instance.listaPrevision)
        model_internal::_listaPrevisionIsValid_der = (valRes.results == null);
        model_internal::_listaPrevisionIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::listaPrevisionValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::listaPrevisionValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get listaPrevisionValidationFailureMessages():Array
    {
        if (model_internal::_listaPrevisionValidationFailureMessages == null)
            model_internal::calculateListaPrevisionIsValid();

        return _listaPrevisionValidationFailureMessages;
    }

    model_internal function set listaPrevisionValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_listaPrevisionValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_listaPrevisionValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "listaPrevisionValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            case("nombre"):
            {
                return nombreValidationFailureMessages;
            }
            case("configData"):
            {
                return configDataValidationFailureMessages;
            }
            case("direccion"):
            {
                return direccionValidationFailureMessages;
            }
            case("CodResult"):
            {
                return CodResultValidationFailureMessages;
            }
            case("MsgResult"):
            {
                return MsgResultValidationFailureMessages;
            }
            case("listaPrevision"):
            {
                return listaPrevisionValidationFailureMessages;
            }
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
