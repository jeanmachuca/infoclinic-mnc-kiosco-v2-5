/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - Prevision.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.util.FiberUtils;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.Event;
import flash.events.EventDispatcher;
import mx.binding.utils.ChangeWatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import mx.validators.ValidationResult;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_Prevision extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _PrevisionEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_codigoPrevision : String;
    private var _internal_tipoPrevision : String;
    private var _internal_nombrePrevision : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_Prevision()
    {
        _model = new _PrevisionEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "codigoPrevision", model_internal::setterListenerCodigoPrevision));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "tipoPrevision", model_internal::setterListenerTipoPrevision));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "nombrePrevision", model_internal::setterListenerNombrePrevision));

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get codigoPrevision() : String
    {
        return _internal_codigoPrevision;
    }

    [Bindable(event="propertyChange")]
    public function get tipoPrevision() : String
    {
        return _internal_tipoPrevision;
    }

    [Bindable(event="propertyChange")]
    public function get nombrePrevision() : String
    {
        return _internal_nombrePrevision;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set codigoPrevision(value:String) : void
    {
        var oldValue:String = _internal_codigoPrevision;
        if (oldValue !== value)
        {
            _internal_codigoPrevision = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "codigoPrevision", oldValue, _internal_codigoPrevision));
        }
    }

    public function set tipoPrevision(value:String) : void
    {
        var oldValue:String = _internal_tipoPrevision;
        if (oldValue !== value)
        {
            _internal_tipoPrevision = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "tipoPrevision", oldValue, _internal_tipoPrevision));
        }
    }

    public function set nombrePrevision(value:String) : void
    {
        var oldValue:String = _internal_nombrePrevision;
        if (oldValue !== value)
        {
            _internal_nombrePrevision = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "nombrePrevision", oldValue, _internal_nombrePrevision));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */

    model_internal function setterListenerCodigoPrevision(value:flash.events.Event):void
    {
        _model.invalidateDependentOnCodigoPrevision();
    }

    model_internal function setterListenerTipoPrevision(value:flash.events.Event):void
    {
        _model.invalidateDependentOnTipoPrevision();
    }

    model_internal function setterListenerNombrePrevision(value:flash.events.Event):void
    {
        _model.invalidateDependentOnNombrePrevision();
    }


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;
        if (!_model.codigoPrevisionIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_codigoPrevisionValidationFailureMessages);
        }
        if (!_model.tipoPrevisionIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_tipoPrevisionValidationFailureMessages);
        }
        if (!_model.nombrePrevisionIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_nombrePrevisionValidationFailureMessages);
        }

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _PrevisionEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _PrevisionEntityMetadata) : void
    {
        var oldValue : _PrevisionEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }

    model_internal var _doValidationCacheOfCodigoPrevision : Array = null;
    model_internal var _doValidationLastValOfCodigoPrevision : String;

    model_internal function _doValidationForCodigoPrevision(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfCodigoPrevision != null && model_internal::_doValidationLastValOfCodigoPrevision == value)
           return model_internal::_doValidationCacheOfCodigoPrevision ;

        _model.model_internal::_codigoPrevisionIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isCodigoPrevisionAvailable && _internal_codigoPrevision == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "codigoPrevision is required"));
        }

        model_internal::_doValidationCacheOfCodigoPrevision = validationFailures;
        model_internal::_doValidationLastValOfCodigoPrevision = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfTipoPrevision : Array = null;
    model_internal var _doValidationLastValOfTipoPrevision : String;

    model_internal function _doValidationForTipoPrevision(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfTipoPrevision != null && model_internal::_doValidationLastValOfTipoPrevision == value)
           return model_internal::_doValidationCacheOfTipoPrevision ;

        _model.model_internal::_tipoPrevisionIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isTipoPrevisionAvailable && _internal_tipoPrevision == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "tipoPrevision is required"));
        }

        model_internal::_doValidationCacheOfTipoPrevision = validationFailures;
        model_internal::_doValidationLastValOfTipoPrevision = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfNombrePrevision : Array = null;
    model_internal var _doValidationLastValOfNombrePrevision : String;

    model_internal function _doValidationForNombrePrevision(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfNombrePrevision != null && model_internal::_doValidationLastValOfNombrePrevision == value)
           return model_internal::_doValidationCacheOfNombrePrevision ;

        _model.model_internal::_nombrePrevisionIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isNombrePrevisionAvailable && _internal_nombrePrevision == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "nombrePrevision is required"));
        }

        model_internal::_doValidationCacheOfNombrePrevision = validationFailures;
        model_internal::_doValidationLastValOfNombrePrevision = value;

        return validationFailures;
    }
    

}

}
