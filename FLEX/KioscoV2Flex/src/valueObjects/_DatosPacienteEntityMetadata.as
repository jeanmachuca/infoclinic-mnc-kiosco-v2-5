
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.styles.StyleValidator;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import mx.events.ValidationResultEvent;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _DatosPacienteEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("CodResult", "MsgResult", "datosCertificacionAfiliacion", "datosPersonales", "estaEnrolado", "nombre", "identificacion", "genero", "email", "fonoCelular", "apellidoMaterno", "apellidoPaterno", "fonoFijo", "fechaNacimiento");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array("CodResult", "MsgResult", "datosCertificacionAfiliacion", "datosPersonales", "estaEnrolado", "nombre", "identificacion", "genero", "email", "fonoCelular", "apellidoMaterno", "apellidoPaterno", "fonoFijo", "fechaNacimiento");
    model_internal static var allAlwaysAvailableProperties:Array = new Array("CodResult", "MsgResult", "datosCertificacionAfiliacion", "datosPersonales", "estaEnrolado", "nombre", "identificacion", "genero", "email", "fonoCelular", "apellidoMaterno", "apellidoPaterno", "fonoFijo", "fechaNacimiento");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("CodResult", "MsgResult", "datosCertificacionAfiliacion", "datosPersonales", "estaEnrolado", "nombre", "identificacion", "genero", "email", "fonoCelular", "apellidoMaterno", "apellidoPaterno", "fonoFijo", "fechaNacimiento");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("CodResult", "MsgResult", "datosCertificacionAfiliacion", "datosPersonales", "estaEnrolado", "nombre", "identificacion", "genero", "email", "fonoCelular", "apellidoMaterno", "apellidoPaterno", "fonoFijo", "fechaNacimiento");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array();
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "DatosPaciente";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;

    
    model_internal var _CodResultIsValid:Boolean;
    model_internal var _CodResultValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _CodResultIsValidCacheInitialized:Boolean = false;
    model_internal var _CodResultValidationFailureMessages:Array;
    
    model_internal var _MsgResultIsValid:Boolean;
    model_internal var _MsgResultValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _MsgResultIsValidCacheInitialized:Boolean = false;
    model_internal var _MsgResultValidationFailureMessages:Array;
    
    model_internal var _datosCertificacionAfiliacionIsValid:Boolean;
    model_internal var _datosCertificacionAfiliacionValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _datosCertificacionAfiliacionIsValidCacheInitialized:Boolean = false;
    model_internal var _datosCertificacionAfiliacionValidationFailureMessages:Array;
    
    model_internal var _datosPersonalesIsValid:Boolean;
    model_internal var _datosPersonalesValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _datosPersonalesIsValidCacheInitialized:Boolean = false;
    model_internal var _datosPersonalesValidationFailureMessages:Array;
    
    model_internal var _estaEnroladoIsValid:Boolean;
    model_internal var _estaEnroladoValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _estaEnroladoIsValidCacheInitialized:Boolean = false;
    model_internal var _estaEnroladoValidationFailureMessages:Array;
    
    model_internal var _nombreIsValid:Boolean;
    model_internal var _nombreValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _nombreIsValidCacheInitialized:Boolean = false;
    model_internal var _nombreValidationFailureMessages:Array;
    
    model_internal var _identificacionIsValid:Boolean;
    model_internal var _identificacionValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _identificacionIsValidCacheInitialized:Boolean = false;
    model_internal var _identificacionValidationFailureMessages:Array;
    
    model_internal var _generoIsValid:Boolean;
    model_internal var _generoValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _generoIsValidCacheInitialized:Boolean = false;
    model_internal var _generoValidationFailureMessages:Array;
    
    model_internal var _emailIsValid:Boolean;
    model_internal var _emailValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _emailIsValidCacheInitialized:Boolean = false;
    model_internal var _emailValidationFailureMessages:Array;
    
    model_internal var _fonoCelularIsValid:Boolean;
    model_internal var _fonoCelularValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _fonoCelularIsValidCacheInitialized:Boolean = false;
    model_internal var _fonoCelularValidationFailureMessages:Array;
    
    model_internal var _apellidoMaternoIsValid:Boolean;
    model_internal var _apellidoMaternoValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _apellidoMaternoIsValidCacheInitialized:Boolean = false;
    model_internal var _apellidoMaternoValidationFailureMessages:Array;
    
    model_internal var _apellidoPaternoIsValid:Boolean;
    model_internal var _apellidoPaternoValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _apellidoPaternoIsValidCacheInitialized:Boolean = false;
    model_internal var _apellidoPaternoValidationFailureMessages:Array;
    
    model_internal var _fonoFijoIsValid:Boolean;
    model_internal var _fonoFijoValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _fonoFijoIsValidCacheInitialized:Boolean = false;
    model_internal var _fonoFijoValidationFailureMessages:Array;
    
    model_internal var _fechaNacimientoIsValid:Boolean;
    model_internal var _fechaNacimientoValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _fechaNacimientoIsValidCacheInitialized:Boolean = false;
    model_internal var _fechaNacimientoValidationFailureMessages:Array;

    model_internal var _instance:_Super_DatosPaciente;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _DatosPacienteEntityMetadata(value : _Super_DatosPaciente)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["CodResult"] = new Array();
            model_internal::dependentsOnMap["MsgResult"] = new Array();
            model_internal::dependentsOnMap["datosCertificacionAfiliacion"] = new Array();
            model_internal::dependentsOnMap["datosPersonales"] = new Array();
            model_internal::dependentsOnMap["estaEnrolado"] = new Array();
            model_internal::dependentsOnMap["nombre"] = new Array();
            model_internal::dependentsOnMap["identificacion"] = new Array();
            model_internal::dependentsOnMap["genero"] = new Array();
            model_internal::dependentsOnMap["email"] = new Array();
            model_internal::dependentsOnMap["fonoCelular"] = new Array();
            model_internal::dependentsOnMap["apellidoMaterno"] = new Array();
            model_internal::dependentsOnMap["apellidoPaterno"] = new Array();
            model_internal::dependentsOnMap["fonoFijo"] = new Array();
            model_internal::dependentsOnMap["fechaNacimiento"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["CodResult"] = "String";
        model_internal::propertyTypeMap["MsgResult"] = "String";
        model_internal::propertyTypeMap["datosCertificacionAfiliacion"] = "Object";
        model_internal::propertyTypeMap["datosPersonales"] = "Object";
        model_internal::propertyTypeMap["estaEnrolado"] = "String";
        model_internal::propertyTypeMap["nombre"] = "String";
        model_internal::propertyTypeMap["identificacion"] = "String";
        model_internal::propertyTypeMap["genero"] = "String";
        model_internal::propertyTypeMap["email"] = "String";
        model_internal::propertyTypeMap["fonoCelular"] = "String";
        model_internal::propertyTypeMap["apellidoMaterno"] = "String";
        model_internal::propertyTypeMap["apellidoPaterno"] = "String";
        model_internal::propertyTypeMap["fonoFijo"] = "String";
        model_internal::propertyTypeMap["fechaNacimiento"] = "String";

        model_internal::_instance = value;
        model_internal::_CodResultValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForCodResult);
        model_internal::_CodResultValidator.required = true;
        model_internal::_CodResultValidator.requiredFieldError = "CodResult is required";
        //model_internal::_CodResultValidator.source = model_internal::_instance;
        //model_internal::_CodResultValidator.property = "CodResult";
        model_internal::_MsgResultValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForMsgResult);
        model_internal::_MsgResultValidator.required = true;
        model_internal::_MsgResultValidator.requiredFieldError = "MsgResult is required";
        //model_internal::_MsgResultValidator.source = model_internal::_instance;
        //model_internal::_MsgResultValidator.property = "MsgResult";
        model_internal::_datosCertificacionAfiliacionValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForDatosCertificacionAfiliacion);
        model_internal::_datosCertificacionAfiliacionValidator.required = true;
        model_internal::_datosCertificacionAfiliacionValidator.requiredFieldError = "datosCertificacionAfiliacion is required";
        //model_internal::_datosCertificacionAfiliacionValidator.source = model_internal::_instance;
        //model_internal::_datosCertificacionAfiliacionValidator.property = "datosCertificacionAfiliacion";
        model_internal::_datosPersonalesValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForDatosPersonales);
        model_internal::_datosPersonalesValidator.required = true;
        model_internal::_datosPersonalesValidator.requiredFieldError = "datosPersonales is required";
        //model_internal::_datosPersonalesValidator.source = model_internal::_instance;
        //model_internal::_datosPersonalesValidator.property = "datosPersonales";
        model_internal::_estaEnroladoValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForEstaEnrolado);
        model_internal::_estaEnroladoValidator.required = true;
        model_internal::_estaEnroladoValidator.requiredFieldError = "estaEnrolado is required";
        //model_internal::_estaEnroladoValidator.source = model_internal::_instance;
        //model_internal::_estaEnroladoValidator.property = "estaEnrolado";
        model_internal::_nombreValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForNombre);
        model_internal::_nombreValidator.required = true;
        model_internal::_nombreValidator.requiredFieldError = "nombre is required";
        //model_internal::_nombreValidator.source = model_internal::_instance;
        //model_internal::_nombreValidator.property = "nombre";
        model_internal::_identificacionValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForIdentificacion);
        model_internal::_identificacionValidator.required = true;
        model_internal::_identificacionValidator.requiredFieldError = "identificacion is required";
        //model_internal::_identificacionValidator.source = model_internal::_instance;
        //model_internal::_identificacionValidator.property = "identificacion";
        model_internal::_generoValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForGenero);
        model_internal::_generoValidator.required = true;
        model_internal::_generoValidator.requiredFieldError = "genero is required";
        //model_internal::_generoValidator.source = model_internal::_instance;
        //model_internal::_generoValidator.property = "genero";
        model_internal::_emailValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForEmail);
        model_internal::_emailValidator.required = true;
        model_internal::_emailValidator.requiredFieldError = "email is required";
        //model_internal::_emailValidator.source = model_internal::_instance;
        //model_internal::_emailValidator.property = "email";
        model_internal::_fonoCelularValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForFonoCelular);
        model_internal::_fonoCelularValidator.required = true;
        model_internal::_fonoCelularValidator.requiredFieldError = "fonoCelular is required";
        //model_internal::_fonoCelularValidator.source = model_internal::_instance;
        //model_internal::_fonoCelularValidator.property = "fonoCelular";
        model_internal::_apellidoMaternoValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForApellidoMaterno);
        model_internal::_apellidoMaternoValidator.required = true;
        model_internal::_apellidoMaternoValidator.requiredFieldError = "apellidoMaterno is required";
        //model_internal::_apellidoMaternoValidator.source = model_internal::_instance;
        //model_internal::_apellidoMaternoValidator.property = "apellidoMaterno";
        model_internal::_apellidoPaternoValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForApellidoPaterno);
        model_internal::_apellidoPaternoValidator.required = true;
        model_internal::_apellidoPaternoValidator.requiredFieldError = "apellidoPaterno is required";
        //model_internal::_apellidoPaternoValidator.source = model_internal::_instance;
        //model_internal::_apellidoPaternoValidator.property = "apellidoPaterno";
        model_internal::_fonoFijoValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForFonoFijo);
        model_internal::_fonoFijoValidator.required = true;
        model_internal::_fonoFijoValidator.requiredFieldError = "fonoFijo is required";
        //model_internal::_fonoFijoValidator.source = model_internal::_instance;
        //model_internal::_fonoFijoValidator.property = "fonoFijo";
        model_internal::_fechaNacimientoValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForFechaNacimiento);
        model_internal::_fechaNacimientoValidator.required = true;
        model_internal::_fechaNacimientoValidator.requiredFieldError = "fechaNacimiento is required";
        //model_internal::_fechaNacimientoValidator.source = model_internal::_instance;
        //model_internal::_fechaNacimientoValidator.property = "fechaNacimiento";
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity DatosPaciente");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity DatosPaciente");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of DatosPaciente");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity DatosPaciente");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity DatosPaciente");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity DatosPaciente");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isCodResultAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMsgResultAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDatosCertificacionAfiliacionAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDatosPersonalesAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEstaEnroladoAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isNombreAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIdentificacionAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isGeneroAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEmailAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFonoCelularAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isApellidoMaternoAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isApellidoPaternoAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFonoFijoAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFechaNacimientoAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */
    public function invalidateDependentOnCodResult():void
    {
        if (model_internal::_CodResultIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfCodResult = null;
            model_internal::calculateCodResultIsValid();
        }
    }
    public function invalidateDependentOnMsgResult():void
    {
        if (model_internal::_MsgResultIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfMsgResult = null;
            model_internal::calculateMsgResultIsValid();
        }
    }
    public function invalidateDependentOnDatosCertificacionAfiliacion():void
    {
        if (model_internal::_datosCertificacionAfiliacionIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfDatosCertificacionAfiliacion = null;
            model_internal::calculateDatosCertificacionAfiliacionIsValid();
        }
    }
    public function invalidateDependentOnDatosPersonales():void
    {
        if (model_internal::_datosPersonalesIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfDatosPersonales = null;
            model_internal::calculateDatosPersonalesIsValid();
        }
    }
    public function invalidateDependentOnEstaEnrolado():void
    {
        if (model_internal::_estaEnroladoIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfEstaEnrolado = null;
            model_internal::calculateEstaEnroladoIsValid();
        }
    }
    public function invalidateDependentOnNombre():void
    {
        if (model_internal::_nombreIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfNombre = null;
            model_internal::calculateNombreIsValid();
        }
    }
    public function invalidateDependentOnIdentificacion():void
    {
        if (model_internal::_identificacionIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfIdentificacion = null;
            model_internal::calculateIdentificacionIsValid();
        }
    }
    public function invalidateDependentOnGenero():void
    {
        if (model_internal::_generoIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfGenero = null;
            model_internal::calculateGeneroIsValid();
        }
    }
    public function invalidateDependentOnEmail():void
    {
        if (model_internal::_emailIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfEmail = null;
            model_internal::calculateEmailIsValid();
        }
    }
    public function invalidateDependentOnFonoCelular():void
    {
        if (model_internal::_fonoCelularIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfFonoCelular = null;
            model_internal::calculateFonoCelularIsValid();
        }
    }
    public function invalidateDependentOnApellidoMaterno():void
    {
        if (model_internal::_apellidoMaternoIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfApellidoMaterno = null;
            model_internal::calculateApellidoMaternoIsValid();
        }
    }
    public function invalidateDependentOnApellidoPaterno():void
    {
        if (model_internal::_apellidoPaternoIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfApellidoPaterno = null;
            model_internal::calculateApellidoPaternoIsValid();
        }
    }
    public function invalidateDependentOnFonoFijo():void
    {
        if (model_internal::_fonoFijoIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfFonoFijo = null;
            model_internal::calculateFonoFijoIsValid();
        }
    }
    public function invalidateDependentOnFechaNacimiento():void
    {
        if (model_internal::_fechaNacimientoIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfFechaNacimiento = null;
            model_internal::calculateFechaNacimientoIsValid();
        }
    }

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get CodResultStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get CodResultValidator() : StyleValidator
    {
        return model_internal::_CodResultValidator;
    }

    model_internal function set _CodResultIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_CodResultIsValid;         
        if (oldValue !== value)
        {
            model_internal::_CodResultIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CodResultIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get CodResultIsValid():Boolean
    {
        if (!model_internal::_CodResultIsValidCacheInitialized)
        {
            model_internal::calculateCodResultIsValid();
        }

        return model_internal::_CodResultIsValid;
    }

    model_internal function calculateCodResultIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_CodResultValidator.validate(model_internal::_instance.CodResult)
        model_internal::_CodResultIsValid_der = (valRes.results == null);
        model_internal::_CodResultIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::CodResultValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::CodResultValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get CodResultValidationFailureMessages():Array
    {
        if (model_internal::_CodResultValidationFailureMessages == null)
            model_internal::calculateCodResultIsValid();

        return _CodResultValidationFailureMessages;
    }

    model_internal function set CodResultValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_CodResultValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_CodResultValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CodResultValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get MsgResultStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get MsgResultValidator() : StyleValidator
    {
        return model_internal::_MsgResultValidator;
    }

    model_internal function set _MsgResultIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_MsgResultIsValid;         
        if (oldValue !== value)
        {
            model_internal::_MsgResultIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MsgResultIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get MsgResultIsValid():Boolean
    {
        if (!model_internal::_MsgResultIsValidCacheInitialized)
        {
            model_internal::calculateMsgResultIsValid();
        }

        return model_internal::_MsgResultIsValid;
    }

    model_internal function calculateMsgResultIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_MsgResultValidator.validate(model_internal::_instance.MsgResult)
        model_internal::_MsgResultIsValid_der = (valRes.results == null);
        model_internal::_MsgResultIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::MsgResultValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::MsgResultValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get MsgResultValidationFailureMessages():Array
    {
        if (model_internal::_MsgResultValidationFailureMessages == null)
            model_internal::calculateMsgResultIsValid();

        return _MsgResultValidationFailureMessages;
    }

    model_internal function set MsgResultValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_MsgResultValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_MsgResultValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MsgResultValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get datosCertificacionAfiliacionStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get datosCertificacionAfiliacionValidator() : StyleValidator
    {
        return model_internal::_datosCertificacionAfiliacionValidator;
    }

    model_internal function set _datosCertificacionAfiliacionIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_datosCertificacionAfiliacionIsValid;         
        if (oldValue !== value)
        {
            model_internal::_datosCertificacionAfiliacionIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "datosCertificacionAfiliacionIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get datosCertificacionAfiliacionIsValid():Boolean
    {
        if (!model_internal::_datosCertificacionAfiliacionIsValidCacheInitialized)
        {
            model_internal::calculateDatosCertificacionAfiliacionIsValid();
        }

        return model_internal::_datosCertificacionAfiliacionIsValid;
    }

    model_internal function calculateDatosCertificacionAfiliacionIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_datosCertificacionAfiliacionValidator.validate(model_internal::_instance.datosCertificacionAfiliacion)
        model_internal::_datosCertificacionAfiliacionIsValid_der = (valRes.results == null);
        model_internal::_datosCertificacionAfiliacionIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::datosCertificacionAfiliacionValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::datosCertificacionAfiliacionValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get datosCertificacionAfiliacionValidationFailureMessages():Array
    {
        if (model_internal::_datosCertificacionAfiliacionValidationFailureMessages == null)
            model_internal::calculateDatosCertificacionAfiliacionIsValid();

        return _datosCertificacionAfiliacionValidationFailureMessages;
    }

    model_internal function set datosCertificacionAfiliacionValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_datosCertificacionAfiliacionValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_datosCertificacionAfiliacionValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "datosCertificacionAfiliacionValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get datosPersonalesStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get datosPersonalesValidator() : StyleValidator
    {
        return model_internal::_datosPersonalesValidator;
    }

    model_internal function set _datosPersonalesIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_datosPersonalesIsValid;         
        if (oldValue !== value)
        {
            model_internal::_datosPersonalesIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "datosPersonalesIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get datosPersonalesIsValid():Boolean
    {
        if (!model_internal::_datosPersonalesIsValidCacheInitialized)
        {
            model_internal::calculateDatosPersonalesIsValid();
        }

        return model_internal::_datosPersonalesIsValid;
    }

    model_internal function calculateDatosPersonalesIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_datosPersonalesValidator.validate(model_internal::_instance.datosPersonales)
        model_internal::_datosPersonalesIsValid_der = (valRes.results == null);
        model_internal::_datosPersonalesIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::datosPersonalesValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::datosPersonalesValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get datosPersonalesValidationFailureMessages():Array
    {
        if (model_internal::_datosPersonalesValidationFailureMessages == null)
            model_internal::calculateDatosPersonalesIsValid();

        return _datosPersonalesValidationFailureMessages;
    }

    model_internal function set datosPersonalesValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_datosPersonalesValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_datosPersonalesValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "datosPersonalesValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get estaEnroladoStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get estaEnroladoValidator() : StyleValidator
    {
        return model_internal::_estaEnroladoValidator;
    }

    model_internal function set _estaEnroladoIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_estaEnroladoIsValid;         
        if (oldValue !== value)
        {
            model_internal::_estaEnroladoIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "estaEnroladoIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get estaEnroladoIsValid():Boolean
    {
        if (!model_internal::_estaEnroladoIsValidCacheInitialized)
        {
            model_internal::calculateEstaEnroladoIsValid();
        }

        return model_internal::_estaEnroladoIsValid;
    }

    model_internal function calculateEstaEnroladoIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_estaEnroladoValidator.validate(model_internal::_instance.estaEnrolado)
        model_internal::_estaEnroladoIsValid_der = (valRes.results == null);
        model_internal::_estaEnroladoIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::estaEnroladoValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::estaEnroladoValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get estaEnroladoValidationFailureMessages():Array
    {
        if (model_internal::_estaEnroladoValidationFailureMessages == null)
            model_internal::calculateEstaEnroladoIsValid();

        return _estaEnroladoValidationFailureMessages;
    }

    model_internal function set estaEnroladoValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_estaEnroladoValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_estaEnroladoValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "estaEnroladoValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get nombreStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get nombreValidator() : StyleValidator
    {
        return model_internal::_nombreValidator;
    }

    model_internal function set _nombreIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_nombreIsValid;         
        if (oldValue !== value)
        {
            model_internal::_nombreIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "nombreIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get nombreIsValid():Boolean
    {
        if (!model_internal::_nombreIsValidCacheInitialized)
        {
            model_internal::calculateNombreIsValid();
        }

        return model_internal::_nombreIsValid;
    }

    model_internal function calculateNombreIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_nombreValidator.validate(model_internal::_instance.nombre)
        model_internal::_nombreIsValid_der = (valRes.results == null);
        model_internal::_nombreIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::nombreValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::nombreValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get nombreValidationFailureMessages():Array
    {
        if (model_internal::_nombreValidationFailureMessages == null)
            model_internal::calculateNombreIsValid();

        return _nombreValidationFailureMessages;
    }

    model_internal function set nombreValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_nombreValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_nombreValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "nombreValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get identificacionStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get identificacionValidator() : StyleValidator
    {
        return model_internal::_identificacionValidator;
    }

    model_internal function set _identificacionIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_identificacionIsValid;         
        if (oldValue !== value)
        {
            model_internal::_identificacionIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "identificacionIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get identificacionIsValid():Boolean
    {
        if (!model_internal::_identificacionIsValidCacheInitialized)
        {
            model_internal::calculateIdentificacionIsValid();
        }

        return model_internal::_identificacionIsValid;
    }

    model_internal function calculateIdentificacionIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_identificacionValidator.validate(model_internal::_instance.identificacion)
        model_internal::_identificacionIsValid_der = (valRes.results == null);
        model_internal::_identificacionIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::identificacionValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::identificacionValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get identificacionValidationFailureMessages():Array
    {
        if (model_internal::_identificacionValidationFailureMessages == null)
            model_internal::calculateIdentificacionIsValid();

        return _identificacionValidationFailureMessages;
    }

    model_internal function set identificacionValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_identificacionValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_identificacionValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "identificacionValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get generoStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get generoValidator() : StyleValidator
    {
        return model_internal::_generoValidator;
    }

    model_internal function set _generoIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_generoIsValid;         
        if (oldValue !== value)
        {
            model_internal::_generoIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "generoIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get generoIsValid():Boolean
    {
        if (!model_internal::_generoIsValidCacheInitialized)
        {
            model_internal::calculateGeneroIsValid();
        }

        return model_internal::_generoIsValid;
    }

    model_internal function calculateGeneroIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_generoValidator.validate(model_internal::_instance.genero)
        model_internal::_generoIsValid_der = (valRes.results == null);
        model_internal::_generoIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::generoValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::generoValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get generoValidationFailureMessages():Array
    {
        if (model_internal::_generoValidationFailureMessages == null)
            model_internal::calculateGeneroIsValid();

        return _generoValidationFailureMessages;
    }

    model_internal function set generoValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_generoValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_generoValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "generoValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get emailStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get emailValidator() : StyleValidator
    {
        return model_internal::_emailValidator;
    }

    model_internal function set _emailIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_emailIsValid;         
        if (oldValue !== value)
        {
            model_internal::_emailIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "emailIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get emailIsValid():Boolean
    {
        if (!model_internal::_emailIsValidCacheInitialized)
        {
            model_internal::calculateEmailIsValid();
        }

        return model_internal::_emailIsValid;
    }

    model_internal function calculateEmailIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_emailValidator.validate(model_internal::_instance.email)
        model_internal::_emailIsValid_der = (valRes.results == null);
        model_internal::_emailIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::emailValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::emailValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get emailValidationFailureMessages():Array
    {
        if (model_internal::_emailValidationFailureMessages == null)
            model_internal::calculateEmailIsValid();

        return _emailValidationFailureMessages;
    }

    model_internal function set emailValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_emailValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_emailValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "emailValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get fonoCelularStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get fonoCelularValidator() : StyleValidator
    {
        return model_internal::_fonoCelularValidator;
    }

    model_internal function set _fonoCelularIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_fonoCelularIsValid;         
        if (oldValue !== value)
        {
            model_internal::_fonoCelularIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "fonoCelularIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get fonoCelularIsValid():Boolean
    {
        if (!model_internal::_fonoCelularIsValidCacheInitialized)
        {
            model_internal::calculateFonoCelularIsValid();
        }

        return model_internal::_fonoCelularIsValid;
    }

    model_internal function calculateFonoCelularIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_fonoCelularValidator.validate(model_internal::_instance.fonoCelular)
        model_internal::_fonoCelularIsValid_der = (valRes.results == null);
        model_internal::_fonoCelularIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::fonoCelularValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::fonoCelularValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get fonoCelularValidationFailureMessages():Array
    {
        if (model_internal::_fonoCelularValidationFailureMessages == null)
            model_internal::calculateFonoCelularIsValid();

        return _fonoCelularValidationFailureMessages;
    }

    model_internal function set fonoCelularValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_fonoCelularValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_fonoCelularValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "fonoCelularValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get apellidoMaternoStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get apellidoMaternoValidator() : StyleValidator
    {
        return model_internal::_apellidoMaternoValidator;
    }

    model_internal function set _apellidoMaternoIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_apellidoMaternoIsValid;         
        if (oldValue !== value)
        {
            model_internal::_apellidoMaternoIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "apellidoMaternoIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get apellidoMaternoIsValid():Boolean
    {
        if (!model_internal::_apellidoMaternoIsValidCacheInitialized)
        {
            model_internal::calculateApellidoMaternoIsValid();
        }

        return model_internal::_apellidoMaternoIsValid;
    }

    model_internal function calculateApellidoMaternoIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_apellidoMaternoValidator.validate(model_internal::_instance.apellidoMaterno)
        model_internal::_apellidoMaternoIsValid_der = (valRes.results == null);
        model_internal::_apellidoMaternoIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::apellidoMaternoValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::apellidoMaternoValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get apellidoMaternoValidationFailureMessages():Array
    {
        if (model_internal::_apellidoMaternoValidationFailureMessages == null)
            model_internal::calculateApellidoMaternoIsValid();

        return _apellidoMaternoValidationFailureMessages;
    }

    model_internal function set apellidoMaternoValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_apellidoMaternoValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_apellidoMaternoValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "apellidoMaternoValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get apellidoPaternoStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get apellidoPaternoValidator() : StyleValidator
    {
        return model_internal::_apellidoPaternoValidator;
    }

    model_internal function set _apellidoPaternoIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_apellidoPaternoIsValid;         
        if (oldValue !== value)
        {
            model_internal::_apellidoPaternoIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "apellidoPaternoIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get apellidoPaternoIsValid():Boolean
    {
        if (!model_internal::_apellidoPaternoIsValidCacheInitialized)
        {
            model_internal::calculateApellidoPaternoIsValid();
        }

        return model_internal::_apellidoPaternoIsValid;
    }

    model_internal function calculateApellidoPaternoIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_apellidoPaternoValidator.validate(model_internal::_instance.apellidoPaterno)
        model_internal::_apellidoPaternoIsValid_der = (valRes.results == null);
        model_internal::_apellidoPaternoIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::apellidoPaternoValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::apellidoPaternoValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get apellidoPaternoValidationFailureMessages():Array
    {
        if (model_internal::_apellidoPaternoValidationFailureMessages == null)
            model_internal::calculateApellidoPaternoIsValid();

        return _apellidoPaternoValidationFailureMessages;
    }

    model_internal function set apellidoPaternoValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_apellidoPaternoValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_apellidoPaternoValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "apellidoPaternoValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get fonoFijoStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get fonoFijoValidator() : StyleValidator
    {
        return model_internal::_fonoFijoValidator;
    }

    model_internal function set _fonoFijoIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_fonoFijoIsValid;         
        if (oldValue !== value)
        {
            model_internal::_fonoFijoIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "fonoFijoIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get fonoFijoIsValid():Boolean
    {
        if (!model_internal::_fonoFijoIsValidCacheInitialized)
        {
            model_internal::calculateFonoFijoIsValid();
        }

        return model_internal::_fonoFijoIsValid;
    }

    model_internal function calculateFonoFijoIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_fonoFijoValidator.validate(model_internal::_instance.fonoFijo)
        model_internal::_fonoFijoIsValid_der = (valRes.results == null);
        model_internal::_fonoFijoIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::fonoFijoValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::fonoFijoValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get fonoFijoValidationFailureMessages():Array
    {
        if (model_internal::_fonoFijoValidationFailureMessages == null)
            model_internal::calculateFonoFijoIsValid();

        return _fonoFijoValidationFailureMessages;
    }

    model_internal function set fonoFijoValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_fonoFijoValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_fonoFijoValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "fonoFijoValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get fechaNacimientoStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get fechaNacimientoValidator() : StyleValidator
    {
        return model_internal::_fechaNacimientoValidator;
    }

    model_internal function set _fechaNacimientoIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_fechaNacimientoIsValid;         
        if (oldValue !== value)
        {
            model_internal::_fechaNacimientoIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "fechaNacimientoIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get fechaNacimientoIsValid():Boolean
    {
        if (!model_internal::_fechaNacimientoIsValidCacheInitialized)
        {
            model_internal::calculateFechaNacimientoIsValid();
        }

        return model_internal::_fechaNacimientoIsValid;
    }

    model_internal function calculateFechaNacimientoIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_fechaNacimientoValidator.validate(model_internal::_instance.fechaNacimiento)
        model_internal::_fechaNacimientoIsValid_der = (valRes.results == null);
        model_internal::_fechaNacimientoIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::fechaNacimientoValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::fechaNacimientoValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get fechaNacimientoValidationFailureMessages():Array
    {
        if (model_internal::_fechaNacimientoValidationFailureMessages == null)
            model_internal::calculateFechaNacimientoIsValid();

        return _fechaNacimientoValidationFailureMessages;
    }

    model_internal function set fechaNacimientoValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_fechaNacimientoValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_fechaNacimientoValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "fechaNacimientoValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            case("CodResult"):
            {
                return CodResultValidationFailureMessages;
            }
            case("MsgResult"):
            {
                return MsgResultValidationFailureMessages;
            }
            case("datosCertificacionAfiliacion"):
            {
                return datosCertificacionAfiliacionValidationFailureMessages;
            }
            case("datosPersonales"):
            {
                return datosPersonalesValidationFailureMessages;
            }
            case("estaEnrolado"):
            {
                return estaEnroladoValidationFailureMessages;
            }
            case("nombre"):
            {
                return nombreValidationFailureMessages;
            }
            case("identificacion"):
            {
                return identificacionValidationFailureMessages;
            }
            case("genero"):
            {
                return generoValidationFailureMessages;
            }
            case("email"):
            {
                return emailValidationFailureMessages;
            }
            case("fonoCelular"):
            {
                return fonoCelularValidationFailureMessages;
            }
            case("apellidoMaterno"):
            {
                return apellidoMaternoValidationFailureMessages;
            }
            case("apellidoPaterno"):
            {
                return apellidoPaternoValidationFailureMessages;
            }
            case("fonoFijo"):
            {
                return fonoFijoValidationFailureMessages;
            }
            case("fechaNacimiento"):
            {
                return fechaNacimientoValidationFailureMessages;
            }
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
