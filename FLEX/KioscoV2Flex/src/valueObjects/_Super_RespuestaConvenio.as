/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - RespuestaConvenio.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.util.FiberUtils;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.Event;
import flash.events.EventDispatcher;
import mx.binding.utils.ChangeWatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import mx.validators.ValidationResult;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_RespuestaConvenio extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _RespuestaConvenioEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_CodResult : String;
    private var _internal_MontoACobrar : int;
    private var _internal_MsgResult : String;
    private var _internal_MontoConsumo : int;
    private var _internal_Descuento : String;
    private var _internal_CodConvenio : String;
    private var _internal_CodigoReembolso : String;
    private var _internal_CodConsumo : String;
    private var _internal_MontoDescuento : int;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_RespuestaConvenio()
    {
        _model = new _RespuestaConvenioEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "CodResult", model_internal::setterListenerCodResult));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "MsgResult", model_internal::setterListenerMsgResult));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "Descuento", model_internal::setterListenerDescuento));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "CodConvenio", model_internal::setterListenerCodConvenio));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "CodigoReembolso", model_internal::setterListenerCodigoReembolso));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "CodConsumo", model_internal::setterListenerCodConsumo));

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get CodResult() : String
    {
        return _internal_CodResult;
    }

    [Bindable(event="propertyChange")]
    public function get MontoACobrar() : int
    {
        return _internal_MontoACobrar;
    }

    [Bindable(event="propertyChange")]
    public function get MsgResult() : String
    {
        return _internal_MsgResult;
    }

    [Bindable(event="propertyChange")]
    public function get MontoConsumo() : int
    {
        return _internal_MontoConsumo;
    }

    [Bindable(event="propertyChange")]
    public function get Descuento() : String
    {
        return _internal_Descuento;
    }

    [Bindable(event="propertyChange")]
    public function get CodConvenio() : String
    {
        return _internal_CodConvenio;
    }

    [Bindable(event="propertyChange")]
    public function get CodigoReembolso() : String
    {
        return _internal_CodigoReembolso;
    }

    [Bindable(event="propertyChange")]
    public function get CodConsumo() : String
    {
        return _internal_CodConsumo;
    }

    [Bindable(event="propertyChange")]
    public function get MontoDescuento() : int
    {
        return _internal_MontoDescuento;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set CodResult(value:String) : void
    {
        var oldValue:String = _internal_CodResult;
        if (oldValue !== value)
        {
            _internal_CodResult = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CodResult", oldValue, _internal_CodResult));
        }
    }

    public function set MontoACobrar(value:int) : void
    {
        var oldValue:int = _internal_MontoACobrar;
        if (oldValue !== value)
        {
            _internal_MontoACobrar = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MontoACobrar", oldValue, _internal_MontoACobrar));
        }
    }

    public function set MsgResult(value:String) : void
    {
        var oldValue:String = _internal_MsgResult;
        if (oldValue !== value)
        {
            _internal_MsgResult = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MsgResult", oldValue, _internal_MsgResult));
        }
    }

    public function set MontoConsumo(value:int) : void
    {
        var oldValue:int = _internal_MontoConsumo;
        if (oldValue !== value)
        {
            _internal_MontoConsumo = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MontoConsumo", oldValue, _internal_MontoConsumo));
        }
    }

    public function set Descuento(value:String) : void
    {
        var oldValue:String = _internal_Descuento;
        if (oldValue !== value)
        {
            _internal_Descuento = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "Descuento", oldValue, _internal_Descuento));
        }
    }

    public function set CodConvenio(value:String) : void
    {
        var oldValue:String = _internal_CodConvenio;
        if (oldValue !== value)
        {
            _internal_CodConvenio = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CodConvenio", oldValue, _internal_CodConvenio));
        }
    }

    public function set CodigoReembolso(value:String) : void
    {
        var oldValue:String = _internal_CodigoReembolso;
        if (oldValue !== value)
        {
            _internal_CodigoReembolso = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CodigoReembolso", oldValue, _internal_CodigoReembolso));
        }
    }

    public function set CodConsumo(value:String) : void
    {
        var oldValue:String = _internal_CodConsumo;
        if (oldValue !== value)
        {
            _internal_CodConsumo = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CodConsumo", oldValue, _internal_CodConsumo));
        }
    }

    public function set MontoDescuento(value:int) : void
    {
        var oldValue:int = _internal_MontoDescuento;
        if (oldValue !== value)
        {
            _internal_MontoDescuento = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MontoDescuento", oldValue, _internal_MontoDescuento));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */

    model_internal function setterListenerCodResult(value:flash.events.Event):void
    {
        _model.invalidateDependentOnCodResult();
    }

    model_internal function setterListenerMsgResult(value:flash.events.Event):void
    {
        _model.invalidateDependentOnMsgResult();
    }

    model_internal function setterListenerDescuento(value:flash.events.Event):void
    {
        _model.invalidateDependentOnDescuento();
    }

    model_internal function setterListenerCodConvenio(value:flash.events.Event):void
    {
        _model.invalidateDependentOnCodConvenio();
    }

    model_internal function setterListenerCodigoReembolso(value:flash.events.Event):void
    {
        _model.invalidateDependentOnCodigoReembolso();
    }

    model_internal function setterListenerCodConsumo(value:flash.events.Event):void
    {
        _model.invalidateDependentOnCodConsumo();
    }


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;
        if (!_model.CodResultIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_CodResultValidationFailureMessages);
        }
        if (!_model.MsgResultIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_MsgResultValidationFailureMessages);
        }
        if (!_model.DescuentoIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_DescuentoValidationFailureMessages);
        }
        if (!_model.CodConvenioIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_CodConvenioValidationFailureMessages);
        }
        if (!_model.CodigoReembolsoIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_CodigoReembolsoValidationFailureMessages);
        }
        if (!_model.CodConsumoIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_CodConsumoValidationFailureMessages);
        }

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _RespuestaConvenioEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _RespuestaConvenioEntityMetadata) : void
    {
        var oldValue : _RespuestaConvenioEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }

    model_internal var _doValidationCacheOfCodResult : Array = null;
    model_internal var _doValidationLastValOfCodResult : String;

    model_internal function _doValidationForCodResult(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfCodResult != null && model_internal::_doValidationLastValOfCodResult == value)
           return model_internal::_doValidationCacheOfCodResult ;

        _model.model_internal::_CodResultIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isCodResultAvailable && _internal_CodResult == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "CodResult is required"));
        }

        model_internal::_doValidationCacheOfCodResult = validationFailures;
        model_internal::_doValidationLastValOfCodResult = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfMsgResult : Array = null;
    model_internal var _doValidationLastValOfMsgResult : String;

    model_internal function _doValidationForMsgResult(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfMsgResult != null && model_internal::_doValidationLastValOfMsgResult == value)
           return model_internal::_doValidationCacheOfMsgResult ;

        _model.model_internal::_MsgResultIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isMsgResultAvailable && _internal_MsgResult == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "MsgResult is required"));
        }

        model_internal::_doValidationCacheOfMsgResult = validationFailures;
        model_internal::_doValidationLastValOfMsgResult = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfDescuento : Array = null;
    model_internal var _doValidationLastValOfDescuento : String;

    model_internal function _doValidationForDescuento(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfDescuento != null && model_internal::_doValidationLastValOfDescuento == value)
           return model_internal::_doValidationCacheOfDescuento ;

        _model.model_internal::_DescuentoIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isDescuentoAvailable && _internal_Descuento == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "Descuento is required"));
        }

        model_internal::_doValidationCacheOfDescuento = validationFailures;
        model_internal::_doValidationLastValOfDescuento = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfCodConvenio : Array = null;
    model_internal var _doValidationLastValOfCodConvenio : String;

    model_internal function _doValidationForCodConvenio(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfCodConvenio != null && model_internal::_doValidationLastValOfCodConvenio == value)
           return model_internal::_doValidationCacheOfCodConvenio ;

        _model.model_internal::_CodConvenioIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isCodConvenioAvailable && _internal_CodConvenio == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "CodConvenio is required"));
        }

        model_internal::_doValidationCacheOfCodConvenio = validationFailures;
        model_internal::_doValidationLastValOfCodConvenio = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfCodigoReembolso : Array = null;
    model_internal var _doValidationLastValOfCodigoReembolso : String;

    model_internal function _doValidationForCodigoReembolso(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfCodigoReembolso != null && model_internal::_doValidationLastValOfCodigoReembolso == value)
           return model_internal::_doValidationCacheOfCodigoReembolso ;

        _model.model_internal::_CodigoReembolsoIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isCodigoReembolsoAvailable && _internal_CodigoReembolso == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "CodigoReembolso is required"));
        }

        model_internal::_doValidationCacheOfCodigoReembolso = validationFailures;
        model_internal::_doValidationLastValOfCodigoReembolso = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfCodConsumo : Array = null;
    model_internal var _doValidationLastValOfCodConsumo : String;

    model_internal function _doValidationForCodConsumo(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfCodConsumo != null && model_internal::_doValidationLastValOfCodConsumo == value)
           return model_internal::_doValidationCacheOfCodConsumo ;

        _model.model_internal::_CodConsumoIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isCodConsumoAvailable && _internal_CodConsumo == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "CodConsumo is required"));
        }

        model_internal::_doValidationCacheOfCodConsumo = validationFailures;
        model_internal::_doValidationLastValOfCodConsumo = value;

        return validationFailures;
    }
    

}

}
