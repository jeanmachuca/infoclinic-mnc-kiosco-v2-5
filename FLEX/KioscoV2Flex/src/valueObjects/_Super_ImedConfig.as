/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - ImedConfig.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.util.FiberUtils;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.Event;
import flash.events.EventDispatcher;
import mx.binding.utils.ChangeWatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import mx.validators.ValidationResult;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_ImedConfig extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _ImedConfigEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ipAnularBono : String;
    private var _internal_ipBono : String;
    private var _internal_ipBotonPago : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_ImedConfig()
    {
        _model = new _ImedConfigEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "ipAnularBono", model_internal::setterListenerIpAnularBono));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "ipBono", model_internal::setterListenerIpBono));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "ipBotonPago", model_internal::setterListenerIpBotonPago));

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ipAnularBono() : String
    {
        return _internal_ipAnularBono;
    }

    [Bindable(event="propertyChange")]
    public function get ipBono() : String
    {
        return _internal_ipBono;
    }

    [Bindable(event="propertyChange")]
    public function get ipBotonPago() : String
    {
        return _internal_ipBotonPago;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ipAnularBono(value:String) : void
    {
        var oldValue:String = _internal_ipAnularBono;
        if (oldValue !== value)
        {
            _internal_ipAnularBono = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ipAnularBono", oldValue, _internal_ipAnularBono));
        }
    }

    public function set ipBono(value:String) : void
    {
        var oldValue:String = _internal_ipBono;
        if (oldValue !== value)
        {
            _internal_ipBono = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ipBono", oldValue, _internal_ipBono));
        }
    }

    public function set ipBotonPago(value:String) : void
    {
        var oldValue:String = _internal_ipBotonPago;
        if (oldValue !== value)
        {
            _internal_ipBotonPago = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ipBotonPago", oldValue, _internal_ipBotonPago));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */

    model_internal function setterListenerIpAnularBono(value:flash.events.Event):void
    {
        _model.invalidateDependentOnIpAnularBono();
    }

    model_internal function setterListenerIpBono(value:flash.events.Event):void
    {
        _model.invalidateDependentOnIpBono();
    }

    model_internal function setterListenerIpBotonPago(value:flash.events.Event):void
    {
        _model.invalidateDependentOnIpBotonPago();
    }


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;
        if (!_model.ipAnularBonoIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_ipAnularBonoValidationFailureMessages);
        }
        if (!_model.ipBonoIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_ipBonoValidationFailureMessages);
        }
        if (!_model.ipBotonPagoIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_ipBotonPagoValidationFailureMessages);
        }

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _ImedConfigEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _ImedConfigEntityMetadata) : void
    {
        var oldValue : _ImedConfigEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }

    model_internal var _doValidationCacheOfIpAnularBono : Array = null;
    model_internal var _doValidationLastValOfIpAnularBono : String;

    model_internal function _doValidationForIpAnularBono(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfIpAnularBono != null && model_internal::_doValidationLastValOfIpAnularBono == value)
           return model_internal::_doValidationCacheOfIpAnularBono ;

        _model.model_internal::_ipAnularBonoIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isIpAnularBonoAvailable && _internal_ipAnularBono == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "ipAnularBono is required"));
        }

        model_internal::_doValidationCacheOfIpAnularBono = validationFailures;
        model_internal::_doValidationLastValOfIpAnularBono = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfIpBono : Array = null;
    model_internal var _doValidationLastValOfIpBono : String;

    model_internal function _doValidationForIpBono(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfIpBono != null && model_internal::_doValidationLastValOfIpBono == value)
           return model_internal::_doValidationCacheOfIpBono ;

        _model.model_internal::_ipBonoIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isIpBonoAvailable && _internal_ipBono == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "ipBono is required"));
        }

        model_internal::_doValidationCacheOfIpBono = validationFailures;
        model_internal::_doValidationLastValOfIpBono = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfIpBotonPago : Array = null;
    model_internal var _doValidationLastValOfIpBotonPago : String;

    model_internal function _doValidationForIpBotonPago(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfIpBotonPago != null && model_internal::_doValidationLastValOfIpBotonPago == value)
           return model_internal::_doValidationCacheOfIpBotonPago ;

        _model.model_internal::_ipBotonPagoIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isIpBotonPagoAvailable && _internal_ipBotonPago == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "ipBotonPago is required"));
        }

        model_internal::_doValidationCacheOfIpBotonPago = validationFailures;
        model_internal::_doValidationLastValOfIpBotonPago = value;

        return validationFailures;
    }
    

}

}
