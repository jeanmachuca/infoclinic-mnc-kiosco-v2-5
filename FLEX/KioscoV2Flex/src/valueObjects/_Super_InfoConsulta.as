/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - InfoConsulta.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.util.FiberUtils;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.Event;
import flash.events.EventDispatcher;
import mx.binding.utils.ChangeWatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import mx.validators.ValidationResult;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_InfoConsulta extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _InfoConsultaEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_CodResult : String;
    private var _internal_MsgResult : String;
    private var _internal_rutMedico : String;
    private var _internal_numeroPacientesEspera : String;
    private var _internal_idBox : String;
    private var _internal_tiempoEspera : String;
    private var _internal_nombreMedico : String;
    private var _internal_idMedico : String;
    private var _internal_horaAtencion : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_InfoConsulta()
    {
        _model = new _InfoConsultaEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "CodResult", model_internal::setterListenerCodResult));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "MsgResult", model_internal::setterListenerMsgResult));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "rutMedico", model_internal::setterListenerRutMedico));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "numeroPacientesEspera", model_internal::setterListenerNumeroPacientesEspera));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "idBox", model_internal::setterListenerIdBox));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "tiempoEspera", model_internal::setterListenerTiempoEspera));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "nombreMedico", model_internal::setterListenerNombreMedico));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "idMedico", model_internal::setterListenerIdMedico));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "horaAtencion", model_internal::setterListenerHoraAtencion));

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get CodResult() : String
    {
        return _internal_CodResult;
    }

    [Bindable(event="propertyChange")]
    public function get MsgResult() : String
    {
        return _internal_MsgResult;
    }

    [Bindable(event="propertyChange")]
    public function get rutMedico() : String
    {
        return _internal_rutMedico;
    }

    [Bindable(event="propertyChange")]
    public function get numeroPacientesEspera() : String
    {
        return _internal_numeroPacientesEspera;
    }

    [Bindable(event="propertyChange")]
    public function get idBox() : String
    {
        return _internal_idBox;
    }

    [Bindable(event="propertyChange")]
    public function get tiempoEspera() : String
    {
        return _internal_tiempoEspera;
    }

    [Bindable(event="propertyChange")]
    public function get nombreMedico() : String
    {
        return _internal_nombreMedico;
    }

    [Bindable(event="propertyChange")]
    public function get idMedico() : String
    {
        return _internal_idMedico;
    }

    [Bindable(event="propertyChange")]
    public function get horaAtencion() : String
    {
        return _internal_horaAtencion;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set CodResult(value:String) : void
    {
        var oldValue:String = _internal_CodResult;
        if (oldValue !== value)
        {
            _internal_CodResult = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CodResult", oldValue, _internal_CodResult));
        }
    }

    public function set MsgResult(value:String) : void
    {
        var oldValue:String = _internal_MsgResult;
        if (oldValue !== value)
        {
            _internal_MsgResult = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MsgResult", oldValue, _internal_MsgResult));
        }
    }

    public function set rutMedico(value:String) : void
    {
        var oldValue:String = _internal_rutMedico;
        if (oldValue !== value)
        {
            _internal_rutMedico = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "rutMedico", oldValue, _internal_rutMedico));
        }
    }

    public function set numeroPacientesEspera(value:String) : void
    {
        var oldValue:String = _internal_numeroPacientesEspera;
        if (oldValue !== value)
        {
            _internal_numeroPacientesEspera = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "numeroPacientesEspera", oldValue, _internal_numeroPacientesEspera));
        }
    }

    public function set idBox(value:String) : void
    {
        var oldValue:String = _internal_idBox;
        if (oldValue !== value)
        {
            _internal_idBox = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "idBox", oldValue, _internal_idBox));
        }
    }

    public function set tiempoEspera(value:String) : void
    {
        var oldValue:String = _internal_tiempoEspera;
        if (oldValue !== value)
        {
            _internal_tiempoEspera = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "tiempoEspera", oldValue, _internal_tiempoEspera));
        }
    }

    public function set nombreMedico(value:String) : void
    {
        var oldValue:String = _internal_nombreMedico;
        if (oldValue !== value)
        {
            _internal_nombreMedico = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "nombreMedico", oldValue, _internal_nombreMedico));
        }
    }

    public function set idMedico(value:String) : void
    {
        var oldValue:String = _internal_idMedico;
        if (oldValue !== value)
        {
            _internal_idMedico = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "idMedico", oldValue, _internal_idMedico));
        }
    }

    public function set horaAtencion(value:String) : void
    {
        var oldValue:String = _internal_horaAtencion;
        if (oldValue !== value)
        {
            _internal_horaAtencion = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "horaAtencion", oldValue, _internal_horaAtencion));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */

    model_internal function setterListenerCodResult(value:flash.events.Event):void
    {
        _model.invalidateDependentOnCodResult();
    }

    model_internal function setterListenerMsgResult(value:flash.events.Event):void
    {
        _model.invalidateDependentOnMsgResult();
    }

    model_internal function setterListenerRutMedico(value:flash.events.Event):void
    {
        _model.invalidateDependentOnRutMedico();
    }

    model_internal function setterListenerNumeroPacientesEspera(value:flash.events.Event):void
    {
        _model.invalidateDependentOnNumeroPacientesEspera();
    }

    model_internal function setterListenerIdBox(value:flash.events.Event):void
    {
        _model.invalidateDependentOnIdBox();
    }

    model_internal function setterListenerTiempoEspera(value:flash.events.Event):void
    {
        _model.invalidateDependentOnTiempoEspera();
    }

    model_internal function setterListenerNombreMedico(value:flash.events.Event):void
    {
        _model.invalidateDependentOnNombreMedico();
    }

    model_internal function setterListenerIdMedico(value:flash.events.Event):void
    {
        _model.invalidateDependentOnIdMedico();
    }

    model_internal function setterListenerHoraAtencion(value:flash.events.Event):void
    {
        _model.invalidateDependentOnHoraAtencion();
    }


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;
        if (!_model.CodResultIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_CodResultValidationFailureMessages);
        }
        if (!_model.MsgResultIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_MsgResultValidationFailureMessages);
        }
        if (!_model.rutMedicoIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_rutMedicoValidationFailureMessages);
        }
        if (!_model.numeroPacientesEsperaIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_numeroPacientesEsperaValidationFailureMessages);
        }
        if (!_model.idBoxIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_idBoxValidationFailureMessages);
        }
        if (!_model.tiempoEsperaIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_tiempoEsperaValidationFailureMessages);
        }
        if (!_model.nombreMedicoIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_nombreMedicoValidationFailureMessages);
        }
        if (!_model.idMedicoIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_idMedicoValidationFailureMessages);
        }
        if (!_model.horaAtencionIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_horaAtencionValidationFailureMessages);
        }

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _InfoConsultaEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _InfoConsultaEntityMetadata) : void
    {
        var oldValue : _InfoConsultaEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }

    model_internal var _doValidationCacheOfCodResult : Array = null;
    model_internal var _doValidationLastValOfCodResult : String;

    model_internal function _doValidationForCodResult(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfCodResult != null && model_internal::_doValidationLastValOfCodResult == value)
           return model_internal::_doValidationCacheOfCodResult ;

        _model.model_internal::_CodResultIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isCodResultAvailable && _internal_CodResult == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "CodResult is required"));
        }

        model_internal::_doValidationCacheOfCodResult = validationFailures;
        model_internal::_doValidationLastValOfCodResult = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfMsgResult : Array = null;
    model_internal var _doValidationLastValOfMsgResult : String;

    model_internal function _doValidationForMsgResult(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfMsgResult != null && model_internal::_doValidationLastValOfMsgResult == value)
           return model_internal::_doValidationCacheOfMsgResult ;

        _model.model_internal::_MsgResultIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isMsgResultAvailable && _internal_MsgResult == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "MsgResult is required"));
        }

        model_internal::_doValidationCacheOfMsgResult = validationFailures;
        model_internal::_doValidationLastValOfMsgResult = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfRutMedico : Array = null;
    model_internal var _doValidationLastValOfRutMedico : String;

    model_internal function _doValidationForRutMedico(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfRutMedico != null && model_internal::_doValidationLastValOfRutMedico == value)
           return model_internal::_doValidationCacheOfRutMedico ;

        _model.model_internal::_rutMedicoIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isRutMedicoAvailable && _internal_rutMedico == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "rutMedico is required"));
        }

        model_internal::_doValidationCacheOfRutMedico = validationFailures;
        model_internal::_doValidationLastValOfRutMedico = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfNumeroPacientesEspera : Array = null;
    model_internal var _doValidationLastValOfNumeroPacientesEspera : String;

    model_internal function _doValidationForNumeroPacientesEspera(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfNumeroPacientesEspera != null && model_internal::_doValidationLastValOfNumeroPacientesEspera == value)
           return model_internal::_doValidationCacheOfNumeroPacientesEspera ;

        _model.model_internal::_numeroPacientesEsperaIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isNumeroPacientesEsperaAvailable && _internal_numeroPacientesEspera == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "numeroPacientesEspera is required"));
        }

        model_internal::_doValidationCacheOfNumeroPacientesEspera = validationFailures;
        model_internal::_doValidationLastValOfNumeroPacientesEspera = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfIdBox : Array = null;
    model_internal var _doValidationLastValOfIdBox : String;

    model_internal function _doValidationForIdBox(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfIdBox != null && model_internal::_doValidationLastValOfIdBox == value)
           return model_internal::_doValidationCacheOfIdBox ;

        _model.model_internal::_idBoxIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isIdBoxAvailable && _internal_idBox == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "idBox is required"));
        }

        model_internal::_doValidationCacheOfIdBox = validationFailures;
        model_internal::_doValidationLastValOfIdBox = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfTiempoEspera : Array = null;
    model_internal var _doValidationLastValOfTiempoEspera : String;

    model_internal function _doValidationForTiempoEspera(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfTiempoEspera != null && model_internal::_doValidationLastValOfTiempoEspera == value)
           return model_internal::_doValidationCacheOfTiempoEspera ;

        _model.model_internal::_tiempoEsperaIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isTiempoEsperaAvailable && _internal_tiempoEspera == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "tiempoEspera is required"));
        }

        model_internal::_doValidationCacheOfTiempoEspera = validationFailures;
        model_internal::_doValidationLastValOfTiempoEspera = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfNombreMedico : Array = null;
    model_internal var _doValidationLastValOfNombreMedico : String;

    model_internal function _doValidationForNombreMedico(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfNombreMedico != null && model_internal::_doValidationLastValOfNombreMedico == value)
           return model_internal::_doValidationCacheOfNombreMedico ;

        _model.model_internal::_nombreMedicoIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isNombreMedicoAvailable && _internal_nombreMedico == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "nombreMedico is required"));
        }

        model_internal::_doValidationCacheOfNombreMedico = validationFailures;
        model_internal::_doValidationLastValOfNombreMedico = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfIdMedico : Array = null;
    model_internal var _doValidationLastValOfIdMedico : String;

    model_internal function _doValidationForIdMedico(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfIdMedico != null && model_internal::_doValidationLastValOfIdMedico == value)
           return model_internal::_doValidationCacheOfIdMedico ;

        _model.model_internal::_idMedicoIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isIdMedicoAvailable && _internal_idMedico == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "idMedico is required"));
        }

        model_internal::_doValidationCacheOfIdMedico = validationFailures;
        model_internal::_doValidationLastValOfIdMedico = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfHoraAtencion : Array = null;
    model_internal var _doValidationLastValOfHoraAtencion : String;

    model_internal function _doValidationForHoraAtencion(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfHoraAtencion != null && model_internal::_doValidationLastValOfHoraAtencion == value)
           return model_internal::_doValidationCacheOfHoraAtencion ;

        _model.model_internal::_horaAtencionIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isHoraAtencionAvailable && _internal_horaAtencion == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "horaAtencion is required"));
        }

        model_internal::_doValidationCacheOfHoraAtencion = validationFailures;
        model_internal::_doValidationLastValOfHoraAtencion = value;

        return validationFailures;
    }
    

}

}
