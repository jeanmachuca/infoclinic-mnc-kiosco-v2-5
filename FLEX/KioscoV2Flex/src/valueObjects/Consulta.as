package valueObjects
{
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.collections.AsyncListView;
	
	[Bindable]
	public dynamic class Consulta extends MiniclinicVO
	{
		private var _estadoTransaccion:int = 0;
		
		private var _sintomasSeleccionados:Vector.<Object>;
		private var _sintomaSeleccionado:Sintoma;
		private var _detalleSeleccionado:TipoConsulta;
		private var _motivoSeleccionado:valueObjects.TipoConsulta;
		private var _identificacion:String;
		private var _tipoIdentificacion:int;
		private var _codigoPrevision:String;
		private var _prevision:Prevision;
		private var _datosPersonales:DatosPaciente = new DatosPaciente();
		private var _tipoPrevision:int;
		private var _tipoFlujo:int;
		
		private var _enviarSMS:Boolean;
		
		private var _enviarInfoMail:Boolean;
		
		private var _accionTransaccion:String;
		
		private var _botonPagoIMED:int;
		
		private var _activarAutentia:Boolean;
		
		private var _idTipoConsulta:String;
		
		private var _idSintoma:String;
		
		private var _formaPago:int;
		
		private var _ticket:String;
		
		public function Consulta(target:IEventDispatcher=null)
		{
			//TODO: implement function
			super(target);
		}

		[Bindable(event="ticketChange")]
		public function get ticket():String
		{
			return _ticket;
		}

		public function set ticket(value:String):void
		{
			if( _ticket !== value)
			{
				_ticket = value;
				dispatchEvent(new Event("ticketChange"));
			}
		}

		[Bindable(event="sintomaSeleccionadoChange")]
		public function get sintomaSeleccionado():Sintoma
		{
			return _sintomaSeleccionado;
		}

		public function set sintomaSeleccionado(value:Sintoma):void
		{
			if( _sintomaSeleccionado !== value)
			{
				_sintomaSeleccionado = value;
				dispatchEvent(new Event("sintomaSeleccionadoChange"));
			}
		}

		[Bindable(event="detalleSeleccionadoChange")]
		public function get detalleSeleccionado():TipoConsulta
		{
			return _detalleSeleccionado;
		}

		public function set detalleSeleccionado(value:TipoConsulta):void
		{
			if( _detalleSeleccionado !== value)
			{
				_detalleSeleccionado = value;
				dispatchEvent(new Event("detalleSeleccionadoChange"));
			}
		}

		[Bindable(event="motivoSeleccionadoChange")]
		public function get motivoSeleccionado():valueObjects.TipoConsulta
		{
			return _motivoSeleccionado;
		}

		public function set motivoSeleccionado(value:TipoConsulta):void
		{
			if( _motivoSeleccionado !== value)
			{
				_motivoSeleccionado = value;
				dispatchEvent(new Event("motivoSeleccionadoChange"));
			}
		}

		[Bindable(event="identificacionChange")]
		public function get identificacion():String
		{
			return (_identificacion != null )?(_identificacion.toLocaleUpperCase()):("");
		}

		public function set identificacion(value:String):void
		{
			if( _identificacion !== value)
			{
				value = value.replace('-','');
				value = value.replace('.','');
				_identificacion = value.substring(0,value.length-1) +'-'+ value.substr(-1,1);
				dispatchEvent(new Event("identificacionChange"));
			}
		}

		[Bindable(event="tipoIdentificacionChange")]
		public function get tipoIdentificacion():int
		{
			return _tipoIdentificacion;
		}

		public function set tipoIdentificacion(value:int):void
		{
			if( _tipoIdentificacion !== value)
			{
				_tipoIdentificacion = value;
				dispatchEvent(new Event("tipoIdentificacionChange"));
			}
		}

		[Bindable(event="codigoPrevisionChange")]
		public function get codigoPrevision():String
		{
			return _codigoPrevision;
		}

		public function set codigoPrevision(value:String):void
		{
			if( _codigoPrevision !== value)
			{
				_codigoPrevision = value;
				dispatchEvent(new Event("codigoPrevisionChange"));
			}
		}

		[Bindable(event="previsionChange")]
		public function get prevision():Prevision
		{
			return _prevision;
		}

		public function set prevision(value:Prevision):void
		{
			if( _prevision !== value)
			{
				_prevision = value;
				dispatchEvent(new Event("previsionChange"));
			}
		}

		[Bindable(event="datosPersonalesChange")]
		public function get datosPersonales():DatosPaciente
		{
			return _datosPersonales;
		}

		public function set datosPersonales(value:DatosPaciente):void
		{
			if( _datosPersonales !== value)
			{
				_datosPersonales = value;
				dispatchEvent(new Event("datosPersonalesChange"));
			}
		}

		[Bindable(event="tipoPrevisionChange")]
		public function get tipoPrevision():int
		{
			return _tipoPrevision;
		}

		public function set tipoPrevision(value:int):void
		{
			if( _tipoPrevision !== value)
			{
				_tipoPrevision = value;
				dispatchEvent(new Event("tipoPrevisionChange"));
			}
		}

		[Bindable(event="tipoFlujoChange")]
		public function get tipoFlujo():int
		{
			return _tipoFlujo;
		}

		public function set tipoFlujo(value:int):void
		{
			if( _tipoFlujo !== value)
			{
				_tipoFlujo = value;
				dispatchEvent(new Event("tipoFlujoChange"));
			}
		}

		[Bindable(event="accionTransaccionChange")]
		public function get accionTransaccion():String
		{
			return _accionTransaccion;
		}

		public function set accionTransaccion(value:String):void
		{
			if( _accionTransaccion !== value)
			{
				_accionTransaccion = value;
				dispatchEvent(new Event("accionTransaccionChange"));
			}
		}

		[Bindable(event="botonPagoIMEDChange")]
		public function get botonPagoIMED():int
		{
			return _botonPagoIMED;
		}

		public function set botonPagoIMED(value:int):void
		{
			if( _botonPagoIMED !== value)
			{
				_botonPagoIMED = value;
				dispatchEvent(new Event("botonPagoIMEDChange"));
			}
		}

		[Bindable(event="activarAutentiaChange")]
		public function get activarAutentia():Boolean
		{
			return _activarAutentia;
		}

		public function set activarAutentia(value:Boolean):void
		{
			if( _activarAutentia !== value)
			{
				_activarAutentia = value;
				dispatchEvent(new Event("activarAutentiaChange"));
			}
		}

		[Bindable(event="sintomasSeleccionadosChange")]
		public function get sintomasSeleccionados():Vector.<Object>
		{
			return _sintomasSeleccionados;
		}

		public function set sintomasSeleccionados(value:Vector.<Object>):void
		{
			if( _sintomasSeleccionados !== value)
			{
				_sintomasSeleccionados = value;
				dispatchEvent(new Event("sintomasSeleccionadosChange"));
			}
		}

		[Bindable(event="enviarSMSChange")]
		public function get enviarSMS():Boolean
		{
			return _enviarSMS;
		}

		public function set enviarSMS(value:Boolean):void
		{
			if( _enviarSMS !== value)
			{
				_enviarSMS = value;
				dispatchEvent(new Event("enviarSMSChange"));
			}
		}

		[Bindable(event="enviarInfoMailChange")]
		public function get enviarInfoMail():Boolean
		{
			return _enviarInfoMail;
		}

		public function set enviarInfoMail(value:Boolean):void
		{
			if( _enviarInfoMail !== value)
			{
				_enviarInfoMail = value;
				dispatchEvent(new Event("enviarInfoMailChange"));
			}
		}

		[Bindable(event="idTipoConsultaChange")]
		public function get idTipoConsulta():String
		{
			if (motivoSeleccionado.idPadre>0){
				_idTipoConsulta = motivoSeleccionado.idPadre+','+motivoSeleccionado.idTipoConsulta;
			} else {
				_idTipoConsulta = motivoSeleccionado.idTipoConsulta;
			}
			return _idTipoConsulta;
		}

		public function set idTipoConsulta(value:String):void
		{
			if( _idTipoConsulta !== value)
			{
				_idTipoConsulta = value;
				dispatchEvent(new Event("idTipoConsultaChange"));
			}
		}

		[Bindable(event="idSintomaChange")]
		public function get idSintoma():String
		{
			_idSintoma = "";
			for each (var i:int in _sintomasSeleccionados) 
			{
				_idSintoma += (_sintomasSeleccionados[i] as Sintoma).idSintoma + ',';
			}
			return (_idSintoma.length>1)?(_idSintoma.substr(0,-1)):("");
		}

		public function set idSintoma(value:String):void
		{
			if( _idSintoma !== value)
			{
				_idSintoma = value;
				dispatchEvent(new Event("idSintomaChange"));
			}
		}

		[Bindable(event="formaPagoChange")]
		public function get formaPago():int
		{
			return _formaPago;
		}

		public function set formaPago(value:int):void
		{
			if( _formaPago !== value)
			{
				_formaPago = value;
				dispatchEvent(new Event("formaPagoChange"));
			}
		}

		[Bindable(event="estadoTransaccionChange")]
		public function get estadoTransaccion():int
		{
			return _estadoTransaccion;
		}

		public function set estadoTransaccion(value:int):void
		{
			if( _estadoTransaccion !== value)
			{
				_estadoTransaccion = value;
				dispatchEvent(new Event("estadoTransaccionChange"));
			}
		}


	}
}