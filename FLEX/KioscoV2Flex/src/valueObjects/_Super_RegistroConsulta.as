/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - RegistroConsulta.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.util.FiberUtils;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.Event;
import flash.events.EventDispatcher;
import mx.binding.utils.ChangeWatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import mx.validators.ValidationResult;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_RegistroConsulta extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _RegistroConsultaEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_FormaPago : String;
    private var _internal_CodResult : String;
    private var _internal_idConsulta : String;
    private var _internal_MsgResult : String;
    private var _internal_idCliente : String;
    private var _internal_PasoAnterior : String;
    private var _internal_idTransaccionIMed : String;
    private var _internal_PasoSiguiente : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_RegistroConsulta()
    {
        _model = new _RegistroConsultaEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "FormaPago", model_internal::setterListenerFormaPago));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "CodResult", model_internal::setterListenerCodResult));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "idConsulta", model_internal::setterListenerIdConsulta));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "MsgResult", model_internal::setterListenerMsgResult));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "idCliente", model_internal::setterListenerIdCliente));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "PasoAnterior", model_internal::setterListenerPasoAnterior));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "idTransaccionIMed", model_internal::setterListenerIdTransaccionIMed));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "PasoSiguiente", model_internal::setterListenerPasoSiguiente));

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get FormaPago() : String
    {
        return _internal_FormaPago;
    }

    [Bindable(event="propertyChange")]
    public function get CodResult() : String
    {
        return _internal_CodResult;
    }

    [Bindable(event="propertyChange")]
    public function get idConsulta() : String
    {
        return _internal_idConsulta;
    }

    [Bindable(event="propertyChange")]
    public function get MsgResult() : String
    {
        return _internal_MsgResult;
    }

    [Bindable(event="propertyChange")]
    public function get idCliente() : String
    {
        return _internal_idCliente;
    }

    [Bindable(event="propertyChange")]
    public function get PasoAnterior() : String
    {
        return _internal_PasoAnterior;
    }

    [Bindable(event="propertyChange")]
    public function get idTransaccionIMed() : String
    {
        return _internal_idTransaccionIMed;
    }

    [Bindable(event="propertyChange")]
    public function get PasoSiguiente() : String
    {
        return _internal_PasoSiguiente;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set FormaPago(value:String) : void
    {
        var oldValue:String = _internal_FormaPago;
        if (oldValue !== value)
        {
            _internal_FormaPago = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FormaPago", oldValue, _internal_FormaPago));
        }
    }

    public function set CodResult(value:String) : void
    {
        var oldValue:String = _internal_CodResult;
        if (oldValue !== value)
        {
            _internal_CodResult = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CodResult", oldValue, _internal_CodResult));
        }
    }

    public function set idConsulta(value:String) : void
    {
        var oldValue:String = _internal_idConsulta;
        if (oldValue !== value)
        {
            _internal_idConsulta = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "idConsulta", oldValue, _internal_idConsulta));
        }
    }

    public function set MsgResult(value:String) : void
    {
        var oldValue:String = _internal_MsgResult;
        if (oldValue !== value)
        {
            _internal_MsgResult = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MsgResult", oldValue, _internal_MsgResult));
        }
    }

    public function set idCliente(value:String) : void
    {
        var oldValue:String = _internal_idCliente;
        if (oldValue !== value)
        {
            _internal_idCliente = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "idCliente", oldValue, _internal_idCliente));
        }
    }

    public function set PasoAnterior(value:String) : void
    {
        var oldValue:String = _internal_PasoAnterior;
        if (oldValue !== value)
        {
            _internal_PasoAnterior = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PasoAnterior", oldValue, _internal_PasoAnterior));
        }
    }

    public function set idTransaccionIMed(value:String) : void
    {
        var oldValue:String = _internal_idTransaccionIMed;
        if (oldValue !== value)
        {
            _internal_idTransaccionIMed = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "idTransaccionIMed", oldValue, _internal_idTransaccionIMed));
        }
    }

    public function set PasoSiguiente(value:String) : void
    {
        var oldValue:String = _internal_PasoSiguiente;
        if (oldValue !== value)
        {
            _internal_PasoSiguiente = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PasoSiguiente", oldValue, _internal_PasoSiguiente));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */

    model_internal function setterListenerFormaPago(value:flash.events.Event):void
    {
        _model.invalidateDependentOnFormaPago();
    }

    model_internal function setterListenerCodResult(value:flash.events.Event):void
    {
        _model.invalidateDependentOnCodResult();
    }

    model_internal function setterListenerIdConsulta(value:flash.events.Event):void
    {
        _model.invalidateDependentOnIdConsulta();
    }

    model_internal function setterListenerMsgResult(value:flash.events.Event):void
    {
        _model.invalidateDependentOnMsgResult();
    }

    model_internal function setterListenerIdCliente(value:flash.events.Event):void
    {
        _model.invalidateDependentOnIdCliente();
    }

    model_internal function setterListenerPasoAnterior(value:flash.events.Event):void
    {
        _model.invalidateDependentOnPasoAnterior();
    }

    model_internal function setterListenerIdTransaccionIMed(value:flash.events.Event):void
    {
        _model.invalidateDependentOnIdTransaccionIMed();
    }

    model_internal function setterListenerPasoSiguiente(value:flash.events.Event):void
    {
        _model.invalidateDependentOnPasoSiguiente();
    }


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;
        if (!_model.FormaPagoIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_FormaPagoValidationFailureMessages);
        }
        if (!_model.CodResultIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_CodResultValidationFailureMessages);
        }
        if (!_model.idConsultaIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_idConsultaValidationFailureMessages);
        }
        if (!_model.MsgResultIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_MsgResultValidationFailureMessages);
        }
        if (!_model.idClienteIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_idClienteValidationFailureMessages);
        }
        if (!_model.PasoAnteriorIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_PasoAnteriorValidationFailureMessages);
        }
        if (!_model.idTransaccionIMedIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_idTransaccionIMedValidationFailureMessages);
        }
        if (!_model.PasoSiguienteIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_PasoSiguienteValidationFailureMessages);
        }

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _RegistroConsultaEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _RegistroConsultaEntityMetadata) : void
    {
        var oldValue : _RegistroConsultaEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }

    model_internal var _doValidationCacheOfFormaPago : Array = null;
    model_internal var _doValidationLastValOfFormaPago : String;

    model_internal function _doValidationForFormaPago(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfFormaPago != null && model_internal::_doValidationLastValOfFormaPago == value)
           return model_internal::_doValidationCacheOfFormaPago ;

        _model.model_internal::_FormaPagoIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isFormaPagoAvailable && _internal_FormaPago == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "FormaPago is required"));
        }

        model_internal::_doValidationCacheOfFormaPago = validationFailures;
        model_internal::_doValidationLastValOfFormaPago = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfCodResult : Array = null;
    model_internal var _doValidationLastValOfCodResult : String;

    model_internal function _doValidationForCodResult(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfCodResult != null && model_internal::_doValidationLastValOfCodResult == value)
           return model_internal::_doValidationCacheOfCodResult ;

        _model.model_internal::_CodResultIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isCodResultAvailable && _internal_CodResult == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "CodResult is required"));
        }

        model_internal::_doValidationCacheOfCodResult = validationFailures;
        model_internal::_doValidationLastValOfCodResult = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfIdConsulta : Array = null;
    model_internal var _doValidationLastValOfIdConsulta : String;

    model_internal function _doValidationForIdConsulta(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfIdConsulta != null && model_internal::_doValidationLastValOfIdConsulta == value)
           return model_internal::_doValidationCacheOfIdConsulta ;

        _model.model_internal::_idConsultaIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isIdConsultaAvailable && _internal_idConsulta == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "idConsulta is required"));
        }

        model_internal::_doValidationCacheOfIdConsulta = validationFailures;
        model_internal::_doValidationLastValOfIdConsulta = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfMsgResult : Array = null;
    model_internal var _doValidationLastValOfMsgResult : String;

    model_internal function _doValidationForMsgResult(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfMsgResult != null && model_internal::_doValidationLastValOfMsgResult == value)
           return model_internal::_doValidationCacheOfMsgResult ;

        _model.model_internal::_MsgResultIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isMsgResultAvailable && _internal_MsgResult == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "MsgResult is required"));
        }

        model_internal::_doValidationCacheOfMsgResult = validationFailures;
        model_internal::_doValidationLastValOfMsgResult = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfIdCliente : Array = null;
    model_internal var _doValidationLastValOfIdCliente : String;

    model_internal function _doValidationForIdCliente(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfIdCliente != null && model_internal::_doValidationLastValOfIdCliente == value)
           return model_internal::_doValidationCacheOfIdCliente ;

        _model.model_internal::_idClienteIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isIdClienteAvailable && _internal_idCliente == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "idCliente is required"));
        }

        model_internal::_doValidationCacheOfIdCliente = validationFailures;
        model_internal::_doValidationLastValOfIdCliente = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfPasoAnterior : Array = null;
    model_internal var _doValidationLastValOfPasoAnterior : String;

    model_internal function _doValidationForPasoAnterior(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfPasoAnterior != null && model_internal::_doValidationLastValOfPasoAnterior == value)
           return model_internal::_doValidationCacheOfPasoAnterior ;

        _model.model_internal::_PasoAnteriorIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isPasoAnteriorAvailable && _internal_PasoAnterior == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "PasoAnterior is required"));
        }

        model_internal::_doValidationCacheOfPasoAnterior = validationFailures;
        model_internal::_doValidationLastValOfPasoAnterior = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfIdTransaccionIMed : Array = null;
    model_internal var _doValidationLastValOfIdTransaccionIMed : String;

    model_internal function _doValidationForIdTransaccionIMed(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfIdTransaccionIMed != null && model_internal::_doValidationLastValOfIdTransaccionIMed == value)
           return model_internal::_doValidationCacheOfIdTransaccionIMed ;

        _model.model_internal::_idTransaccionIMedIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isIdTransaccionIMedAvailable && _internal_idTransaccionIMed == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "idTransaccionIMed is required"));
        }

        model_internal::_doValidationCacheOfIdTransaccionIMed = validationFailures;
        model_internal::_doValidationLastValOfIdTransaccionIMed = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfPasoSiguiente : Array = null;
    model_internal var _doValidationLastValOfPasoSiguiente : String;

    model_internal function _doValidationForPasoSiguiente(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfPasoSiguiente != null && model_internal::_doValidationLastValOfPasoSiguiente == value)
           return model_internal::_doValidationCacheOfPasoSiguiente ;

        _model.model_internal::_PasoSiguienteIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isPasoSiguienteAvailable && _internal_PasoSiguiente == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "PasoSiguiente is required"));
        }

        model_internal::_doValidationCacheOfPasoSiguiente = validationFailures;
        model_internal::_doValidationLastValOfPasoSiguiente = value;

        return validationFailures;
    }
    

}

}
