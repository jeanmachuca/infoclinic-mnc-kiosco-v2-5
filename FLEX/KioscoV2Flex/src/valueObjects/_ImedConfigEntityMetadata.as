
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.styles.StyleValidator;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import mx.events.ValidationResultEvent;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _ImedConfigEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("ipAnularBono", "ipBono", "ipBotonPago");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array("ipAnularBono", "ipBono", "ipBotonPago");
    model_internal static var allAlwaysAvailableProperties:Array = new Array("ipAnularBono", "ipBono", "ipBotonPago");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("ipAnularBono", "ipBono", "ipBotonPago");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("ipAnularBono", "ipBono", "ipBotonPago");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array();
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "ImedConfig";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;

    
    model_internal var _ipAnularBonoIsValid:Boolean;
    model_internal var _ipAnularBonoValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _ipAnularBonoIsValidCacheInitialized:Boolean = false;
    model_internal var _ipAnularBonoValidationFailureMessages:Array;
    
    model_internal var _ipBonoIsValid:Boolean;
    model_internal var _ipBonoValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _ipBonoIsValidCacheInitialized:Boolean = false;
    model_internal var _ipBonoValidationFailureMessages:Array;
    
    model_internal var _ipBotonPagoIsValid:Boolean;
    model_internal var _ipBotonPagoValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _ipBotonPagoIsValidCacheInitialized:Boolean = false;
    model_internal var _ipBotonPagoValidationFailureMessages:Array;

    model_internal var _instance:_Super_ImedConfig;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _ImedConfigEntityMetadata(value : _Super_ImedConfig)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["ipAnularBono"] = new Array();
            model_internal::dependentsOnMap["ipBono"] = new Array();
            model_internal::dependentsOnMap["ipBotonPago"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["ipAnularBono"] = "String";
        model_internal::propertyTypeMap["ipBono"] = "String";
        model_internal::propertyTypeMap["ipBotonPago"] = "String";

        model_internal::_instance = value;
        model_internal::_ipAnularBonoValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForIpAnularBono);
        model_internal::_ipAnularBonoValidator.required = true;
        model_internal::_ipAnularBonoValidator.requiredFieldError = "ipAnularBono is required";
        //model_internal::_ipAnularBonoValidator.source = model_internal::_instance;
        //model_internal::_ipAnularBonoValidator.property = "ipAnularBono";
        model_internal::_ipBonoValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForIpBono);
        model_internal::_ipBonoValidator.required = true;
        model_internal::_ipBonoValidator.requiredFieldError = "ipBono is required";
        //model_internal::_ipBonoValidator.source = model_internal::_instance;
        //model_internal::_ipBonoValidator.property = "ipBono";
        model_internal::_ipBotonPagoValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForIpBotonPago);
        model_internal::_ipBotonPagoValidator.required = true;
        model_internal::_ipBotonPagoValidator.requiredFieldError = "ipBotonPago is required";
        //model_internal::_ipBotonPagoValidator.source = model_internal::_instance;
        //model_internal::_ipBotonPagoValidator.property = "ipBotonPago";
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity ImedConfig");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity ImedConfig");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of ImedConfig");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity ImedConfig");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity ImedConfig");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity ImedConfig");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isIpAnularBonoAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIpBonoAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIpBotonPagoAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */
    public function invalidateDependentOnIpAnularBono():void
    {
        if (model_internal::_ipAnularBonoIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfIpAnularBono = null;
            model_internal::calculateIpAnularBonoIsValid();
        }
    }
    public function invalidateDependentOnIpBono():void
    {
        if (model_internal::_ipBonoIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfIpBono = null;
            model_internal::calculateIpBonoIsValid();
        }
    }
    public function invalidateDependentOnIpBotonPago():void
    {
        if (model_internal::_ipBotonPagoIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfIpBotonPago = null;
            model_internal::calculateIpBotonPagoIsValid();
        }
    }

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get ipAnularBonoStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get ipAnularBonoValidator() : StyleValidator
    {
        return model_internal::_ipAnularBonoValidator;
    }

    model_internal function set _ipAnularBonoIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_ipAnularBonoIsValid;         
        if (oldValue !== value)
        {
            model_internal::_ipAnularBonoIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ipAnularBonoIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get ipAnularBonoIsValid():Boolean
    {
        if (!model_internal::_ipAnularBonoIsValidCacheInitialized)
        {
            model_internal::calculateIpAnularBonoIsValid();
        }

        return model_internal::_ipAnularBonoIsValid;
    }

    model_internal function calculateIpAnularBonoIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_ipAnularBonoValidator.validate(model_internal::_instance.ipAnularBono)
        model_internal::_ipAnularBonoIsValid_der = (valRes.results == null);
        model_internal::_ipAnularBonoIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::ipAnularBonoValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::ipAnularBonoValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get ipAnularBonoValidationFailureMessages():Array
    {
        if (model_internal::_ipAnularBonoValidationFailureMessages == null)
            model_internal::calculateIpAnularBonoIsValid();

        return _ipAnularBonoValidationFailureMessages;
    }

    model_internal function set ipAnularBonoValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_ipAnularBonoValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_ipAnularBonoValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ipAnularBonoValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get ipBonoStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get ipBonoValidator() : StyleValidator
    {
        return model_internal::_ipBonoValidator;
    }

    model_internal function set _ipBonoIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_ipBonoIsValid;         
        if (oldValue !== value)
        {
            model_internal::_ipBonoIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ipBonoIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get ipBonoIsValid():Boolean
    {
        if (!model_internal::_ipBonoIsValidCacheInitialized)
        {
            model_internal::calculateIpBonoIsValid();
        }

        return model_internal::_ipBonoIsValid;
    }

    model_internal function calculateIpBonoIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_ipBonoValidator.validate(model_internal::_instance.ipBono)
        model_internal::_ipBonoIsValid_der = (valRes.results == null);
        model_internal::_ipBonoIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::ipBonoValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::ipBonoValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get ipBonoValidationFailureMessages():Array
    {
        if (model_internal::_ipBonoValidationFailureMessages == null)
            model_internal::calculateIpBonoIsValid();

        return _ipBonoValidationFailureMessages;
    }

    model_internal function set ipBonoValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_ipBonoValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_ipBonoValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ipBonoValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get ipBotonPagoStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get ipBotonPagoValidator() : StyleValidator
    {
        return model_internal::_ipBotonPagoValidator;
    }

    model_internal function set _ipBotonPagoIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_ipBotonPagoIsValid;         
        if (oldValue !== value)
        {
            model_internal::_ipBotonPagoIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ipBotonPagoIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get ipBotonPagoIsValid():Boolean
    {
        if (!model_internal::_ipBotonPagoIsValidCacheInitialized)
        {
            model_internal::calculateIpBotonPagoIsValid();
        }

        return model_internal::_ipBotonPagoIsValid;
    }

    model_internal function calculateIpBotonPagoIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_ipBotonPagoValidator.validate(model_internal::_instance.ipBotonPago)
        model_internal::_ipBotonPagoIsValid_der = (valRes.results == null);
        model_internal::_ipBotonPagoIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::ipBotonPagoValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::ipBotonPagoValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get ipBotonPagoValidationFailureMessages():Array
    {
        if (model_internal::_ipBotonPagoValidationFailureMessages == null)
            model_internal::calculateIpBotonPagoIsValid();

        return _ipBotonPagoValidationFailureMessages;
    }

    model_internal function set ipBotonPagoValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_ipBotonPagoValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_ipBotonPagoValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ipBotonPagoValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            case("ipAnularBono"):
            {
                return ipAnularBonoValidationFailureMessages;
            }
            case("ipBono"):
            {
                return ipBonoValidationFailureMessages;
            }
            case("ipBotonPago"):
            {
                return ipBotonPagoValidationFailureMessages;
            }
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
