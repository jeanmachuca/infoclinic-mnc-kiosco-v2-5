
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.styles.StyleValidator;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import mx.events.ValidationResultEvent;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _RegistroConsultaEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("FormaPago", "CodResult", "idConsulta", "MsgResult", "idCliente", "PasoAnterior", "idTransaccionIMed", "PasoSiguiente");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array("FormaPago", "CodResult", "idConsulta", "MsgResult", "idCliente", "PasoAnterior", "idTransaccionIMed", "PasoSiguiente");
    model_internal static var allAlwaysAvailableProperties:Array = new Array("FormaPago", "CodResult", "idConsulta", "MsgResult", "idCliente", "PasoAnterior", "idTransaccionIMed", "PasoSiguiente");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("FormaPago", "CodResult", "idConsulta", "MsgResult", "idCliente", "PasoAnterior", "idTransaccionIMed", "PasoSiguiente");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("FormaPago", "CodResult", "idConsulta", "MsgResult", "idCliente", "PasoAnterior", "idTransaccionIMed", "PasoSiguiente");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array();
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "RegistroConsulta";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;

    
    model_internal var _FormaPagoIsValid:Boolean;
    model_internal var _FormaPagoValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _FormaPagoIsValidCacheInitialized:Boolean = false;
    model_internal var _FormaPagoValidationFailureMessages:Array;
    
    model_internal var _CodResultIsValid:Boolean;
    model_internal var _CodResultValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _CodResultIsValidCacheInitialized:Boolean = false;
    model_internal var _CodResultValidationFailureMessages:Array;
    
    model_internal var _idConsultaIsValid:Boolean;
    model_internal var _idConsultaValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _idConsultaIsValidCacheInitialized:Boolean = false;
    model_internal var _idConsultaValidationFailureMessages:Array;
    
    model_internal var _MsgResultIsValid:Boolean;
    model_internal var _MsgResultValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _MsgResultIsValidCacheInitialized:Boolean = false;
    model_internal var _MsgResultValidationFailureMessages:Array;
    
    model_internal var _idClienteIsValid:Boolean;
    model_internal var _idClienteValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _idClienteIsValidCacheInitialized:Boolean = false;
    model_internal var _idClienteValidationFailureMessages:Array;
    
    model_internal var _PasoAnteriorIsValid:Boolean;
    model_internal var _PasoAnteriorValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _PasoAnteriorIsValidCacheInitialized:Boolean = false;
    model_internal var _PasoAnteriorValidationFailureMessages:Array;
    
    model_internal var _idTransaccionIMedIsValid:Boolean;
    model_internal var _idTransaccionIMedValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _idTransaccionIMedIsValidCacheInitialized:Boolean = false;
    model_internal var _idTransaccionIMedValidationFailureMessages:Array;
    
    model_internal var _PasoSiguienteIsValid:Boolean;
    model_internal var _PasoSiguienteValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _PasoSiguienteIsValidCacheInitialized:Boolean = false;
    model_internal var _PasoSiguienteValidationFailureMessages:Array;

    model_internal var _instance:_Super_RegistroConsulta;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _RegistroConsultaEntityMetadata(value : _Super_RegistroConsulta)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["FormaPago"] = new Array();
            model_internal::dependentsOnMap["CodResult"] = new Array();
            model_internal::dependentsOnMap["idConsulta"] = new Array();
            model_internal::dependentsOnMap["MsgResult"] = new Array();
            model_internal::dependentsOnMap["idCliente"] = new Array();
            model_internal::dependentsOnMap["PasoAnterior"] = new Array();
            model_internal::dependentsOnMap["idTransaccionIMed"] = new Array();
            model_internal::dependentsOnMap["PasoSiguiente"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["FormaPago"] = "String";
        model_internal::propertyTypeMap["CodResult"] = "String";
        model_internal::propertyTypeMap["idConsulta"] = "String";
        model_internal::propertyTypeMap["MsgResult"] = "String";
        model_internal::propertyTypeMap["idCliente"] = "String";
        model_internal::propertyTypeMap["PasoAnterior"] = "String";
        model_internal::propertyTypeMap["idTransaccionIMed"] = "String";
        model_internal::propertyTypeMap["PasoSiguiente"] = "String";

        model_internal::_instance = value;
        model_internal::_FormaPagoValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForFormaPago);
        model_internal::_FormaPagoValidator.required = true;
        model_internal::_FormaPagoValidator.requiredFieldError = "FormaPago is required";
        //model_internal::_FormaPagoValidator.source = model_internal::_instance;
        //model_internal::_FormaPagoValidator.property = "FormaPago";
        model_internal::_CodResultValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForCodResult);
        model_internal::_CodResultValidator.required = true;
        model_internal::_CodResultValidator.requiredFieldError = "CodResult is required";
        //model_internal::_CodResultValidator.source = model_internal::_instance;
        //model_internal::_CodResultValidator.property = "CodResult";
        model_internal::_idConsultaValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForIdConsulta);
        model_internal::_idConsultaValidator.required = true;
        model_internal::_idConsultaValidator.requiredFieldError = "idConsulta is required";
        //model_internal::_idConsultaValidator.source = model_internal::_instance;
        //model_internal::_idConsultaValidator.property = "idConsulta";
        model_internal::_MsgResultValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForMsgResult);
        model_internal::_MsgResultValidator.required = true;
        model_internal::_MsgResultValidator.requiredFieldError = "MsgResult is required";
        //model_internal::_MsgResultValidator.source = model_internal::_instance;
        //model_internal::_MsgResultValidator.property = "MsgResult";
        model_internal::_idClienteValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForIdCliente);
        model_internal::_idClienteValidator.required = true;
        model_internal::_idClienteValidator.requiredFieldError = "idCliente is required";
        //model_internal::_idClienteValidator.source = model_internal::_instance;
        //model_internal::_idClienteValidator.property = "idCliente";
        model_internal::_PasoAnteriorValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForPasoAnterior);
        model_internal::_PasoAnteriorValidator.required = true;
        model_internal::_PasoAnteriorValidator.requiredFieldError = "PasoAnterior is required";
        //model_internal::_PasoAnteriorValidator.source = model_internal::_instance;
        //model_internal::_PasoAnteriorValidator.property = "PasoAnterior";
        model_internal::_idTransaccionIMedValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForIdTransaccionIMed);
        model_internal::_idTransaccionIMedValidator.required = true;
        model_internal::_idTransaccionIMedValidator.requiredFieldError = "idTransaccionIMed is required";
        //model_internal::_idTransaccionIMedValidator.source = model_internal::_instance;
        //model_internal::_idTransaccionIMedValidator.property = "idTransaccionIMed";
        model_internal::_PasoSiguienteValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForPasoSiguiente);
        model_internal::_PasoSiguienteValidator.required = true;
        model_internal::_PasoSiguienteValidator.requiredFieldError = "PasoSiguiente is required";
        //model_internal::_PasoSiguienteValidator.source = model_internal::_instance;
        //model_internal::_PasoSiguienteValidator.property = "PasoSiguiente";
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity RegistroConsulta");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity RegistroConsulta");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of RegistroConsulta");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity RegistroConsulta");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity RegistroConsulta");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity RegistroConsulta");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isFormaPagoAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCodResultAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIdConsultaAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMsgResultAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIdClienteAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPasoAnteriorAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIdTransaccionIMedAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPasoSiguienteAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */
    public function invalidateDependentOnFormaPago():void
    {
        if (model_internal::_FormaPagoIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfFormaPago = null;
            model_internal::calculateFormaPagoIsValid();
        }
    }
    public function invalidateDependentOnCodResult():void
    {
        if (model_internal::_CodResultIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfCodResult = null;
            model_internal::calculateCodResultIsValid();
        }
    }
    public function invalidateDependentOnIdConsulta():void
    {
        if (model_internal::_idConsultaIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfIdConsulta = null;
            model_internal::calculateIdConsultaIsValid();
        }
    }
    public function invalidateDependentOnMsgResult():void
    {
        if (model_internal::_MsgResultIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfMsgResult = null;
            model_internal::calculateMsgResultIsValid();
        }
    }
    public function invalidateDependentOnIdCliente():void
    {
        if (model_internal::_idClienteIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfIdCliente = null;
            model_internal::calculateIdClienteIsValid();
        }
    }
    public function invalidateDependentOnPasoAnterior():void
    {
        if (model_internal::_PasoAnteriorIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfPasoAnterior = null;
            model_internal::calculatePasoAnteriorIsValid();
        }
    }
    public function invalidateDependentOnIdTransaccionIMed():void
    {
        if (model_internal::_idTransaccionIMedIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfIdTransaccionIMed = null;
            model_internal::calculateIdTransaccionIMedIsValid();
        }
    }
    public function invalidateDependentOnPasoSiguiente():void
    {
        if (model_internal::_PasoSiguienteIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfPasoSiguiente = null;
            model_internal::calculatePasoSiguienteIsValid();
        }
    }

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get FormaPagoStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get FormaPagoValidator() : StyleValidator
    {
        return model_internal::_FormaPagoValidator;
    }

    model_internal function set _FormaPagoIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_FormaPagoIsValid;         
        if (oldValue !== value)
        {
            model_internal::_FormaPagoIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FormaPagoIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get FormaPagoIsValid():Boolean
    {
        if (!model_internal::_FormaPagoIsValidCacheInitialized)
        {
            model_internal::calculateFormaPagoIsValid();
        }

        return model_internal::_FormaPagoIsValid;
    }

    model_internal function calculateFormaPagoIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_FormaPagoValidator.validate(model_internal::_instance.FormaPago)
        model_internal::_FormaPagoIsValid_der = (valRes.results == null);
        model_internal::_FormaPagoIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::FormaPagoValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::FormaPagoValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get FormaPagoValidationFailureMessages():Array
    {
        if (model_internal::_FormaPagoValidationFailureMessages == null)
            model_internal::calculateFormaPagoIsValid();

        return _FormaPagoValidationFailureMessages;
    }

    model_internal function set FormaPagoValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_FormaPagoValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_FormaPagoValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FormaPagoValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get CodResultStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get CodResultValidator() : StyleValidator
    {
        return model_internal::_CodResultValidator;
    }

    model_internal function set _CodResultIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_CodResultIsValid;         
        if (oldValue !== value)
        {
            model_internal::_CodResultIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CodResultIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get CodResultIsValid():Boolean
    {
        if (!model_internal::_CodResultIsValidCacheInitialized)
        {
            model_internal::calculateCodResultIsValid();
        }

        return model_internal::_CodResultIsValid;
    }

    model_internal function calculateCodResultIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_CodResultValidator.validate(model_internal::_instance.CodResult)
        model_internal::_CodResultIsValid_der = (valRes.results == null);
        model_internal::_CodResultIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::CodResultValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::CodResultValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get CodResultValidationFailureMessages():Array
    {
        if (model_internal::_CodResultValidationFailureMessages == null)
            model_internal::calculateCodResultIsValid();

        return _CodResultValidationFailureMessages;
    }

    model_internal function set CodResultValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_CodResultValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_CodResultValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CodResultValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get idConsultaStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get idConsultaValidator() : StyleValidator
    {
        return model_internal::_idConsultaValidator;
    }

    model_internal function set _idConsultaIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_idConsultaIsValid;         
        if (oldValue !== value)
        {
            model_internal::_idConsultaIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "idConsultaIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get idConsultaIsValid():Boolean
    {
        if (!model_internal::_idConsultaIsValidCacheInitialized)
        {
            model_internal::calculateIdConsultaIsValid();
        }

        return model_internal::_idConsultaIsValid;
    }

    model_internal function calculateIdConsultaIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_idConsultaValidator.validate(model_internal::_instance.idConsulta)
        model_internal::_idConsultaIsValid_der = (valRes.results == null);
        model_internal::_idConsultaIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::idConsultaValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::idConsultaValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get idConsultaValidationFailureMessages():Array
    {
        if (model_internal::_idConsultaValidationFailureMessages == null)
            model_internal::calculateIdConsultaIsValid();

        return _idConsultaValidationFailureMessages;
    }

    model_internal function set idConsultaValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_idConsultaValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_idConsultaValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "idConsultaValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get MsgResultStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get MsgResultValidator() : StyleValidator
    {
        return model_internal::_MsgResultValidator;
    }

    model_internal function set _MsgResultIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_MsgResultIsValid;         
        if (oldValue !== value)
        {
            model_internal::_MsgResultIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MsgResultIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get MsgResultIsValid():Boolean
    {
        if (!model_internal::_MsgResultIsValidCacheInitialized)
        {
            model_internal::calculateMsgResultIsValid();
        }

        return model_internal::_MsgResultIsValid;
    }

    model_internal function calculateMsgResultIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_MsgResultValidator.validate(model_internal::_instance.MsgResult)
        model_internal::_MsgResultIsValid_der = (valRes.results == null);
        model_internal::_MsgResultIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::MsgResultValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::MsgResultValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get MsgResultValidationFailureMessages():Array
    {
        if (model_internal::_MsgResultValidationFailureMessages == null)
            model_internal::calculateMsgResultIsValid();

        return _MsgResultValidationFailureMessages;
    }

    model_internal function set MsgResultValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_MsgResultValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_MsgResultValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MsgResultValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get idClienteStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get idClienteValidator() : StyleValidator
    {
        return model_internal::_idClienteValidator;
    }

    model_internal function set _idClienteIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_idClienteIsValid;         
        if (oldValue !== value)
        {
            model_internal::_idClienteIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "idClienteIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get idClienteIsValid():Boolean
    {
        if (!model_internal::_idClienteIsValidCacheInitialized)
        {
            model_internal::calculateIdClienteIsValid();
        }

        return model_internal::_idClienteIsValid;
    }

    model_internal function calculateIdClienteIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_idClienteValidator.validate(model_internal::_instance.idCliente)
        model_internal::_idClienteIsValid_der = (valRes.results == null);
        model_internal::_idClienteIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::idClienteValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::idClienteValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get idClienteValidationFailureMessages():Array
    {
        if (model_internal::_idClienteValidationFailureMessages == null)
            model_internal::calculateIdClienteIsValid();

        return _idClienteValidationFailureMessages;
    }

    model_internal function set idClienteValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_idClienteValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_idClienteValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "idClienteValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get PasoAnteriorStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get PasoAnteriorValidator() : StyleValidator
    {
        return model_internal::_PasoAnteriorValidator;
    }

    model_internal function set _PasoAnteriorIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_PasoAnteriorIsValid;         
        if (oldValue !== value)
        {
            model_internal::_PasoAnteriorIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PasoAnteriorIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get PasoAnteriorIsValid():Boolean
    {
        if (!model_internal::_PasoAnteriorIsValidCacheInitialized)
        {
            model_internal::calculatePasoAnteriorIsValid();
        }

        return model_internal::_PasoAnteriorIsValid;
    }

    model_internal function calculatePasoAnteriorIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_PasoAnteriorValidator.validate(model_internal::_instance.PasoAnterior)
        model_internal::_PasoAnteriorIsValid_der = (valRes.results == null);
        model_internal::_PasoAnteriorIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::PasoAnteriorValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::PasoAnteriorValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get PasoAnteriorValidationFailureMessages():Array
    {
        if (model_internal::_PasoAnteriorValidationFailureMessages == null)
            model_internal::calculatePasoAnteriorIsValid();

        return _PasoAnteriorValidationFailureMessages;
    }

    model_internal function set PasoAnteriorValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_PasoAnteriorValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_PasoAnteriorValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PasoAnteriorValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get idTransaccionIMedStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get idTransaccionIMedValidator() : StyleValidator
    {
        return model_internal::_idTransaccionIMedValidator;
    }

    model_internal function set _idTransaccionIMedIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_idTransaccionIMedIsValid;         
        if (oldValue !== value)
        {
            model_internal::_idTransaccionIMedIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "idTransaccionIMedIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get idTransaccionIMedIsValid():Boolean
    {
        if (!model_internal::_idTransaccionIMedIsValidCacheInitialized)
        {
            model_internal::calculateIdTransaccionIMedIsValid();
        }

        return model_internal::_idTransaccionIMedIsValid;
    }

    model_internal function calculateIdTransaccionIMedIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_idTransaccionIMedValidator.validate(model_internal::_instance.idTransaccionIMed)
        model_internal::_idTransaccionIMedIsValid_der = (valRes.results == null);
        model_internal::_idTransaccionIMedIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::idTransaccionIMedValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::idTransaccionIMedValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get idTransaccionIMedValidationFailureMessages():Array
    {
        if (model_internal::_idTransaccionIMedValidationFailureMessages == null)
            model_internal::calculateIdTransaccionIMedIsValid();

        return _idTransaccionIMedValidationFailureMessages;
    }

    model_internal function set idTransaccionIMedValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_idTransaccionIMedValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_idTransaccionIMedValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "idTransaccionIMedValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get PasoSiguienteStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get PasoSiguienteValidator() : StyleValidator
    {
        return model_internal::_PasoSiguienteValidator;
    }

    model_internal function set _PasoSiguienteIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_PasoSiguienteIsValid;         
        if (oldValue !== value)
        {
            model_internal::_PasoSiguienteIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PasoSiguienteIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get PasoSiguienteIsValid():Boolean
    {
        if (!model_internal::_PasoSiguienteIsValidCacheInitialized)
        {
            model_internal::calculatePasoSiguienteIsValid();
        }

        return model_internal::_PasoSiguienteIsValid;
    }

    model_internal function calculatePasoSiguienteIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_PasoSiguienteValidator.validate(model_internal::_instance.PasoSiguiente)
        model_internal::_PasoSiguienteIsValid_der = (valRes.results == null);
        model_internal::_PasoSiguienteIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::PasoSiguienteValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::PasoSiguienteValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get PasoSiguienteValidationFailureMessages():Array
    {
        if (model_internal::_PasoSiguienteValidationFailureMessages == null)
            model_internal::calculatePasoSiguienteIsValid();

        return _PasoSiguienteValidationFailureMessages;
    }

    model_internal function set PasoSiguienteValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_PasoSiguienteValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_PasoSiguienteValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PasoSiguienteValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            case("FormaPago"):
            {
                return FormaPagoValidationFailureMessages;
            }
            case("CodResult"):
            {
                return CodResultValidationFailureMessages;
            }
            case("idConsulta"):
            {
                return idConsultaValidationFailureMessages;
            }
            case("MsgResult"):
            {
                return MsgResultValidationFailureMessages;
            }
            case("idCliente"):
            {
                return idClienteValidationFailureMessages;
            }
            case("PasoAnterior"):
            {
                return PasoAnteriorValidationFailureMessages;
            }
            case("idTransaccionIMed"):
            {
                return idTransaccionIMedValidationFailureMessages;
            }
            case("PasoSiguiente"):
            {
                return PasoSiguienteValidationFailureMessages;
            }
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
