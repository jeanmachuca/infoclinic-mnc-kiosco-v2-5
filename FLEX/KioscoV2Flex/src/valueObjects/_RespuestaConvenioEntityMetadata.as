
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.styles.StyleValidator;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import mx.events.ValidationResultEvent;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _RespuestaConvenioEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("CodResult", "MontoACobrar", "MsgResult", "MontoConsumo", "Descuento", "CodConvenio", "CodigoReembolso", "CodConsumo", "MontoDescuento");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array("CodResult", "MontoACobrar", "MsgResult", "MontoConsumo", "Descuento", "CodConvenio", "CodigoReembolso", "CodConsumo", "MontoDescuento");
    model_internal static var allAlwaysAvailableProperties:Array = new Array("CodResult", "MontoACobrar", "MsgResult", "MontoConsumo", "Descuento", "CodConvenio", "CodigoReembolso", "CodConsumo", "MontoDescuento");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("CodResult", "MontoACobrar", "MsgResult", "MontoConsumo", "Descuento", "CodConvenio", "CodigoReembolso", "CodConsumo", "MontoDescuento");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("CodResult", "MontoACobrar", "MsgResult", "MontoConsumo", "Descuento", "CodConvenio", "CodigoReembolso", "CodConsumo", "MontoDescuento");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array();
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "RespuestaConvenio";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;

    
    model_internal var _CodResultIsValid:Boolean;
    model_internal var _CodResultValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _CodResultIsValidCacheInitialized:Boolean = false;
    model_internal var _CodResultValidationFailureMessages:Array;
    
    model_internal var _MsgResultIsValid:Boolean;
    model_internal var _MsgResultValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _MsgResultIsValidCacheInitialized:Boolean = false;
    model_internal var _MsgResultValidationFailureMessages:Array;
    
    model_internal var _DescuentoIsValid:Boolean;
    model_internal var _DescuentoValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _DescuentoIsValidCacheInitialized:Boolean = false;
    model_internal var _DescuentoValidationFailureMessages:Array;
    
    model_internal var _CodConvenioIsValid:Boolean;
    model_internal var _CodConvenioValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _CodConvenioIsValidCacheInitialized:Boolean = false;
    model_internal var _CodConvenioValidationFailureMessages:Array;
    
    model_internal var _CodigoReembolsoIsValid:Boolean;
    model_internal var _CodigoReembolsoValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _CodigoReembolsoIsValidCacheInitialized:Boolean = false;
    model_internal var _CodigoReembolsoValidationFailureMessages:Array;
    
    model_internal var _CodConsumoIsValid:Boolean;
    model_internal var _CodConsumoValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _CodConsumoIsValidCacheInitialized:Boolean = false;
    model_internal var _CodConsumoValidationFailureMessages:Array;

    model_internal var _instance:_Super_RespuestaConvenio;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _RespuestaConvenioEntityMetadata(value : _Super_RespuestaConvenio)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["CodResult"] = new Array();
            model_internal::dependentsOnMap["MontoACobrar"] = new Array();
            model_internal::dependentsOnMap["MsgResult"] = new Array();
            model_internal::dependentsOnMap["MontoConsumo"] = new Array();
            model_internal::dependentsOnMap["Descuento"] = new Array();
            model_internal::dependentsOnMap["CodConvenio"] = new Array();
            model_internal::dependentsOnMap["CodigoReembolso"] = new Array();
            model_internal::dependentsOnMap["CodConsumo"] = new Array();
            model_internal::dependentsOnMap["MontoDescuento"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["CodResult"] = "String";
        model_internal::propertyTypeMap["MontoACobrar"] = "int";
        model_internal::propertyTypeMap["MsgResult"] = "String";
        model_internal::propertyTypeMap["MontoConsumo"] = "int";
        model_internal::propertyTypeMap["Descuento"] = "String";
        model_internal::propertyTypeMap["CodConvenio"] = "String";
        model_internal::propertyTypeMap["CodigoReembolso"] = "String";
        model_internal::propertyTypeMap["CodConsumo"] = "String";
        model_internal::propertyTypeMap["MontoDescuento"] = "int";

        model_internal::_instance = value;
        model_internal::_CodResultValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForCodResult);
        model_internal::_CodResultValidator.required = true;
        model_internal::_CodResultValidator.requiredFieldError = "CodResult is required";
        //model_internal::_CodResultValidator.source = model_internal::_instance;
        //model_internal::_CodResultValidator.property = "CodResult";
        model_internal::_MsgResultValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForMsgResult);
        model_internal::_MsgResultValidator.required = true;
        model_internal::_MsgResultValidator.requiredFieldError = "MsgResult is required";
        //model_internal::_MsgResultValidator.source = model_internal::_instance;
        //model_internal::_MsgResultValidator.property = "MsgResult";
        model_internal::_DescuentoValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForDescuento);
        model_internal::_DescuentoValidator.required = true;
        model_internal::_DescuentoValidator.requiredFieldError = "Descuento is required";
        //model_internal::_DescuentoValidator.source = model_internal::_instance;
        //model_internal::_DescuentoValidator.property = "Descuento";
        model_internal::_CodConvenioValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForCodConvenio);
        model_internal::_CodConvenioValidator.required = true;
        model_internal::_CodConvenioValidator.requiredFieldError = "CodConvenio is required";
        //model_internal::_CodConvenioValidator.source = model_internal::_instance;
        //model_internal::_CodConvenioValidator.property = "CodConvenio";
        model_internal::_CodigoReembolsoValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForCodigoReembolso);
        model_internal::_CodigoReembolsoValidator.required = true;
        model_internal::_CodigoReembolsoValidator.requiredFieldError = "CodigoReembolso is required";
        //model_internal::_CodigoReembolsoValidator.source = model_internal::_instance;
        //model_internal::_CodigoReembolsoValidator.property = "CodigoReembolso";
        model_internal::_CodConsumoValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForCodConsumo);
        model_internal::_CodConsumoValidator.required = true;
        model_internal::_CodConsumoValidator.requiredFieldError = "CodConsumo is required";
        //model_internal::_CodConsumoValidator.source = model_internal::_instance;
        //model_internal::_CodConsumoValidator.property = "CodConsumo";
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity RespuestaConvenio");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity RespuestaConvenio");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of RespuestaConvenio");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity RespuestaConvenio");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity RespuestaConvenio");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity RespuestaConvenio");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isCodResultAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMontoACobrarAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMsgResultAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMontoConsumoAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDescuentoAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCodConvenioAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCodigoReembolsoAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCodConsumoAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMontoDescuentoAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */
    public function invalidateDependentOnCodResult():void
    {
        if (model_internal::_CodResultIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfCodResult = null;
            model_internal::calculateCodResultIsValid();
        }
    }
    public function invalidateDependentOnMsgResult():void
    {
        if (model_internal::_MsgResultIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfMsgResult = null;
            model_internal::calculateMsgResultIsValid();
        }
    }
    public function invalidateDependentOnDescuento():void
    {
        if (model_internal::_DescuentoIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfDescuento = null;
            model_internal::calculateDescuentoIsValid();
        }
    }
    public function invalidateDependentOnCodConvenio():void
    {
        if (model_internal::_CodConvenioIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfCodConvenio = null;
            model_internal::calculateCodConvenioIsValid();
        }
    }
    public function invalidateDependentOnCodigoReembolso():void
    {
        if (model_internal::_CodigoReembolsoIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfCodigoReembolso = null;
            model_internal::calculateCodigoReembolsoIsValid();
        }
    }
    public function invalidateDependentOnCodConsumo():void
    {
        if (model_internal::_CodConsumoIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfCodConsumo = null;
            model_internal::calculateCodConsumoIsValid();
        }
    }

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get CodResultStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get CodResultValidator() : StyleValidator
    {
        return model_internal::_CodResultValidator;
    }

    model_internal function set _CodResultIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_CodResultIsValid;         
        if (oldValue !== value)
        {
            model_internal::_CodResultIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CodResultIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get CodResultIsValid():Boolean
    {
        if (!model_internal::_CodResultIsValidCacheInitialized)
        {
            model_internal::calculateCodResultIsValid();
        }

        return model_internal::_CodResultIsValid;
    }

    model_internal function calculateCodResultIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_CodResultValidator.validate(model_internal::_instance.CodResult)
        model_internal::_CodResultIsValid_der = (valRes.results == null);
        model_internal::_CodResultIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::CodResultValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::CodResultValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get CodResultValidationFailureMessages():Array
    {
        if (model_internal::_CodResultValidationFailureMessages == null)
            model_internal::calculateCodResultIsValid();

        return _CodResultValidationFailureMessages;
    }

    model_internal function set CodResultValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_CodResultValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_CodResultValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CodResultValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get MontoACobrarStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get MsgResultStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get MsgResultValidator() : StyleValidator
    {
        return model_internal::_MsgResultValidator;
    }

    model_internal function set _MsgResultIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_MsgResultIsValid;         
        if (oldValue !== value)
        {
            model_internal::_MsgResultIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MsgResultIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get MsgResultIsValid():Boolean
    {
        if (!model_internal::_MsgResultIsValidCacheInitialized)
        {
            model_internal::calculateMsgResultIsValid();
        }

        return model_internal::_MsgResultIsValid;
    }

    model_internal function calculateMsgResultIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_MsgResultValidator.validate(model_internal::_instance.MsgResult)
        model_internal::_MsgResultIsValid_der = (valRes.results == null);
        model_internal::_MsgResultIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::MsgResultValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::MsgResultValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get MsgResultValidationFailureMessages():Array
    {
        if (model_internal::_MsgResultValidationFailureMessages == null)
            model_internal::calculateMsgResultIsValid();

        return _MsgResultValidationFailureMessages;
    }

    model_internal function set MsgResultValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_MsgResultValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_MsgResultValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MsgResultValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get MontoConsumoStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DescuentoStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get DescuentoValidator() : StyleValidator
    {
        return model_internal::_DescuentoValidator;
    }

    model_internal function set _DescuentoIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_DescuentoIsValid;         
        if (oldValue !== value)
        {
            model_internal::_DescuentoIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DescuentoIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get DescuentoIsValid():Boolean
    {
        if (!model_internal::_DescuentoIsValidCacheInitialized)
        {
            model_internal::calculateDescuentoIsValid();
        }

        return model_internal::_DescuentoIsValid;
    }

    model_internal function calculateDescuentoIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_DescuentoValidator.validate(model_internal::_instance.Descuento)
        model_internal::_DescuentoIsValid_der = (valRes.results == null);
        model_internal::_DescuentoIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::DescuentoValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::DescuentoValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get DescuentoValidationFailureMessages():Array
    {
        if (model_internal::_DescuentoValidationFailureMessages == null)
            model_internal::calculateDescuentoIsValid();

        return _DescuentoValidationFailureMessages;
    }

    model_internal function set DescuentoValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_DescuentoValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_DescuentoValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DescuentoValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get CodConvenioStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get CodConvenioValidator() : StyleValidator
    {
        return model_internal::_CodConvenioValidator;
    }

    model_internal function set _CodConvenioIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_CodConvenioIsValid;         
        if (oldValue !== value)
        {
            model_internal::_CodConvenioIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CodConvenioIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get CodConvenioIsValid():Boolean
    {
        if (!model_internal::_CodConvenioIsValidCacheInitialized)
        {
            model_internal::calculateCodConvenioIsValid();
        }

        return model_internal::_CodConvenioIsValid;
    }

    model_internal function calculateCodConvenioIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_CodConvenioValidator.validate(model_internal::_instance.CodConvenio)
        model_internal::_CodConvenioIsValid_der = (valRes.results == null);
        model_internal::_CodConvenioIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::CodConvenioValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::CodConvenioValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get CodConvenioValidationFailureMessages():Array
    {
        if (model_internal::_CodConvenioValidationFailureMessages == null)
            model_internal::calculateCodConvenioIsValid();

        return _CodConvenioValidationFailureMessages;
    }

    model_internal function set CodConvenioValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_CodConvenioValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_CodConvenioValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CodConvenioValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get CodigoReembolsoStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get CodigoReembolsoValidator() : StyleValidator
    {
        return model_internal::_CodigoReembolsoValidator;
    }

    model_internal function set _CodigoReembolsoIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_CodigoReembolsoIsValid;         
        if (oldValue !== value)
        {
            model_internal::_CodigoReembolsoIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CodigoReembolsoIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get CodigoReembolsoIsValid():Boolean
    {
        if (!model_internal::_CodigoReembolsoIsValidCacheInitialized)
        {
            model_internal::calculateCodigoReembolsoIsValid();
        }

        return model_internal::_CodigoReembolsoIsValid;
    }

    model_internal function calculateCodigoReembolsoIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_CodigoReembolsoValidator.validate(model_internal::_instance.CodigoReembolso)
        model_internal::_CodigoReembolsoIsValid_der = (valRes.results == null);
        model_internal::_CodigoReembolsoIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::CodigoReembolsoValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::CodigoReembolsoValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get CodigoReembolsoValidationFailureMessages():Array
    {
        if (model_internal::_CodigoReembolsoValidationFailureMessages == null)
            model_internal::calculateCodigoReembolsoIsValid();

        return _CodigoReembolsoValidationFailureMessages;
    }

    model_internal function set CodigoReembolsoValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_CodigoReembolsoValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_CodigoReembolsoValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CodigoReembolsoValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get CodConsumoStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get CodConsumoValidator() : StyleValidator
    {
        return model_internal::_CodConsumoValidator;
    }

    model_internal function set _CodConsumoIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_CodConsumoIsValid;         
        if (oldValue !== value)
        {
            model_internal::_CodConsumoIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CodConsumoIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get CodConsumoIsValid():Boolean
    {
        if (!model_internal::_CodConsumoIsValidCacheInitialized)
        {
            model_internal::calculateCodConsumoIsValid();
        }

        return model_internal::_CodConsumoIsValid;
    }

    model_internal function calculateCodConsumoIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_CodConsumoValidator.validate(model_internal::_instance.CodConsumo)
        model_internal::_CodConsumoIsValid_der = (valRes.results == null);
        model_internal::_CodConsumoIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::CodConsumoValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::CodConsumoValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get CodConsumoValidationFailureMessages():Array
    {
        if (model_internal::_CodConsumoValidationFailureMessages == null)
            model_internal::calculateCodConsumoIsValid();

        return _CodConsumoValidationFailureMessages;
    }

    model_internal function set CodConsumoValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_CodConsumoValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_CodConsumoValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CodConsumoValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get MontoDescuentoStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            case("CodResult"):
            {
                return CodResultValidationFailureMessages;
            }
            case("MsgResult"):
            {
                return MsgResultValidationFailureMessages;
            }
            case("Descuento"):
            {
                return DescuentoValidationFailureMessages;
            }
            case("CodConvenio"):
            {
                return CodConvenioValidationFailureMessages;
            }
            case("CodigoReembolso"):
            {
                return CodigoReembolsoValidationFailureMessages;
            }
            case("CodConsumo"):
            {
                return CodConsumoValidationFailureMessages;
            }
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
