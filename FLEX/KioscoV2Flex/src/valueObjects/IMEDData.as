package valueObjects
{
	import flash.events.IEventDispatcher;
	
	import mx.core.FlexGlobals;
	
	public dynamic class IMEDData extends MiniclinicVO {
		
		//Datos enviados a Transbank
		public var NumTransaccion:String;
		public var NumOperacion:String;
		public var montocopago:String; // valor copago
		public var monto:String; // valor convenio
		
		// Datos enviados a la url de exito y fracaso
		public var NumTransac:String;
		public var CodError:String;
		public var GloError:String;
		public var FolioBono:String;
		public var FormaPago:String;
		public var montotransaccion:String; // valor consulta
		public var idConsulta:String;
		public var idKiosco:String;
		
		public function IMEDData(target:IEventDispatcher=null) {
			super(target);
		}

		public function setResult(result:Array):void {
			var name:String;
			var value:String;
			for (var i:int;i<result.length;i++) {
				name = result[i].name;
				value = result[i].value;
				if (value != ""){
					this[name] = value as String;
				}
			}
		}
	}
}