/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - RespuestaMotivos.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.util.FiberUtils;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.Event;
import flash.events.EventDispatcher;
import mx.binding.utils.ChangeWatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import mx.validators.ValidationResult;
import valueObjects.ListaTipoConsultas;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_RespuestaMotivos extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.ListaTipoConsultas.initRemoteClassAliasSingleChild();
        valueObjects.TipoConsulta.initRemoteClassAliasSingleChild();
        valueObjects.ListaVacunas.initRemoteClassAliasSingleChild();
        valueObjects.ListaSintomas.initRemoteClassAliasSingleChild();
        valueObjects.Sintoma.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _RespuestaMotivosEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_CodResult : String;
    private var _internal_MsgResult : String;
    private var _internal_PasoAnterior : String;
    private var _internal_PasoSiguiente : String;
    private var _internal_ListaTipoConsultas : valueObjects.ListaTipoConsultas;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_RespuestaMotivos()
    {
        _model = new _RespuestaMotivosEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "CodResult", model_internal::setterListenerCodResult));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "MsgResult", model_internal::setterListenerMsgResult));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "PasoAnterior", model_internal::setterListenerPasoAnterior));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "PasoSiguiente", model_internal::setterListenerPasoSiguiente));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "ListaTipoConsultas", model_internal::setterListenerListaTipoConsultas));

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get CodResult() : String
    {
        return _internal_CodResult;
    }

    [Bindable(event="propertyChange")]
    public function get MsgResult() : String
    {
        return _internal_MsgResult;
    }

    [Bindable(event="propertyChange")]
    public function get PasoAnterior() : String
    {
        return _internal_PasoAnterior;
    }

    [Bindable(event="propertyChange")]
    public function get PasoSiguiente() : String
    {
        return _internal_PasoSiguiente;
    }

    [Bindable(event="propertyChange")]
    public function get ListaTipoConsultas() : valueObjects.ListaTipoConsultas
    {
        return _internal_ListaTipoConsultas;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set CodResult(value:String) : void
    {
        var oldValue:String = _internal_CodResult;
        if (oldValue !== value)
        {
            _internal_CodResult = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CodResult", oldValue, _internal_CodResult));
        }
    }

    public function set MsgResult(value:String) : void
    {
        var oldValue:String = _internal_MsgResult;
        if (oldValue !== value)
        {
            _internal_MsgResult = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MsgResult", oldValue, _internal_MsgResult));
        }
    }

    public function set PasoAnterior(value:String) : void
    {
        var oldValue:String = _internal_PasoAnterior;
        if (oldValue !== value)
        {
            _internal_PasoAnterior = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PasoAnterior", oldValue, _internal_PasoAnterior));
        }
    }

    public function set PasoSiguiente(value:String) : void
    {
        var oldValue:String = _internal_PasoSiguiente;
        if (oldValue !== value)
        {
            _internal_PasoSiguiente = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PasoSiguiente", oldValue, _internal_PasoSiguiente));
        }
    }

    public function set ListaTipoConsultas(value:valueObjects.ListaTipoConsultas) : void
    {
        var oldValue:valueObjects.ListaTipoConsultas = _internal_ListaTipoConsultas;
        if (oldValue !== value)
        {
            _internal_ListaTipoConsultas = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ListaTipoConsultas", oldValue, _internal_ListaTipoConsultas));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */

    model_internal function setterListenerCodResult(value:flash.events.Event):void
    {
        _model.invalidateDependentOnCodResult();
    }

    model_internal function setterListenerMsgResult(value:flash.events.Event):void
    {
        _model.invalidateDependentOnMsgResult();
    }

    model_internal function setterListenerPasoAnterior(value:flash.events.Event):void
    {
        _model.invalidateDependentOnPasoAnterior();
    }

    model_internal function setterListenerPasoSiguiente(value:flash.events.Event):void
    {
        _model.invalidateDependentOnPasoSiguiente();
    }

    model_internal function setterListenerListaTipoConsultas(value:flash.events.Event):void
    {
        _model.invalidateDependentOnListaTipoConsultas();
    }


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;
        if (!_model.CodResultIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_CodResultValidationFailureMessages);
        }
        if (!_model.MsgResultIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_MsgResultValidationFailureMessages);
        }
        if (!_model.PasoAnteriorIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_PasoAnteriorValidationFailureMessages);
        }
        if (!_model.PasoSiguienteIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_PasoSiguienteValidationFailureMessages);
        }
        if (!_model.ListaTipoConsultasIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_ListaTipoConsultasValidationFailureMessages);
        }

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _RespuestaMotivosEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _RespuestaMotivosEntityMetadata) : void
    {
        var oldValue : _RespuestaMotivosEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }

    model_internal var _doValidationCacheOfCodResult : Array = null;
    model_internal var _doValidationLastValOfCodResult : String;

    model_internal function _doValidationForCodResult(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfCodResult != null && model_internal::_doValidationLastValOfCodResult == value)
           return model_internal::_doValidationCacheOfCodResult ;

        _model.model_internal::_CodResultIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isCodResultAvailable && _internal_CodResult == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "CodResult is required"));
        }

        model_internal::_doValidationCacheOfCodResult = validationFailures;
        model_internal::_doValidationLastValOfCodResult = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfMsgResult : Array = null;
    model_internal var _doValidationLastValOfMsgResult : String;

    model_internal function _doValidationForMsgResult(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfMsgResult != null && model_internal::_doValidationLastValOfMsgResult == value)
           return model_internal::_doValidationCacheOfMsgResult ;

        _model.model_internal::_MsgResultIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isMsgResultAvailable && _internal_MsgResult == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "MsgResult is required"));
        }

        model_internal::_doValidationCacheOfMsgResult = validationFailures;
        model_internal::_doValidationLastValOfMsgResult = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfPasoAnterior : Array = null;
    model_internal var _doValidationLastValOfPasoAnterior : String;

    model_internal function _doValidationForPasoAnterior(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfPasoAnterior != null && model_internal::_doValidationLastValOfPasoAnterior == value)
           return model_internal::_doValidationCacheOfPasoAnterior ;

        _model.model_internal::_PasoAnteriorIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isPasoAnteriorAvailable && _internal_PasoAnterior == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "PasoAnterior is required"));
        }

        model_internal::_doValidationCacheOfPasoAnterior = validationFailures;
        model_internal::_doValidationLastValOfPasoAnterior = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfPasoSiguiente : Array = null;
    model_internal var _doValidationLastValOfPasoSiguiente : String;

    model_internal function _doValidationForPasoSiguiente(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfPasoSiguiente != null && model_internal::_doValidationLastValOfPasoSiguiente == value)
           return model_internal::_doValidationCacheOfPasoSiguiente ;

        _model.model_internal::_PasoSiguienteIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isPasoSiguienteAvailable && _internal_PasoSiguiente == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "PasoSiguiente is required"));
        }

        model_internal::_doValidationCacheOfPasoSiguiente = validationFailures;
        model_internal::_doValidationLastValOfPasoSiguiente = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfListaTipoConsultas : Array = null;
    model_internal var _doValidationLastValOfListaTipoConsultas : valueObjects.ListaTipoConsultas;

    model_internal function _doValidationForListaTipoConsultas(valueIn:Object):Array
    {
        var value : valueObjects.ListaTipoConsultas = valueIn as valueObjects.ListaTipoConsultas;

        if (model_internal::_doValidationCacheOfListaTipoConsultas != null && model_internal::_doValidationLastValOfListaTipoConsultas == value)
           return model_internal::_doValidationCacheOfListaTipoConsultas ;

        _model.model_internal::_ListaTipoConsultasIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isListaTipoConsultasAvailable && _internal_ListaTipoConsultas == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "ListaTipoConsultas is required"));
        }

        model_internal::_doValidationCacheOfListaTipoConsultas = validationFailures;
        model_internal::_doValidationLastValOfListaTipoConsultas = value;

        return validationFailures;
    }
    

}

}
