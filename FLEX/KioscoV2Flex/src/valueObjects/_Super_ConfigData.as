/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - ConfigData.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.util.FiberUtils;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.Event;
import flash.events.EventDispatcher;
import mx.binding.utils.ChangeWatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import mx.validators.ValidationResult;
import valueObjects.Boleta;
import valueObjects.ImedConfig;
import valueObjects.Kiosco;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_ConfigData extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.Boleta.initRemoteClassAliasSingleChild();
        valueObjects.Kiosco.initRemoteClassAliasSingleChild();
        valueObjects.ImedConfig.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _ConfigDataEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_Boleta : valueObjects.Boleta;
    private var _internal_Kiosco : valueObjects.Kiosco;
    private var _internal_Theme : Object;
    private var _internal_oArtifacts : Object;
    private var _internal_Imed : valueObjects.ImedConfig;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_ConfigData()
    {
        _model = new _ConfigDataEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "Boleta", model_internal::setterListenerBoleta));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "Kiosco", model_internal::setterListenerKiosco));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "Theme", model_internal::setterListenerTheme));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "oArtifacts", model_internal::setterListenerOArtifacts));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "Imed", model_internal::setterListenerImed));

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get Boleta() : valueObjects.Boleta
    {
        return _internal_Boleta;
    }

    [Bindable(event="propertyChange")]
    public function get Kiosco() : valueObjects.Kiosco
    {
        return _internal_Kiosco;
    }

    [Bindable(event="propertyChange")]
    public function get Theme() : Object
    {
        return _internal_Theme;
    }

    [Bindable(event="propertyChange")]
    public function get oArtifacts() : Object
    {
        return _internal_oArtifacts;
    }

    [Bindable(event="propertyChange")]
    public function get Imed() : valueObjects.ImedConfig
    {
        return _internal_Imed;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set Boleta(value:valueObjects.Boleta) : void
    {
        var oldValue:valueObjects.Boleta = _internal_Boleta;
        if (oldValue !== value)
        {
            _internal_Boleta = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "Boleta", oldValue, _internal_Boleta));
        }
    }

    public function set Kiosco(value:valueObjects.Kiosco) : void
    {
        var oldValue:valueObjects.Kiosco = _internal_Kiosco;
        if (oldValue !== value)
        {
            _internal_Kiosco = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "Kiosco", oldValue, _internal_Kiosco));
        }
    }

    public function set Theme(value:Object) : void
    {
        var oldValue:Object = _internal_Theme;
        if (oldValue !== value)
        {
            _internal_Theme = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "Theme", oldValue, _internal_Theme));
        }
    }

    public function set oArtifacts(value:Object) : void
    {
        var oldValue:Object = _internal_oArtifacts;
        if (oldValue !== value)
        {
            _internal_oArtifacts = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "oArtifacts", oldValue, _internal_oArtifacts));
        }
    }

    public function set Imed(value:valueObjects.ImedConfig) : void
    {
        var oldValue:valueObjects.ImedConfig = _internal_Imed;
        if (oldValue !== value)
        {
            _internal_Imed = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "Imed", oldValue, _internal_Imed));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */

    model_internal function setterListenerBoleta(value:flash.events.Event):void
    {
        _model.invalidateDependentOnBoleta();
    }

    model_internal function setterListenerKiosco(value:flash.events.Event):void
    {
        _model.invalidateDependentOnKiosco();
    }

    model_internal function setterListenerTheme(value:flash.events.Event):void
    {
        _model.invalidateDependentOnTheme();
    }

    model_internal function setterListenerOArtifacts(value:flash.events.Event):void
    {
        _model.invalidateDependentOnOArtifacts();
    }

    model_internal function setterListenerImed(value:flash.events.Event):void
    {
        _model.invalidateDependentOnImed();
    }


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;
        if (!_model.BoletaIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_BoletaValidationFailureMessages);
        }
        if (!_model.KioscoIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_KioscoValidationFailureMessages);
        }
        if (!_model.ThemeIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_ThemeValidationFailureMessages);
        }
        if (!_model.oArtifactsIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_oArtifactsValidationFailureMessages);
        }
        if (!_model.ImedIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_ImedValidationFailureMessages);
        }

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _ConfigDataEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _ConfigDataEntityMetadata) : void
    {
        var oldValue : _ConfigDataEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }

    model_internal var _doValidationCacheOfBoleta : Array = null;
    model_internal var _doValidationLastValOfBoleta : valueObjects.Boleta;

    model_internal function _doValidationForBoleta(valueIn:Object):Array
    {
        var value : valueObjects.Boleta = valueIn as valueObjects.Boleta;

        if (model_internal::_doValidationCacheOfBoleta != null && model_internal::_doValidationLastValOfBoleta == value)
           return model_internal::_doValidationCacheOfBoleta ;

        _model.model_internal::_BoletaIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isBoletaAvailable && _internal_Boleta == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "Boleta is required"));
        }

        model_internal::_doValidationCacheOfBoleta = validationFailures;
        model_internal::_doValidationLastValOfBoleta = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfKiosco : Array = null;
    model_internal var _doValidationLastValOfKiosco : valueObjects.Kiosco;

    model_internal function _doValidationForKiosco(valueIn:Object):Array
    {
        var value : valueObjects.Kiosco = valueIn as valueObjects.Kiosco;

        if (model_internal::_doValidationCacheOfKiosco != null && model_internal::_doValidationLastValOfKiosco == value)
           return model_internal::_doValidationCacheOfKiosco ;

        _model.model_internal::_KioscoIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isKioscoAvailable && _internal_Kiosco == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "Kiosco is required"));
        }

        model_internal::_doValidationCacheOfKiosco = validationFailures;
        model_internal::_doValidationLastValOfKiosco = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfTheme : Array = null;
    model_internal var _doValidationLastValOfTheme : Object;

    model_internal function _doValidationForTheme(valueIn:Object):Array
    {
        var value : Object = valueIn as Object;

        if (model_internal::_doValidationCacheOfTheme != null && model_internal::_doValidationLastValOfTheme == value)
           return model_internal::_doValidationCacheOfTheme ;

        _model.model_internal::_ThemeIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isThemeAvailable && _internal_Theme == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "Theme is required"));
        }

        model_internal::_doValidationCacheOfTheme = validationFailures;
        model_internal::_doValidationLastValOfTheme = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfOArtifacts : Array = null;
    model_internal var _doValidationLastValOfOArtifacts : Object;

    model_internal function _doValidationForOArtifacts(valueIn:Object):Array
    {
        var value : Object = valueIn as Object;

        if (model_internal::_doValidationCacheOfOArtifacts != null && model_internal::_doValidationLastValOfOArtifacts == value)
           return model_internal::_doValidationCacheOfOArtifacts ;

        _model.model_internal::_oArtifactsIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isOArtifactsAvailable && _internal_oArtifacts == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "oArtifacts is required"));
        }

        model_internal::_doValidationCacheOfOArtifacts = validationFailures;
        model_internal::_doValidationLastValOfOArtifacts = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfImed : Array = null;
    model_internal var _doValidationLastValOfImed : valueObjects.ImedConfig;

    model_internal function _doValidationForImed(valueIn:Object):Array
    {
        var value : valueObjects.ImedConfig = valueIn as valueObjects.ImedConfig;

        if (model_internal::_doValidationCacheOfImed != null && model_internal::_doValidationLastValOfImed == value)
           return model_internal::_doValidationCacheOfImed ;

        _model.model_internal::_ImedIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isImedAvailable && _internal_Imed == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "Imed is required"));
        }

        model_internal::_doValidationCacheOfImed = validationFailures;
        model_internal::_doValidationLastValOfImed = value;

        return validationFailures;
    }
    

}

}
