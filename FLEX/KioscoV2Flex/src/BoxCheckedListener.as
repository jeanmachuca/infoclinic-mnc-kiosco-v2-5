package
{
	import views.component.CustomCheckBox;

	public interface BoxCheckedListener
	{
		function boxChecked(checkBox:CustomCheckBox):void;
	}
}